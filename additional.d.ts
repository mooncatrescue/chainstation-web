import * as IronSession from 'iron-session'
import { SiweMessage } from 'viem/siwe'

interface LocalUserSession {
  siwe: Omit<SiweMessage, 'issuedAt' | 'expirationTime'> & { issuedAt?: string; expirationTime?: string }
  signature: `0x${string}`
}

declare module 'iron-session' {
  interface IronSessionData {
    keyring: LocalUserSession[]
    nonce?: string
  }
}

declare module 'react' {
  interface CSSProperties {
    '--thumb-height'?: string | number
    '--drawer-height'?: string | number
    '--scale'?: number
  }
}
