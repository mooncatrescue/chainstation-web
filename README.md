This is the code repository for the main MoonCat​Rescue application (UI relaunch). Named "ChainStation", it fits into the lore of the MoonCat​Rescue ecosystem as a front-end interface for interacting with the Ethereum blockchain. The live version of this app is deployed as `https://chainstation.mooncatrescue.com`.

# Development

This application is a [Next.js](https://nextjs.org/) application (so, using [React](https://reactjs.org/) and [Typescript](https://www.typescriptlang.org/) under-the-hood), with Web3 integrations added in ([viem](https://www.npmjs.com/package/viem) and [web3modal](https://www.npmjs.com/package/@web3modal/react)).

To run a local development environment, you'll need to do a few things to set up your machine: install Docker, and create the `node:mooncat` base image used by many MoonCatRescue projects (the [MoonCat​Rescue development](https://gitlab.com/mooncatrescue/dev-environment) repository has full instructions for that). The first time you clone this repository, you'll need to run `npm install` in it, to get the needed JavaScript packages. Then run:

    docker compose up

That will start a local version of the [Firebase Emulator Suite](https://firebase.google.com/docs/emulator-suite), which should should be able to access the Emulator web console at https://localhost:25605, and the running application at http://localhost:25602.

Before pushing changes, run `next lint` (use `lint.sh` script to run locally in a Docker container) and review the output. All "Errors" must be resolved and all "Warnings" should be reviewed and corrected if applicable.

## Local API

This application uses the global/public MoonCat Data API server for client-side data-fetching. That style of data-fetching triggers a call to the public API server and gets up-to-date information about the Ethereum assests. However, fetching that data from the blockchain can be rather slow. To keep this application as zippy as possible, it has a stored cache of static metadata that doesn't change ([`mooncat_traits.json`](/lib/mooncat_traits.json)), and enumerating/searching through the whole collection uses that. Using that static JSON file is faster, but it's a several-megabyte-sized data blob. If it were included as part of the client-side application, the user's first visit would need to download that large data blob before the appliation loaded. Since most users aren't going to fetch data about _all_ MoonCats in a single session, that's a bunch of wasted data.

To resolve those issues, this application makes use of the Next.js [API routes](https://nextjs.org/docs/api-routes/introduction) functionality; any files located under `/pages/api/` are server-side components only. The [`mooncats.ts`](/pages/api/mooncats.ts) API route includes the `mooncat_traits.json` file (the webserver needs to load that file into memory when the server starts up), and it only serves that data to the client-side pages one "page" of results at a time (not the whole collection).

## Seed Data

This application uses some local data that is not generated on-chain, in order to provide additional functionality (e.g. Treasures that users can claim, based on their on-chain activity). For local development, to seed the local data into your local Firebase Emulator's running Firestore area, the scripts in the `/seed-data` folder can be used.

    FIRESTORE_EMULATOR_HOST="localhost:25603" node seed-data/treasures

# Deployment

This project uses Google Firebase's built in [Next.js support](https://firebase.google.com/docs/hosting/nextjs) to deploy to Firebase. Every merge into the default branch automatically will re-deploy the application. For project maintainers, if they wish to manually deploy the application, the process is to launch a `node:mooncat` container, and from within that, run:

    firebase login --no-localhost
    firebase experiments:enable webframeworks
    firebase deploy

The `firebase deploy` step does a `next build` and then pushes the static content to a Firebase Hosting site, and the dynamic content gets served via a Firebase Function.
