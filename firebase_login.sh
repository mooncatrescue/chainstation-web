#!/usr/bin/env bash

# This script only verifies that the login process to Google Cloud is working properly (by listing out the project names the current credential can see).
# It is intended to use the same setup as the `firebase_deploy.sh` script, but stop short of actually deploying the project.

# Save GitLab OIDC JWT to a local file
echo "${FIREBASE_ID_TOKEN}" > "${PWD}/gitlab_oidc_token"

# Configuration file telling the Firebase CLI tools how to use that token
export GOOGLE_APPLICATION_CREDENTIALS="${PWD}/clientLibraryConfig-gitlab.json"

# Deploy to Firebase
npm i firebase-tools
export NODE_ENV="production"

npx firebase experiments:enable webframeworks
ls -alh
npx firebase projects:list
