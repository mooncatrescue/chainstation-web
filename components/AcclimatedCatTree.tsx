'use client'

const PER_TREE = 10
const AcclimatedCatTree = ({ moonCats }: { moonCats: number[] }) => {
  // Distribute the MoonCats as evenly as possible
  const treesNeeded = Math.ceil(moonCats.length / PER_TREE)
  const minCatsPerTree = Math.floor(moonCats.length / treesNeeded)
  const remainder = moonCats.length % minCatsPerTree

  const moonCatsToDisplay: number[][] = []
  let i = 0
  while (i < moonCats.length) {
    const chunkSize = moonCatsToDisplay.length < remainder ? minCatsPerTree + 1 : minCatsPerTree
    moonCatsToDisplay.push(moonCats.slice(i, i + chunkSize))
    i += chunkSize
  }

  return (
    <div style={{ display: 'flex', gap: '30px', alignItems: 'flex-end', flexWrap: 'wrap' }}>
      {moonCatsToDisplay.slice(0, 10).map((moonCats, index) => (
        <div key={index} style={{ flex: '1 1 auto', textAlign: 'center' }}>
          <picture>
            <img
              style={{ maxWidth: '80vw' }}
              src={`/api/img/mooncat-tree?mooncats=${moonCats.join(',')}`}
              alt={`MoonCats on a cat tree: ${moonCats.map((mc) => '#' + mc).join(', ')}`}
            />
          </picture>
        </div>
      ))}
    </div>
  )
}
export default AcclimatedCatTree
