'use client'
import { useAppKit } from '@reown/appkit/react'
import { customEvent } from 'lib/analytics'
import { OwnedMoonCat, MoonCatData } from 'lib/types'
import useIsMounted from 'lib/useIsMounted'
import useTabHandler from 'lib/useTabHandler'
import useTxStatus from 'lib/useTxStatus'
import { API_SERVER_ROOT, switchToChain } from 'lib/util'
import { useContext, useRef, useState } from 'react'
import { Address, parseAbi } from 'viem'
import { useAccount, useReadContract } from 'wagmi'
import LoadingIndicator from './LoadingIndicator'
import MoonCatGrid from './MoonCatSelectorGrid'
import { readContract } from 'wagmi/actions'
import TabRow, { Tab } from './TabRow'
import { AppVisitorContext } from 'lib/AppVisitorProvider'
import useTabbieHook from 'lib/useTabbieHook'
import getMoonCatData from 'lib/getMoonCatData'
import { config } from 'lib/wagmi-config'

interface TabProps {
  ownedMoonCats: OwnedMoonCat[]
  needsAnnouncing?: number[]
  fetchNeedsAnnouncing: Function
}

const AnnouncedMoonCats = ({ ownedMoonCats, needsAnnouncing }: TabProps) => {
  const announcedMoonCats = getMoonCatData(
    ownedMoonCats
      .filter((mc) => {
        if (mc.collection.address == '0x60cd862c9c687a9de49aecdc3a99b74a4fc54ab6') return false
        if (typeof needsAnnouncing == 'undefined') return true
        if (needsAnnouncing.includes(mc.rescueOrder)) return false
        return true
      })
      .map((mc) => mc.rescueOrder)
  )

  const {
    state: { awokenMoonCats },
  } = useContext(AppVisitorContext)
  const announcedRef = useRef<HTMLTableElement>(null)

  useTabbieHook(announcedMoonCats, awokenMoonCats, (mc) => {
    const thumbNode = announcedRef.current?.querySelector(`#mooncat-${mc.rescueOrder} .thumb-img`)
    if (!thumbNode) {
      console.warn('Failed to find element for MoonCat', mc)
      return { x: window.innerWidth / 2, y: window.innerHeight / 2 }
    }
    const bounds = thumbNode.getBoundingClientRect()
    return {
      x: bounds.x + window.scrollX + bounds.width / 2,
      y: bounds.y + window.scrollY + bounds.height - 5,
    }
  })

  if (typeof needsAnnouncing == 'undefined') return <LoadingIndicator />

  if (announcedMoonCats.length == 0) {
    return (
      <div className="text-container">
        <section className="card">
          <p>No MoonCats in this wallet have Announced their MCNS names yet.</p>
        </section>
      </div>
    )
  }

  return (
    <div
      style={{ display: 'flex', columnGap: '2em', flexWrap: 'wrap', justifyContent: 'space-evenly' }}
      ref={announcedRef}
    >
      {announcedMoonCats.map((moonCat) => {
        let name = null
        if (typeof moonCat.name != 'undefined') {
          if (moonCat.name === true) {
            name = moonCat.catId
          } else {
            name = moonCat.name
          }
        } else {
          name = moonCat.catId
        }

        return (
          <section
            key={moonCat.rescueOrder}
            className="card"
            id={`mooncat-${moonCat.rescueOrder}`}
            style={{
              flex: '1 1 auto',
              maxWidth: '500px',
              display: 'flex',
              alignItems: 'center',
              gap: '1em',
            }}
          >
            <div
              className="thumb-img"
              style={{
                flex: '2 2 120px',
                backgroundImage: awokenMoonCats.has(moonCat.rescueOrder)
                  ? undefined
                  : `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5&costumes=true)`,
              }}
            />
            <div style={{ flex: '1 1 230px', textAlign: 'right' }}>
              <h3 style={{ margin: '0' }}>{name}</h3>
              <p style={{ fontSize: '0.8em' }}>
                My owner can be found at:
                <code>{moonCat.rescueOrder}.ismymooncat.eth</code>
                <br />
                <code>{moonCat.catId}.ismymooncat.eth</code>
              </p>
            </div>
          </section>
        )
      })}
    </div>
  )
}

const PendingMoonCats = ({ ownedMoonCats, needsAnnouncing, fetchNeedsAnnouncing }: TabProps) => {
  const isMounted = useIsMounted()
  const { isConnected } = useAccount()
  const { open } = useAppKit()
  const [selectedMoonCats, setSelectedMoonCats] = useState<Set<bigint>>(new Set())
  const { viewMessage, setStatus, isProcessing, processTransaction } = useTxStatus()

  const handleClick = (mc: MoonCatData) => {
    const rescueOrder = BigInt(mc.rescueOrder)
    setSelectedMoonCats((current) => {
      let next = new Set(current)
      if (next.has(rescueOrder)) {
        next.delete(rescueOrder)
      } else {
        next.add(rescueOrder)
      }
      return next
    })
  }

  const handleAnnounce = async () => {
    if (selectedMoonCats.size == 0) {
      setStatus('error', 'No MoonCats selected')
      return
    }
    setStatus('building')

    const MCNS = {
      address: '0x2eCcDA7C84837Da55ce34A36a5AD2B936479E76e' as Address,
      chainId: 1,
      abi: parseAbi([
        'function addr(bytes32 nodeID) external view returns (address)',
        'function addr(uint256 rescueOrder) external view returns (address)',
        'function mapMoonCat(uint256 rescueOrder) external',
        'function announceMoonCat(uint256 rescueOrder) external',
        'function mapMoonCats(uint256[] memory rescueOrders) external',
      ]),
    }
    let txConfig: Parameters<typeof processTransaction>[0]
    if (selectedMoonCats.size == 1) {
      // Determine if this MoonCat needs mapping, or just announcing
      const rescueOrder: bigint = Array.from(selectedMoonCats)[0]
      let isMapped = true
      try {
        await readContract(config, {
          ...MCNS,
          functionName: 'addr',
          args: [rescueOrder],
        })
      } catch (err) {
        isMapped = false
      }
      if (isMapped === false) {
        txConfig = {
          ...MCNS,
          functionName: 'mapMoonCats',
          args: [[rescueOrder]],
        }
      } else {
        txConfig = {
          ...MCNS,
          functionName: 'announceMoonCat',
          args: [rescueOrder],
        }
      }
    } else {
      // Do a batch Announce
      const rescueOrders = Array.from(selectedMoonCats)
      txConfig = {
        ...MCNS,
        functionName: 'mapMoonCats',
        args: [rescueOrders],
      }
    }
    let rs = await processTransaction(txConfig)
    if (rs) {
      Array.from(selectedMoonCats).forEach((num) => {
        customEvent('mcns_announce', {
          'mooncat': Number(num),
        })
      })
      setSelectedMoonCats(new Set())
      fetchNeedsAnnouncing()
    }
  }

  const actionButton = isConnected ? (
    <button disabled={isProcessing} onClick={handleAnnounce}>
      Announce
    </button>
  ) : (
    <button onClick={() => open()}>Connect</button>
  )

  const label = selectedMoonCats.size == 1 ? 'MoonCat' : 'MoonCats'
  const selectionView =
    selectedMoonCats.size > 0 ? (
      <>
        <p>
          {selectedMoonCats.size} {label} selected for Announcing
        </p>
        <p>{actionButton}</p>
        {viewMessage}
      </>
    ) : (
      <p>Click to select some MoonCats to Announce</p>
    )

  return (
    <>
      <div className="text-container">
        <section className="card-help">
          <p>
            MCNS subdomains need to be <em>Announced</em>, in order for the wider ENS ecosystem to be made aware they
            exist (triggers on-chain Events that ENS-supporting tools watch for). A MCNS address only needs to be
            Announced again if the MoonCat changes wallets.
          </p>
        </section>
      </div>
      {isMounted && typeof needsAnnouncing == 'undefined' && <LoadingIndicator style={{ marginBottom: '1em' }} />}
      <MoonCatGrid
        moonCats={ownedMoonCats
          .filter(
            (mc) =>
              mc.collection.address.toLowerCase() != '0xc3f733ca98e0dad0386979eb96fb1722a1a05e69' ||
              needsAnnouncing?.includes(mc.rescueOrder)
          )
          .map((mc) => mc.rescueOrder)}
        minCellWidth={100}
        highlightedMoonCats={Array.from(selectedMoonCats).map((i) => Number(i))}
        disabledMoonCats={ownedMoonCats
          .filter((mc) => mc.collection.address.toLowerCase() == '0x60cd862c9c687a9de49aecdc3a99b74a4fc54ab6')
          .map((mc) => mc.rescueOrder)}
        onClick={handleClick}
      >
        {(mc: MoonCatData) => <p>#{mc.rescueOrder}</p>}
      </MoonCatGrid>
      <div className="text-container">
        <section className="card" style={{ textAlign: 'center' }}>
          {selectionView}
        </section>
      </div>
    </>
  )
}

const tabs = {
  'announced': {
    label: 'Announced',
    content: AnnouncedMoonCats,
  } as Tab<TabProps>,
  'pending': {
    label: 'Pending',
    content: PendingMoonCats,
  } as Tab<TabProps>,
}
type TabName = keyof typeof tabs
const defaultTab: TabName = 'announced'
function isTabName(name: string): name is TabName {
  return Object.keys(tabs).includes(name)
}

const MCNSProfile = ({ ownerAddress, moonCats }: { ownerAddress: Address; moonCats: OwnedMoonCat[] }) => {
  const { activeTab, toggleTab } = useTabHandler(defaultTab)
  if (!isTabName(activeTab)) console.debug('Invalid tab name', activeTab)

  const humanAbi = ['function needsAnnouncing(address) external view returns (uint256[])'] as const
  const {
    data: needsAnnouncing,
    isError,
    isLoading,
    refetch: fetchNeedsAnnouncing,
  } = useReadContract({
    address: '0x2eCcDA7C84837Da55ce34A36a5AD2B936479E76e',
    abi: parseAbi(humanAbi),
    chainId: 1,
    functionName: 'needsAnnouncing',
    args: [ownerAddress],
    query: { select: (rs) => rs.map((id) => Number(id)).sort((a, b) => a - b) },
  })

  return (
    <TabRow tabs={tabs} activeTab={isTabName(activeTab) ? activeTab : defaultTab} toggleTab={toggleTab}>
      {(TabContent) => (
        <TabContent
          ownedMoonCats={moonCats}
          needsAnnouncing={!isLoading && !isError ? needsAnnouncing : undefined}
          fetchNeedsAnnouncing={fetchNeedsAnnouncing}
        />
      )}
    </TabRow>
  )
}
export default MCNSProfile
