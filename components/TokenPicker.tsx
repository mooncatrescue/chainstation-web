import { tokenDisplayLabel, TokenMeta } from 'lib/tokens'
import { CSSProperties, useEffect, useState } from 'react'
import Pagination from './Pagination'

type SelectableTokenMeta = TokenMeta & { isSelected?: boolean; isDisabled?: boolean }
interface PaginationState {
  currentPage: number
  pageTokens: SelectableTokenMeta[]
}

const TokenPicker = ({
  tokens,
  onClick,
  perPage = 50,
  style
}: {
  tokens: SelectableTokenMeta[]
  onClick: (token: TokenMeta) => void
  perPage?: number
  style?: CSSProperties
}) => {
  const [paginationState, setPaginationState] = useState<PaginationState>({
    currentPage: 0,
    pageTokens: tokens.slice(0, perPage),
  })

  useEffect(() => {
    setPaginationState((curVal) => {
      return {
        currentPage: curVal.currentPage,
        pageTokens: tokens.slice(curVal.currentPage * perPage, (curVal.currentPage + 1) * perPage),
      }
    })
  }, [tokens, perPage])

  return (
    <>
      <div style={{ columnWidth: '15em', marginBottom: '2rem', ...style }}>
        <table cellSpacing="0" cellPadding="0" className="zebra" style={{ width: '100%' }}>
          <tbody>
            {paginationState.pageTokens.map((t) => {
              const key = t.collection.address + '::' + t.id
              let classes = []
              let clickHandler = () => {
                onClick(t)
              }
              if (t.isSelected) classes.push('highlight')
              if (t.isDisabled) {
                classes.push('disabled')
                clickHandler = () => {}
              }
              const [collectionLabel, tokenName] = tokenDisplayLabel(t)
              return (
                <tr key={key} style={{ cursor: 'pointer' }} className={classes.join(' ')} onClick={clickHandler}>
                  <td style={{ textAlign: 'center', padding: '0.2rem', lineHeight: 1 }}>
                    <picture>
                      <img
                        style={{ height: '2.5em', verticalAlign: 'middle' }}
                        src={t.imageSrc}
                        alt={`${t.collection.address} ${t.id}`}
                      />
                    </picture>
                  </td>
                  <td>
                    {collectionLabel} {tokenName}
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
      <Pagination
        currentPage={paginationState.currentPage}
        maxPage={Math.ceil(tokens.length / perPage) - 1}
        setCurrentPage={(newPage) => {
          setPaginationState((curVal) => {
            if (curVal.currentPage == newPage) {
              // Already on this page
              return curVal
            }
            return {
              currentPage: newPage,
              pageTokens: tokens.slice(newPage * perPage, (newPage + 1) * perPage),
            }
          })
        }}
      />
    </>
  )
}
export default TokenPicker
