'use client'
import useSignedIn from 'lib/useSignedIn'
import { Address } from 'viem'
import LoadingIndicator from './LoadingIndicator'
import { useAppKit } from '@reown/appkit/react'
import WarningIndicator from './WarningIndicator'
import { useState } from 'react'
import useIsMounted from 'lib/useIsMounted'

const SignInFirst = ({
  className = 'card',
  targetAddress,
  preamble,
  children,
}: {
  className?: string
  targetAddress?: Address
  preamble?: React.ReactNode
  children:
    | JSX.Element
    | JSX.Element[]
    | ((didInteract: boolean, preamble: React.ReactNode) => JSX.Element | JSX.Element[])
}): JSX.Element | JSX.Element[] => {
  const isMounted = useIsMounted()
  const { isConnected, connectedAddress, isSignedIn, status, verificationStatus, doSignIn } = useSignedIn()
  const { open } = useAppKit()
  const [didInteract, setDidInteract] = useState<boolean>(false)

  if (!isMounted) {
    return <LoadingIndicator message="Checking your credentials..." />
  }
  if (!isConnected) {
    // If the visitor isn't connected, they need to make a connection first. After that they can sign in
    const notice = preamble ? (
      preamble
    ) : (
      <p>
        You do not have an active connection to your Ethereum wallet. To view this page, you need to connect your wallet
        first.
      </p>
    )
    return (
      <section className={className}>
        {notice}
        <p>
          <button
            onClick={(e) => {
              open()
            }}
          >
            Connect
          </button>
        </p>
      </section>
    )
  }
  if (!isSignedIn) {
    // User has not signed-in yet
    let statusView: React.ReactNode
    if (status == 'pending') {
      statusView = <LoadingIndicator message="Signing in..." />
    } else if (status == 'error') {
      statusView = <WarningIndicator message="Meteor shower! Error encountered logging in" />
    } else if (verificationStatus == 'pending') {
      statusView = <LoadingIndicator message="Verifying addressess..." />
    } else if (verificationStatus == 'error') {
      statusView = <WarningIndicator message="Meteor shower! Error encountered checking logged in status" />
    }
    const notice = preamble ? (
      preamble
    ) : (
      <p>To view this page, you need to verify you are the owner of this wallet address first.</p>
    )
    return (
      <section className={className}>
        {notice}
        <p>
          <button
            onClick={(e) => {
              doSignIn().then(() => {
                setDidInteract(true)
              })
            }}
            disabled={status == 'pending' || verificationStatus == 'pending'}
          >
            Sign In
          </button>
        </p>
        {statusView}
      </section>
    )
  }

  if (typeof targetAddress != 'undefined' && connectedAddress != targetAddress) {
    return (
      <section className={className}>
        <p>
          This section is only accessible to the address <code>{targetAddress}</code>, but that is not the active
          address in your wallet. If you are the owner of that address, switch your wallet to it to see this page.
        </p>
      </section>
    )
  }

  if (typeof children == 'function') {
    return children(didInteract, preamble)
  }
  return children
}
export default SignInFirst
