import { useContext } from 'react'
import Link from 'next/link'
import EthereumAddress from 'components/EthereumAddress'
import NotificationPill from 'components/NotificationPill'
import { TabProps } from 'components/ProfilePage'
import { ActionQueueContext } from 'lib/ActionsQueueProvider'
import { CartContext } from 'lib/ShoppingCartProvider'
import { actionIsComplete } from 'lib/onchainActions'
import useSignedIn from 'lib/useSignedIn'

const CHAIN_NAMES: Record<number, string> = {
  1: 'Mainnet',
  42161: 'Arbitrum',
}

const MyInfo = ({ addresses }: TabProps) => {
  const { connectedAddress, connectedChain, doLogOut } = useSignedIn()
  const addressDisplay = addresses.length > 1 ? addresses.length + ' addresses' : '1 address'
  const connectedChainLabel = typeof connectedChain == 'undefined' ? 'unknown chain' : CHAIN_NAMES[connectedChain]

  const { cart } = useContext(CartContext)
  const { actions } = useContext(ActionQueueContext)
  const pendingActions = actions.filter((a) => a.fromAddress == connectedAddress && !actionIsComplete(a))

  return (
    <>
      <section className="card">
        <h2>Connected Address</h2>
        <p>
          Your active wallet connection is <EthereumAddress address={connectedAddress!} /> on {connectedChainLabel}
        </p>
      </section>
      <section className="card">
        <h2>Verified Addresses</h2>
        <p>
          You are known and verified on this site as {addressDisplay}. To disconnect individual addresses click their
          &ldquo;Log Out&rdquo; buttons below.
        </p>
        <ul>
          {addresses.map((a) => (
            <li key={a.chainId + '::' + a.address}>
              <EthereumAddress address={a.address} /> on {CHAIN_NAMES[a.chainId]}{' '}
              <button className="btn-small" style={{ verticalAlign: '1px' }} onClick={(e) => doLogOut(a.address)}>
                Log Out
              </button>
            </li>
          ))}
        </ul>
      </section>
      <section className="card">
        <h2>My Content</h2>
        <ul>
          <li>
            <Link href="/profile/treasures">Treasures</Link>
          </li>
          <li>
            <Link href="/profile/moments">Moments</Link>
          </li>
          <li>
            <Link href="/profile/lists">Lists</Link>
          </li>
          <li>
            <Link href="/profile/stars">Stars</Link>
          </li>
          <li>
            <Link href="/profile/cart">Shopping Cart</Link>
            <NotificationPill count={cart.length} label={['item', 'items']} />
          </li>
          <li>
            <Link href="/profile/actions">Actions Queue</Link>
            <NotificationPill count={pendingActions.length} label={['action', 'actions']} />
          </li>
        </ul>
      </section>
    </>
  )
}
export default MyInfo
