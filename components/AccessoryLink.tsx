import { useQuery } from '@tanstack/react-query'
import { API2_SERVER_ROOT, ONE_DAY } from 'lib/util'
import Link from 'next/link'

const AccessoryLink = ({ id }: { id: number }) => {
  const { data: name } = useQuery({
    queryKey: ['accessory-traits', id],
    queryFn: async () => {
      const rs = await fetch(`${API2_SERVER_ROOT}/accessory/traits/${id}`)
      if (!rs.ok) throw new Error('Failed to fetch trait info')
      return await rs.json()
    },
    staleTime: ONE_DAY,
    select: (traits) => traits.name,
  })
  const displayName = typeof name == 'undefined' ? `Accessory #${id}` : name

  return <Link href={'/accessories/' + id}>{displayName}</Link>
}
export default AccessoryLink
