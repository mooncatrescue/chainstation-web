'use client'
import React, { CSSProperties } from 'react'
import Link from 'next/link'
import { MoonCatData, MoonCatDetails } from 'lib/types'
import { API_SERVER_ROOT } from 'lib/util'
import useTabHandler from 'lib/useTabHandler'
import MoonCatTabInfo from 'components/MoonCatTabInfo'
import MoonCatTabOnChain from 'components/MoonCatTabOnChain'
import MoonCatTabTimeline from 'components/MoonCatTabTimeline'
import MoonCatTabPurrse from 'components/MoonCatTabPurrse'
import MoonCatTabAccessories from 'components/MoonCatTabAccessories'
import MoonCatTabPhotobooth from 'components/MoonCatTabPhotobooth'
import Icon from 'components/Icon'
import { Listing } from 'lib/reservoirData'
import JsonLd from './JsonLd'
import TabRow, { Tab } from './TabRow'

export interface TabProps {
  moonCat: MoonCatData
  details: MoonCatDetails
  listings: Listing[]
}

const tabs = {
  'info': {
    label: 'Info',
    content: MoonCatTabInfo,
  } as Tab<TabProps>,
  'onchain': {
    label: 'On-Chain',
    content: MoonCatTabOnChain,
  } as Tab<TabProps>,
  'timeline': {
    label: 'Timeline',
    content: MoonCatTabTimeline,
  } as Tab<TabProps>,
  'purrse': {
    label: 'Purrse',
    content: MoonCatTabPurrse,
  } as Tab<TabProps>,
  'accessories': {
    label: 'Accessories',
    content: MoonCatTabAccessories,
  } as Tab<TabProps>,
  'photobooth': {
    label: 'Photobooth',
    content: MoonCatTabPhotobooth,
  } as Tab<TabProps>,
}
type TabName = keyof typeof tabs
const defaultTab: TabName = 'info'
function isTabName(name: string): name is TabName {
  return Object.keys(tabs).includes(name)
}

interface Props {
  moonCat: MoonCatData
  details: MoonCatDetails | null
  listings: Listing[]
}

/**
 * Show detail page for a specific MoonCat
 * Static metadata for the MoonCat is passed in as a property.
 */
const MoonCatDetail = ({ moonCat, details, listings }: Props) => {
  const { activeTab, toggleTab } = useTabHandler(defaultTab)
  if (!isTabName(activeTab)) console.debug('Invalid tab name', activeTab)

  let heroImgStyle: CSSProperties = { maxHeight: 350, maxWidth: '95vw', display: 'block', margin: '0 auto' }
  if (details && details.glow.reduce((sum, n) => sum + n, 0) < 180) {
    // If the MoonCat has a very dark glow, lighten the background behind them just a bit, to make it more clear
    heroImgStyle.filter = 'drop-shadow(0px 0px 120px rgba(255,255,255, 0.35))'
  }

  return (
    <div id="content-container" itemScope itemType="https://schema.org/ItemPage">
      <meta itemProp="isFamilyFriendly" content="true" />
      <nav className="breadcrumb">
        <Link href="/mooncats">
          <>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Browse all MoonCats
          </>
        </Link>
      </nav>
      <JsonLd
        data={{
          '@context': 'https://schema.org',
          '@type': 'BreadcrumbList',
          'itemListElement': [
            {
              '@type': 'ListItem',
              'position': 1,
              'name': 'MoonCats',
              'item': 'https://chainstation.mooncatrescue.com/mooncats',
            },
            {
              '@type': 'ListItem',
              'position': 2,
              'name': `MoonCat #${moonCat.rescueOrder} (${moonCat.catId})`,
            },
          ],
        }}
      />
      <h1 className="hero" itemProp="name">
        MoonCat #{moonCat.rescueOrder}
      </h1>
      <div style={{ margin: '2rem 0' }}>
        <picture>
          <img
            src={`${API_SERVER_ROOT}/image/${moonCat.catId}?costumes=true`}
            alt={`MoonCat #${moonCat.rescueOrder} (${moonCat.catId})`}
            style={heroImgStyle}
            itemProp="image"
          />
        </picture>
      </div>
      <JsonLd
        data={{
          '@context': 'https://schema.org/',
          '@type': 'ImageObject',
          'contentUrl': `${API_SERVER_ROOT}/image/${moonCat.catId}?costumes=true`,
          'license': 'https://mooncat.community/mooncatlicense',
          'creator': {
            '@type': 'Organization',
            'name': 'MoonCatRescue',
          },
          'creditText': `MoonCat #${moonCat.rescueOrder} MoonCatRescue`,
          'copyrightHolder': {
            '@type': 'Organization',
            'name': 'MoonCatRescue LLC',
          },
          'copyrightNotice': details?.owner,
        }}
      />
      <TabRow tabs={tabs} activeTab={isTabName(activeTab) ? activeTab : defaultTab} toggleTab={toggleTab}>
        {(TabContent) => {
          if (details == null)
            return (
              <p className="message-error">
                <Icon name="warning" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
                Failed to fetch MoonCat info
              </p>
            )
          return <TabContent moonCat={moonCat} details={details} listings={listings} />
        }}
      </TabRow>
    </div>
  )
}

export default MoonCatDetail
