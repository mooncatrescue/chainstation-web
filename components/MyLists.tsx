'use client'
import { useEas, attestHelpText, SCHEMAS, AttestationDoc, BasicAttestation } from 'lib/eas'
import { UserListTokenMeta, tokenDisplayLabel, parseTokenListMoonCats, parseTokenListAttestations } from 'lib/tokens'
import { MoonCatData, TokenListMeta, OwnerProfile } from 'lib/types'
import getMoonCatData from 'lib/getMoonCatData'
import useDialog from 'lib/useDialog'
import useFetchStatus from 'lib/useFetchStatus'
import { MOONCAT_TRAITS_ARB, API2_SERVER_ROOT, FIVE_MINUTES, ONE_HOUR, CHAIN_IDS, switchToChain } from 'lib/util'
import { useState, CSSProperties, useContext } from 'react'
import { Address, pad, stringToHex } from 'viem'
import { useAccount } from 'wagmi'
import { TextField } from './FormFields'
import Icon from './Icon'
import LoadingIndicator from './LoadingIndicator'
import Pagination from './Pagination'
import WarningIndicator from './WarningIndicator'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import {
  actionIsComplete,
  ModifyUserListAction,
  RevokeAttestationsStepPending,
  stepIsComplete,
} from 'lib/onchainActions'
import { ActionQueueContext } from 'lib/ActionsQueueProvider'
import { useRouter } from 'next/navigation'

// https://arbitrum.easscan.org/schema/view/0x58de78ba175d13cb1699fdbc6c25a80ba2b21a943b5aac252ec367c878808d09
const SCHEMA_TOKEN_LIST = SCHEMAS.TOKEN_LIST as `0x${string}`

// https://arbitrum.easscan.org/schema/view/0xeae0ff58b769be065ba9d54f4dd9b88c07de93a00e1e3b889a7099de02dd5549
const SCHEMA_PRODUCT_REVIEW = SCHEMAS.PRODUCT_REVIEW

/**
 * Form controller for manually creating a new User Token List
 */
const NewList = ({ onListCreated }: { onListCreated: () => void }) => {
  const { offchainSign } = useEas(CHAIN_IDS['arb'].chainId)
  const [status, setStatus] = useFetchStatus()
  const [errorMessage, setErrorMessage] = useState<string>('')

  const [newListTitle, setNewListTitle] = useState<string>('')
  const [moonCatIDs, setMoonCatIDs] = useState<string>('')

  async function createList() {
    if (newListTitle.length == 0) {
      setErrorMessage('List title cannot be blank')
      setStatus('error')
      return
    }
    setStatus('pending')
    const rawIDs = moonCatIDs
      .replaceAll(' ', ',')
      .split(',')
      .filter((i) => i.length > 0)
    if (rawIDs.length == 0) {
      setErrorMessage('Need at least one MoonCat in the list')
      setStatus('error')
      return
    }
    console.debug('raw IDs', rawIDs.length, rawIDs)
    // The attestation needs Hex IDs. Iterate through and find IDs that look like rescue IDs, to look up their Hex IDs.
    const rescueOrders = rawIDs.filter((i) => i.indexOf('0x') < 0).map((i) => Number(i))
    console.debug('rescue orders', rescueOrders)
    const mcTraits: Record<string, `0x${string}`> = {}
    if (rescueOrders.length > 0) {
      const traitsData = getMoonCatData(rescueOrders)
      traitsData.forEach((mc: MoonCatData) => {
        mcTraits[mc.rescueOrder] = mc.catId as `0x${string}`
      })
    }

    console.debug('Rescue order mapping', Object.entries(mcTraits).length, mcTraits)
    const tokenIds = rawIDs
      .map((i) => {
        if (i.indexOf('0x') == 0) return pad(i as `0x${string}`)
        if (typeof mcTraits[i] == 'undefined') {
          console.warn('Failed to look up MoonCat rescue order', i)
          return undefined
        }
        return pad(mcTraits[i])
      })
      .filter((i) => typeof i != 'undefined')

    try {
      await offchainSign({
        schemaId: SCHEMA_TOKEN_LIST,
        recipient: MOONCAT_TRAITS_ARB,
        revocable: true,
        data: [
          { name: 'tokenIds', value: tokenIds, type: 'bytes32[]' },
          { name: 'name', value: newListTitle, type: 'bytes32' },
        ],
      })
    } catch (err: any) {
      console.error(err)
      setErrorMessage('Attestation signature failed')
      setStatus('error')
      return
    }

    onListCreated()
    setNewListTitle('')
    setMoonCatIDs('')
    setStatus('done')
  }

  return (
    <section className="card">
      <h2>Create new List</h2>
      <p>
        Add one or multiple MoonCats to a new list in one action here. If you have multiple MoonCats to add, separate
        their IDs with a comma or space character in the form below.
      </p>
      <div className="form-grid">
        <TextField
          meta={{ type: 'text', name: 'listTitle', label: 'List Title' }}
          style={{ width: '25rem' }}
          currentValue={newListTitle}
          onChange={(e) => setNewListTitle(e.target.value.substring(0, 20))}
        />
        {newListTitle.length >= 17 && (
          <p style={{ marginLeft: '11rem', fontSize: '0.8rem', lineHeight: '1.2em', color: '#ccc' }}>
            List titles need to be 20 characters or less. If you have more to say about your list, you can add a
            description to it later.
          </p>
        )}
        <TextField
          meta={{ type: 'text', name: 'mooncats', label: 'MoonCat IDs' }}
          currentValue={moonCatIDs}
          onChange={(e) => setMoonCatIDs(e.target.value)}
        />
        <div style={{ gridColumnStart: '2' }}>
          <button onClick={createList} disabled={status == 'pending'}>
            Create
          </button>
        </div>
      </div>
      {status == 'error' && <WarningIndicator message={errorMessage} />}
      {status == 'pending' && <LoadingIndicator message="Formatting new list data..." />}
    </section>
  )
}

interface PaginationState {
  currentPage: number
  pageTokens: UserListTokenMeta[]
}

const PER_PAGE = 50
const EditList = ({
  owner,
  listMeta,
  tokens,
  atts,
  onUpdate,
}: {
  owner: Address
  listMeta: TokenListMeta
  tokens: UserListTokenMeta[]
  atts: AttestationDoc[]
  onUpdate: () => void
}) => {
  let [listDescription, setListDescription] = useState<string>(listMeta.description ? listMeta.description : '')
  const { offchainSign, revokeAttestations } = useEas(CHAIN_IDS['arb'].chainId)
  const { ref: dialogRef, setIsOpen: setDialogIsOpen } = useDialog()
  const [fetchStatus, setFetchStatus] = useFetchStatus()
  const [pageError, setPageError] = useState<string | false>(false)
  const { actions, dispatch } = useContext(ActionQueueContext)
  const [pendingRemovals, setPendingRemovals] = useState<UserListTokenMeta[]>([])
  const [paginationState, setPaginationState] = useState<PaginationState>({
    currentPage: 0,
    pageTokens: tokens.slice(0, PER_PAGE),
  })

  // Is there already a multi-step action queued up that is incomplete for this User List?
  const existingAction: ModifyUserListAction | undefined = actions
    .filter((a): a is ModifyUserListAction => 'listTitle' in a)
    .find((a) => a.fromAddress == owner && a.listTitle == listMeta.title && !actionIsComplete(a))

  /**
   * Given an existing multi-step action, step the user through resolving it
   */
  async function doAction(action: ModifyUserListAction) {
    if (!(await switchToChain(CHAIN_IDS['arb'].chainId))) return // Must be on the Arbitrum network
    setFetchStatus('pending')
    setPageError(false)
    for (let i = 0; i < action.steps.length; i++) {
      const step = action.steps[i]
      if (stepIsComplete(step)) continue
      switch (step.type) {
        case 'REVOKE': {
          try {
            await revokeAttestations(step.uids)
          } catch (err) {
            setPageError(
              'Cosmic microwave interference! Failed to submit the transaction to revoke the existing attestation'
            )
            console.error(err)
            setFetchStatus('error')
            return
          }

          // Revoke successful. Mark this step as complete
          action.steps[i] = {
            ...step,
            timestamp: Math.floor(Date.now() / 1000),
          }
          dispatch({
            type: 'UPDATE',
            payload: action,
          })
          break
        }
        case 'ATTEST': {
          let att: BasicAttestation | null
          try {
            att = await offchainSign(step.msg)
          } catch (err) {
            setPageError('Cosmic microwave interference! Failed to submit updated attestation')
            console.error(err)
            setFetchStatus('error')
            return
          }

          // Attest successful. Mark this step a complete
          action.steps[i] = {
            ...step,
            uid: att!.sig.uid as `0x${string}`,
            timestamp: Number(att!.sig.message.time),
          }
          dispatch({
            type: 'UPDATE',
            payload: action,
          })
          break
        }
        default: {
          setPageError('Cosmic microwave interference! Failed to parse that action')
        }
      }
    }
    onUpdate()
    setFetchStatus('done')
  }

  /**
   * Convert the pending removals into actions
   * Iterate through the `pendingRemovals` and merge them into a multi-step Action
   */
  async function commitRemovals() {
    const affectedAttestations = new Set<`0x${string}`>()
    for (const r of pendingRemovals) {
      affectedAttestations.add(r.attUID)
    }

    // Find all tokens defined by those attestations
    const keepTokens: Record<Address, Set<`0x${string}`>> = {}
    atts
      .filter((att) => affectedAttestations.has(att.sig.uid as `0x${string}`))
      .forEach((att) => {
        const collectionAddress = att.sig.message.recipient as `0x${string}`
        if (typeof keepTokens[collectionAddress] == 'undefined') {
          keepTokens[collectionAddress] = new Set<`0x${string}`>()
        }
        for (const tokenId of att.data.tokenIds.value as `0x${string}`[]) {
          keepTokens[collectionAddress].add(tokenId)
        }
      })

    // Remove the ones marked for deletion
    for (const r of pendingRemovals) {
      keepTokens[r.collection.address].delete(r.id)
    }

    const removalLabel =
      pendingRemovals.length <= 5
        ? pendingRemovals.map((r) => `${r.collection.label ?? r.collection.address} ${r.name ?? r.id}`).join(', ')
        : pendingRemovals
            .slice(0, 5)
            .map((r) => `${r.collection.label ?? r.collection.address} ${r.name ?? r.id}`)
            .join(', ') + ` and ${pendingRemovals.length - 5} more tokens`

    const steps: ModifyUserListAction['steps'] = [
      {
        type: 'REVOKE',
        label: `Undo the previous ${
          affectedAttestations.size > 1 ? 'Attestations' : 'Attestation'
        } that added ${removalLabel}`,
        uids: Array.from(affectedAttestations),
        chainId: CHAIN_IDS['arb'].chainId,
        userAddress: owner,
      },
    ]
    for (const [collection, items] of Object.entries(keepTokens)) {
      if (items.size > 0) {
        steps.push({
          type: 'ATTEST',
          label: `Re-attest adding the other tokens from the ${collection} collection to this list`,
          msg: {
            schemaId: SCHEMA_TOKEN_LIST,
            recipient: collection as `0x${string}`,
            revocable: true,
            data: [
              { name: 'tokenIds', value: Array.from(items), type: 'bytes32[]' },
              { name: 'name', value: listMeta.title, type: 'bytes32' },
            ],
          },
          chainId: CHAIN_IDS['arb'].chainId,
          userAddress: owner,
        })
      }
    }

    if (steps.length === 1) {
      // There are no extra tokens to keep; the only step is the initial revoking.
      // Therefore, this isn't a multi-step action; just do the revoke
      if (!(await switchToChain(CHAIN_IDS['arb'].chainId))) return // Must be on the Arbitrum network
      setFetchStatus('pending')
      setPageError(false)
      try {
        await revokeAttestations((steps[0] as RevokeAttestationsStepPending).uids)
      } catch (err) {
        setPageError(
          'Cosmic microwave interference! Failed to submit the transaction to revoke the existing attestation'
        )
        console.error(err)
        setFetchStatus('error')
        return
      }

      // Revoke successful.
      setPendingRemovals([])
      onUpdate()
      return
    }

    const action: ModifyUserListAction = {
      label: `Remove tokens from User List ${listMeta.title}`,
      id: `user-list-token-remove-${crypto.getRandomValues(new Uint32Array(3)).join('')}`,
      listTitle: listMeta.title,
      fromAddress: owner,
      steps: steps,
    }

    dispatch({
      type: 'UPDATE',
      payload: action,
    })
    setPendingRemovals([])

    // Do the action now
    doAction(action)
  }

  /**
   * Update the list's description.
   * This needs to be a multi-step action if there's an existing description, to first revert the existing Attestation
   * @returns
   */
  async function saveDescription() {
    if (!(await switchToChain(CHAIN_IDS['arb'].chainId))) return // Must be on the Arbitrum network
    setFetchStatus('pending')

    const productName = pad(stringToHex('Token List: ' + listMeta.title), { dir: 'right', size: 32 })

    if (typeof listMeta.description !== 'undefined') {
      // There is an existing atteststion for this description
      const rs = await fetch(
        `${API2_SERVER_ROOT}/attestations?${new URLSearchParams({
          schema: `arb:${SCHEMA_PRODUCT_REVIEW}`,
          sender: `arb:${owner}`,
          data_key: 'productName',
          data_value: productName,
        })}`
      )
      if (!rs.ok) {
        console.error('Failed to fetch description attestation', rs)
        setPageError('Failed to find previous attestation to revoke')
        setFetchStatus('error')
        return
      }
      const rawAtts = await rs.json()
      if (rawAtts.length > 0) {
        // Revoke that attestation first
        try {
          await revokeAttestations(rawAtts[0].sig.uid) // Await user signing transaction
        } catch (err: any) {
          setPageError('Failed to submit the revoking transaction for the existing description attestation')
          console.error(err)
          setFetchStatus('error')
          return
        }
      }
    }

    setDialogIsOpen(true)
    try {
      await offchainSign({
        schemaId: SCHEMA_PRODUCT_REVIEW as `0x${string}`,
        recipient: owner,
        revocable: true,
        data: [
          { name: 'productName', value: productName, type: 'bytes32' },
          { name: 'review', value: listDescription, type: 'string' },
          { name: 'rating', value: 0n, type: 'uint8' },
        ],
      })
    } catch (err: any) {
      console.error(err)
      setDialogIsOpen(false)
      setFetchStatus('error')
      return
    }

    setDialogIsOpen(false)
    setFetchStatus('done')
  }

  /**
   * Event handler for updates from Pagination component when user navigates to a new page
   */
  function handlePageUpdate(newPage: number) {
    setPaginationState((curVal) => {
      if (curVal.currentPage == newPage) {
        // Already set to that value
        return curVal
      }
      return {
        currentPage: newPage,
        pageTokens: tokens.slice(newPage * PER_PAGE, (newPage + 1) * PER_PAGE),
      }
    })
  }

  return (
    <>
      <h2>{listMeta.title}</h2>
      <section className="text-scrim">
        <p>
          Click individual tokens below to mark them for removal from this list. Removing tokens from a list requires an
          on-chain action (<a href="https://docs.attest.org/docs/core--concepts/revocation">revoking</a> the
          attestation) on the Arbitrum blockchain; clicking the &ldquo;Finalize&rdquo; button will trigger an action in
          your wallet to confirm the revocation.
        </p>
        {fetchStatus == 'pending' && <LoadingIndicator message="Composing message to sign..." />}
        {pageError !== false && <WarningIndicator message={pageError} />}
        {fetchStatus !== 'pending' && typeof existingAction !== 'undefined' && (
          <WarningIndicator
            message={
              <>
                Need to resolve existing edit action before editing further
                <button className="btn-small" style={{ marginLeft: '1em' }} onClick={() => doAction(existingAction)}>
                  <Icon name="rocket" style={{ marginRight: '0.5em' }} />
                  Finish Existing
                </button>
                <button
                  className="btn-small"
                  style={{ marginLeft: '1em' }}
                  onClick={() =>
                    dispatch({
                      type: 'DELETE',
                      payload: existingAction.id,
                    })
                  }
                >
                  <Icon name="trash" style={{ marginRight: '0.5em' }} />
                  Abort Change
                </button>
              </>
            }
          />
        )}
      </section>
      <div className="item-grid">
        {paginationState.pageTokens.map((t) => {
          const key = t.collection.address + '::' + t.id
          const [collectionLabel, tokenName] = tokenDisplayLabel(t)
          const classes = ['item-thumb']
          const findMatching = (i: UserListTokenMeta) => i.collection.address == t.collection.address && i.id == t.id
          if (pendingRemovals.some(findMatching)) classes.push('disabled')
          return (
            <div
              key={key}
              className={classes.join(' ')}
              style={{ cursor: 'pointer' }}
              onClick={() => {
                setPendingRemovals((curVal) => {
                  const i = curVal.findIndex(findMatching)
                  if (i < 0) {
                    // No pending item yet; add it
                    return [...curVal, t]
                  } else {
                    // Pending item exists; remove it
                    const newVal = [...curVal]
                    newVal.splice(i, 1)
                    return newVal
                  }
                })
              }}
            >
              <div className="thumb-img" style={{ backgroundImage: `url(${t.imageSrc})` }} />
              <p>{collectionLabel}</p>
              <p>{tokenName}</p>
            </div>
          )
        })}
      </div>
      <Pagination
        currentPage={paginationState.currentPage}
        maxPage={Math.ceil(tokens.length / PER_PAGE) - 1}
        setCurrentPage={handlePageUpdate}
      />
      <p style={{ marginTop: '1em', textAlign: 'right' }}>
        <button
          disabled={pendingRemovals.length == 0 || typeof existingAction != 'undefined' || fetchStatus == 'pending'}
          onClick={commitRemovals}
        >
          Finalize Removals
        </button>
      </p>

      <div className="text-scrim">
        <h3>Description</h3>
        <p>
          <textarea
            value={listDescription}
            placeholder="Describe your cosmic collection..."
            onChange={(e) => setListDescription(e.target.value)}
          ></textarea>
        </p>
        <p style={{ textAlign: 'right', margin: '1em 0 2em' }}>
          <button
            onClick={(e) => {
              e.preventDefault()
              saveDescription()
            }}
            disabled={fetchStatus == 'pending'}
          >
            Save
          </button>
        </p>
      </div>
      <dialog ref={dialogRef} aria-modal="true" onCancel={(e) => setDialogIsOpen(false)}>
        <h3>Check your wallet to confirm this Attestation</h3>
        {attestHelpText}
      </dialog>
    </>
  )
}

const MyLists = () => {
  const { address } = useAccount()
  const [editList, setEditList] = useState<TokenListMeta | null>(null)
  const queryClient = useQueryClient()
  const router = useRouter()

  const { data: userLists, status: listFetchStatus } = useQuery({
    queryKey: ['owner-profile', address],
    queryFn: async (): Promise<OwnerProfile> => {
      const rs = await fetch(`${API2_SERVER_ROOT}/owner-profile/${address}`)
      if (!rs.ok) throw new Error('Failed to fetch owner profile')
      return await rs.json()
    },
    enabled: typeof address !== 'undefined',
    staleTime: FIVE_MINUTES,
    select: (ownerProfile) => ownerProfile.tokenLists.sort((a, b) => a.title.localeCompare(b.title)),
  })

  const { data: rawAtts, status: tokenFetchStatus } = useQuery({
    queryKey: ['user-list-tokens', address],
    queryFn: async (): Promise<AttestationDoc[]> => {
      const rs = await fetch(
        `${API2_SERVER_ROOT}/attestations?${new URLSearchParams({
          schema: `arb:${SCHEMA_TOKEN_LIST}`,
          sender: `arb:${address}`,
          limit: '100',
        })}`
      )
      if (!rs.ok) throw new Error('Failed to fetch attestations')
      return (await rs.json()).reverse()
    },
    enabled: typeof address !== 'undefined',
    staleTime: ONE_HOUR,
  })
  const listEntries = rawAtts ? parseTokenListMoonCats(parseTokenListAttestations(rawAtts)) : []

  if (listFetchStatus != 'success' || tokenFetchStatus != 'success') {
    return <LoadingIndicator className="text-scrim" />
  }

  // Group entries by list title
  const nameMap: Record<string, UserListTokenMeta[]> = {}
  for (const e of listEntries) {
    if (typeof nameMap[e.listTitle] == 'undefined') {
      nameMap[e.listTitle] = [e]
    } else {
      nameMap[e.listTitle].push(e)
    }
  }

  if (editList != null) {
    // Edit an individual list
    return (
      <>
        <nav className="breadcrumb">
          <a
            href="#"
            onClick={(e) => {
              e.preventDefault()
              setEditList(null)
            }}
          >
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Back to all your lists
          </a>
        </nav>
        <EditList
          owner={address!}
          listMeta={editList}
          tokens={nameMap[editList.title]}
          atts={rawAtts}
          onUpdate={() => {
            queryClient.invalidateQueries({ queryKey: ['user-list-tokens', address] })
            queryClient.invalidateQueries({ queryKey: ['owner-profile', address] })
            router.refresh()
          }}
        />
      </>
    )
  }

  // Individual list not selected for editing. Show all list titles for user to pick from
  const MAX_SHOWN_TOKENS = 5
  const tokenStyle: CSSProperties = { height: 40 }
  return (
    <>
      <section className="card-help">
        <p>
          Token Lists you&rsquo;ve created appear here. Click the Remove button on an individual token to remove it from
          the list. Removing tokens from a list requires an on-chain action on the Arbitrum blockchain (
          <a href="https://docs.attest.org/docs/core--concepts/revocation">revoking the attestation</a>); clicking a
          remove button will trigger an action in your wallet to confirm the revocation.
        </p>
      </section>
      <ul className="user-list-list">
        {userLists.map((l) => {
          const tokens = nameMap[l.title]
          const shownMoonCats = tokens.slice(0, MAX_SHOWN_TOKENS).map((t) => (
            <picture key={t.collection.address + '::' + t.id}>
              <img src={t.imageSrc} alt={`MoonCat ${t.name}`} style={tokenStyle} />
            </picture>
          ))
          return (
            <li key={l.title}>
              <a style={{ cursor: 'pointer' }} onClick={(e) => setEditList(l)}>
                <div className="name">{l.title}</div>
                <div className="details">
                  {tokens.length} {tokens.length > 1 ? 'MoonCats' : 'MoonCat'}
                </div>
                <div className="list-tokens-preview">
                  {shownMoonCats}
                  {tokens.length > MAX_SHOWN_TOKENS && '...'}
                </div>
              </a>
            </li>
          )
        })}
      </ul>
      <NewList
        onListCreated={() => {
          queryClient.invalidateQueries({ queryKey: ['user-list-tokens', address] })
          queryClient.invalidateQueries({ queryKey: ['owner-profile', address] })
          router.refresh()
        }}
      />
    </>
  )
}
export default MyLists
