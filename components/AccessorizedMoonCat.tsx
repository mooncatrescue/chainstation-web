import useMoonCatAccessories from 'lib/useMoonCatAccessories'
import { Address, parseAbi } from 'viem'
import { useReadContract } from 'wagmi'
import Image from 'next/image'

const ACCESSORIZED = {
  address: `0x91CF36c92fEb5c11D3F5fe3e8b9e212f7472Ec14` as Address,
  abi: parseAbi([
    'struct AccessoryData { uint232 accessoryId; uint8 paletteIndex; uint16 zIndex; }',
    'function accessorizedImageOf(uint256 rescueOrder, AccessoryData[] accessories, uint8 glowLevel, bool allowUnverified) external view returns (string)',
  ]),
  chainId: 1,
}

/**
 * Take an accessory identifier and parse it into an accessory ID and palette index.
 *
 * To specify just an accessory, pass in its ID number as a bigint or string. To specify a palette of that accessory,
 * pass in a string of the accessory ID and palette index joined by a colon.
 */
function parseId(id: bigint | string): [accId: bigint, paletteIndex: number | false] {
  if (typeof id == 'bigint') return [id, false]
  const pieces = id.split(':')
  if (pieces.length == 1) return [BigInt(pieces[0]), false]
  return [BigInt(pieces[0]), parseInt(pieces[1])]
}

const AccessorizedMoonCat = ({
  moonCat,
  accessories,
  children,
  style,
  className,
}: {
  moonCat: number
  accessories: (bigint | string)[]
  children?: (svg: string) => JSX.Element
  style?: React.CSSProperties
  className?: string
}) => {
  const { status: accStatus, ownedAccessories } = useMoonCatAccessories(moonCat)
  const parsedAccessories: { accessoryId: bigint; paletteIndex: number; zIndex: number }[] = accessories.map(
    (id, index) => {
      const [accId, paletteIndex] = parseId(id)
      if (paletteIndex !== false) {
        // Passed-in parameters specified a palette index; use it
        return {
          accessoryId: accId,
          paletteIndex: paletteIndex,
          zIndex: index + 1,
        }
      }
      if (accStatus !== 'success') {
        // No passed-in parameter specified, but we're not done fetching the specified MoonCat's data; just use default palette
        return {
          accessoryId: accId,
          paletteIndex: 0,
          zIndex: index + 1,
        }
      }

      // We have the MoonCat's metadata now; use the last palette index this MoonCat had worn for this accessory
      const existing = ownedAccessories.find((a) => a.accessoryId == accId)
      return {
        accessoryId: accId,
        paletteIndex: existing?.paletteIndex ?? 0,
        zIndex: index + 1,
      }
    }
  )

  const { data: svgData, status: imgStatus } = useReadContract({
    ...ACCESSORIZED,
    functionName: 'accessorizedImageOf',
    args: [BigInt(moonCat), parsedAccessories, 1, true],
    query: { enabled: accStatus == 'success' }, // Don't fetch until we have the MoonCat's metadata
  })
  if (imgStatus != 'success' || typeof svgData == 'undefined') {
    if (typeof children == 'function') return null
    return (
      <div
        style={{ position: 'relative', width: '2em', height: '2em', margin: '0 auto', ...style }}
        className={className}
      >
        <Image src="/img/loading.svg" alt="" fill={true} />
      </div>
    )
  }

  if (typeof children == 'function') {
    return children(svgData)
  }
  return (
    <picture>
      <img
        alt={`MoonCat #${moonCat}`}
        src={'data:image/svg+xml,' + encodeURIComponent(svgData)}
        style={style}
        className={className}
      />
    </picture>
  )
}
export default AccessorizedMoonCat
