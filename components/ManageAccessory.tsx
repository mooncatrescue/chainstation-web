'use client'
import ColorPicker from 'components/ColorPicker'
import { SelectField } from 'components/FormFields'
import LoadingIndicator from 'components/LoadingIndicator'
import SignInFirst from 'components/SignInFirst'
import WarningIndicator from 'components/WarningIndicator'
import { attestHelpText, SCHEMAS, useEas } from 'lib/eas'
import { AccessoryTraits, MoonCatData } from 'lib/types'
import useDialog from 'lib/useDialog'
import useFetchStatus from 'lib/useFetchStatus'
import useTxStatus from 'lib/useTxStatus'
import {
  ACCESSORIES_ADDRESS,
  AccessoryImageDetails,
  API2_SERVER_ROOT,
  COLORS,
  formatMoonCatK,
  parseMoonCatK,
  switchToChain,
} from 'lib/util'
import { useRouter } from 'next/navigation'
import Script from 'next/script'
import React, { CSSProperties, useState } from 'react'
import { Address, bytesToHex, hexToBytes, numberToHex, parseAbi } from 'viem'
import { useReadContract } from 'wagmi'

declare global {
  interface Window {
    LibMoonCat: any
  }
}

interface Props {
  accessory: number
  traits: AccessoryTraits | null
}

const config = {
  address: ACCESSORIES_ADDRESS as Address,
  chainId: 1,
  abi: parseAbi([
    'function accessoryInfo(uint256 accessoryId) external view returns (uint16 totalSupply, uint16 availableSupply, bytes28 name, address manager, uint8 metabyte, uint8 availablePalettes, bytes2[4] memory positions, bool availableForPurchase, uint256 price)',
    'function accessoryImageData(uint256 accessoryId) external view returns (bytes2[4] positions, bytes8[7] palettes, uint8 width, uint8 height, uint8 meta, bytes IDAT)',
    'function accessoryPaletteCount(uint256 accessoryId) external view returns (uint8)',
    'function accessoryPalette(uint256 accessoryId, uint256 paletteIndex) external view returns (bytes8)',
    'function addAccessoryPalette(uint256 accessoryId, bytes8 newPalette) external',
  ]),
}

const SetDescription = ({
  accessory,
  manager,
  description,
}: {
  accessory: number
  manager: Address
  description?: string
}) => {
  const [newDesc, setNewDesc] = useState<string>(description ?? '')
  const [hasDescription, setHasDescription] = useState<boolean>(
    typeof description !== 'undefined' && description !== ''
  )
  const { offchainSign, revokeAttestations } = useEas(1)
  const { ref: dialogRef, setIsOpen: setDialogIsOpen } = useDialog()
  const [fetchStatus, setFetchStatus] = useFetchStatus()
  const [pageError, setPageError] = useState<string | false>(false)

  async function saveDescription() {
    if (!(await switchToChain(1))) return
    setFetchStatus('pending')

    const productName = numberToHex(accessory, { size: 32 })

    if (hasDescription) {
      // There is an existing atteststion for this description
      const rs = await fetch(
        `${API2_SERVER_ROOT}/attestations?${new URLSearchParams({
          schema: `eth:${SCHEMAS.PRODUCT_REVIEW}`,
          sender: `eth:${manager}`,
          data_key: 'productName',
          data_value: productName,
        })}`
      )
      if (!rs.ok) {
        console.error('Failed to fetch description attestation', rs)
        setPageError('Failed to find previous attestation to revoke')
        setFetchStatus('error')
        return
      }
      const rawAtts = await rs.json()
      if (rawAtts.length > 0 && rawAtts[0].sig.uid) {
        // Revoke that attestation first
        try {
          await revokeAttestations(rawAtts[0].sig.uid)
        } catch (err) {
          setPageError('Failed to submit the revoking transaction for the existing description attestation')
          console.error(err)
          setFetchStatus('error')
          return
        }
      }
    }
    if (newDesc === '') {
      // No new description to set
      setHasDescription(false)
      setFetchStatus('done')
      return
    }

    setDialogIsOpen(true)
    try {
      await offchainSign({
        schemaId: SCHEMAS.PRODUCT_REVIEW as Address,
        recipient: ACCESSORIES_ADDRESS,
        revocable: true,
        data: [
          { name: 'productName', value: productName, type: 'bytes32' },
          { name: 'review', value: newDesc, type: 'string' },
          { name: 'rating', value: 0n, type: 'uint8' },
        ],
      })
    } catch (err: any) {
      console.error(err)
      setDialogIsOpen(false)
      setFetchStatus('error')
      return
    }

    setDialogIsOpen(false)
    setHasDescription(true)
    setFetchStatus('done')
  }

  return (
    <section className="card">
      <h2>Set Description</h2>
      <textarea value={newDesc} style={{ height: '10em' }} onChange={(e) => setNewDesc(e.target.value)} />
      <button
        onClick={(e) => {
          e.preventDefault()
          saveDescription()
        }}
      >
        Update
      </button>
      {fetchStatus == 'pending' && <LoadingIndicator message="Composing message to sign..." />}
      {pageError !== false && <WarningIndicator message={pageError} />}
      <dialog ref={dialogRef} aria-modal="true" onCancel={(e) => setDialogIsOpen(false)}>
        <h3>Check your wallet to confirm this Attestation</h3>
        {attestHelpText}
      </dialog>
    </section>
  )
}

const PaletteEditor = ({ accessory, accImageData }: { accessory: number; accImageData: AccessoryImageDetails }) => {
  const firstBlank = accImageData.palettes.findIndex((p) => p.join('') == '00000000')
  const activePalettes = firstBlank >= 0 ? accImageData.palettes.slice(0, firstBlank) : accImageData.palettes

  const [activePalette, setActivePalette] = useState<number[]>(activePalettes[0])
  const [activeMoonCat, setActiveMoonCat] = useState<Uint8Array>(Uint8Array.from([0, 0, 255, 0, 0]))
  const [editingColor, setEditingColor] = useState<number>(0)
  const { viewMessage, isProcessing, processTransaction } = useTxStatus()
  const router = useRouter()

  const moonCatK = parseMoonCatK(activeMoonCat[1])

  async function doAddPalette() {
    let rs = await processTransaction({
      ...config,
      functionName: 'addAccessoryPalette',
      args: [BigInt(accessory), bytesToHex(Uint8Array.from(activePalette))],
    })
    if (rs) {
      router.refresh()
    }
  }

  let imgSrc
  if (window.LibMoonCat) {
    const accImageConfig: AccessoryImageDetails = {
      ...accImageData,
      palettes: [activePalette],
      paletteIndex: 0,
      zIndex: 1,
    }
    imgSrc = window.LibMoonCat.generateImage(bytesToHex(activeMoonCat), [accImageConfig], { scale: 4 })
  }

  const dialogStyle: CSSProperties = {
    position: 'absolute',
    left: 110,
    top: 5,
    width: '400px',
    border: 'solid 2px #fff',
    padding: '1rem',
    background: '#111',
  }

  return (
    <section className="card">
      <h2>Design New Palette</h2>
      <p>
        Each accessory palette can have up to 8 colors, and each accessory can have up to 7 palettes. The existing
        palettes for this Accessory are shown below, and can serve as a basis for a new palette by clicking the
        &ldquo;Copy&rdquo; button next to each one. Click the color buttons running down the right-hand side of the
        accessory preview to pick a new color for that slot.
      </p>
      <ol>
        {activePalettes.map((p, i) => (
          <li key={i}>
            <div
              style={{ boxShadow: '0 0 5px 3px rgb(136 238 136 / 0.2)', display: 'inline-block', height: '20px' }}
              className="color-palette-stripes"
            >
              {p.map((colorIndex, i) => {
                const c = COLORS[colorIndex]
                return (
                  <div
                    key={i}
                    title={c.label}
                    style={{
                      display: 'inline-block',
                      background: `rgb(${c.r} ${c.g} ${c.b} / ${c.a / 255})`,
                      width: '2em',
                      height: '20px',
                    }}
                  />
                )
              })}
            </div>
            <button
              className="btn-small"
              style={{ verticalAlign: 6, margin: '5px 0 0 1rem' }}
              onClick={() => setActivePalette(p)}
            >
              Copy
            </button>
          </li>
        ))}
      </ol>
      <table style={{ marginBottom: '2rem' }}>
        <tbody>
          <tr>
            <td style={{ paddingRight: '2rem' }}>
              <picture>
                <img alt="" src={imgSrc} style={{ maxWidth: '30vw', display: 'block', margin: '0 auto' }} />
              </picture>
              <div className="form-grid">
                <SelectField
                  meta={{
                    type: 'select',
                    name: 'pose',
                    label: 'Pose',
                    options: {
                      standing: 'Standing',
                      sleeping: 'Sleeping',
                      pouncing: 'Pouncing',
                      stalking: 'Stalking',
                    },
                  }}
                  currentValue={moonCatK.pose}
                  onChange={(e) => {
                    setActiveMoonCat((currentValue) => {
                      const newCatId = Uint8Array.from([...currentValue])
                      newCatId[1] = formatMoonCatK({ ...moonCatK, pose: e.target.value as MoonCatData['pose'] })
                      return newCatId
                    })
                  }}
                />
                <SelectField
                  meta={{
                    type: 'select',
                    name: 'pattern',
                    label: 'Pattern',
                    options: {
                      pure: 'Pure',
                      tabby: 'Tabby',
                      spotted: 'Spotted',
                      tortie: 'Tortie',
                    },
                  }}
                  currentValue={moonCatK.pattern}
                  onChange={(e) => {
                    setActiveMoonCat((currentValue) => {
                      const newCatId = Uint8Array.from([...currentValue])
                      newCatId[1] = formatMoonCatK({ ...moonCatK, pattern: e.target.value as MoonCatData['pattern'] })
                      return newCatId
                    })
                  }}
                />
              </div>
            </td>
            <td>
              {[0, 1, 2, 3, 4, 5, 6, 7].map((i) => {
                const c = COLORS[activePalette[i]]
                return (
                  <div key={i} style={{ position: 'relative', margin: '0 0 0.5rem 0' }}>
                    <button
                      className="btn-color-picker"
                      onClick={() => {
                        setEditingColor((curValue) => {
                          if (curValue == i + 1) return 0
                          return i + 1
                        })
                      }}
                      style={{ borderColor: `rgb(${c.r} ${c.g} ${c.b})` }}
                    >
                      Color {i + 1}
                    </button>
                    <div
                      style={{
                        ...dialogStyle,
                        display: editingColor == i + 1 ? 'block' : 'none',
                      }}
                    >
                      <ColorPicker
                        highlighted={activePalette[i]}
                        onClick={(colorIndex) => {
                          setActivePalette((curValue) => {
                            let newValue = [...curValue]
                            newValue[i] = colorIndex
                            return newValue
                          })
                          setEditingColor(0)
                        }}
                      />
                    </div>
                  </div>
                )
              })}
            </td>
          </tr>
        </tbody>
      </table>
      <p>
        Ready to finalize your new palette? Clicking this button will trigger a transaction to finalize the new palette
        for this accessory. You will need to sign this transaction with your wallet and will be charged an Ethereum
        mainnet gas fee to finalize it. Once finalized, pages that look to the blockchain data for their content will
        update immediately, but pages that use generated imagery of your accessory may have cached data that will take a
        few hours to show updated palettes.
      </p>
      <p>
        <button disabled={isProcessing} onClick={(e) => doAddPalette()}>
          Add as new palette
        </button>
      </p>
      {viewMessage}
    </section>
  )
}

const ManageAccessory = ({ accessory, traits }: Props) => {
  const [scriptLoaded, setScriptLoaded] = useState<boolean>(false)

  const { data: accImageData } = useReadContract({
    ...config,
    functionName: 'accessoryImageData',
    args: [BigInt(accessory)],
    query: {
      select: (data) =>
        ({
          idat: data[5],
          palettes: data[1].map((hex) => Array.from(hexToBytes(hex))),
          positions: data[0].map((hex) => Array.from(hexToBytes(hex)) as [number, number]),
          width: data[2],
          height: data[3],
          meta: data[4],
        } as AccessoryImageDetails),
    },
  })

  const pageTitle = traits == null ? `Manage Accessory #${accessory}` : `${traits.name} - Accessory #${accessory}`

  if (traits == null) {
    return (
      <div id="content-container">
        <div className="text-container">
          <h1 className="hero">{pageTitle}</h1>
          <WarningIndicator message="Failed to load Accessory data" />
        </div>
      </div>
    )
  }

  if (typeof accImageData == 'undefined') {
    return (
      <div id="content-container">
        <div className="text-container">
          <h1 className="hero">{pageTitle}</h1>
          <LoadingIndicator />
        </div>
      </div>
    )
  }

  return (
    <>
      <Script
        src="/libmooncat.js"
        onLoad={() => {
          setScriptLoaded(true)
        }}
      />
      <div id="content-container">
        <div className="text-container">
          <h1 className="hero">{pageTitle}</h1>
          <SignInFirst className="card-help" targetAddress={traits.manager.value}>
            <>
              <SetDescription accessory={accessory} manager={traits.manager.value} description={traits.description} />
              <PaletteEditor accessory={accessory} accImageData={accImageData} />
            </>
          </SignInFirst>
        </div>
      </div>
    </>
  )
}
export default ManageAccessory
