import React, { ChangeEventHandler, useCallback, useEffect, useState } from 'react'
import { SelectFieldMeta, TextFieldMeta, isSelectFieldMeta } from 'lib/types'
import { interleave } from 'lib/util'
import { SelectField, TextField } from './FormFields'
import { AppEventHandler, AppEventType, useGlobalEvents } from 'lib/useGlobalEvents'

interface Props<T> {
  currentFilters: T
  filterMeta: readonly (SelectFieldMeta | TextFieldMeta)[]
  filteredCount?: number
  totalCount?: number
  label: string
  onChange: (propName: keyof T, newValue: any) => void
}
const Filter = <T extends Record<string, any>>({
  currentFilters,
  filterMeta,
  filteredCount,
  totalCount,
  label,
  onChange,
}: Props<T>) => {
  const [isOpen, setOpen] = useState<boolean>(false)

  /**
   * Handle a change to one of the form elements
   */
  const handleChange: ChangeEventHandler<HTMLSelectElement | HTMLInputElement> = function (e) {
    const propName = e.target.name as keyof T
    const newValue = e.target.value
    onChange(propName, newValue)
  }

  /**
   * Document-level event listener
   * For every key press, if it was the Escape key, close the filters form
   */
  const escListener = useCallback(
    (event: KeyboardEvent) => {
      if (event.key == 'Escape') {
        setOpen(false)
      }
    },
    [setOpen]
  )

  /**
   * On mount, start listening for keypresses
   */
  useEffect(() => {
    document.addEventListener('keydown', escListener, false)
    return () => {
      document.removeEventListener('keydown', escListener, false)
    }
  }, [escListener])

  const handleGlobalEvent: AppEventHandler = useCallback(
    (e) => {
      if (e.type !== AppEventType.CLOSE_DIALOGS) return
      setOpen(false)
    },
    [setOpen]
  )
  useGlobalEvents(handleGlobalEvent)

  // Build UI labels for the current filter state
  let filterPhrases = []
  for (let [propName, currentValue] of Object.entries(currentFilters)) {
    const filterName = propName as keyof T
    const meta = filterMeta.find((m) => m.name == filterName)
    if (typeof meta == 'undefined') {
      console.error('Failed to find form meta for', filterName)
    }
    if (typeof meta !== 'undefined' && !!currentValue) {
      const displayValue = isSelectFieldMeta(meta) ? (meta as SelectFieldMeta).options[currentValue] : currentValue
      filterPhrases.push(
        <React.Fragment key={filterName as string}>
          {meta.label}: <em>{displayValue}</em>
        </React.Fragment>
      )
    }
  }
  let filterPhrase: Array<JSX.Element | string> = ['']
  if (filterPhrases.length > 0) {
    filterPhrase = interleave(filterPhrases, ', ')
    filterPhrase.push('. ')
  }
  let filterSize: React.ReactNode
  if (filteredCount && totalCount) {
    if (filteredCount < totalCount) {
      filterSize = `${filteredCount.toLocaleString()} of ${totalCount.toLocaleString()} ${label}`
    } else if (totalCount == 1) {
      // Showing just one asset? No label for this
    } else if (totalCount == 2) {
      filterSize = `Showing both ${label}`
    } else if (totalCount > 0) {
      filterSize = `Showing all ${totalCount.toLocaleString()} ${label}`
    }
  }

  const modalStyle = isOpen ? {} : { display: 'none' }
  return (
    <div style={{ position: 'relative', display: 'flex', alignItems: 'baseline' }}>
      <button
        onClick={(e) => {
          e.stopPropagation()
          setOpen(!isOpen)
        }}
        aria-expanded={isOpen}
        aria-controls="filter-modal"
      >
        Filter
      </button>
      <div style={{ padding: '0 1rem', fontSize: '0.8rem' }}>
        {filterPhrase}
        {filterSize}
      </div>
      <div
        id="filter-modal"
        className="filter-modal"
        role="dialog"
        style={modalStyle}
        onClick={(e) => e.stopPropagation()}
      >
        <div className="form-grid" style={{ gridTemplateColumns: 'auto auto' }}>
          {filterMeta.map((m) => {
            if (isSelectFieldMeta(m)) {
              return (
                <SelectField
                  key={m.name}
                  meta={m}
                  currentValue={currentFilters[m.name]}
                  onChange={handleChange}
                />
              )
            } else {
              return (
                <TextField
                  key={m.name}
                  meta={m}
                  currentValue={currentFilters[m.name]}
                  onChange={handleChange}
                />
              )
            }
          })}
        </div>
      </div>
    </div>
  )
}
export default Filter
