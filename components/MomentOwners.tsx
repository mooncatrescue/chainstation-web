'use client'
import { Moment } from 'lib/types'
import useMomentOwners from 'lib/useMomentOwners'
import LoadingIndicator from './LoadingIndicator'
import WarningIndicator from './WarningIndicator'
import { API_SERVER_ROOT } from 'lib/util'
import Link from 'next/link'
import EthereumAddress from './EthereumAddress'

const MomentOwners = ({ moment }: { moment: Moment }) => {
  const { status, data: owners } = useMomentOwners(moment)

  if (status == 'pending') {
    return <LoadingIndicator />
  }
  if (status == 'error') {
    return <WarningIndicator message="Disturbance in the spaceways... Failed to fetch owners of this Moment" />
  }

  if (owners.length == 0) {
    return <p className="text-scrim">No one owns a copy of this Moment yet.</p>
  }

  return (
    <div style={{ columnWidth: '20em', marginBottom: '1.5em' }}>
      <table cellSpacing="0" cellPadding="0" className="zebra" style={{ margin: '0 auto' }}>
        <tbody>
          {owners.map((o) => {
            return (
              <tr key={o.tokenId.toString()}>
                <td style={{ textAlign: 'center', lineHeight: 1 }}>
                  {o.moonCat ? (
                    <Link href={'/mooncats/' + o.moonCat}>
                      <picture>
                        <img
                          alt={'MoonCat ' + o.moonCat}
                          title={'MoonCat ' + o.moonCat}
                          src={`${API_SERVER_ROOT}/image/${o.moonCat}?scale=1&padding=1&costumes=true`}
                          style={{ height: '1.5em' }}
                        />
                      </picture>
                    </Link>
                  ) : (
                    ' '
                  )}
                </td>
                <td style={{ fontSize: '0.8rem' }}>
                  <EthereumAddress address={o.owner} />
                </td>
                <td style={{ padding: '0.2rem 1rem', textAlign: 'right' }}>#{Number(o.tokenId)}</td>
              </tr>
            )
          })}
        </tbody>
      </table>
    </div>
  )
}
export default MomentOwners
