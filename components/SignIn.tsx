import React from 'react'
import LoadingIndicator from 'components/LoadingIndicator'
import WarningIndicator from 'components/WarningIndicator'
import Link from 'next/link'
import useSignedIn from 'lib/useSignedIn'

interface Props {
  onSignedIn?: () => void
  onSignedOut?: () => void
}
const SignIn = ({ onSignedIn, onSignedOut }: Props) => {
  const { verifiedAddresses, isSignedIn, status, doSignIn, doLogOut } = useSignedIn()

  // User clicked the sign-in button
  const signIn: React.MouseEventHandler<any> = async function signIn(e) {
    e.preventDefault()
    let rs = await doSignIn()
    if (!rs || typeof onSignedIn != 'function') return
    onSignedIn()
  }

  // User clicked the log-out button
  const logOut: React.MouseEventHandler<any> = async function logOut(e) {
    e.preventDefault()
    await doLogOut()
    if (typeof onSignedOut == 'function') onSignedOut()
  }

  if (isSignedIn) {
    return (
      <section className="card-notice">
        <p>
          You are verified as the owner of this wallet. You can view <Link href="/profile">your profile</Link> for more
          details. <button onClick={logOut}>Log Out</button>
        </p>
      </section>
    )
  }

  let loginStatusView: React.ReactNode
  if (status == 'error') {
    loginStatusView = <WarningIndicator message="Failed to authorize your user session" />
  } else if (status == 'pending') {
    loginStatusView = <LoadingIndicator message="Signing in..." />
  }

  return (
    <section className="card-notice">
      <p>
        Your browser claims this is your address, but you have not verified that yet. Click the button below to sign a
        message with your Ethereum wallet to verify you&rsquo;re the owner (more details on the sign-in process{' '}
        <Link href="/docs/login">over here</Link>).
      </p>
      <div style={{ textAlign: 'center' }}>
        <p style={{ marginBottom: 0 }}>
          <button onClick={signIn} disabled={status == 'pending'}>
            Sign In
          </button>
        </p>
        {loginStatusView}
      </div>
    </section>
  )
}

export default SignIn
