import { IPFS_GATEWAY, MOMENTS_ADDRESS, ZWS } from 'lib/util'
import { Address, parseAbi } from 'viem'
import LoadingIndicator from './LoadingIndicator'
import { multicall, readContract } from 'wagmi/actions'
import { Moment } from 'lib/types'
import Link from 'next/link'
import { useQuery } from '@tanstack/react-query'
import WarningIndicator from './WarningIndicator'

import momentsData from 'lib/moments_meta.json'
import { config } from 'lib/wagmi-config'
const moments = momentsData as Moment[]

const MOMENTS = {
  address: MOMENTS_ADDRESS as Address,
  chainId: 1,
  abi: parseAbi([
    'function balanceOf(address owner) external view returns (uint256)',
    'function tokenOfOwnerByIndex(address owner, uint256 index) external view returns (uint256)',
    'function getMomentId(uint256 tokenId) external view returns (uint256)',
  ]),
} as const

interface MomentRef {
  tokenId: number
  moment: Moment
}

interface Props {
  address: Address
}

/**
 * Show the MoonCatMoments tokens directly-held by a specific Ethereum address.
 *
 * This component fetches on-chain data on mount, rather than using an API-fetched summary of ownership information.
 */
export const MomentsOwned = ({ address }: Props) => {
  const { data: ownedMoments, status } = useQuery({
    queryKey: ['owned-moments', address],
    queryFn: async () => {
      const count = await readContract(config, {
        ...MOMENTS,
        functionName: 'balanceOf',
        args: [address],
      })
      if (count == 0n) {
        return []
      }

      // Iterate up to that moment count number to fetch which Moment ID it is
      let multicalls = []
      for (let i = 0n; i < count; i++) {
        multicalls.push({
          ...MOMENTS,
          functionName: 'tokenOfOwnerByIndex',
          args: [address, i],
        } as const)
      }
      const ownedTokenIds = await multicall(config, {
        contracts: multicalls,
        allowFailure: false,
      })

      // For each token ID, find out which Moment batch it's from
      multicalls = []
      for (let tokenId of ownedTokenIds) {
        multicalls.push({
          ...MOMENTS,
          functionName: 'getMomentId',
          args: [tokenId],
        } as const)
      }
      const ownedMomentIds = await multicall(config, {
        contracts: multicalls,
        allowFailure: false,
      })

      const ownedMoments: MomentRef[] = []
      for (let i = 0; i < ownedTokenIds.length; i++) {
        const tokenId = ownedTokenIds[i]
        const momentId = ownedMomentIds[i]
        ownedMoments.push({
          tokenId: Number(tokenId),
          moment: moments[Number(momentId)],
        })
      }

      return ownedMoments
    },
  })

  if (status == 'pending') {
    return <LoadingIndicator />
  }
  if (status == 'error') {
    return <WarningIndicator message="Telescope out of focus! Failed to parse Moment ownership information." />
  }

  if (ownedMoments.length == 0) {
    return (
      <section className="card">
        This address does not own any <Link href="/moments">{`MoonCat${ZWS}Moments`}</Link> directly.
      </section>
    )
  }

  return (
    <div className="item-grid" style={{ margin: '1em 0' }}>
      {ownedMoments.map((m) => {
        const imgSrc = m.moment.meta.image.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/')
        return (
          <a key={m.tokenId} className="item-thumb" href={`/moments/${m.moment.momentId}`}>
            <div className="thumb-img" style={{ backgroundImage: `url(${imgSrc})` }} />
            <p>#{m.tokenId}</p>
            <p>{m.moment.meta.name}</p>
          </a>
        )
      })}
    </div>
  )
}
