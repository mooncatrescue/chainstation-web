import { TokenMeta, getTokenMeta } from 'lib/tokens'
import { OwnerProfile, OwnedMoonCat, AccessoryContractData } from 'lib/types'
import { CartContext } from 'lib/ShoppingCartProvider'
import useSignedIn from 'lib/useSignedIn'
import { API2_SERVER_ROOT, FIVE_MINUTES, ACCLIMATOR_ADDRESS, parseMoonCatK, AccessoryCartItem } from 'lib/util'
import Link from 'next/link'
import { useContext, useState } from 'react'
import { numberToHex, hexToNumber } from 'viem'
import LoadingIndicator from './LoadingIndicator'
import TokenPicker from './TokenPicker'
import { useQuery } from '@tanstack/react-query'

const PurchaseAccessory = ({
  didInteract,
  preamble,
  accessoryId,
  accessoryDetail,
  name,
  background,
  price,
}: {
  didInteract: boolean
  preamble: React.ReactNode
  accessoryId: number
  accessoryDetail: AccessoryContractData
  name: string
  background: boolean
  price: bigint
}) => {
  const { connectedAddress } = useSignedIn()
  const [startProcess, setStartProcess] = useState<boolean>(didInteract)
  const { cart, dispatch } = useContext(CartContext)
  const [tokenChoices, setTokenChoices] = useState<TokenMeta[]>([])
  const [pageStatus, setPageStatus] = useState<JSX.Element | null>(null)

  const { data: ownedTokens, status } = useQuery({
    queryKey: ['owner-profile', connectedAddress],
    queryFn: async (): Promise<OwnerProfile> => {
      const rs = await fetch(`${API2_SERVER_ROOT}/owner-profile/${connectedAddress}`)
      if (!rs.ok) throw new Error('Failed to fetch owner profile')
      return await rs.json()
    },
    enabled: typeof connectedAddress !== 'undefined' && startProcess,
    staleTime: FIVE_MINUTES,
    select: (ownerProfile) =>
      (ownerProfile.ownedMoonCats as OwnedMoonCat[]).map((mc) =>
        getTokenMeta({
          collection: { address: ACCLIMATOR_ADDRESS },
          id: numberToHex(mc.rescueOrder),
          extra: { catId: mc.catId },
        })
      ),
  })

  if (!startProcess) {
    // The visitor is connected and signed-in, but hasn't given indication they want to give a gift
    // Wait for them to click a button to signal that intent before starting owned token enumeration
    return (
      <section className="card">
        {preamble}
        <p>
          <button
            onClick={(e) => {
              setStartProcess(true)
            }}
          >
            I want it!
          </button>
        </p>
      </section>
    )
  }

  // If the visitor is connected and signed in, and the async process to enumerate their balances kicked off. Is it in progress?
  if (status !== 'success') {
    return (
      <section className="card">
        {preamble}
        <LoadingIndicator message="Finding eligible tokens" />
      </section>
    )
  }

  // The async process has finished; enumerate which tokens the visitor owns
  // If the visitor doesn't have any MoonCats, show error message
  if (ownedTokens.length == 0) {
    return (
      <section className="card">
        {preamble}
        <p>
          You don&rsquo;t own any MoonCats.{' '}
          <a href="https://v2.nftx.io/vault/0x98968f0747e0a261532cacc0be296375f5c08398/buy/">Go adopt one</a> in order
          to buy accessories for it.
        </p>
      </section>
    )
  }

  const eligibleListIsActive = (BigInt(accessoryDetail.eligibleList[99]) & 1n) == 1n
  const eligibleMoonCats: number[] = []
  if (eligibleListIsActive) {
    for (let rescueOrder = 0; rescueOrder < 25440; rescueOrder++) {
      const wordIndex = Math.floor(rescueOrder / 256)
      const bitIndex = rescueOrder % 256
      const isEligible = BigInt(accessoryDetail.eligibleList[wordIndex]) & (1n << BigInt(255 - bitIndex))
      if (isEligible) eligibleMoonCats.push(rescueOrder)
    }
  }

  /**
   * Check if an accessory is already in the cart for a given MoonCat
   */
  function isInCart(rescueOrder: number) {
    return cart.some((i) => i.type == 'ACCESSORY' && i.moonCat == rescueOrder && i.accessory.id == accessoryId)
  }

  // The visitor has MoonCats; show those that are eligible to buy this accessory
  const eligiblePoses = ['standing', 'sleeping', 'pouncing', 'stalking'].filter(
    (_pose, i) => accessoryDetail.positions[i] != '0xffff'
  )
  return (
    <section className="card">
      {preamble}
      <TokenPicker
        tokens={ownedTokens.map((t) => {
          const meta = parseMoonCatK(parseInt(t.extra!.catId.slice(4, 6), 16))
          return {
            ...t,
            isSelected: tokenChoices.some((c) => c.collection.address == t.collection.address && c.id == t.id),
            isDisabled:
              isInCart(hexToNumber(t.id)) || // This accessory is already in the cart pending for this MoonCat
              !eligiblePoses.includes(meta.pose) || // This accessory doesn't allow this MoonCat's pose
              (eligibleListIsActive && !eligibleMoonCats.includes(hexToNumber(t.id))), // This Accessory has an eligibility list and this MoonCat isn't on it
          }
        })}
        onClick={(t) => {
          setTokenChoices((existing) => {
            if (existing.some((c) => c.collection.address == t.collection.address && c.id == t.id)) {
              // Already exists in the selected list; toggle it off
              return existing.filter((c) => c.collection.address !== t.collection.address || c.id !== t.id)
            } else {
              // Does not already exist; add it to the selected list
              return [...existing, t]
            }
          })
        }}
      />
      <p>
        <button
          disabled={tokenChoices.length == 0}
          onClick={(e) => {
            for (const c of tokenChoices) {
              const rescueOrder = hexToNumber(c.id)
              if (!isInCart(rescueOrder)) {
                dispatch({
                  type: 'UPDATE',
                  payload: {
                    type: 'ACCESSORY',
                    label: `${name} accessory for MoonCat #${rescueOrder}`,
                    accessory: {
                      id: accessoryId,
                      name: name,
                      background,
                      price: String(price),
                    },
                    moonCat: rescueOrder,
                    palette: 0,
                    zIndex: 0,
                  } as AccessoryCartItem,
                })
              }
              const formattedMoonCats = tokenChoices.map((c) => `#${hexToNumber(c.id)}`)
              const moonCatList =
                formattedMoonCats.length == 1
                  ? `MoonCat #${formattedMoonCats[0]}`
                  : formattedMoonCats.length > 5
                  ? 'MoonCats ' +
                    formattedMoonCats.slice(0, 5).join(', ') +
                    ` (and ${formattedMoonCats.length - 5} more)`
                  : 'MoonCats ' + formattedMoonCats.join(', ')
              setPageStatus(
                <>
                  Purchasing this accessory for {moonCatList} has been added to{' '}
                  <Link href="/profile/cart">your cart</Link>
                </>
              )
              setTokenChoices([])
            }
          }}
        >
          Add to cart
        </button>
      </p>
      {pageStatus && <p>{pageStatus}</p>}
    </section>
  )
}
export default PurchaseAccessory
