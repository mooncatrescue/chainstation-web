import React, { CSSProperties, MouseEventHandler } from 'react'
import Icon from './Icon'
import Image from 'next/image'

interface Props {
  name: string
  onClick: MouseEventHandler<HTMLButtonElement>
  title?: string
  style?: CSSProperties
  disabled?: boolean
}

const IconButton = ({ name, onClick, title, style, disabled }: Props) => {
  const icon =
    name == 'loading' ? (
      <span
        style={{
          display: 'inline-block',
          position: 'relative',
          width: '1em',
          height: '1em',
        }}
      >
        <Image src="/img/loading.svg" alt="" fill={true} />
      </span>
    ) : (
      <Icon name={name} />
    )
  return (
    <button className="icon-button" onClick={onClick} title={title} style={style} disabled={disabled}>
      {icon}
    </button>
  )
}

export default IconButton
