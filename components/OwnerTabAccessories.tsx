import { useInfiniteQuery } from '@tanstack/react-query'
import { AccessorySummary } from 'lib/types'
import { API2_SERVER_ROOT, FIVE_MINUTES } from 'lib/util'
import { TabProps } from './OwnerDetailsPage'
import LoadingIndicator from './LoadingIndicator'
import AccessoryThumb from './AccessoryThumb'

const PER_PAGE = 50
const OwnerTabAccessories = ({ ownerAddress, isOwnWallet, details }: TabProps) => {
  const {
    data: items,
    isFetchingNextPage,
    fetchNextPage,
    hasNextPage,
  } = useInfiniteQuery({
    queryKey: ['managed-accessories', ownerAddress],
    queryFn: ({ pageParam }: { pageParam: string | null }) => {
      const params = new URLSearchParams({
        manager: ownerAddress,
        sort: 'oldest',
        limit: String(PER_PAGE),
      })
      if (pageParam != null) params.set('start_after', pageParam)
      return fetch(`${API2_SERVER_ROOT}/accessories/?${params.toString()}`).then((rs) => {
        if (!rs.ok) throw new Error('Failed to fetch accessory data')
        return rs.json() as Promise<AccessorySummary[]>
      })
    },
    staleTime: FIVE_MINUTES,
    initialPageParam: null,
    getNextPageParam: (lastPage) =>
      lastPage.length == 0 || lastPage.length < PER_PAGE ? null : lastPage[lastPage.length - 1].id,
  })

  if (!items || !items.pages) {
    return <LoadingIndicator className="text-scrim" />
  }

  const parsedItems = items.pages.flat()
  const preamble = (
    <p>
      Accessories that get added to the MoonCat Boutique for MoonCats to acquire{' '}
      <a href="https://mooncat.community/accessory-designer">get designed</a> by a variety of artists across the vast
      reaches of Deep Space. The designer of each Accessory can change prices and set eligibility criteria for that
      Accessory, as well as pass management of the Accessory on to another Etherian.
    </p>
  )

  if (parsedItems.length == 0) {
    const summary = isOwnWallet ? (
      <p>You don&rsquo;t manage any Accessories, currently.</p>
    ) : (
      <p>This address doesn&rsquo;t manage any Accessories, currently.</p>
    )
    return (
      <section className="card">
        {preamble}
        {summary}
      </section>
    )
  }

  return (
    <>
      <section className="card">{preamble}</section>
      <div className="item-grid" style={{ margin: '0 0 2rem' }}>
        {parsedItems.map((acc) => (
          <AccessoryThumb key={acc.id} accessory={acc} />
        ))}
      </div>
      {hasNextPage && (
        <button onClick={() => fetchNextPage()} disabled={isFetchingNextPage}>
          Explore Further
        </button>
      )}
    </>
  )
}
export default OwnerTabAccessories
