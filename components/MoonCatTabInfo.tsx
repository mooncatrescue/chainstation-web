import React, { useContext, useRef } from 'react'
import EthereumAddress from 'components/EthereumAddress'
import Icon from 'components/Icon'
import NFTXAdopt from 'components/NFTXAdopt'
import { TabProps } from './MoonCatDetail'
import { MoonCatData, MoonCatDetails } from 'lib/types'
import { API_SERVER_ROOT, ADDRESS_DETAILS, MOONCAT_TRAITS_ARB, ZWS, interleave } from 'lib/util'
import { getMoonCatTidbits } from 'lib/tidbits'
import { pad } from 'viem'
import Link from 'next/link'
import StarButton from './StarButton'
import AddToListButton from './AddToListButton'
import useSignedIn from 'lib/useSignedIn'
import EthAmount from './EthAmount'
import { Listing } from 'lib/reservoirData'
import getMoonCatData from 'lib/getMoonCatData'
import { AppVisitorContext } from 'lib/AppVisitorProvider'
import useTabbieHook from 'lib/useTabbieHook'

interface TidbitDefinition {
  label: string
  detail: React.ReactNode
}
const tidbitDetails: Record<string, TidbitDefinition> = {
  'numericId': {
    label: 'Numeric ID',
    detail: 'This MoonCat has an identifier that contains only numbers',
  },
  'alphaId': {
    label: 'Alpha ID',
    detail: 'This MoonCat has an identifier that contains only letters',
  },
  'repeatId': {
    label: 'Repeat ID',
    detail: 'This MoonCat has an identifier that has a run of repeated characters at least 3 characters long',
  },
  'earlyRescue': {
    label: 'Early Rescue',
    detail: 'This MoonCat was rescued near the beginning of the Rescue operation',
  },
  'yearStart': {
    label: 'New Year',
    detail: 'This MoonCat was the first rescued in an individual calendar year',
  },
  'yearEnd': {
    label: 'Year End',
    detail: 'This MoonCat was the last rescued in an individual calendar year',
  },
  'earlyNaming': {
    label: 'Early Naming',
    detail: 'This MoonCat was named before the Rescue concluded',
  },
  'trueColor': {
    label: 'True Color',
    detail: 'This MoonCat has a coat that is right in the middle of that hue definition',
  },
  'edgeColor': {
    label: 'Edge Color',
    detail: 'This MoonCat has a coat that is very nearly another color label',
  },
  'darkGlow': {
    label: 'Dark Glow',
    detail: 'This MoonCat has a glow that is so dark it appears almost black',
  },
  'brightGlow': {
    label: 'Bright Glow',
    detail: 'This MoonCat has a glow that is so bright it appears almost white',
  },
  'inMoments': {
    label: 'In Moment',
    detail: (
      <>
        This MoonCat has been part of <Link href="/moments">MoonCatMoment</Link> experiences
      </>
    ),
  },
  'spokesCat': {
    label: 'MoonCatPop SpokesCat',
    detail: 'This MoonCat is the featured mascot of a flavor of MoonCatPop',
  },
  'firstPlushieMoonCat': {
    label: 'First MoonCat Plushie',
    detail: 'Celebrated the first MoonCat plushie merch offering',
  },
}

const marketplaces: { label: string; url: string }[] = [
  { label: 'OpenSea', url: 'https://opensea.io/item/ethereum/0xc3f733ca98e0dad0386979eb96fb1722a1a05e69/' },
  { label: 'LooksRare', url: 'https://looksrare.org/collections/0xc3f733ca98e0dad0386979eb96fb1722a1a05e69/' },
  { label: 'Blur', url: 'https://blur.io/eth/asset/0xc3f733ca98e0dad0386979eb96fb1722a1a05e69/' },
  { label: 'Sudoswap', url: 'https://sudoswap.xyz/#/item/0xc3f733ca98e0dad0386979eb96fb1722a1a05e69/' },
]

function ucFirst(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1)
}

const LitterView = ({ details }: { details: MoonCatDetails }) => {
  const littermates = getMoonCatData(details.litter)
  const {
    state: { awokenMoonCats },
  } = useContext(AppVisitorContext)
  const gridRef = useRef<HTMLDivElement>(null)
  useTabbieHook(littermates, awokenMoonCats, (mc) => {
    const cell = gridRef.current?.querySelector(`#mooncat-${mc.rescueOrder} .thumb-img`)
    if (!cell) {
      console.warn('Failed to find element for MoonCat', mc)
      return { x: window.innerWidth / 2, y: window.innerHeight / 2 }
    }
    const bounds = cell.getBoundingClientRect()
    return {
      x: bounds.x + window.scrollX + bounds.width / 2,
      y: bounds.y + window.scrollY + bounds.height - 20,
    }
  })

  if (details.onlyChild) {
    return (
      <p>
        This MoonCat is an only child (they have no &ldquo;littermates&rdquo;; MoonCats who have the exact same{' '}
        {details.hue} color (in the {details.hueValue}&deg; hue range) and pattern as them).
      </p>
    )
  }

  return (
    <>
      <h2>Litter</h2>
      <p>
        This MoonCat&rsquo;s litter (MoonCats who share the exact same {details.hue} color (in the {details.hueValue}
        &deg; hue range) and pattern) has {details.litterSize} MoonCats in it:
      </p>
      <div className="item-grid" style={{ margin: '1em 0' }} ref={gridRef}>
        {details.litter.map((rescueIndex) => {
          if (rescueIndex == details.rescueIndex) {
            return (
              <div key={rescueIndex} className="item-thumb highlight" id={`mooncat-${rescueIndex}`}>
                <div
                  className="thumb-img"
                  style={{
                    backgroundImage: !awokenMoonCats.has(rescueIndex)
                      ? `url(${API_SERVER_ROOT}/image/${rescueIndex}?scale=3&padding=5&costumes=true)`
                      : undefined,
                  }}
                />
                <p>#{rescueIndex}</p>
                <p>
                  <em>(This MoonCat)</em>
                </p>
              </div>
            )
          }
          let tag: React.ReactNode
          if (details.cloneSet.includes(rescueIndex)) {
            tag = (
              <p>
                <em>Clone</em>
              </p>
            )
          } else if (details.mirrorSet.includes(rescueIndex)) {
            tag = (
              <p>
                <em>Mirror</em>
              </p>
            )
          } else if (details.twinSet.includes(rescueIndex)) {
            tag = (
              <p>
                <em>Twin</em>
              </p>
            )
          }
          return (
            <a key={rescueIndex} className="item-thumb" href={'/mooncats/' + rescueIndex} id={`mooncat-${rescueIndex}`}>
              <div
                className="thumb-img"
                style={{
                  backgroundImage: !awokenMoonCats.has(rescueIndex)
                    ? `url(${API_SERVER_ROOT}/image/${rescueIndex}?scale=3&padding=5&costumes=true)`
                    : undefined,
                }}
              />
              <p>#{rescueIndex}</p>
              {tag}
            </a>
          )
        })}
      </div>
    </>
  )
}

const LinkToListing = ({ listings, moonCat }: { listings: Listing[]; moonCat: MoonCatData }) => {
  if (listings.length == 0) return null

  return (
    <section className="sub-card" style={{ marginBottom: '1.5em' }} itemScope itemType="https://schema.org/Product">
      <meta itemProp="name" content={`MoonCat #${moonCat.rescueOrder}`} />
      <link itemProp="image" href={`${API_SERVER_ROOT}/image/${moonCat.rescueOrder}`} />
      {listings.map((l) => (
        <p key={l.source.url} itemProp="offers" itemScope itemType="https://schema.org/Offer">
          <link itemProp="availability" href="https://schema.org/InStock" />
          <meta itemProp="price" content={String(l.price.amount.decimal)} />
          <meta itemProp="priceCurrency" content="ETH" />
          Adopt me for <EthAmount amount={l.price.amount.raw} truncateTo={7} />{' '}
          <a target="_blank" rel="noreferrer" href={l.source.url}>
            on {l.source.name}
          </a>
        </p>
      ))}
    </section>
  )
}

const MoonCatTabInfo = ({ moonCat, details, listings }: TabProps) => {
  const owner = details.owner
  const { isSignedIn } = useSignedIn()
  const listing =
    listings.length > 0 ? listings.sort((a, b) => a.price.amount.decimal - b.price.amount.decimal)[0] : undefined

  let marketplaceDetails: React.ReactNode
  if (typeof listing !== 'undefined') {
    marketplaceDetails = (
      <p>
        <a target="_blank" rel="noreferrer" href={listing.source.url}>
          Adopt this MoonCat on {listing.source.name}
          <Icon name="link-external" style={{ marginLeft: '0.5em', verticalAlign: '-0.2em' }} />
        </a>
      </p>
    )
  } else if (details.isAcclimated) {
    marketplaceDetails = (
      <p>
        View this MoonCat on NFT marketplaces:{' '}
        {interleave(
          marketplaces.map((m) => (
            <a key={m.label} target="_blank" rel="noreferrer" href={`${m.url}${moonCat.rescueOrder}`}>
              {m.label}
              <Icon name="link-external" style={{ marginLeft: '0.5em', verticalAlign: '-0.2em' }} />
            </a>
          )),
          ', '
        )}
      </p>
    )
  } else {
    marketplaceDetails = (
      <p>
        This MoonCat is currently held in the original MoonCat{ZWS}Rescue contract, and therefore won&rsquo;t show up on
        most NFT marketplaces. Their owner might wrap them into an ERC721 token (&ldquo;Acclimate&rdquo; them) in the
        future, and after that point they would be visible in tools that enumerate ERC721 tokens.
      </p>
    )
  }

  let coatDetail = `${ucFirst(details.hue)} ${ucFirst(moonCat.pattern)}`
  if (!details.genesis && details.isPale) coatDetail = 'Pale ' + coatDetail

  let nameDetails: React.ReactNode
  if (details !== null && details.isNamed != 'No' && details.name !== null) {
    // Use the MoonCat details information from the API server response
    if (details.isNamed == 'Invalid UTF8') {
      // The API server response doesn't have the raw hex value of the name; grab it from the local value if available
      let displayName: React.ReactNode = moonCat.nameRaw ? <code>{moonCat.nameRaw}</code> : details.name
      nameDetails = <>This MoonCat is named {displayName} (not a valid string name).</>
    } else if (details.isNamed == 'Redacted') {
      nameDetails = <>This MoonCat is named, but we have elected not to display that name here.</>
    } else {
      nameDetails = (
        <>
          This MoonCat is named <em>{details.name.trim()}</em>.
        </>
      )
    }
  } else if (!!moonCat.name) {
    // Use the MoonCat details cached locally
    if (moonCat.name === true) {
      nameDetails = (
        <>
          This MoonCat is named <code>{moonCat.nameRaw}</code> (a raw bit of data; not a valid string name).
        </>
      )
    } else if (moonCat.name == '\ufffd') {
      nameDetails = <>This MoonCat is named, but we have elected not to display that name here.</>
    } else {
      nameDetails = (
        <>
          This MoonCat is named <em>{moonCat.name.trim()}</em>.
        </>
      )
    }
  } else {
    nameDetails = <>This MoonCat doesn&rsquo;t have a name yet. It&rsquo;s owner can choose to give it a name.</>
  }

  let genesisDetails
  if (details.genesis) {
    genesisDetails = (
      <p className="highlight">
        This is a <em>Genesis</em> MoonCat! These sort of MoonCats watch over all the other MoonCats. They weren&rsquo;t
        rescued by Ethereans like the other MoonCats, but willingly joined with the Ethereans in the rescue efforts.
        Genesis MoonCats joined the rescue operation in groups of 16 (this one was part of group {details.genesisGroup}
        ).
      </p>
    )
  }

  let ownerDetails: React.ReactNode
  if (!details.rescuedBy) {
    // This should only happen for Genesis MoonCats.
    if (owner && ADDRESS_DETAILS[owner]?.type == 'pool') {
      // This MoonCat is in an NFT pool address
      const poolDetails = ADDRESS_DETAILS[owner]
      if (owner == '0x98968f0747E0A261532cAcC0BE296375F5c08398') {
        // Use custom component for NFTX pool
        ownerDetails = (
          <>
            <p>
              This MoonCat is in the {poolDetails.label} pool; <em>you can adopt them right now!</em>
            </p>
            <NFTXAdopt tokenId={moonCat.rescueOrder} quoteType="mooncat" style={{ marginBottom: '1.5em' }} />
          </>
        )
      } else if (owner == '0xA8b42C82a628DC43c2c2285205313e5106EA2853') {
        // Use custom component for NFTX pool
        ownerDetails = (
          <>
            <p>
              This MoonCat is in the {poolDetails.label} pool; <em>you can adopt them right now!</em>
            </p>
            <NFTXAdopt tokenId={moonCat.rescueOrder} quoteType="mcat17" style={{ marginBottom: '1.5em' }} />
          </>
        )
      } else {
        ownerDetails = (
          <p>
            This MoonCat is in the <Link href={`/owners/${owner}`}>{poolDetails.label} pool</Link>; you could visit{' '}
            <Link href={poolDetails.link} target="_blank" rel="noreferrer">
              that pool&rsquo;s website
            </Link>{' '}
            and adopt them right now!
          </p>
        )
      }
    } else if (owner) {
      ownerDetails = (
        <>
          <p>
            This MoonCat is currently adopted by <EthereumAddress address={owner} />.
          </p>
          <LinkToListing listings={listings} moonCat={moonCat} />
        </>
      )
    }
  } else if (owner == details.rescuedBy) {
    // MoonCat is in the same address that rescued them.
    ownerDetails = (
      <p>
        This MoonCat was rescued by <EthereumAddress address={details.rescuedBy} />, and is still owned by that wallet.
      </p>
    )
  } else {
    // MoonCat was rescued by one address, and currently owned by another; the most common situation.
    if (owner && ADDRESS_DETAILS[owner]?.type == 'pool') {
      // This MoonCat is in an NFT pool address
      const poolDetails = ADDRESS_DETAILS[owner]
      if (owner == '0x98968f0747E0A261532cAcC0BE296375F5c08398') {
        // Use custom component for NFTX pool
        ownerDetails = (
          <>
            <p>
              This MoonCat was originally rescued by <EthereumAddress address={details.rescuedBy} />, but currently is
              in the {poolDetails.label}; <em>you can adopt them right now!</em>
            </p>
            <NFTXAdopt tokenId={moonCat.rescueOrder} quoteType="mooncat" style={{ marginBottom: '1.5em' }} />
          </>
        )
      } else if (owner == '0xA8b42C82a628DC43c2c2285205313e5106EA2853') {
        // Use custom component for NFTX pool
        ownerDetails = (
          <>
            <p>
              This MoonCat was originally rescued by <EthereumAddress address={details.rescuedBy} />, but currently is
              in the {poolDetails.label}; <em>you can adopt them right now!</em>
            </p>
            <NFTXAdopt tokenId={moonCat.rescueOrder} quoteType="mcat17" style={{ marginBottom: '1.5em' }} />
          </>
        )
      } else {
        ownerDetails = (
          <p>
            This MoonCat was originally rescued by <EthereumAddress address={details.rescuedBy} />, but currently is in
            the <em>{poolDetails.label}</em>; you could visit{' '}
            <a href={poolDetails.link} target="_blank" rel="noreferrer">
              that pool&rsquo;s website
            </a>{' '}
            and adopt them right now! Or browse <Link href={`/owners/${owner}`}>that pool&rsquo;s collection</Link> here
            on this site.
          </p>
        )
      }
    } else if (owner) {
      ownerDetails = (
        <>
          <p>
            This MoonCat was originally rescued by <EthereumAddress address={details.rescuedBy} />, but has currently
            been adopted by <EthereumAddress address={owner} />.
          </p>
          <LinkToListing listings={listings} moonCat={moonCat} />
        </>
      )
    } else {
      ownerDetails = (
        <p>
          This MoonCat was originally rescued by <EthereumAddress address={details.rescuedBy} />, but is currently in an
          unsupported wrapper contract.
        </p>
      )
    }
  }

  const tidbits = getMoonCatTidbits(moonCat)
  let tidbitsList = []
  for (let key in tidbitDetails) {
    if (tidbits[key]) {
      let detailView
      if (tidbits[key] !== true) {
        detailView = <>({tidbits[key]})</>
      }
      tidbitsList.push(
        <li key={key}>
          <strong>{tidbitDetails[key].label}</strong> {tidbitDetails[key].detail} {detailView}
        </li>
      )
    }
  }

  let tidbitsView
  if (tidbitsList.length > 0) {
    tidbitsView = (
      <>
        <h2>Interesting Tidbits</h2>
        <ul>{tidbitsList}</ul>
      </>
    )
  }

  return (
    <div className="text-container">
      <section className="card-help">
        <p>
          This MoonCat has an identifier of <code>{moonCat.catId}</code>. This hexadecimal number is a bit like DNA in
          that it contains compressed information about this MoonCat&rsquo;s basic traits (color, facing, epression,
          pattern, and pose). Other traits have been gleaned by this MoonCat over time, as they and their human owner
          leave their mark on the blockchain.
        </p>
        {marketplaceDetails}
      </section>
      <section className="card">
        <div className="btn-bar">
          <StarButton
            targetAddress={MOONCAT_TRAITS_ARB}
            targetItem={pad(moonCat.catId as `0x${string}`)}
            itemLabel="MoonCat"
            message={'I like this MoonCat! \u{01F63B}'}
          />
          {isSignedIn && (
            <AddToListButton
              style={{ marginLeft: '0.5rem' }}
              targetAddress={MOONCAT_TRAITS_ARB}
              targetItems={[pad(moonCat.catId as `0x${string}`)]}
            />
          )}
        </div>
        <ul style={{ margin: '0 0 1em' }} className="two-col">
          <li>
            <strong>Rescue Order</strong> #{moonCat.rescueOrder}
          </li>
          {typeof moonCat.namedOrder != 'undefined' && (
            <li>
              <strong>Named Order</strong> #{moonCat.namedOrder}
            </li>
          )}
          <li>
            <strong>Identifier</strong> {moonCat.catId}
          </li>
          <li>
            <strong>Facing</strong> {ucFirst(moonCat.facing)}
          </li>
          <li>
            <strong>Expression</strong> {ucFirst(moonCat.expression)}
          </li>
          <li>
            <strong>Coat</strong> {coatDetail}
          </li>
          <li>
            <strong>Pose</strong> {ucFirst(moonCat.pose)}
          </li>
          <li>
            <strong>Rescue Year</strong> {moonCat.rescueYear}
          </li>
          {moonCat.namedYear && (
            <li>
              <strong>Named Year</strong> {moonCat.namedYear}
            </li>
          )}
        </ul>
        {genesisDetails}
        <p>
          {nameDetails} Once a MoonCat is named, it cannot be changed, and will be part of that MoonCat&rsquo;s traits
          forever after.
        </p>
        {ownerDetails}
        <LitterView details={details} />
        {tidbitsView}
      </section>
    </div>
  )
}
export default MoonCatTabInfo
