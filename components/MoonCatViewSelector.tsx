import { AppVisitorContext } from 'lib/AppVisitorProvider'
import { isValidViewPreference, MoonCatViewPreference } from 'lib/types'
import { AppGlobalActionType, AppGlobalViewAction } from 'lib/util'
import React, { useContext } from 'react'

interface Props {
  isEventActive: boolean
}

const MoonCatViewSelector = ({ isEventActive }: Props) => {
  const {
    state: { viewPreference },
    dispatch,
  } = useContext(AppVisitorContext)

  // If this instance of the MoonCatGrid is not showing special-event views, fall back to Accessorized view
  let parsedViewPreference: MoonCatViewPreference =
    viewPreference == 'event' && !isEventActive ? 'accessorized' : viewPreference

  function handleOnChange(e: React.ChangeEvent<HTMLSelectElement>) {
    if (dispatch && isValidViewPreference(e.target.value))
      dispatch({
        type: AppGlobalActionType.SET_VIEW_PREFERENCE,
        payload: e.target.value as MoonCatViewPreference,
      } as AppGlobalViewAction)
  }
  return (
    <div>
      <label style={{ fontSize: '0.8rem' }}>
        View Style:
        <select value={parsedViewPreference as string} onChange={handleOnChange} style={{ marginLeft: '0.5rem' }}>
          {isEventActive && <option value="event">Current Event</option>}
          <option value="accessorized">Accessorized</option>
          <option value="mooncat">MoonCat</option>
          <option value="face">MoonCat Face</option>
        </select>
      </label>
    </div>
  )
}
export default MoonCatViewSelector
