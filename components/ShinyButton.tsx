const ShinyButton = ({
  children,
  onClick,
  disabled,
}: {
  children: React.ReactNode
  onClick: (e: React.MouseEvent<HTMLButtonElement>) => void
  disabled?: boolean
}) => {
  return (
    <span className={`btn-shiny ${disabled ? 'disabled' : ''}`}>
      <button className="shiny" onClick={onClick} disabled={disabled}>
        {children}
      </button>
    </span>
  )
}

export default ShinyButton
