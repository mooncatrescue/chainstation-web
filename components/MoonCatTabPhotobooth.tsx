import React, { useEffect, useState } from 'react'
import { TabProps } from './MoonCatDetail'
import { ACCESSORIES_ADDRESS, API_SERVER_ROOT, IPFS_GATEWAY, switchToChain } from 'lib/util'
import { SelectField } from './FormFields'
import AccessoryPicker from './AccessoryPicker'
import useMoonCatAccessories, { OwnedAccessory } from 'lib/useMoonCatAccessories'
import LoadingIndicator from './LoadingIndicator'
import { waitForTransactionReceipt, writeContract } from 'wagmi/actions'
import { Address, BaseError, parseAbi } from 'viem'
import useTxStatus from 'lib/useTxStatus'
import useSignedIn from 'lib/useSignedIn'
import { customEvent } from 'lib/analytics'
import MoonCatSprite from './MoonCatSprite'
import { config } from 'lib/wagmi-config'

interface SizeChoice {
  padding?: number
  label: string
}
interface SizeScaleChoice extends SizeChoice {
  scale: number
}
interface SizeBoundsChoice extends SizeChoice {
  width: number
  height: number
}
function isScaleChoice(choice: SizeChoice | SizeScaleChoice | SizeBoundsChoice): choice is SizeScaleChoice {
  return typeof (choice as SizeScaleChoice).scale != 'undefined'
}
function isBoundsChoice(choice: SizeChoice | SizeScaleChoice | SizeBoundsChoice): choice is SizeBoundsChoice {
  return (
    typeof (choice as SizeBoundsChoice).width != 'undefined' ||
    typeof (choice as SizeBoundsChoice).height != 'undefined'
  )
}

const ACCESSORIES = {
  address: ACCESSORIES_ADDRESS as Address,
  abi: parseAbi([
    'struct AccessoryBatchData { uint256 rescueOrder; uint232 ownedIndexOrAccessoryId; uint8 paletteIndex; uint16 zIndex; }',
    'function alterAccessories(AccessoryBatchData[] calldata alterations) external',
    'function alterAccessory(uint256 rescueOrder, uint256 ownedAccessoryIndex, uint8 paletteIndex, uint16 zIndex) external',
  ]),
  chainId: 1,
} as const

const sizeChoices: Record<string, SizeChoice | SizeScaleChoice | SizeBoundsChoice> = {
  default: { label: 'Default' },
  tiny: { scale: 1, padding: 0, label: 'Tiny' },
  small: { scale: 2, padding: 0, label: 'Small' },
  medium: { scale: 5, padding: 0, label: 'Medium' },
  large: { scale: 8, padding: 0, label: 'Large' },
  twitter: { width: 400, height: 400, label: 'Twitter' },
  discord: { width: 128, height: 128, label: 'Discord' },
  reddit: { width: 256, height: 256, label: 'Reddit' },
  youtube: { width: 800, height: 800, label: 'YouTube' },
}
let sizeOptions: Record<string, string> = {}
Object.keys(sizeChoices).forEach((s) => {
  sizeOptions[s] = sizeChoices[s].label
})

async function getImageBounds(url: string): Promise<{ height: number; width: number }> {
  let rs = await fetch(url)
  if (!rs.ok) {
    return {
      height: 100,
      width: 100,
    }
  }
  if (!rs.headers.has('X-Image-Height')) {
    return {
      height: 100,
      width: 100,
    }
  }
  return {
    height: parseInt(rs.headers.get('X-Image-Height') as string),
    width: parseInt(rs.headers.get('X-Image-Width') as string),
  }
}

const MoonCatTabPhotobooth = ({ moonCat, details }: TabProps) => {
  const { status, ownedAccessories } = useMoonCatAccessories(moonCat.rescueOrder)
  const [sizeChoice, setSizeChoice] = useState('default')
  const [moonCatChoice, setMoonCatChoice] = useState('full')
  const { connectedAddress, isSignedIn } = useSignedIn()
  const wardrobeTx = useTxStatus()

  const [chosenAccessories, setChosenAccessories] = useState<OwnedAccessory[]>([])
  useEffect(() => {
    if (status == 'pending' || ownedAccessories.length == 0) return
    setChosenAccessories(ownedAccessories)
  }, [status, ownedAccessories])
  const [imageURI, setImageURI] = useState<string>(API_SERVER_ROOT + '/image/' + moonCat.rescueOrder)

  // If the user picks a size option that is a bounds-style config, that requires async calls, so this
  // needs to be an effect, which then re-triggers whenever one of the form fields change their value.
  useEffect(() => {
    let ignore = false
    let params = ['costumes=true']
    if (moonCatChoice == 'head') params.push('headOnly')
    const sizeChoiceData = sizeChoices[sizeChoice]

    const urlBase = API_SERVER_ROOT + '/image/' + moonCat.rescueOrder

    // Add in chosen Accessories
    const chosenForegroundAccessories = chosenAccessories.filter((a) => a.zIndex > 0 && !a.isBackground).reverse()
    const chosenBackgroundAccessories = chosenAccessories.filter((a) => a.zIndex > 0 && a.isBackground)
    params.push(
      'acc=' +
        chosenBackgroundAccessories
          .concat(chosenForegroundAccessories)
          .map((a) => {
            if (a.availablePalettes > 1) return `${a.accessoryId}:${a.paletteIndex}`
            return a.accessoryId
          })
          .join(',')
    )

    if (isScaleChoice(sizeChoiceData)) {
      // Scale value is specified; just use it
      params.push('scale=' + sizeChoiceData.scale)
      if (typeof sizeChoiceData.padding != 'undefined') params.push('padding=' + sizeChoiceData.padding)
      setImageURI(urlBase + '?' + params.join('&'))
      return
    } else if (isBoundsChoice(sizeChoiceData)) {
      // Calculate scale based on target height/width
      getImageBounds(urlBase + '?scale=1&padding=0&' + params.join('&')).then((baseSize) => {
        if (ignore) return
        let widthFactor = sizeChoiceData.width / baseSize.width
        let heightFactor = sizeChoiceData.height / baseSize.height
        let scale, padding
        if (widthFactor < heightFactor) {
          scale = Math.floor(widthFactor)
          padding = Math.floor((sizeChoiceData.height - baseSize.height * scale) / 2)
        } else {
          scale = Math.floor(heightFactor)
          padding = Math.floor((sizeChoiceData.width - baseSize.width * scale) / 2)
        }
        if (scale < 1) scale = 1
        if (padding < 1) padding = 0
        params.push('scale=' + scale)
        params.push('padding=' + padding)
        setImageURI(urlBase + '?' + params.join('&'))
        return
      })
    } else {
      // Just using defaults
      if (typeof sizeChoiceData.padding != 'undefined') params.push('padding=' + sizeChoiceData.padding)
      setImageURI(urlBase + '?' + params.join('&'))
      return
    }

    return () => {
      ignore = true
    }
  }, [moonCat.rescueOrder, sizeChoice, moonCatChoice, chosenAccessories])

  // As the owner of the MoonCat, update the on-chain worn Accessories for this MoonCat
  async function applyWardrobe() {
    wardrobeTx.setStatus('building')
    const alterations: {
      rescueOrder: bigint
      ownedIndexOrAccessoryId: bigint
      paletteIndex: number
      zIndex: number
    }[] = []
    for (let i = 0; i < ownedAccessories.length; i++) {
      const accId = ownedAccessories[i].accessoryId
      const updatedAcc = chosenAccessories.find((a) => a.accessoryId == accId)
      if (updatedAcc) {
        if (
          ownedAccessories[i].zIndex != updatedAcc.zIndex ||
          ownedAccessories[i].paletteIndex != updatedAcc.paletteIndex
        ) {
          alterations.push({
            rescueOrder: BigInt(moonCat.rescueOrder),
            ownedIndexOrAccessoryId: BigInt(updatedAcc.ownedIndex),
            paletteIndex: updatedAcc.paletteIndex,
            zIndex: updatedAcc.zIndex,
          })
        }
      }
    }
    if (alterations.length == 0) {
      wardrobeTx.setStatus('error', 'No wardrobe changes made')
      return
    }

    if (!(await switchToChain(1))) {
      wardrobeTx.setStatus(
        'error',
        'Failed to switch to the correct network. Please switch to Ethereum mainnet manually.'
      )
      return
    }
    let txPromise
    if (alterations.length == 1) {
      // Use `alterAccessory` singular-change function
      const a = alterations[0]
      txPromise = writeContract(config, {
        ...ACCESSORIES,
        functionName: 'alterAccessory',
        args: [a.rescueOrder, a.ownedIndexOrAccessoryId, a.paletteIndex, a.zIndex],
      })
    } else {
      // Use `alterAccessories` batch-change function
      txPromise = writeContract(config, {
        ...ACCESSORIES,
        functionName: 'alterAccessories',
        args: [alterations],
      })
    }
    wardrobeTx.setStatus('authorizing')
    try {
      let hash = await txPromise
      wardrobeTx.setStatus('done', <p>Wardrobe change submitted; waiting for blockchain confirmation...</p>)
      customEvent('mooncat_wardrobe_update', { mooncat: moonCat.rescueOrder })
      await waitForTransactionReceipt(config, { hash })
      wardrobeTx.setStatus('done', <p>Confirmed on the blockchain!</p>)
    } catch (err) {
      if (err instanceof BaseError) {
        console.error('Transaction failure', {
          name: err.name,
          shortMessage: err.shortMessage,
          cause: err.cause,
        })
        wardrobeTx.setStatus('error', err.shortMessage)
      } else {
        console.error('Transaction unknown error', err)
        wardrobeTx.setStatus('error', 'Transaction error')
      }
    }
  }

  return (
    <>
      <section>
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          <div id="form-controls" style={{ flex: '1 1 40%', padding: '2rem 0.5rem 0 0' }}>
            <div className="form-grid">
              <SelectField
                meta={{
                  name: 'size',
                  type: 'select',
                  label: 'Size',
                  options: sizeOptions,
                }}
                currentValue={sizeChoice}
                onChange={(e) => setSizeChoice(e.target.value)}
              />
              <SelectField
                meta={{
                  name: 'mooncat',
                  type: 'select',
                  label: 'MoonCat appearance',
                  options: {
                    full: 'Full body',
                    head: 'Head only',
                  },
                }}
                currentValue={moonCatChoice}
                onChange={(e) => setMoonCatChoice(e.target.value)}
              />
            </div>
            <div id="accessories-configuration">
              <h2>Accessories</h2>
              {status != 'success' && <LoadingIndicator />}
              {status == 'success' && (
                <>
                  <AccessoryPicker
                    accessories={chosenAccessories}
                    onChange={setChosenAccessories}
                    style={{ marginBottom: '1em' }}
                    className="text-scrim"
                  />
                  {isSignedIn && connectedAddress === details.owner && (
                    <>
                      <p>
                        <button onClick={applyWardrobe}>Change Wardrobe</button>
                      </p>
                      {wardrobeTx.viewMessage}
                    </>
                  )}
                </>
              )}
            </div>
          </div>
          <div style={{ flex: '1 1 40%', padding: '0 0.5rem', fontSize: '0.8rem' }}>
            <div style={{ display: 'flex', justifyContent: 'center', marginBottom: '2em' }}>
              <picture>
                <img
                  alt={'MoonCat ' + moonCat.rescueOrder}
                  src={imageURI}
                  style={{ maxWidth: 640, border: 'solid 3px #888', boxShadow: '8px 8px 0 #888' }}
                />
              </picture>
            </div>
            <p>
              Right-click and select <strong>&ldquo;Save image as...&rdquo;</strong> to download.
            </p>
            <p>
              To reference this image directly, use <code>{imageURI}</code>
            </p>
          </div>
        </div>
      </section>
      <section className="card">
        <section style={{ display: 'flex', flexWrap: 'wrap' }}>
          <div style={{ flex: '3 3 auto', minWidth: '20em' }}>
            <h2 style={{ marginTop: 0 }}>Extras</h2>
            <ul style={{ margin: 0 }}>
              <li>
                <a
                  href={`${IPFS_GATEWAY}/ipfs/bafybeib5iedrzr7unbp4zq6rkrab3caik7nw7rfzlcfvu4xqs6bfk7dgje/${moonCat.rescueOrder}.png`}
                >
                  Sprite Sheet:{' '}
                  <MoonCatSprite style={{ verticalAlign: '-8px', marginTop: -20 }} rescueOrder={moonCat.rescueOrder} />
                </a>
              </li>
              <li>
                <a
                  href={`${IPFS_GATEWAY}/ipfs/bafybeibsfarvkx7cowc2uta55mw76aczjqkund6htjiq5pzvg4ljr7yeqi/${moonCat.rescueOrder}.png`}
                >
                  Hex ID Explainer:{' '}
                  <picture>
                    <img
                      alt=""
                      src={`${IPFS_GATEWAY}/ipfs/bafybeibsfarvkx7cowc2uta55mw76aczjqkund6htjiq5pzvg4ljr7yeqi/${moonCat.rescueOrder}.png`}
                      style={{ height: 50, verticalAlign: 'middle', margin: '10px 0' }}
                    />
                  </picture>
                </a>
              </li>
            </ul>
          </div>
          <div style={{ flex: '1 1 310px', textAlign: 'center' }}>
            <iframe
              style={{ borderWidth: 0, verticalAlign: 'middle', width: 300, height: 200 }}
              src={`/mooncats/${moonCat.rescueOrder}/vox`}
            />
            <p>
              <a
                href={`${IPFS_GATEWAY}/ipfs/bafybeifxqtzf635xy3q3lrp2cygrz2vatregvtnfe2dsl4jskjyuneexzu/${moonCat.rescueOrder}.vox`}
              >
                3D Voxel Model
              </a>{' '}
              (<a href={`/mooncats/${moonCat.rescueOrder}/vox`}>viewer</a>)
            </p>
          </div>
        </section>
      </section>
    </>
  )
}

export default MoonCatTabPhotobooth
