'use client'
import { AccessoryFilterSettings, AccessorySummary, SelectFieldMeta, TextFieldMeta } from 'lib/types'
import { API2_SERVER_ROOT } from 'lib/util'
import { useState } from 'react'
import Filter from './Filter'
import { useInfiniteQuery } from '@tanstack/react-query'
import LoadingIndicator from './LoadingIndicator'
import { parseUnits } from 'viem'
import AccessoryThumb from './AccessoryThumb'
import useDebounce from 'lib/useDebounce'

const DEFAULT_PER_PAGE = 50

const filterMeta: readonly (SelectFieldMeta | TextFieldMeta)[] = [
  {
    name: 'for_sale',
    type: 'select',
    label: 'For Sale?',
    defaultLabel: 'Any',
    options: {
      no: 'No',
      yes: 'Yes',
    },
  },
  {
    name: 'verified',
    type: 'select',
    label: 'Is Verified?',
    defaultLabel: 'Any',
    options: {
      no: 'No',
      yes: 'Yes',
    },
  },
  {
    name: 'available',
    type: 'select',
    label: 'Has Supply Left?',
    defaultLabel: 'Any',
    options: {
      no: 'No',
      yes: 'Yes',
    },
  },
  {
    name: 'pose',
    type: 'select',
    label: 'Pose',
    defaultLabel: 'Any',
    options: {
      standing: 'Standing',
      sleeping: 'Sleeping',
      pouncing: 'Pouncing',
      stalking: 'Stalking',
    },
  },
  {
    name: 'price_min',
    type: 'text',
    label: 'Minimum Price',
  },
  {
    name: 'price_max',
    type: 'text',
    label: 'Maximum Price',
  },
  {
    name: 'eligible_list',
    type: 'select',
    label: 'Only Eligible MoonCats?',
    defaultLabel: 'Any',
    options: {
      no: 'No',
      yes: 'Yes',
    },
  },
]

interface Props {
  perPage?: number
}

const AccessoryGrid = ({ perPage = DEFAULT_PER_PAGE }: Props) => {
  const [filters, setFilters] = useState<AccessoryFilterSettings>({
    for_sale: 'yes',
    verified: 'yes',
  })
  const debouncedPriceMin = useDebounce(filters.price_min)
  const debouncedPriceMax = useDebounce(filters.price_max)

  let params = new URLSearchParams(filters as Record<string, string>)
  if (debouncedPriceMin != null && debouncedPriceMin != '') {
    params.set('price_min', parseUnits(debouncedPriceMin, 18).toString())
  }
  if (debouncedPriceMax != null && debouncedPriceMax != '') {
    params.set('price_max', parseUnits(debouncedPriceMax, 18).toString())
  }
  params.set('limit', String(perPage))
  params.set('sort', 'oldest')

  const {
    data: items,
    isFetchingNextPage,
    fetchNextPage,
    hasNextPage,
  } = useInfiniteQuery({
    queryKey: ['accessory-search', params.toString()],
    queryFn: ({ pageParam }: { pageParam: string | null }) => {
      if (pageParam != null) params.set('start_after', pageParam)
      return fetch(`${API2_SERVER_ROOT}/accessories?${params.toString()}`).then((rs) => {
        if (!rs.ok) throw new Error('Failed to fetch accessory data')
        return rs.json() as Promise<AccessorySummary[]>
      })
    },
    initialPageParam: null,
    getNextPageParam: (lastPage) => (lastPage.length == 0 ? null : lastPage[lastPage.length - 1].id),
  })

  // Event handler for updates from Filter component when a user picks a new filter value
  function handleFilterUpdate(prop: keyof AccessoryFilterSettings, newValue: any) {
    setFilters((curFilters) => {
      let newFilters = Object.assign({}, curFilters)
      if (newValue == '') {
        if (!curFilters[prop]) {
          // Already blank
          return curFilters
        }
        delete newFilters[prop]
      } else {
        if (curFilters[prop] == newValue) {
          // Already set to that value
          return curFilters
        }
        newFilters[prop] = newValue
      }

      return newFilters
    })
  }

  return (
    <div className="accessory-grid">
      <div
        style={{
          display: 'flex',
          alignItems: 'baseline',
          padding: '1rem 0',
        }}
        className="text-scrim"
      >
        <Filter currentFilters={filters} filterMeta={filterMeta} label="Accessories" onChange={handleFilterUpdate} />
        <div style={{ flexGrow: 10 }} />
      </div>
      {(!items || !items.pages) && <LoadingIndicator />}
      <div className="item-grid" style={{ margin: '0 0 2rem' }}>
        {items && items.pages.flat().map((acc) => <AccessoryThumb key={acc.id} accessory={acc} />)}
      </div>
      {hasNextPage && (
        <button onClick={() => fetchNextPage()} disabled={isFetchingNextPage}>
          Explore Further
        </button>
      )}
    </div>
  )
}
export default AccessoryGrid
