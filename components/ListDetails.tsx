'use client'
import parseDescription from 'lib/parseDescription'
import { UserListTokenMeta, parseTokenListMoonCats, tokenDisplayLabel } from 'lib/tokens'
import { TokenListMeta } from 'lib/types'
import useSignedIn from 'lib/useSignedIn'
import { useContext, useRef, useState } from 'react'
import { Address, pad, stringToHex } from 'viem'
import EthereumAddress from './EthereumAddress'
import Pagination from './Pagination'
import StarButton from './StarButton'
import getMoonCatData from 'lib/getMoonCatData'
import { MOONCAT_TRAITS_ARB } from 'lib/util'
import { AppVisitorContext } from 'lib/AppVisitorProvider'
import useTabbieHook from 'lib/useTabbieHook'

interface Props {
  ownerAddress: Address
  listMeta: TokenListMeta
  tokens: UserListTokenMeta[]
}

const PER_PAGE = 50
const ListDetails = ({ ownerAddress, listMeta, tokens }: Props) => {
  const [page, setPage] = useState<number>(0)
  const { connectedAddress } = useSignedIn()

  const pageTokens = tokens.slice(page * PER_PAGE, (page + 1) * PER_PAGE)
  const parsedTokens = parseTokenListMoonCats(pageTokens)

  const pageMoonCats = parsedTokens
    ? getMoonCatData(
        parsedTokens.filter((t) => t.collection.address == MOONCAT_TRAITS_ARB).map((t) => Number(t.name!.substring(1)))
      )
    : undefined
  const {
    state: { awokenMoonCats },
  } = useContext(AppVisitorContext)
  const gridRef = useRef<HTMLDivElement>(null)

  useTabbieHook(pageMoonCats, awokenMoonCats, (mc) => {
    const cell = gridRef.current?.querySelector(`#col${MOONCAT_TRAITS_ARB}-${mc.rescueOrder} .thumb-img`)
    if (!cell) {
      console.warn('Failed to find element for MoonCat', mc)
      return { x: window.innerWidth / 2, y: window.innerHeight / 2 }
    }
    const bounds = cell.getBoundingClientRect()
    return {
      x: bounds.x + window.scrollX + bounds.width / 2,
      y: bounds.y + window.scrollY + bounds.height - 20,
    }
  })

  // Event handler for updates from Pagination component when user navigates to a new page
  function handlePageUpdate(newPage: number) {
    if (page == newPage) return
    setPage(newPage)
  }

  return (
    <div id="content-container" itemScope itemType="https://schema.org/ItemPage">
      <meta itemProp="isFamilyFriendly" content="true" />
      <div className="text-container">
        <h1 className="hero">{listMeta.title}</h1>
        <section className="card">
          <div className="btn-bar">
            <StarButton
              targetAddress={ownerAddress}
              targetItem={pad(stringToHex('Token List: ' + listMeta.title), { dir: 'right', size: 32 })}
              itemLabel="list"
              message={'I like this list!'}
              readOnly={connectedAddress == ownerAddress}
            />
          </div>
          <p>
            A listing of {listMeta.tokenCount} tokens, curated by <EthereumAddress address={ownerAddress} />:
          </p>
          {listMeta.description && parseDescription(listMeta.description)}
        </section>
      </div>
      <div className="item-grid" ref={gridRef}>
        {parsedTokens.map((t) => {
          const key = t.collection.address + '::' + t.id
          const [collectionLabel, tokenName] = tokenDisplayLabel(t)
          const rescueOrder =
            t.collection.address == MOONCAT_TRAITS_ARB && typeof t.name === 'string' ? Number(t.name!.substring(1)) : -1
          if (t.link) {
            return (
              <a
                key={key}
                href={t.link}
                id={`col${t.collection.address}-${tokenName}`.replaceAll('#', '')}
                className="item-thumb"
                style={{ position: 'relative' }}
              >
                <div
                  className="thumb-img"
                  style={{ backgroundImage: !awokenMoonCats.has(rescueOrder) ? `url(${t.imageSrc})` : undefined }}
                />
                <p>{collectionLabel}</p>
                <p>{tokenName}</p>
              </a>
            )
          } else {
            return (
              <div
                key={key}
                id={`col${t.collection.address}-${tokenName}`.replaceAll('#', '')}
                className="item-thumb"
                style={{ position: 'relative' }}
              >
                <div
                  className="thumb-img"
                  style={{ backgroundImage: !awokenMoonCats.has(rescueOrder) ? `url(${t.imageSrc})` : undefined }}
                />
                <p>{collectionLabel}</p>
                <p>{tokenName}</p>
              </div>
            )
          }
        })}
      </div>
      <Pagination
        currentPage={page}
        maxPage={Math.ceil(tokens.length / PER_PAGE) - 1}
        setCurrentPage={handlePageUpdate}
      />
    </div>
  )
}
export default ListDetails
