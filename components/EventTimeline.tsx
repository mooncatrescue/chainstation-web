import {
  BlockchainEvent,
  EventSubject,
  generateTimelineNodes,
  getFunctionName,
  getMessageForEvent,
} from 'lib/blockchainEvents'
import { formatAsDate } from 'lib/util'
import EthereumTransaction from './EthereumTransaction'
import EthereumAddress from './EthereumAddress'

const EventTimeline = ({
  events,
  subject,
  loadNext,
}: {
  events: BlockchainEvent[]
  subject: EventSubject
  loadNext?: () => void
}) => {
  const timelineNodes = generateTimelineNodes(events)

  let footerView: React.ReactNode
  if (loadNext) {
    footerView = (
      <>
        <h2 style={{ paddingBottom: '1rem' }}>The Distant Past...</h2>
        <div className="timeline-event" style={{ paddingTop: 0 }}>
          <p>
            <button onClick={loadNext}>Search further back</button>
          </p>
        </div>
        <div className="timeline-gap" />
      </>
    )
  } else {
    footerView = <h2 style={{ paddingBottom: '1rem' }}>The Beginning of It All...</h2>
  }

  return (
    <section className="timeline-container">
      <h2 style={{ paddingTop: '1rem' }}>Present Time...</h2>
      {timelineNodes.map((n) => {
        if (n.type == 'label') {
          return <h2 key={`label-${n.data}`}>{n.data}</h2>
        }
        const event = n.data
        const message = getMessageForEvent(event, subject)
        const eventDate = new Date(event.timestamp * 1000)
        const shortDate = formatAsDate(eventDate)
        return (
          <div key={event.blockNumber + '::' + event.logIndex} className="timeline-event">
            <div>
              <div className={message.dotClass} />
              <h3>
                {shortDate} &mdash; {message.title}
              </h3>
              {message.description}
              <p className="blockchain-details">
                <strong>Blockchain Details:</strong>
                <br />
                In block {event.blockNumber} ({eventDate.toISOString()}), as part of transaction{' '}
                <EthereumTransaction hash={event.tx.hash} />. Address <EthereumAddress address={event.tx.from} /> called
                function <code>{getFunctionName(event.tx.data)}</code> on contract{' '}
                <EthereumAddress address={event.tx.to} />, causing <code>{event.eventName}</code> event to be fired.
              </p>
            </div>
            {message.image && (
              <div className="thumbnail">
                <picture>
                  <img alt="" src={message.image} />
                </picture>
              </div>
            )}
          </div>
        )
      })}
      {footerView}
    </section>
  )
}
export default EventTimeline
