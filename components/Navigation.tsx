'use client'
import React, { useContext, useEffect, useState } from 'react'
import Link from 'next/link'
import { usePathname } from 'next/navigation'
import { useAppKit } from '@reown/appkit/react'
import EthereumAddress from 'components/EthereumAddress'
import IconButton from 'components/IconButton'
import Icon from 'components/Icon'
import NotificationPill from 'components/NotificationPill'
import { ActionQueueContext } from 'lib/ActionsQueueProvider'
import { CartContext } from 'lib/ShoppingCartProvider'
import { actionIsComplete } from 'lib/onchainActions'
import { ZWS } from 'lib/util'
import useIsMounted from 'lib/useIsMounted'
import useSignedIn from 'lib/useSignedIn'

const SignInButton = () => {
  const { isSignedIn, doSignIn, doLogOut, status, verificationStatus } = useSignedIn()
  const btnStyle = { fontSize: '0.8rem', marginLeft: '1rem' }
  if (status == 'pending' || verificationStatus == 'pending') {
    return <IconButton style={btnStyle} name="loading" title="Loading..." onClick={() => {}} />
  }
  if (isSignedIn) {
    return <IconButton style={btnStyle} name="lock" title="Click to log out" onClick={(e) => doLogOut()} />
  }
  return <IconButton style={btnStyle} name="unlocked" title="Click to log in" onClick={doSignIn} />
}

const Navigation = () => {
  const [menuOpen, setMenuOpen] = useState<boolean>(false)
  const pathname = usePathname()
  const isMounted = useIsMounted()
  const { isConnected, connectedAddress } = useSignedIn()
  const { open } = useAppKit()

  const { cart } = useContext(CartContext)
  const { actions } = useContext(ActionQueueContext)
  const incompleteActions = actions.filter((a) => a.fromAddress == connectedAddress && !actionIsComplete(a))
  const notificationCount = cart.length + incompleteActions.length

  let connectView: React.ReactNode
  if (isMounted) {
    if (isConnected) {
      connectView = (
        <>
          <EthereumAddress address={connectedAddress} style={{ background: 'transparent' }} />
          <SignInButton />
        </>
      )
    } else {
      connectView = <button onClick={(e) => open()}>Connect</button>
    }
  }

  // When user navigates to a new page, close the menu display
  useEffect(() => {
    setMenuOpen(false)
  }, [pathname])

  const appTitle = 'MoonCat' + ZWS + 'Rescue'
  const menuClass = menuOpen ? 'menu-open' : 'menu-closed'
  return (
    <nav id="sidebar">
      <div id="mobile-menu-button" onClick={() => setMenuOpen(!menuOpen)} style={{ cursor: 'pointer' }}>
        <Icon name="menu" />
      </div>
      <div id="logo">
        <Link href="/">{appTitle}</Link>
      </div>
      <div id="menu" className={menuClass} style={{ flex: '1 10 auto', overflowY: 'auto' }}>
        <h2>Object Spectrometer</h2>
        <ul>
          <li>
            <Link href="/mooncats">MoonCats</Link>
          </li>
          <li>
            <Link href="/accessories">Accessories</Link>
          </li>
          <li>
            <Link href="/lootprints">lootprints</Link>
          </li>
          <li>
            <Link href="/moments">Moments</Link>
          </li>
          <li>
            <Link href="/user-lists">User-created Lists</Link>
          </li>
        </ul>
        <h2>Tools</h2>
        <ul>
          <li>
            <Link href="/owners">Owner Profiles</Link>
          </li>
          <li>
            <Link href="/acclimator">Acclimator</Link>
          </li>
          <li>
            <Link href="/mcns">MoonCatNameService</Link>
          </li>
          <li>
            <Link href="/gm">Send a GM</Link>
          </li>
        </ul>
        <h2>Info</h2>
        <ul>
          <li>
            <Link href="/contract-docs">Contract Documentation</Link>
          </li>
          <li>
            <Link href="/team/contribute">Contribute</Link>
          </li>
          <li>
            <Link href="/profile">My Profile</Link>
            <NotificationPill count={notificationCount} />
          </li>
        </ul>
      </div>
      <div id="wallet-connection">{connectView}</div>
    </nav>
  )
}
export default Navigation
