import { useEffect, useRef, useState } from 'react'

const ScrollStrip = ({
  onScrollEnd,
  style,
  scrollMargin = 60,
  children,
}: {
  onScrollEnd?: () => void
  style?: React.CSSProperties
  scrollMargin?: number
  children: React.ReactNode
}) => {
  const stripRef = useRef<HTMLDivElement | null>(null)
  const [showScrollLeft, setShowScrollLeft] = useState<boolean>(false)
  const [showScrollRight, setShowScrollRight] = useState<boolean>(true)

  const calculateScrollAmount = () => {
    if (stripRef.current == null) return 0
    const bounds = stripRef.current.getBoundingClientRect()
    const cells = Math.floor(bounds.width / 150)
    return cells > 2 ? (cells - 2) * 150 : bounds.width * 0.8
  }

  return (
    <div style={{ position: 'relative' }}>
      <div
        style={{ width: '100%', overflowX: 'scroll', overflowY: 'hidden' }}
        ref={stripRef}
        onScroll={(e) => {
          const t = e.target as HTMLDivElement

          // Show/hide left scroll indicator?
          if (t.scrollLeft < 100 && showScrollLeft) {
            setShowScrollLeft(false)
          } else if (t.scrollLeft >= 100 && !showScrollLeft) {
            setShowScrollLeft(true)
          }

          // How much space is left to scroll?
          const delta = t.scrollWidth - t.clientWidth - t.scrollLeft
          if (delta < 450 && typeof onScrollEnd == 'function') {
            // Nearing the end of the strip!
            onScrollEnd()
          }

          // Show/hide right scroll indicator?
          if (delta < 100 && showScrollRight) {
            setShowScrollRight(false)
          } else if (delta >= 100 && !showScrollRight) {
            setShowScrollRight(true)
          }
        }}
      >
        <div className="item-strip" style={{ margin: '0 0 10px', ...style }}>
          {children}
        </div>
      </div>
      {showScrollRight && (
        <div
          className="scroll-indicator"
          style={{ right: 0 }}
          title="Scroll Right"
          onClick={() => {
            if (stripRef.current == null) return
            const scrollAmount = calculateScrollAmount()
            stripRef.current.scroll({
              left: stripRef.current.scrollLeft + scrollAmount,
              behavior: 'smooth',
            })
          }}
        >
          <p style={{ marginTop: scrollMargin }}>&gt;</p>
        </div>
      )}
      {showScrollLeft && (
        <div
          className="scroll-indicator"
          style={{ left: 0, boxShadow: '20px 0 20px #111' }}
          title="Scroll Left"
          onClick={() => {
            if (stripRef.current == null) return
            const scrollAmount = calculateScrollAmount()
            stripRef.current.scroll({
              left: stripRef.current.scrollLeft - scrollAmount,
              behavior: 'smooth',
            })
          }}
        >
          <p style={{ marginTop: scrollMargin }}>&lt;</p>
        </div>
      )}
    </div>
  )
}
export default ScrollStrip
