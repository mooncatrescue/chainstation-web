'use client'
import { useQuery } from '@tanstack/react-query'
import { BlockchainEvent } from 'lib/blockchainEvents'
import { API2_SERVER_ROOT, FIVE_MINUTES } from 'lib/util'
import Link from 'next/link'
import AccessorizedMoonCat from './AccessorizedMoonCat'
import AccessoryLink from './AccessoryLink'
import LoadingIndicator from './LoadingIndicator'
import WarningIndicator from './WarningIndicator'
import { useContext, useRef } from 'react'
import { AppVisitorContext } from 'lib/AppVisitorProvider'
import useTabbieHook from 'lib/useTabbieHook'
import getMoonCatData from 'lib/getMoonCatData'

const RecentAccessorySales = () => {
  const { data: sales, status } = useQuery({
    queryKey: ['recent-accessory-sales'],
    queryFn: () => {
      return fetch(`${API2_SERVER_ROOT}/events?event=AccessoryPurchased&limit=20`).then((rs) => {
        if (!rs.ok) throw new Error('Failed to fetch events')
        return rs.json() as Promise<BlockchainEvent[]>
      })
    },
    staleTime: FIVE_MINUTES,
  })

  const salesMoonCats = sales ? getMoonCatData(sales.map((e) => e.args.rescueOrder)) : undefined
  const {
    state: { awokenMoonCats },
  } = useContext(AppVisitorContext)
  const tableRef = useRef<HTMLTableElement>(null)

  useTabbieHook(salesMoonCats, awokenMoonCats, (mc) => {
    const td = tableRef.current?.querySelector(`#mooncat-${mc.rescueOrder}`)
    if (!td) {
      console.warn('Failed to find table element for MoonCat', mc)
      return { x: window.innerWidth / 2, y: window.innerHeight / 2 }
    }
    const bounds = td.getBoundingClientRect()
    return {
      x: bounds.x + window.scrollX + 20,
      y: bounds.y + window.scrollY + bounds.height,
    }
  })

  if (status == 'pending') {
    return (
      <section className="card">
        <h2>Recent Sales</h2>
        <LoadingIndicator message="Fetching recent sales..." />
      </section>
    )
  }
  if (status == 'error') {
    return (
      <section className="card">
        <h2>Recent Sales</h2>
        <WarningIndicator message="Black hole encountered! No recent sales data could be fetched." />
      </section>
    )
  }

  return (
    <section className="card">
      <h2>Recent Sales</h2>
      <div style={{ columnWidth: '25em', marginBottom: '1.5em' }}>
        <table
          cellSpacing="0"
          cellPadding="0"
          className="zebra"
          style={{ margin: '0 auto', fontSize: '0.8rem' }}
          ref={tableRef}
        >
          <tbody>
            {sales.map((e) => {
              return (
                <tr key={e.blockNumber + '-' + e.logIndex}>
                  <td
                    style={{ textAlign: 'center', lineHeight: 1, height: '2.5em', width: '2.5em' }}
                    id={`mooncat-${e.args.rescueOrder}`}
                  >
                    {!awokenMoonCats.has(e.args.rescueOrder) && (
                      <AccessorizedMoonCat
                        className="inline"
                        moonCat={e.args.rescueOrder}
                        accessories={[BigInt(e.args.accessoryId)]}
                      />
                    )}
                  </td>
                  <td>
                    <AccessoryLink id={e.args.accessoryId} /> to{' '}
                    <Link href={`/mooncats/${e.args.rescueOrder}`}>{'MoonCat\xa0#' + e.args.rescueOrder}</Link>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </section>
  )
}
export default RecentAccessorySales
