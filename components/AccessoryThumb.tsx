import Icon from './Icon'
import { API_SERVER_ROOT } from '../lib/util'
import { AccessorySummary } from '../lib/types'
import Link from 'next/link'
import EthAmount from './EthAmount'
import { CSSProperties } from 'react'

interface ThumbProps {
  accessory: AccessorySummary
  className?: string
  thumbHandler?: (accessory: AccessorySummary) => React.ReactNode
  href?: string
  onClick?: (accessory: AccessorySummary, event: React.MouseEvent<HTMLElement, MouseEvent>) => any
}

const AccessoryThumb = ({ accessory, className, thumbHandler, href, onClick }: ThumbProps) => {
  let details: React.ReactNode
  if (thumbHandler) {
    details = thumbHandler(accessory)
  } else {
    let priceView: React.ReactNode = null
    if (accessory.availableSupply == 0) {
      priceView = <strong>Sold Out</strong>
    } else if (accessory.price == 0) {
      priceView = <strong>FREE!</strong>
    } else if (accessory.price > 0) {
      priceView = <EthAmount amount={accessory.price} />
    }

    const eligibleTag = accessory.isEligibleListActive ? (
      <Icon
        title="Eligible List active"
        className="icon-pill"
        style={{ position: 'absolute', top: '0.5rem', right: '0.5rem' }}
        name="filter"
      />
    ) : null

    details = (
      <>
        <p>#{accessory.id}</p>
        <p className="green-dk">{accessory.name}</p>
        {priceView}
        {eligibleTag}
      </>
    )
  }

  let classes = ['item-thumb']
  if (className) classes.push(className)

  const thumbStyle = { backgroundImage: `url(${API_SERVER_ROOT}/accessory-image/${accessory.id}?scale=3&padding=5)` }

  if (href) {
    // Use a standard link
    return (
      <Link href={href} className={classes.join(' ')}>
        <>
          <div className="thumb-img" style={thumbStyle} />
          {details}
        </>
      </Link>
    )
  }

  if (typeof onClick != 'function') {
    // Use default link
    return (
      <Link href={`/accessories/${accessory.id}`} className={classes.join(' ')}>
        <>
          <div className="thumb-img" style={thumbStyle} />
          {details}
        </>
      </Link>
    )
  }

  // Do a custom onClick function
  const style: CSSProperties = {
    cursor: classes.includes('disabled') ? '' : 'pointer',
  }
  return (
    <div className={classes.join(' ')} style={style} onClick={(e) => onClick(accessory, e)}>
      <div className="thumb-img" style={thumbStyle} />
      {details}
    </div>
  )
}
export default AccessoryThumb
