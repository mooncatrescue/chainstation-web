import React from 'react'

interface Props {
  name: string
  className?: string
  style?: React.CSSProperties
  title?: string
}

const Icon = ({ name, className, style, title }: Props) => {
  let classes = `icon icon-${name}`
  if (!!className) {
    classes += ' ' + className
  }
  if (title) {
    return (
      <span style={style} title={title}>
        <svg className={classes}>
          <use xlinkHref={`/img/symbol-defs.svg#icon-${name}`}></use>
        </svg>
      </span>
    )
  }
  return (
    <svg className={classes} style={style}>
      <use xlinkHref={`/img/symbol-defs.svg#icon-${name}`}></use>
    </svg>
  )
}
export default Icon
