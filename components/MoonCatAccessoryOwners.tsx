import { Listing } from 'lib/reservoirData'
import { useState } from 'react'
import AccessorizedMoonCat from './AccessorizedMoonCat'
import Icon from './Icon'
import Pagination from './Pagination'

const PER_PAGE = 50

interface MoonCatIdentifier {
  catId: `0x${string}`
  rescueOrder: number
}

interface PaginationState {
  currentPage: number
  pageTokens: MoonCatIdentifier[]
}

/**
 * Show a grid of MoonCats wearing a specific accessory
 */
const MoonCatAccessoryOwners = ({
  accessory,
  moonCatOwners,
  listings,
}: {
  accessory: number
  moonCatOwners: MoonCatIdentifier[]
  listings: Listing[]
}) => {
  const [paginationState, setPaginationState] = useState<PaginationState>({
    currentPage: 0,
    pageTokens: moonCatOwners ? moonCatOwners.slice(0, PER_PAGE) : [],
  })

  if (moonCatOwners.length == 1) return null

  // Event handler for updates from Pagination component when user navigates to a new page
  function handlePageUpdate(newPage: number) {
    setPaginationState((curVal) => {
      if (curVal.currentPage == newPage) {
        // Already set to that value
        return curVal
      }
      return {
        currentPage: newPage,
        pageTokens: moonCatOwners!.slice(newPage * PER_PAGE, (newPage + 1) * PER_PAGE),
      }
    })
  }

  let description: JSX.Element
  if (paginationState.pageTokens.length == 0) {
    description = <p>No MoonCats own this accessory yet</p>
  } else {
    description = (
      <p>
        These are the {moonCatOwners.length} MoonCats that already own this accessory. Each MoonCat can choose to wear
        it at any given time or not, but it is now part of these MoonCats&rsquo; wardobe of accessories into the future.
      </p>
    )
  }

  // Deduplicate the listings, keeping only the lowest-cost one, if there are multiple
  const listingMap =
    listings.length > 0
      ? listings.reduce((agg, listing) => {
          if (
            typeof agg[listing.moonCat] == 'undefined' ||
            listing.price.amount.decimal < agg[listing.moonCat].price.amount.decimal
          ) {
            agg[listing.moonCat] = listing
          }
          return agg
        }, {} as Record<number, Listing>)
      : {}

  return (
    <>
      <div className="text-container">
        <section className="card">
          <h2>MoonCat Owners</h2>
          {description}
        </section>
      </div>
      <div className="item-grid" style={{ margin: '1em 0' }}>
        {paginationState.pageTokens.map((moonCat) => {
          const listing = listingMap[moonCat.rescueOrder]
          return (
            <a key={moonCat.rescueOrder} className="item-thumb" href={'/mooncats/' + moonCat.rescueOrder}>
              {typeof listing !== 'undefined' && (
                <Icon
                  title={`Available to adopt for ${listing.price.amount.decimal} ${listing.price.currency.symbol}`}
                  className="icon-pill"
                  style={{ position: 'absolute', top: '0.5rem', right: '0.5rem' }}
                  name="rocket"
                />
              )}
              <AccessorizedMoonCat moonCat={moonCat.rescueOrder} accessories={[BigInt(accessory)]}>
                {(svg) => (
                  <div
                    className="thumb-img"
                    style={{ backgroundImage: `url("data:image/svg+xml,${encodeURIComponent(svg)}")` }}
                  />
                )}
              </AccessorizedMoonCat>
              <p>#{moonCat.rescueOrder}</p>
            </a>
          )
        })}
      </div>
      <Pagination
        currentPage={paginationState.currentPage}
        maxPage={Math.ceil(moonCatOwners.length / PER_PAGE) - 1}
        setCurrentPage={handlePageUpdate}
      />
    </>
  )
}
export default MoonCatAccessoryOwners
