'use client'
import { useInfiniteQuery } from '@tanstack/react-query'
import { AccessoryFilterSettings, AccessorySummary } from 'lib/types'
import { API2_SERVER_ROOT } from 'lib/util'
import AccessoryThumb from './AccessoryThumb'
import LoadingIndicator from './LoadingIndicator'
import ScrollStrip from './ScrollStrip'

const DEFAULT_PER_PAGE = 15
const AccessoriesStrip = ({
  filters,
  perPage = DEFAULT_PER_PAGE,
}: {
  filters: AccessoryFilterSettings
  perPage?: number
}) => {
  let params = new URLSearchParams(filters as Record<string, string>)
  params.set('limit', String(perPage))
  const {
    data: items,
    isFetchingNextPage,
    fetchNextPage,
    hasNextPage,
  } = useInfiniteQuery({
    queryKey: ['accessory-search', params.toString()],
    queryFn: ({ pageParam }: { pageParam: string | null }) => {
      if (pageParam != null) params.set('start_after', pageParam)
      return fetch(`${API2_SERVER_ROOT}/accessories?${params.toString()}`).then((rs) => {
        if (!rs.ok) throw new Error('Failed to fetch accessory data')
        return rs.json() as Promise<AccessorySummary[]>
      })
    },
    initialPageParam: null,
    getNextPageParam: (lastPage) => (lastPage.length == 0 ? null : lastPage[lastPage.length - 1].id),
  })

  if (!items || !items.pages) {
    return <LoadingIndicator />
  }
  const flatList = items.pages.flat()

  return (
    <ScrollStrip onScrollEnd={fetchNextPage}>
      {flatList.map((acc) => (
        <AccessoryThumb key={acc.id} accessory={acc} />
      ))}
      {isFetchingNextPage && (
        <div className="item-thumb">
          <LoadingIndicator style={{ fontSize: '3em', marginTop: 50 }} message="" />
        </div>
      )}
    </ScrollStrip>
  )
}
export default AccessoriesStrip
