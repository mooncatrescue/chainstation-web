'use client'

import { useQuery } from '@tanstack/react-query'
import getMoonCatData from 'lib/getMoonCatData'
import { ACCLIMATOR_ADDRESS, API_SERVER_ROOT, FIVE_MINUTES } from 'lib/util'
import { parseAbi } from 'viem'
import { useBlockNumber, usePublicClient } from 'wagmi'
import LoadingIndicator from './LoadingIndicator'
import { useContext, useRef } from 'react'
import useTabbieHook from 'lib/useTabbieHook'
import { AppVisitorContext } from 'lib/AppVisitorProvider'
import Link from 'next/link'

interface Acclimation {
  rescueOrder: bigint
  blockNumber: bigint
  logIndex: number
  tx: `0x${string}`
}

function poseHeight(rescueOrder: number | bigint): number {
  const [meta] = getMoonCatData([rescueOrder])
  switch (meta.pose) {
    case 'standing':
      return 84
    case 'sleeping':
      return 72
    case 'pouncing':
      return 104
    case 'stalking':
      return 100
  }
}

const EVENT_STEP_SIZE = 100_000n
const WELCOMING_POSITIONS = [
  [960, 395],
  [1035, 405],
  [1110, 400],
  [1020, 380],
  [1090, 375],
  [1180, 385],
] as const

const RecentAcclimations = ({ acclimations }: { acclimations: Acclimation[] }) => {
  const {
    state: { awokenMoonCats },
  } = useContext(AppVisitorContext)
  const tableRef = useRef<HTMLTableElement>(null)
  const acclimatedMoonCats = getMoonCatData(acclimations.map((a) => a.rescueOrder).slice(0, 10))

  useTabbieHook(acclimatedMoonCats, awokenMoonCats, (mc) => {
    const td = tableRef.current?.querySelector(`#mooncat-${mc.rescueOrder}`)
    if (!td) {
      console.warn('Failed to find table element for MoonCat', mc)
      return { x: window.innerWidth / 2, y: window.innerHeight / 2 }
    }
    const bounds = td.getBoundingClientRect()
    return {
      x: bounds.x + window.scrollX + 20,
      y: bounds.y + window.scrollY + bounds.height,
    }
  })

  if (acclimations.length == 0) {
    return (
      <div className="text-container">
        <section className="card">
          <h2>Recent Acclimations</h2>
          <LoadingIndicator message="Fetching recent Acclimations..." />
        </section>
      </div>
    )
  }

  return (
    <div className="text-container">
      <section className="card">
        <h2>Recent Acclimations</h2>
        <div style={{ columnWidth: '12em', marginBottom: '1.5em' }}>
          <table
            cellSpacing="0"
            cellPadding="0"
            className="zebra"
            style={{ margin: '0 auto', fontSize: '0.8rem' }}
            ref={tableRef}
          >
            <tbody>
              {acclimations.slice(0, 10).map((a) => {
                return (
                  <tr key={String(a.blockNumber) + '-' + a.logIndex}>
                    <td
                      style={{ textAlign: 'center', lineHeight: 1, height: '2.5em', width: '2.5em' }}
                      id={`mooncat-${String(a.rescueOrder)}`}
                    >
                      {!awokenMoonCats.has(Number(a.rescueOrder)) && (
                        <picture>
                          <img
                            className="inline"
                            alt=""
                            src={`${API_SERVER_ROOT}/image/${String(a.rescueOrder)}?scale=1&padding=2&costumes=true`}
                          />
                        </picture>
                      )}
                    </td>
                    <td>
                      <Link href={`/mooncats/${String(a.rescueOrder)}`}>{'MoonCat #' + String(a.rescueOrder)}</Link>
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      </section>
    </div>
  )
}

const AcclimationScene = () => {
  const publicClient = usePublicClient({ chainId: 1 })
  const { data: blockNumber } = useBlockNumber({ chainId: 1, watch: false })

  const { status, data: events } = useQuery({
    queryKey: ['recent-acclimations', String(blockNumber)],
    queryFn: async ({ queryKey, signal }): Promise<Acclimation[]> => {
      const startingBlock = BigInt(queryKey[1])
      console.debug('Starting event hunt from block', startingBlock)
      let curBlock = startingBlock
      let count = 0
      let events: any[] = []
      while (events.length < 10 && count < 100) {
        if (signal.aborted) return events
        console.debug('Finding events...', curBlock, events.length)
        const e = await publicClient!.getContractEvents({
          address: ACCLIMATOR_ADDRESS,
          abi: parseAbi(['event MoonCatAcclimated(uint256 tokenId, address indexed owner)']),
          eventName: 'MoonCatAcclimated',
          toBlock: curBlock,
          fromBlock: curBlock - EVENT_STEP_SIZE,
        })
        events.unshift(...e)
        curBlock -= EVENT_STEP_SIZE
        count++
      }
      return (
        events
          .sort((a, b) => {
            if (a.blockNumber != b.blockNumber) return Number(b.blockNumber - a.blockNumber)
            return Number(b.logIndex - a.logIndex)
          })
          .map(
            (e) =>
              ({
                rescueOrder: e.args.tokenId,
                blockNumber: e.blockNumber,
                logIndex: e.logIndex,
                tx: e.transactionHash,
              } as Acclimation)
          )
          // Remove duplicates, so each MoonCat only appears a maximum of once in the list
          .reduce((unique, e) => {
            if (!unique.find((i) => i.rescueOrder == e.rescueOrder)) {
              // This MoonCat is not present in the accumulator yet; add it in
              unique.push(e)
            }
            return unique
          }, [] as Acclimation[])
      )
    },
    enabled: !!publicClient && !!blockNumber,
    staleTime: FIVE_MINUTES,
  })

  const recentAcclimations = status == 'success' && events ? events : []
  const beingAcclimated = recentAcclimations.length > 0 ? recentAcclimations[0].rescueOrder : null
  const welcomingCommittee = recentAcclimations.length > 0 ? recentAcclimations.slice(1, 7) : []

  return (
    <>
      <div style={{ margin: '0 auto', width: '95%', maxWidth: 1260 }}>
        <svg
          style={{ maxWidth: '100%' }}
          xmlns="http://www.w3.org/2000/svg"
          xmlnsXlink="http://www.w3.org/1999/xlink"
          viewBox="0 0 1260 540"
        >
          <image href="/img/acclimator/background.gif" width="1260" height="540" />

          {/* Acclimator Employees */}
          <image x="185" y="260" href="https://api.mooncat.community/image/19978?scale=2&padding=8" />
          <image x="155" y="295" href="https://api.mooncat.community/image/24222?scale=2&padding=8" />
          <image x="225" y="275" href="https://api.mooncat.community/image/23304?scale=2&padding=8" />

          <image href="/img/acclimator/machine.gif" width="1260" height="540" />

          {/* Being Acclimated */}
          {beingAcclimated != null && (
            <image
              x="580"
              y={425 - poseHeight(beingAcclimated)}
              href={`https://api.mooncat.community/image/${beingAcclimated}?acc=&scale=2&padding=8`}
            />
          )}

          {/* Welcoming Committee */}
          {welcomingCommittee
            .map((a, index) => {
              const [x, y] = WELCOMING_POSITIONS[index]
              return (
                <image
                  key={a.rescueOrder}
                  x={x}
                  y={y - poseHeight(a.rescueOrder)}
                  href={`https://api.mooncat.community/image/${a.rescueOrder}?acc=1&scale=2&padding=8`}
                />
              )
            })
            .reverse()}

          <image href="/img/acclimator/machine_front.gif" width="1260" height="540" />
          <image href="/img/acclimator/glass.gif" width="1260" height="540" opacity="30%" />
        </svg>
      </div>
      <RecentAcclimations acclimations={recentAcclimations} />
    </>
  )
}
export default AcclimationScene
