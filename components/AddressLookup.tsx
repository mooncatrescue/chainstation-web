'use client'
import Link from 'next/link'
import { useRouter } from 'next/navigation'
import { CSSProperties, useState } from 'react'
import { getAddress, isAddress } from 'viem'
import { getEnsAddress } from 'wagmi/actions'
import LoadingIndicator from 'components/LoadingIndicator'
import WarningIndicator from 'components/WarningIndicator'
import useFetchStatus from 'lib/useFetchStatus'
import { config } from 'lib/wagmi-config'
import useSignedIn from 'lib/useSignedIn'
import Icon from './Icon'

interface Props {
  /**
   * Template for the link to redirect the user to.
   * The static string value '{addr}' will be replaced with the lookup result.
   * If there is no '{addr}' in the template, the lookup result will be appended to the template.
   */
  linkTemplate: string
  style?: CSSProperties
}

/**
 * Logic for inserting an address into a custom link template.
 * @param linkTemplate - The template for the link, with '{addr}' as the placeholder for the address.
 * @param address - The address to insert into the template.
 * @returns The link with the address inserted.
 */
function getLink(linkTemplate: string, address: string) {
  if (!linkTemplate.includes('{addr}')) {
    return `${linkTemplate}${address}`
  }
  return linkTemplate.replace('{addr}', address)
}

const AddressLookup = ({ linkTemplate, style }: Props) => {
  const [addressInput, setInputAddress] = useState<string>('')
  const [errorMessage, setErrorMessage] = useState<string>('')
  const [fetchStatus, setFetchStatus] = useFetchStatus()
  const { connectedAddress } = useSignedIn()
  const router = useRouter()

  async function doLookup() {
    setErrorMessage('')
    setFetchStatus('pending')
    if (isAddress(addressInput)) {
      // Valid hex address
      router.push(getLink(linkTemplate, getAddress(addressInput)))
      return
    }

    let ensAddr = await getEnsAddress(config, { name: addressInput, chainId: 1 })
    if (ensAddr != null) {
      // Valid ENS name
      router.push(getLink(linkTemplate, ensAddr))
      return
    }

    setFetchStatus('error')
    setErrorMessage('That is not a valid address')
  }

  let fetchDisplay: React.ReactNode
  switch (fetchStatus) {
    case 'error': {
      fetchDisplay = <WarningIndicator style={{ textAlign: 'center' }} message={errorMessage} />
      break
    }
    case 'pending': {
      fetchDisplay = <LoadingIndicator style={{ textAlign: 'center' }} message="Searching..." />
      break
    }
  }

  return (
    <div className="address-lookup" style={style}>
      <p style={{ textAlign: 'center' }}>
        <input
          type="text"
          value={addressInput}
          onKeyDown={(e) => {
            if (e.key == 'Enter') doLookup()
          }}
          onChange={(e) => setInputAddress(e.target.value)}
          className="address-search"
          style={{ marginRight: '1em' }}
          placeholder="Ethereum address"
          autoCorrect="off"
          autoCapitalize="off"
          spellCheck="false"
        />
        <button
          onClick={(e) => {
            e.preventDefault()
            doLookup()
          }}
        >
          Lookup
        </button>
      </p>
      {connectedAddress && (
        <p style={{ marginTop: '-1rem', textAlign: 'center' }}>
          <Link href={getLink(linkTemplate, connectedAddress)}>
            Jump to your own address <Icon name="arrow-right" style={{ verticalAlign: '-0.2em' }} />
          </Link>
        </p>
      )}
      {fetchDisplay}
    </div>
  )
}
export default AddressLookup
