import { ZWS } from 'lib/util'
import EthereumAddress from './EthereumAddress'
import SuperfluidProgress from './SuperfluidProgress'

const SuperfluidSponsorship = () => {
  return (
    <section id="superfluid-sponsorship">
      <h3>Superfluid Sponsorship</h3>
      <p>
        The most direct way you can financially help the MoonCatRescue team continue to create great content and grow
        the MoonCat Ecosystem is donations directly to the team. Lump-sum donations can be sent to{' '}
        <EthereumAddress address="0xdf2E60Af57C411F848B1eA12B10a404d194bce27" />, which is the team&rsquo;s primary EOA
        (valid on any EVM chain) wallet. Additionally, the MoonCat{ZWS}Rescue team maintains two{' '}
        <a href="https://safe.global/">Safe</a> Smart Accounts:
      </p>
      <ul>
        <li>
          <EthereumAddress address="0x1bca6975046bC1Da4B7a1F357b7D2207b563b6D1" mode="bar" /> on{' '}
          <a href="https://app.safe.global/home?safe=eth:0x1bca6975046bC1Da4B7a1F357b7D2207b563b6D1">
            Ethereum Mainnet
          </a>
        </li>
        <li>
          <EthereumAddress
            address="0xf45349435c4A7cbb519987bd4d7460479568fD63"
            mode="bar"
            linkProfile={false}
            explorerLink="https://arbiscan.io/address/"
          />{' '}
          on <a href="https://app.safe.global/home?safe=arb1:0xf45349435c4A7cbb519987bd4d7460479568fD63">Arbitrum</a>
        </li>
      </ul>
      <p>
        While donations of any sort are always appreciated, making your contribution a{' '}
        <a href="https://www.superfluid.finance/" target="_blank" rel="noreferrer">
          Superfluid
        </a>{' '}
        subscription would help us make our finances a bit more predictable. A Superfluid subscription streams your
        donation over time, allowing you to create a monthly sponsorship that pays out automatically over time. Click
        the button below to jump over to Superfluid and set up a new stream to the MoonCat{ZWS}Rescue team.
      </p>
      <p style={{ textAlign: 'center' }}>
        <a
          className="btn"
          target="_blank"
          rel="noreferrer"
          aria-label="Start a Superfluid payment stream to support the MoonCatRescue Team"
          href={`https://app.superfluid.finance/send?${new URLSearchParams({
            network: 'arbitrum',
            receiver: '0xf45349435c4A7cbb519987bd4d7460479568fD63',
            token: '0x521677a61d101a80ce0fb903b13cb485232774ee',
            'flow-rate': '25/month',
          }).toString()}`}
        >
          Start a Stream
        </a>
      </p>
      <SuperfluidProgress style={{ marginBottom: '1rem' }} />
      <p>
        The MoonCat{ZWS}Rescue Team&rsquo;s current sponsorship goal is to have 50% of the project&rsquo;s expenses come from
        sponsors. This would make the project less dependent on secondary fees from NFT marketplaces for its ongoing
        maintenance. You can track more detail on the team&rsquo;s progress on making that cash flow balance in the{' '}
        <a
          href="https://console.superfluid.finance/arbitrum-one/accounts/0xf45349435c4A7cbb519987bd4d7460479568fD63?tab=map"
          target="_blank"
          rel="noreferrer"
        >
          Superfluid console
        </a>
        .
      </p>
    </section>
  )
}

export default SuperfluidSponsorship
