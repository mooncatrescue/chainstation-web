'use client'
import { useGMs } from 'lib/attestations'
import { AttestationDoc } from 'lib/eas'
import { formatAsDate } from 'lib/util'
import Link from 'next/link'
import EthereumAddress from './EthereumAddress'
import { Address } from 'viem'

const GMLeaderboard = ({ atts: cachedAtts }: { atts: AttestationDoc[] }) => {
  const { data: atts } = useGMs(cachedAtts)

  const signerTallies = atts!.reduce((tallies, a) => {
    tallies[a.signer] = (tallies[a.signer] ?? 0) + 1
    return tallies
  }, {} as Record<string, number>)
  const signers = Object.keys(signerTallies).sort((a, b) => {
    const aCount = signerTallies[a]
    const bCount = signerTallies[b]
    return bCount - aCount
  })

  const oldestAtt = atts![atts!.length - 1]
  const oldestDate = new Date(Number(oldestAtt.sig.message.time) * 1000)
  return (
    <div id="content-container" className="leaderboard">
      <div className="text-container leaderboard-wrap">
        <h1 className="hero">GM Leaderboard</h1>
        <section className="card" style={{ marginTop: 140 }}>
          <p>
            Sending a &ldquo;GM&rdquo; is quite the vibe. Sending a ton is even more fun! Of all the{' '}
            <Link href="/gm">verifiable GMs</Link> recorded since {formatAsDate(oldestDate)}, check out which Etherians
            have been sending the most out into the far corners of Deep Space:
          </p>
        </section>
        <section className="card">
          <ul className="zebra">
            {signers.map((s) => (
              <li key={s}>
                <Link href={`/owners/${s}`}>
                  <span style={{ flex: '1 1 auto' }}>
                    <EthereumAddress address={s as Address} mode="text" />
                  </span>
                  <span>
                    {signerTallies[s]} {signerTallies[s] == 1 ? 'GM' : 'GMs'}
                  </span>
                </Link>
              </li>
            ))}
          </ul>
        </section>
      </div>
    </div>
  )
}
export default GMLeaderboard
