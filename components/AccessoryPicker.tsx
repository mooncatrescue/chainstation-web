import { OwnedAccessory } from 'lib/useMoonCatAccessories'
import { CSSProperties } from 'react'
import AccessoryGroup from './AccessoryGroup'

interface Props {
  accessories: OwnedAccessory[]
  onChange: (list: OwnedAccessory[]) => void
  style?: CSSProperties
  className?: string
}

const AccessoryPicker = ({ accessories, onChange, style, className }: Props) => {
  const sortedAccessories = accessories.sort((a, b) => {
    if (a.zIndex != b.zIndex) {
      return b.zIndex - a.zIndex
    }
    return a.name.localeCompare(b.name)
  })
  const foregroundAccessories = sortedAccessories.filter((a) => !a.isBackground)
  const backgroundAccessories = sortedAccessories.filter((a) => a.isBackground)

  return (
    <section className={'accessory-picker ' + className} style={style}>
      <h3>Foreground</h3>
      <AccessoryGroup
        accessories={foregroundAccessories}
        label="foreground"
        onChange={(newList) => onChange(newList.concat(...backgroundAccessories))}
      />
      <h3>Background</h3>
      <AccessoryGroup
        accessories={backgroundAccessories}
        label="background"
        onChange={(newList) => onChange(foregroundAccessories.concat(...newList))}
      />
    </section>
  )
}

export default AccessoryPicker
