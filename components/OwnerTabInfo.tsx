import { ReactNode, useContext, useEffect, useRef, useState } from 'react'
import { TabProps } from './OwnerDetailsPage'
import LoadingIndicator from './LoadingIndicator'
import { MoonCatData, OwnerProfile } from 'lib/types'
import Link from 'next/link'
import { API_SERVER_ROOT, FIVE_MINUTES, ONE_HOUR, ZWS } from 'lib/util'
import Pagination from './Pagination'
import { useEnsAvatar, useEnsName, usePublicClient } from 'wagmi'
import { normalize } from 'viem/ens'
import WarningIndicator from './WarningIndicator'
import HexString from './HexString'
import { getOwnerTidbits } from 'lib/tidbits'
import useIsMounted from 'lib/useIsMounted'
import { useQuery } from '@tanstack/react-query'
import useTabbieHook from 'lib/useTabbieHook'
import { AppVisitorContext } from 'lib/AppVisitorProvider'
import { Address } from 'viem'

interface RescuedMoonCats {
  moonCats: MoonCatData[]
  totalLength: number
}

const PER_PAGE = 50

/**
 * Show a condensed table view of all MoonCats rescued by this address
 */
const MoonCatsRescued = ({ address }: { address: Address }) => {
  const [page, setPage] = useState<number>(0)
  const params = new URLSearchParams({
    limit: String(PER_PAGE),
    offset: String(page * PER_PAGE),
    rescuedby: address,
  })
  const { data: rescuedMoonCats } = useQuery({
    queryKey: ['rescued-mooncats', params.toString()],
    queryFn: async (): Promise<RescuedMoonCats> => {
      const rs = await fetch(`/api/mooncats?${params}`)
      if (!rs.ok) throw new Error('Failed to fetch MoonCat data')
      return await rs.json()
    },
    staleTime: ONE_HOUR,
  })
  const {
    state: { awokenMoonCats },
  } = useContext(AppVisitorContext)
  const rescuedTable = useRef<HTMLTableElement>(null)

  useTabbieHook(rescuedMoonCats?.moonCats, awokenMoonCats, (mc) => {
    const td = rescuedTable.current?.querySelector(`#mooncat-${mc.rescueOrder}`)
    if (!td) {
      console.warn('Failed to find table element for MoonCat', mc)
      return { x: window.innerWidth / 2, y: window.innerHeight / 2 }
    }
    const bounds = td.getBoundingClientRect()
    return {
      x: bounds.x + window.scrollX + 20,
      y: bounds.y + window.scrollY + bounds.height,
    }
  })

  if (!rescuedMoonCats) {
    return (
      <>
        <h2>MoonCats Rescued</h2>
        <LoadingIndicator />
      </>
    )
  }

  if (rescuedMoonCats.totalLength == 0) {
    return (
      <>
        <h2>MoonCats Rescued</h2>
        <p>
          This address did not rescue any MoonCats as part of the initial{' '}
          <a href="https://mooncat.community/blog/mcr-update4">Insanely Cute Operation</a>.
        </p>
      </>
    )
  }

  // Event handler for updates from Pagination component when user navigates to a new page
  function handlePageUpdate(newPage: number) {
    if (page == newPage) return
    setPage(newPage)
  }

  return (
    <>
      <h2>MoonCats Rescued</h2>
      <p>
        This address rescued {rescuedMoonCats.totalLength} MoonCats as part of the initial{' '}
        <a href="https://mooncat.community/blog/mcr-update4">Insanely Cute Operation</a>:
      </p>
      <div style={{ columnWidth: '13em', marginBottom: '1.5em' }}>
        <table cellSpacing="0" cellPadding="0" className="zebra" style={{ margin: '0 auto' }} ref={rescuedTable}>
          <tbody>
            {rescuedMoonCats.moonCats.map((e) => {
              return (
                <tr key={e.catId} id={`mooncat-${e.rescueOrder}`}>
                  <td style={{ textAlign: 'center', lineHeight: 1 }}>
                    {!awokenMoonCats.has(e.rescueOrder) && (
                      <picture>
                        <img
                          className="inline"
                          alt={'MoonCat ' + e.rescueOrder}
                          src={`${API_SERVER_ROOT}/image/${e.rescueOrder}?scale=1&padding=5&costumes=true`}
                        />
                      </picture>
                    )}
                  </td>
                  <td style={{ paddingRight: '1rem' }}>
                    <Link href={'/mooncats/' + e.rescueOrder}>{`MoonCat #${e.rescueOrder}`}</Link>
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
      <Pagination
        currentPage={page}
        maxPage={Math.ceil(rescuedMoonCats.totalLength / PER_PAGE) - 1}
        setCurrentPage={handlePageUpdate}
      />
    </>
  )
}

interface EnsTextMeta {
  label?: string
  decorator?: (text: string) => ReactNode
}

const ensTextKeys: Record<string, EnsTextMeta> = {
  'description': {},
  'notice': {},
  'email': {
    label: 'Email',
    decorator: (text) => <a href={`mailto:${text}`}>{text}</a>,
  },
  'fediverse': {
    label: 'Fediverse',
    decorator: (text) => {
      const [username, server] = text.replace(/^@/, '').split('@')
      return (
        <a href={`https://${server}/@${username}`} rel="external" itemProp="relatedLink">
          {text}
        </a>
      )
    },
  },
  'location': {
    label: 'Location',
  },
  'url': {
    label: 'URL',
    decorator: (text) => (
      <a href={text} rel="external nofollow" itemProp="relatedLink">
        {text.length > 40 ? text.substring(0, 35) + '...' : text}
      </a>
    ),
  },
  'com.github': {
    label: 'GitHub',
    decorator: (text) => (
      <a href={`https://github.com/${text.replace(/^@/, '')}`} rel="external" itemProp="relatedLink">
        {text}
      </a>
    ),
  },
  'com.linkedin': {
    label: 'LinkedIn',
    decorator: (text) => (
      <a href={`https://www.linkedin.com/in/${text.replace(/^@/, '')}`} rel="external" itemProp="relatedLink">
        {text}
      </a>
    ),
  },
  'com.twitter': {
    label: 'Twitter',
    decorator: (text) => (
      <a href={`https://twitter.com/${text.replace(/^@/, '')}`} rel="external" itemProp="relatedLink">
        {text}
      </a>
    ),
  },
  'io.keybase': {
    label: 'Keybase',
    decorator: (text) => (
      <a href={`https://keybase.io/${text.replace(/^@/, '')}`} rel="external" itemProp="relatedLink">
        {text}
      </a>
    ),
  },
  'org.telegram': {
    label: 'Telegram',
    decorator: (text) => (
      <a href={`https://t.me/${text.replace(/^@/, '')}`} rel="external" itemProp="relatedLink">
        {text}
      </a>
    ),
  },
  'com.warpcast': {
    label: 'Warpcast',
    decorator: (text) => (
      <a href={`https://warpcast.com/${text.replace(/^@/, '')}`} rel="external" itemProp="relatedLink">
        {text}
      </a>
    ),
  },
}

interface TidbitDefinition {
  label: string
  detail: React.ReactNode
}
const tidbitDetails: Record<string, TidbitDefinition> = {
  'moonCats': {
    label: 'Owns MoonCats',
    detail: 'Currently has adopted some MoonCats',
  },
  'heroVoter': {
    label: 'Hero Voter',
    detail: `Participated in the original MoonCat${ZWS}Rescue Community Vote`,
  },
  'adoptionCenterWithdrawal': {
    label: 'Adoption Center Withdrawals',
    detail: `Has Ether waiting for withdrawal in the original MoonCat${ZWS}Rescue contract`,
  },
  'firstPlushieParticipants': {
    label: 'First MoonCat Plushie',
    detail: 'Placed an order in the first MoonCat plushie merch offering',
  },
  'gmCount': {
    label: 'Sent a GM',
    detail: (
      <>
        Has sent a <Link href="/gm">verifiable GM attestation</Link>
      </>
    ),
  },
}
const OwnerTidbits = ({ address, details }: { address: Address; details: OwnerProfile }) => {
  const { data: tidbits, status } = useQuery({
    queryKey: ['owner-tidbits', address],
    queryFn: async () => getOwnerTidbits(address, details),
    staleTime: FIVE_MINUTES,
  })

  if (status !== 'success') return null

  const tidbitsList = Object.keys(tidbitDetails)
    .filter((k) => typeof tidbits[k] != 'undefined' && tidbits[k] !== false)
    .map((k) => (
      <li key={k}>
        <strong>{tidbitDetails[k].label}</strong> {tidbitDetails[k].detail} {tidbits[k] !== true && <>({tidbits[k]})</>}
      </li>
    ))

  if (tidbits.length == 0) return null
  return (
    <>
      <h2>Interesting Tidbits</h2>
      <ul>{tidbitsList}</ul>
    </>
  )
}

interface MetaDetails {
  label: string
  detail: ReactNode
}

const OwnerTabInfo = ({ ownerAddress, isOwnWallet, details }: TabProps) => {
  const isMounted = useIsMounted()
  const { data: ensName } = useEnsName({ address: ownerAddress, chainId: 1 })
  const { data: avatar } = useEnsAvatar({ name: ensName ?? undefined, chainId: 1 })
  const [textData, setTextData] = useState<Record<string, string>>({})
  const publicClient = usePublicClient({ chainId: 1 })

  useEffect(() => {
    if (!ensName || !publicClient) return
    const keyNames = Object.keys(ensTextKeys)
    Promise.all(keyNames.map((key) => publicClient.getEnsText({ name: normalize(ensName), key }))).then((rs) => {
      let out: Record<string, string> = {}
      for (let i = 0; i < rs.length; i++) {
        if (rs[i] != null) out[keyNames[i]] = rs[i] as string
      }
      setTextData(out)
    })
  }, [ensName, publicClient])

  const ownerMeta: MetaDetails[] = []
  ownerMeta.push({
    label: 'Address',
    detail: (
      <span itemProp="identifier">
        <HexString>{ownerAddress}</HexString>
      </span>
    ),
  })
  if (isMounted && ensName) {
    ownerMeta.push({
      label: 'ENS Name',
      detail: <code itemProp="alternateName">{ensName}</code>,
    })
  }
  Object.keys(ensTextKeys).forEach((key) => {
    const config = ensTextKeys[key]
    if (!config.label || !textData[key]) return
    const detail = typeof config.decorator != 'undefined' ? config.decorator(textData[key]) : textData[key]
    ownerMeta.push({
      label: config.label,
      detail: detail,
    })
  })

  return (
    <section className="card">
      {isMounted && avatar && (
        <picture>
          <img alt="" style={{ float: 'right', maxWidth: '20%', maxHeight: '200px' }} src={avatar} />
        </picture>
      )}
      <h2 style={{ marginTop: 0 }}>Owner Details</h2>
      {textData.notice && <WarningIndicator message={textData.notice} />}
      {textData.description && <p>{textData.description}</p>}
      <ul className="two-col">
        {ownerMeta.map((d, i) => (
          <li key={i}>
            <strong>{d.label}:</strong> {d.detail}
          </li>
        ))}
      </ul>
      <MoonCatsRescued address={ownerAddress} />
      <OwnerTidbits address={ownerAddress} details={details} />
    </section>
  )
}
export default OwnerTabInfo
