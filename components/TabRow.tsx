export interface Tab<T = Record<string, unknown>> {
  label: string
  content: (props: T) => JSX.Element
}

const TabRow = <T,>({
  tabs,
  activeTab,
  toggleTab,
  children,
}: {
  tabs: Record<string, Tab<T>>
  activeTab: keyof typeof tabs
  toggleTab: (tabName: keyof typeof tabs) => void
  children: (ActiveTab: Tab<T>['content']) => JSX.Element
}) => {
  const TabContent = tabs[activeTab].content
  return (
    <>
      <nav className="tabs" role="tablist">
        {Object.entries(tabs).map((tab) => (
          <div
            key={tab[0]}
            role="tab"
            aria-selected={tab[0] === activeTab}
            className={tab[0] == activeTab ? 'active' : ''}
            onClick={() => toggleTab(tab[0])}
          >
            {tab[1].label}
          </div>
        ))}
      </nav>
      <section className="tab-contents" role="tabpanel" aria-labelledby={activeTab}>
        {children(TabContent)}
      </section>
    </>
  )
}
export default TabRow
