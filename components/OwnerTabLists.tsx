import { SCHEMAS } from 'lib/eas'
import { parseTokenListAttestations, parseTokenListMoonCats } from 'lib/tokens'
import { API2_SERVER_ROOT, nameToUriSlug, ONE_HOUR } from 'lib/util'
import { TabProps } from './OwnerDetailsPage'
import { CSSProperties } from 'react'
import LoadingIndicator from './LoadingIndicator'
import { Address, stringToHex } from 'viem'
import { TokenListMeta } from 'lib/types'
import Link from 'next/link'
import { useQuery } from '@tanstack/react-query'

// https://arbitrum.easscan.org/schema/view/0x58de78ba175d13cb1699fdbc6c25a80ba2b21a943b5aac252ec367c878808d09
const SCHEMA_TOKEN_LIST = SCHEMAS.TOKEN_LIST

// https://arbitrum.easscan.org/schema/view/0xeae0ff58b769be065ba9d54f4dd9b88c07de93a00e1e3b889a7099de02dd5549
const SCHEMA_PRODUCT_REVIEW = SCHEMAS.PRODUCT_REVIEW

const MAX_SHOWN_TOKENS = 5
const tokenStyle: CSSProperties = { height: 40 }
const ListSummary = ({ ownerAddress, listDetails }: { ownerAddress: Address; listDetails: TokenListMeta }) => {
  const rawListTitle = stringToHex(listDetails.title, { size: 32 })
  const { data: tokens, status } = useQuery({
    queryKey: ['user-list-tokens', ownerAddress, listDetails.title],
    queryFn: async () => {
      const rs = await fetch(
        `${API2_SERVER_ROOT}/attestations?${new URLSearchParams({
          schema: `arb:${SCHEMA_TOKEN_LIST}`,
          sender: `arb:${ownerAddress}`,
          data_key: 'name',
          data_value: rawListTitle,
          limit: String(MAX_SHOWN_TOKENS),
        })}`
      )
      if (!rs.ok) throw new Error('Failed to fetch attestations')
      return parseTokenListMoonCats(await parseTokenListAttestations(await rs.json()))
    },
    staleTime: ONE_HOUR,
  })

  if (status !== 'success') {
    return (
      <li>
        <Link href={`${ownerAddress}/lists/${nameToUriSlug(listDetails.title)}`}>
          <div className="name">{listDetails.title}</div>
          <div className="details">
            <LoadingIndicator message="Fetching token info..." />
          </div>
          <div className="list-tokens-preview">&nbsp;</div>
        </Link>
      </li>
    )
  }

  const shownMoonCats = tokens.slice(0, MAX_SHOWN_TOKENS).map((t) => (
    <picture key={t.collection.address + '::' + t.id}>
      <img src={t.imageSrc} alt={`MoonCat ${t.name}`} style={tokenStyle} />
    </picture>
  ))

  return (
    <li>
      <Link href={`${ownerAddress}/lists/${nameToUriSlug(listDetails.title)}`}>
        <div className="name">{listDetails.title}</div>
        <div className="details">
          {listDetails.tokenCount} {listDetails.tokenCount > 1 ? 'MoonCats' : 'MoonCat'}
        </div>
        <div className="list-tokens-preview">
          {shownMoonCats}
          {tokens.length > MAX_SHOWN_TOKENS && '...'}
        </div>
      </Link>
    </li>
  )
}

const OwnerTabLists = ({ ownerAddress, isOwnWallet, details }: TabProps) => {
  const userLists = details.tokenLists
  return (
    <section className="card">
      <p>
        As Etherians find their way across the vast cosmos, they have a high liklihood of finding strange and
        interesting things along the way. The ChainStation archives provide a means for any Etherian to keep track of
        discoveries for later, adding items to different lists. These items are not necessarily owned by this Etherian,
        but notable to them.
      </p>
      {userLists.length == 0 && (
        <p>
          <em>This Etherian has not created any lists yet.</em>
        </p>
      )}
      {userLists.length > 0 && (
        <ul className="user-list-list">
          {userLists.map((l) => (
            <ListSummary key={l.title} ownerAddress={ownerAddress} listDetails={l} />
          ))}
        </ul>
      )}
    </section>
  )
}
export default OwnerTabLists
