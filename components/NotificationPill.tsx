const NotificationPill = ({ count, label }: { count: number; label?: [string, string] }) => {
  if (count <= 0) return null // Don't show if there's no notifications

  const suffix = typeof label == 'undefined' ? '' : count > 1 ? ' ' + label[1] : ' ' + label[0]
  return (
    <span className="pill">
      <span>{count + suffix}</span>
    </span>
  )
}

export default NotificationPill
