'use client'
import { useContext } from 'react'
import { Abi, Address, ContractFunctionName } from 'viem'
import { usePublicClient } from 'wagmi'
import { readContract } from 'wagmi/actions'
import { ActionQueueContext } from 'lib/ActionsQueueProvider'
import { BasicAttestation, revokeHelpText, useEas } from 'lib/eas'
import {
  Action,
  actionIsComplete,
  ActionStep,
  AttestationStepComplete,
  AttestationStepPending,
  ContractStepComplete,
  ContractStepPending,
  RevokeAttestationsStepComplete,
  RevokeAttestationsStepPending,
  stepIsComplete,
} from 'lib/onchainActions'
import useSignedIn from 'lib/useSignedIn'
import useTxStatus from 'lib/useTxStatus'
import { formatAsDate, switchToChain, ZWS } from 'lib/util'
import useDialog from 'lib/useDialog'
import Drawer from './Drawer'
import EthereumAddress from './EthereumAddress'
import HexString from './HexString'
import EthereumTransaction from './EthereumTransaction'
import Icon from './Icon'

/**
 * Show an individual Action that is incomplete
 */
const IncompleteActionView = ({ action, address }: { action: Action; address: Address }) => {
  const { dispatch } = useContext(ActionQueueContext)
  const firstIncompleteStep = action.steps.findIndex((s) => !stepIsComplete(s))
  return (
    <section className="user-action" id={`action-${action.id}`}>
      <button
        className="btn-small"
        style={{ float: 'right' }}
        onClick={() => {
          dispatch({
            type: 'DELETE',
            payload: action.id,
          })
        }}
      >
        <Icon name="trash" style={{ marginRight: '0.5em' }} />
        Abort
      </button>
      <h4>{action.label}</h4>
      <ul>
        {action.steps.map((s, i) => (
          <StepView
            key={action.id + ':' + String(i)}
            action={action}
            step={s}
            isNextPending={i == firstIncompleteStep}
            address={address}
            onUpdate={(updatedStep) => {
              const newSteps = [...action.steps]
              newSteps[i] = updatedStep
              dispatch({
                type: 'UPDATE',
                payload: {
                  ...action,
                  steps: newSteps,
                },
              })
            }}
          />
        ))}
      </ul>
    </section>
  )
}

/**
 * Show an individual Action that is complete
 */
const CompleteActionView = ({ action, address }: { action: Action; address: Address }) => {
  const { dispatch } = useContext(ActionQueueContext)
  return (
    <section className="user-action" id={`action-${action.id}`}>
      <button
        className="btn-small"
        style={{ float: 'right' }}
        onClick={() => {
          dispatch({
            type: 'DELETE',
            payload: action.id,
          })
        }}
      >
        <Icon name="trash" style={{ marginRight: '0.5em' }} />
        {'Forget'}
      </button>
      <h4>{action.label}</h4>
      <ul>
        {action.steps.map((s, i) => (
          <StepView
            key={action.id + ':' + String(i)}
            action={action}
            step={s}
            isNextPending={false}
            address={address}
          />
        ))}
      </ul>
    </section>
  )
}

/**
 * Render a list item record for an individual action step.
 * The step may be complete or incomplete, and of any step type.
 */
const StepView = <
  const abi extends Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
>({
  action,
  step,
  isNextPending,
  address,
  onUpdate = (step) => {},
}: {
  action: Action
  step: ActionStep<abi, functionName>
  isNextPending: boolean
  address: Address
  onUpdate?: (step: ActionStep<abi, functionName>) => void
}) => {
  const isComplete = stepIsComplete(step)
  switch (step.type) {
    case 'REVOKE': {
      return <RevokeStep step={step} isNextPending={isNextPending} onUpdate={onUpdate} />
    }
    case 'ATTEST': {
      return <AttestStep step={step} isNextPending={isNextPending} onUpdate={onUpdate} />
    }
    case 'CONTRACT': {
      return <ContractStep step={step} isNextPending={isNextPending} onUpdate={onUpdate} />
    }
    default: {
      // Type assertion since we shouldn't get here if step.type is valid
      const unknownStep = step as { type?: string }
      return (
        <li>
          Unknown {unknownStep.type || 'undefined'} step: {isComplete ? 'Done' : 'Pending'}
        </li>
      )
    }
  }
}

/**
 * Display an individual step that needs to guide the user through triggering a transaction to revoke an Attestation
 */
const RevokeStep = ({
  step,
  isNextPending,
  onUpdate,
}: {
  step: RevokeAttestationsStepPending | RevokeAttestationsStepComplete
  isNextPending: boolean
  onUpdate: (step: RevokeAttestationsStepComplete) => void
}) => {
  const { revokeAttestations } = useEas(step.chainId)
  const { ref: dialogRef, setIsOpen: setDialogIsOpen } = useDialog()
  const isComplete = stepIsComplete(step)

  const attLabel = step.uids.length == 1 ? 'Attestation' : 'Attestations'
  if (isComplete)
    return (
      <li>
        <em>Revoke {attLabel}</em>: {step.label}. Completed on{' '}
        {formatAsDate((step as RevokeAttestationsStepComplete).timestamp)}
      </li>
    )
  return (
    <li>
      <em>Revoke {attLabel}</em>: {step.label}.{' '}
      {isNextPending && (
        <button
          onClick={async () => {
            if (!(await switchToChain(step.chainId))) return // Must be on the correct network
            setDialogIsOpen(true)
            try {
              await revokeAttestations(step.uids)
            } catch (err) {
              console.error(err)
              setDialogIsOpen(false)
              return
            }

            // Revoke successful. Mark this step as complete
            onUpdate({
              ...step,
              timestamp: Math.floor(Date.now() / 1000),
            })
            setDialogIsOpen(false)
          }}
        >
          <Icon name="rocket" style={{ marginRight: '0.5em', verticalAlign: '-3px' }} />
          Do it
        </button>
      )}
      <dialog ref={dialogRef} aria-modal="true" onCancel={(e) => e.preventDefault()}>
        <h3>Check your wallet to confirm revoking this Attestation</h3>
        {revokeHelpText}
      </dialog>
    </li>
  )
}

/**
 * Display an individual step that needs to guide the user through signing an Attestation message
 */
const AttestStep = ({
  step,
  isNextPending,
  onUpdate,
}: {
  step: AttestationStepPending | AttestationStepComplete
  isNextPending: boolean
  onUpdate: (step: AttestationStepComplete) => void
}) => {
  const { offchainSign } = useEas(step.chainId)
  const { ref: dialogRef, setIsOpen: setDialogIsOpen } = useDialog()
  const isComplete = stepIsComplete(step)

  if (isComplete) {
    const s = step as AttestationStepComplete
    return (
      <li>
        <em>Attested</em>: {step.label}. Completed on {formatAsDate(s.timestamp)} as attestation{' '}
        <HexString>{s.uid}</HexString>
      </li>
    )
  }
  return (
    <li>
      <em>Attest</em>: {step.label}.{' '}
      {isNextPending && (
        <button
          onClick={async () => {
            if (!(await switchToChain(step.chainId))) return // Must be on the Arbitrum network
            setDialogIsOpen(true)
            let att: BasicAttestation | null
            try {
              att = await offchainSign(step.msg)
            } catch (err) {
              console.error(err)
              setDialogIsOpen(false)
              return
            }

            // Attest successful. Mark this step as complete
            onUpdate({
              ...step,
              uid: att!.sig.uid as `0x${string}`,
              timestamp: Number(att!.sig.message.time),
            })
            setDialogIsOpen(false)
          }}
        >
          <Icon name="rocket" style={{ marginRight: '0.5em', verticalAlign: '-3px' }} />
          Do it
        </button>
      )}
      <dialog ref={dialogRef} aria-modal="true" onCancel={(e) => e.preventDefault()}>
        <h3>Check your wallet to confirm revoking this Attestation</h3>
        {revokeHelpText}
      </dialog>
    </li>
  )
}

const ContractStep = <
  const abi extends Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
>({
  step,
  isNextPending,
  onUpdate,
}: {
  step: ContractStepPending<abi, functionName> | ContractStepComplete<abi, functionName>
  isNextPending: boolean
  onUpdate: (step: ContractStepComplete<abi, functionName>) => void
}) => {
  const { viewMessage, isProcessing, setStatus, processTransaction } = useTxStatus()
  const publicClient = usePublicClient()
  const isComplete = stepIsComplete(step)

  if (isComplete) {
    if ('skipped' in step) {
      return (
        <li>
          <em>Contract interaction</em>: {step.label}. Skipped as no longer needed.
        </li>
      )
    }
    const s = step as ContractStepComplete<abi, functionName>
    return (
      <li>
        <em>Contract interaction</em>: {s.label}. Completed in transaction <EthereumTransaction hash={s.tx.hash} /> on{' '}
        {formatAsDate(s.tx.timestamp)}
      </li>
    )
  }
  return (
    <li>
      <em>Contract interaction</em>: {step.label}.{' '}
      {isNextPending && (
        <button
          onClick={async () => {
            if (!publicClient) return
            // Do the contract interaction

            // If there is a precheck, check it
            if (step.precheck) {
              const rs = await step.precheck(readContract)
              if (!rs) {
                return
              }
            }

            // No precheck, or precheck failed. Do the transaction.
            const rs = await processTransaction(step.config)
            if (!rs) {
              setStatus('error', 'Communications jammed! Transaction could not be sent')
              return
            }
            if (rs.status == 'reverted') {
              setStatus('error', 'Signal lost... Transaction was added to the blockchain, but reverted.')
              return
            }

            // Interaction was successful. Mark this step as complete
            const block = await publicClient.getBlock({ blockNumber: rs.blockNumber })
            onUpdate({
              ...step,
              tx: {
                hash: rs.transactionHash,
                timestamp: Number(block.timestamp),
              },
            })
          }}
          disabled={isProcessing}
        >
          <Icon name="rocket" style={{ marginRight: '0.5em', verticalAlign: '-3px' }} />
          Do it
        </button>
      )}
      {viewMessage}
    </li>
  )
}

/**
 * Overall page component
 */
const ActionList = () => {
  const { verifiedAddresses } = useSignedIn()
  const targetAddress = verifiedAddresses[0].address
  const { actions } = useContext(ActionQueueContext)
  const actionsForAddress = actions.filter((a) => a.fromAddress == targetAddress)

  if (actionsForAddress.length === 0) {
    return (
      <section className="card">
        <h2>On-Chain Actions</h2>
        <p>
          This browser has no local record of pending or historic actions for{' '}
          <EthereumAddress address={targetAddress} />. As you interact with the ChainStation site, this list will keep
          track of them. Check back later to remind yourself what past adventures you&rsquo;ve had in the MoonCat{ZWS}
          Rescue ecosystem.
        </p>
      </section>
    )
  }

  const { completeActions, incompleteActions } = actions.reduce(
    (agg, a) => {
      if (actionIsComplete(a)) {
        agg.completeActions.push(a)
      } else {
        agg.incompleteActions.push(a)
      }
      return agg
    },
    { completeActions: [], incompleteActions: [] } as { completeActions: Action[]; incompleteActions: Action[] }
  )

  return (
    <section className="card">
      <h2>On-Chain Actions</h2>
      <p>
        Pending and historic actions for <EthereumAddress address={targetAddress} />: These actions are stored locally
        in this browser, to help facilitate complex (multi-step) interactions (if you accidentally stop an action
        mid-way, you can resume it here) and keep a record of past interactions with the MoonCat{ZWS}Rescue ecosystem.
      </p>
      {incompleteActions.length > 0 && (
        <>
          <h3>Pending Actions</h3>
          {incompleteActions.map((a) => (
            <IncompleteActionView key={a.id} action={a} address={targetAddress} />
          ))}
        </>
      )}
      {completeActions.length > 0 && (
        <Drawer title="Completed Actions" style={{ marginTop: '4em', marginBottom: 0 }}>
          <div style={{ background: 'rgba(255,255,255, 0.05)', padding: '0.2em 1em' }}>
            {completeActions.map((a) => (
              <CompleteActionView key={a.id} action={a} address={targetAddress} />
            ))}
          </div>
        </Drawer>
      )}
    </section>
  )
}
export default ActionList
