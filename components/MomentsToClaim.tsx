'use client'
import React from 'react'
import { API2_SERVER_ROOT, API_SERVER_ROOT, FIVE_MINUTES, MOMENTS_ADDRESS, ZWS } from 'lib/util'
import { OwnerProfile } from 'lib/types'
import { Address, parseAbi } from 'viem'
import LoadingIndicator from 'components/LoadingIndicator'
import WarningIndicator from 'components/WarningIndicator'
import useTxStatus from 'lib/useTxStatus'
import Link from 'next/link'
import { useQuery } from '@tanstack/react-query'
import { useReadContracts } from 'wagmi'
import { useRouter } from 'next/navigation'
import useSignedIn from 'lib/useSignedIn'

const MOMENTS = {
  address: MOMENTS_ADDRESS as Address,
  abi: parseAbi([
    'function listClaimableMoments(uint256 rescueOrder) external view returns (uint16[])',
    'function batchClaim(uint256[] momentIds, uint256[] rescueOrders) external',
  ]),
} as const

const MomentsToClaim = () => {
  const { viewMessage, isProcessing, processTransaction } = useTxStatus()
  const { verifiedAddresses } = useSignedIn()
  const router = useRouter()

  // Get a list of all MoonCats owned by this address
  const { data: ownedMoonCats, status: moonCatStatus } = useQuery({
    queryKey: ['owner-profile', verifiedAddresses[0].address],
    queryFn: async (): Promise<OwnerProfile> => {
      const rs = await fetch(`${API2_SERVER_ROOT}/owner-profile/${verifiedAddresses[0].address}`)
      if (!rs.ok) throw new Error('Failed to fetch owner information')
      return await rs.json()
    },
    staleTime: FIVE_MINUTES,
    select: (data) => data.ownedMoonCats,
  })

  // For each MoonCat owned, query to see which Moments are claimable by them
  const { data: claimableMoments, status: momentsStatus } = useReadContracts({
    contracts: ownedMoonCats
      ? ownedMoonCats.map(
          (mc) =>
            ({
              ...MOMENTS,
              functionName: 'listClaimableMoments',
              args: [BigInt(mc.rescueOrder)],
            } as const)
        )
      : [],
    query: { enabled: typeof ownedMoonCats !== 'undefined' },
    allowFailure: false,
  })

  const pendingClaims =
    claimableMoments && ownedMoonCats
      ? ownedMoonCats.reduce((agg, mc, index) => {
          const claimable = claimableMoments[index]
          if (claimable.length > 0) {
            agg[mc.rescueOrder] = claimable
          }
          return agg
        }, {} as Record<number, readonly number[]>)
      : {}

  // Assemble a batch claim for all the pending claims found
  async function doClaims() {
    if (Object.keys(pendingClaims).length == 0) return
    let moonCats: bigint[] = []
    let moments: bigint[] = []
    for (let [rescueOrder, claimable] of Object.entries(pendingClaims)) {
      for (let momentId of claimable) {
        moonCats.push(BigInt(rescueOrder))
        moments.push(BigInt(momentId))
      }
    }
    let rs = await processTransaction({
      ...MOMENTS,
      functionName: 'batchClaim',
      args: [moments, moonCats],
    })
    if (rs) {
      router.refresh()
    }
  }

  if (moonCatStatus == 'pending') {
    return <LoadingIndicator message="Enumerating Moments for you..." />
  } else if (moonCatStatus == 'error' || momentsStatus == 'error') {
    return <WarningIndicator message="Data fetch encountered an error" />
  } else if (Object.keys(pendingClaims).length == 0) {
    // No owned MoonCats have any Moments to claim
    return (
      <section className="card-notice">
        Based on your current address holdings, you don&rsquo;t have any pending{' '}
        <Link href="/moments">{`MoonCat${ZWS}Moments`}</Link> to claim.
      </section>
    )
  }

  return (
    <section className="card">
      <p>
        The following MoonCats you own have been part of specific Moments and so can claim the MoonCat{ZWS}Moment NFT
        for that event. Click the claim button to generate a transaction to claim all these Moments. Moment NFTs wil be
        delivered to the MoonCats themselves (will appear in their Purrse of owned NFTs).
      </p>
      <ul style={{ listStyleType: 'none', paddingLeft: 0, margin: '2rem 0' }}>
        {Object.entries(pendingClaims).map(([rescueOrder, claimable]) => (
          <li key={rescueOrder}>
            <picture>
              <img
                src={`${API_SERVER_ROOT}/image/${rescueOrder}?scale=2&padding=3costumes=true`}
                alt={`MoonCat ${rescueOrder}`}
                title={`MoonCat ${rescueOrder}`}
                style={{ height: 40, verticalAlign: -12, paddingRight: 10 }}
              />
            </picture>
            <em>#{rescueOrder}</em>: {claimable.length == 1 ? 'Moment' : 'Moments'} {claimable.join(', ')}
          </li>
        ))}
      </ul>
      <p>
        <button disabled={isProcessing} onClick={doClaims}>
          Claim Moments
        </button>
      </p>
      {viewMessage}
    </section>
  )
}

export default MomentsToClaim
