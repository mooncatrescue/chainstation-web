'use client'
import { API2_SERVER_ROOT, API_SERVER_ROOT } from 'lib/util'
import Link from 'next/link'
import { zeroAddress } from 'viem'
import EthereumAddress from './EthereumAddress'
import Icon from './Icon'
import { AccessorySummary } from 'lib/types'
import { useQuery } from '@tanstack/react-query'

const AccessoryLeaderboard = ({ accessories: initialAcc }: { accessories: AccessorySummary[] }) => {
  const { data: accessories } = useQuery({
    queryKey: ['accessory-stars'],
    queryFn: async (): Promise<AccessorySummary[]> => {
      const rs = await fetch(`${API2_SERVER_ROOT}/accessories?sort=liked&limit=50`)
      if (!rs.ok) throw new Error('Failed to fetch popular Accessory data from back-end')
      return await rs.json()
    },
    initialData: initialAcc,
  })

  return (
    <div id="content-container" className="leaderboard">
      <div className="text-container leaderboard-wrap">
        <h1 className="hero">Popular Accessories</h1>
        <section className="card" style={{ marginTop: 140 }}>
          <p>
            There&rsquo;s many different ways for <Link href="/mooncats">MoonCats</Link> to express their style, but
            several Accessories stand out as popular among many MoonCats and their Etherian friends. Check out which
            Accessories have collected the most &ldquo;stars&rdquo; from the Etherians they&rsquo;ve met:
          </p>
        </section>
        <section className="card">
          <ul className="zebra">
            {accessories.map((d) => (
              <li key={d.id}>
                <Link href={`/accessories/${d.id}`}>
                  <picture>
                    <img
                      className="inline"
                      alt=""
                      src={`${API_SERVER_ROOT}/accessory-image/${d.id}?scale=1&padding=2`}
                    />
                  </picture>
                  <h3>{d.name}</h3>
                  <div style={{ flex: '1 1 auto' }}>
                    {d.manager != zeroAddress && (
                      <>
                        managed by <EthereumAddress address={d.manager} mode="text" />
                      </>
                    )}
                  </div>
                  {d.likesCount && d.likesCount > 0 && (
                    <div className="icon-pill">
                      {d.likesCount}
                      <Icon style={{ marginLeft: '0.25rem', verticalAlign: '-1px' }} name="star-empty" />
                    </div>
                  )}
                </Link>
              </li>
            ))}
          </ul>
        </section>
      </div>
    </div>
  )
}
export default AccessoryLeaderboard
