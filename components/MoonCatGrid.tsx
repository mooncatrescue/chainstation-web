'use client'
import { useContext, useRef, useState } from 'react'
import MoonCatThumb from './MoonCatThumb'
import MoonCatViewSelector from './MoonCatViewSelector'
import AddToListButton from './AddToListButton'
import Filter from './Filter'
import Pagination from './Pagination'
import LoadingIndicator from './LoadingIndicator'
import useMoonCatListings from 'lib/useMoonCatListings'
import useSignedIn from 'lib/useSignedIn'
import { MoonCatData, MoonCatFilterSettings, SelectFieldMeta, TextFieldMeta } from 'lib/types'
import { MOONCAT_TRAITS_ARB, ONE_HOUR } from 'lib/util'
import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import { pad } from 'viem'
import { Listing } from 'lib/reservoirData'
import { keepPreviousData, useQuery } from '@tanstack/react-query'
import useTabbieHook from 'lib/useTabbieHook'
import { AppVisitorContext } from 'lib/AppVisitorProvider'

const DEFAULT_PER_PAGE = 50

const filterMeta: readonly (SelectFieldMeta | TextFieldMeta)[] = [
  {
    name: 'classification',
    type: 'select',
    label: 'Classification',
    defaultLabel: 'Any',
    options: {
      genesis: 'Genesis',
      rescue: 'Rescue',
    },
  } as SelectFieldMeta,
  {
    name: 'hue',
    type: 'select',
    label: 'Hue',
    defaultLabel: 'Any',
    options: {
      white: 'White',
      black: 'Black',
      red: 'Red',
      orange: 'Orange',
      yellow: 'Yellow',
      chartreuse: 'Chartreuse',
      green: 'Green',
      teal: 'Teal',
      cyan: 'Cyan',
      skyblue: 'SkyBlue',
      blue: 'Blue',
      purple: 'Purple',
      magenta: 'Magenta',
      fuchsia: 'Fuchsia',
    },
  } as SelectFieldMeta,
  {
    name: 'pale',
    type: 'select',
    label: 'Pale-Colored',
    defaultLabel: 'Any',
    options: {
      no: 'No',
      yes: 'Yes',
    },
  } as SelectFieldMeta,
  {
    name: 'facing',
    type: 'select',
    label: 'Facing',
    defaultLabel: 'Any',
    options: {
      right: 'Right',
      left: 'Left',
    },
  } as SelectFieldMeta,
  {
    name: 'expression',
    type: 'select',
    label: 'Expression',
    defaultLabel: 'Any',
    options: {
      smiling: 'Smiling',
      grumpy: 'Grumpy',
      pouting: 'Pouting',
      shy: 'Shy',
    },
  } as SelectFieldMeta,
  {
    name: 'pattern',
    type: 'select',
    label: 'Coat Pattern',
    defaultLabel: 'Any',
    options: {
      pure: 'Pure',
      tabby: 'Tabby',
      spotted: 'Spotted',
      tortie: 'Tortie',
    },
  } as SelectFieldMeta,
  {
    name: 'pose',
    type: 'select',
    label: 'Pose',
    defaultLabel: 'Any',
    options: {
      standing: 'Standing',
      sleeping: 'Sleeping',
      pouncing: 'Pouncing',
      stalking: 'Stalking',
    },
  } as SelectFieldMeta,
  {
    name: 'rescueYear',
    type: 'select',
    label: 'Rescue Year',
    defaultLabel: 'Any',
    options: {
      2017: '2017',
      2018: '2018',
      2019: '2019',
      2020: '2020',
      2021: '2021',
    },
  } as SelectFieldMeta,
  {
    name: 'named',
    type: 'select',
    label: 'Named',
    defaultLabel: 'Any',
    options: {
      no: 'Not Named',
      yes: 'Named',
      valid: 'String Names',
      invalid: 'Other Names',
    },
  } as SelectFieldMeta,
  { name: 'nameKeyword', label: 'Name Keyword' } as TextFieldMeta,
]

interface FilterParams {
  pageNumber: number
  perPage: number
  moonCats: number[] | 'all'
  filters: MoonCatFilterSettings
}

interface FilteredApiPage {
  moonCats: MoonCatData[]
  length: number
  totalLength: number
}

interface Props {
  children?: Parameters<typeof MoonCatThumb>[0]['thumbHandler']
  moonCats: number[] | 'all'
  perPage?: number
  extraParams?: Record<string, string>
  isEventActive?: boolean
  showListings?: boolean
}

const MoonCatGrid = ({
  children: thumbHandler,
  moonCats: allMoonCats,
  perPage,
  extraParams,
  isEventActive = false,
  showListings = true,
}: Props) => {
  const searchParams = useSearchParams()
  const router = useRouter()
  const pathname = usePathname()
  const { isSignedIn } = useSignedIn()
  const moonCatListings = useMoonCatListings()

  if (!perPage) {
    perPage = DEFAULT_PER_PAGE
  }

  let initialPage = 0
  // Parse the URL parameters to see if we should jump to a specific page
  const pagePreference = searchParams?.get('page') ?? null
  if (pagePreference != null) {
    let page = parseInt(pagePreference)
    if (page > 0) {
      // Page is valid; jump to it
      console.debug('Jumping to page:', page)
      initialPage = page - 1
    }
  }

  const [filterProps, setFilterProps] = useState<FilterParams>({
    pageNumber: initialPage,
    perPage: perPage,
    moonCats: allMoonCats,
    filters: {},
  })

  const params = new URLSearchParams(filterProps.filters as Record<string, string>)
  params.set('limit', String(perPage!))
  params.set('offset', String(filterProps.pageNumber * perPage!))
  if (Array.isArray(filterProps.moonCats)) {
    params.set('mooncats', filterProps.moonCats.join(','))
  } else {
    params.set('mooncats', filterProps.moonCats)
  }
  if (extraParams) {
    for (const key of Object.keys(extraParams)) {
      params.set(key, extraParams[key])
    }
  }

  const filteredData = useQuery({
    queryKey: ['filtered-mooncats', params.toString()],
    queryFn: async (): Promise<FilteredApiPage> => {
      const rs =
        params.get('mooncats') == 'all'
          ? await fetch(`/api/mooncats?${params.toString()}`)
          : await fetch(`/api/mooncats`, {
              method: 'POST',
              headers: { 'Content-Type': 'application/json' },
              body: JSON.stringify(Object.fromEntries(params)),
            })
      if (!rs.ok) throw new Error('Failed to fetch filtered data page')
      return (await rs.json()) as FilteredApiPage
    },
    staleTime: ONE_HOUR,
    placeholderData: keepPreviousData,
  })

  const {
    state: { awokenMoonCats },
  } = useContext(AppVisitorContext)
  const gridRef = useRef<HTMLDivElement>(null)

  useTabbieHook(filteredData.data?.moonCats, awokenMoonCats, (mc) => {
    const mcThumb = gridRef.current?.querySelector(`#mooncat-${mc.rescueOrder} .thumb-img`)
    if (!mcThumb) {
      console.warn('Failed to find table element for MoonCat', mc)
      return { x: window.innerWidth / 2, y: window.innerHeight / 2 }
    }
    const bounds = mcThumb.getBoundingClientRect()
    return {
      x: bounds.x + window.scrollX + bounds.width / 2,
      y: bounds.y + window.scrollY + bounds.height - 5,
    }
  })

  /**
   * Event handler for updates from MoonCatFilter component when a user picks a new filter value
   */
  function handleFilterUpdate(prop: keyof MoonCatFilterSettings, newValue: any) {
    setFilterProps((curProps) => {
      let newFilters = Object.assign({}, curProps.filters)
      if (newValue == '') {
        if (!curProps.filters[prop]) {
          // Already blank
          return curProps
        }
        delete newFilters[prop]
      } else {
        if (curProps.filters[prop] == newValue) {
          // Already set to that value
          return curProps
        }
        newFilters[prop] = newValue
      }

      return { ...curProps, filters: newFilters, pageNumber: 0 }
    })
  }

  /**
   * Event handler for updates from Pagination component when user navigates to a new page
   */
  function handlePageUpdate(newPage: number) {
    setFilterProps((curProps) => {
      if (curProps.pageNumber == newPage) {
        // Already set to that value
        return curProps
      }

      // Update the query parameter in the URL.
      const current = searchParams ? new URLSearchParams(Array.from(searchParams.entries())) : new URLSearchParams()
      current.delete('page')
      current.set('page', String(newPage + 1))

      router.replace(`${pathname}?${current}${window.location.hash}`, { scroll: false })
      return { ...curProps, pageNumber: newPage }
    })
  }

  const listingMap: Record<number, Listing> = {}
  if (moonCatListings.data) {
    for (const l of moonCatListings.data) {
      listingMap[l.moonCat] = l
    }
  }
  if (!filteredData.data) return <LoadingIndicator message="Herding all the cats..." />

  return (
    <div className="mooncat-grid">
      <div
        style={{
          display: 'flex',
          alignItems: 'baseline',
          padding: '1rem 0',
        }}
        className="text-scrim"
      >
        {filteredData.data.totalLength > 1 && (
          <Filter
            currentFilters={filterProps.filters}
            filterMeta={filterMeta}
            filteredCount={filteredData.data.length}
            totalCount={filteredData.data.totalLength}
            label="MoonCats"
            onChange={handleFilterUpdate}
          />
        )}
        <div style={{ flexGrow: 10 }} />
        <MoonCatViewSelector isEventActive={isEventActive} />
        {isSignedIn && (
          <div style={{ paddingLeft: '1rem' }}>
            <AddToListButton
              targetAddress={MOONCAT_TRAITS_ARB}
              title="Click to add this page of MoonCats to your saved lists"
              targetItems={filteredData.data.moonCats.map((mc) => pad(mc.catId as `0x${string}`, { size: 32 }))}
            />
          </div>
        )}
      </div>
      <div className="item-grid" style={{ margin: '0 0 2rem' }} ref={gridRef}>
        {filteredData.data.moonCats.map((mc) => (
          <MoonCatThumb
            key={mc.rescueOrder}
            id={`mooncat-${mc.rescueOrder}`}
            moonCat={mc}
            listing={showListings ? listingMap[mc.rescueOrder] : undefined}
            isEventActive={isEventActive}
            isAwoken={awokenMoonCats.has(mc.rescueOrder)}
            thumbHandler={thumbHandler}
          />
        ))}
      </div>
      <Pagination
        currentPage={filterProps.pageNumber}
        maxPage={Math.ceil(filteredData.data.length / perPage) - 1}
        setCurrentPage={handlePageUpdate}
      />
    </div>
  )
}
export default MoonCatGrid
