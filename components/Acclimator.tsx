'use client'
import { useQuery } from '@tanstack/react-query'
import { useCallback, useContext, useState } from 'react'
import { Address, erc721Abi, hexToNumber, numberToHex, parseAbi } from 'viem'
import { usePublicClient } from 'wagmi'
import { readContract, readContracts } from 'wagmi/actions'
import AcclimatedCatTree from './AcclimatedCatTree'
import Icon from './Icon'
import LoadingIndicator from './LoadingIndicator'
import ShinyButton from './ShinyButton'
import SignInFirst from './SignInFirst'
import TokenPicker from './TokenPicker'
import WarningIndicator from './WarningIndicator'
import { ActionQueueContext } from 'lib/ActionsQueueProvider'
import getMoonCatData from 'lib/getMoonCatData'
import { AcclimateAction, constructContractStep, stepIsComplete } from 'lib/onchainActions'
import { TokenMeta } from 'lib/tokens'
import { OwnedMoonCat } from 'lib/types'
import { ACCLIMATOR_ADDRESS, API2_SERVER_ROOT, API_SERVER_ROOT, RESCUE_ADDRESS, ZWS } from 'lib/util'
import useDialog from 'lib/useDialog'
import { AppEventHandler, AppEventType, useGlobalEvents } from 'lib/useGlobalEvents'
import useSignedIn from 'lib/useSignedIn'
import useTxStatus from 'lib/useTxStatus'
import Link from 'next/link'
import { config } from 'lib/wagmi-config'

const OLD_WRAPPER = {
  address: '0x7C40c393DC0f283F318791d746d894DdD3693572' as Address,
  chainId: 1,
  abi: [
    ...erc721Abi,
    ...parseAbi([
      'function _tokenIDToCatID(uint256) external view returns (bytes5)',
      'function tokenOfOwnerByIndex(address,uint256) external view returns (uint256)',
    ]),
  ] as const,
}
const ACCLIMATOR = {
  address: ACCLIMATOR_ADDRESS as Address,
  chainId: 1,
  abi: parseAbi([
    'function wrap(uint256 rescueOrder) external returns (uint256)',
    'function batchWrap(uint256[] rescueOrders) external',
    'function batchReWrap(uint256[] rescueOrders, uint256[] oldTokenIds) external',
  ]),
}
const RESCUE = {
  address: RESCUE_ADDRESS as Address,
  chainId: 1,
  abi: parseAbi([
    'function makeAdoptionOfferToAddress(bytes5 catId, uint price, address to) external',
    'function adoptionOffers(bytes5 catId) external view returns ((bool exists, bytes5 catId, address seller, uint price, address onlyOfferTo))',
  ]),
}

const MAX_ACCLIMATION_COUNT = 20

function getSelectionMode(selectedTokens: TokenMeta[]) {
  return selectedTokens.length > 0
    ? selectedTokens[0].collection.label == 'Wrapped MoonCat'
      ? 'old-wrapped'
      : 'original'
    : 'none'
}

const AcclimationWizard = ({
  selectedTokens,
  owner,
  onDone,
}: {
  selectedTokens: TokenMeta[]
  owner: Address
  onDone: () => void
}) => {
  const { dispatch } = useContext(ActionQueueContext)
  const { viewMessage, isProcessing, setStatus, processTransaction } = useTxStatus()
  const [isComplete, setIsComplete] = useState<boolean>(false)
  const publicClient = usePublicClient()
  const selectionMode = getSelectionMode(selectedTokens)

  async function doAction(action: AcclimateAction) {
    if (!publicClient) return
    setStatus('building')
    for (let i = 0; i < action.steps.length; i++) {
      const step = action.steps[i]
      if (stepIsComplete(step)) continue

      if (step.type !== 'CONTRACT') {
        setStatus('error', 'Cosmic microwave interference! Failed to parse that action')
        return
      }

      // Do the Contract interaction

      // First, if there is a precheck, check it
      if (step.precheck && (await step.precheck(readContract))) {
        // Precheck passed, so this step can be skipped
        action.steps[i] = { ...step, skipped: true }
        dispatch({
          type: 'UPDATE',
          payload: action,
        })
        continue
      }

      // No precheck, or precheck failed. Do the transaction.
      const rs = await processTransaction(step.config)
      if (!rs) {
        setStatus('error', 'Communications jammed! Transaction could not be sent')
        return
      }
      if (rs.status == 'reverted') {
        setStatus('error', 'Signal lost... Transaction was added to the blockchain, but reverted.')
        return
      }

      // Interaction was successful. Mark this step as complete
      const block = await publicClient.getBlock({ blockNumber: rs.blockNumber })
      action.steps[i] = {
        ...step,
        tx: {
          hash: rs.transactionHash,
          timestamp: Number(block.timestamp),
        },
      }
      dispatch({
        type: 'UPDATE',
        payload: action,
      })
    }
    setIsComplete(true)
  }

  /**
   * Turn the selected tokens into an Acclimation action, and start processing it.
   */
  async function startAcclimation() {
    setStatus('building')
    const steps: AcclimateAction['steps'] = []
    if (selectionMode == 'old-wrapped') {
      if (selectedTokens.length == 1) {
        /**
         * Individual old-wrapped MoonCat
         */

        const mc = selectedTokens[0]
        // Just transfer the one token directly to the Acclimator
        steps.push(
          constructContractStep({
            type: 'CONTRACT',
            label: `Transfer Wrapped MoonCat #${hexToNumber(mc.id)} (rescue order ${
              mc.extra!.rescueOrder
            }) to the Acclimator`,
            config: {
              ...OLD_WRAPPER,
              functionName: 'safeTransferFrom',
              args: [
                owner,
                ACCLIMATOR_ADDRESS as Address,
                BigInt(mc.id),
                numberToHex(mc.extra!.rescueOrder, { size: 32 }),
              ],
            },
            userAddress: owner,
          })
        )
      } else {
        /**
         * Multiple old-wrapped MoonCats
         */

        // First, approve the Acclimator to transfer the tokens
        steps.push(
          constructContractStep({
            type: 'CONTRACT',
            label: 'Approve the Acclimator to transfer your Wrapped MoonCats',
            config: {
              ...OLD_WRAPPER,
              functionName: 'setApprovalForAll',
              args: [ACCLIMATOR_ADDRESS, true],
            },
            precheck: async (rc) => {
              const rs = await rc(config, {
                ...OLD_WRAPPER,
                functionName: 'isApprovedForAll',
                args: [owner, ACCLIMATOR_ADDRESS as Address],
              })
              return rs
            },
            userAddress: owner,
          })
        )

        // Then, trigger a batch wrap of the old-wrapped MoonCats
        steps.push(
          constructContractStep({
            type: 'CONTRACT',
            label: `Acclimate Wrapped MoonCats ${selectedTokens
              .map((t) => `#${hexToNumber(t.id)} (rescue order ${t.extra!.rescueOrder})`)
              .join(', ')}`,
            config: {
              ...ACCLIMATOR,
              functionName: 'batchReWrap',
              args: [selectedTokens.map((t) => BigInt(t.extra!.rescueOrder)), selectedTokens.map((t) => BigInt(t.id))],
            },
            userAddress: owner,
          })
        )
      }
    } else {
      if (selectedTokens.length == 1) {
        /**
         * Individual original MoonCat
         */

        const mc = selectedTokens[0]
        // Offer it to the Acclimator
        steps.push(
          constructContractStep({
            type: 'CONTRACT',
            label: `Offer MoonCat ${mc.extra!.rescueOrder} (${mc.id}) to the Acclimator`,
            config: {
              ...RESCUE,
              functionName: 'makeAdoptionOfferToAddress',
              args: [mc.id, BigInt(0), ACCLIMATOR_ADDRESS],
            },
            precheck: async (rc) => {
              const rs = await rc(config, {
                ...RESCUE,
                functionName: 'adoptionOffers',
                args: [mc.id],
              })
              return rs.exists && rs.onlyOfferTo == ACCLIMATOR_ADDRESS
            },
            userAddress: owner,
          })
        )

        // Then, wrap the MoonCat
        steps.push(
          constructContractStep({
            type: 'CONTRACT',
            label: `Acclimate MoonCat ${mc.extra!.rescueOrder} (${mc.id})`,
            config: {
              ...ACCLIMATOR,
              functionName: 'wrap',
              args: [BigInt(mc.extra!.rescueOrder)],
            },
            userAddress: owner,
          })
        )
      } else {
        /**
         * Multiple original MoonCats
         */

        // First, approve the Acclimator to transfer each of the tokens
        for (const token of selectedTokens) {
          steps.push(
            constructContractStep({
              type: 'CONTRACT',
              label: `Offer MoonCat ${token.extra!.rescueOrder} (${token.id}) to the Acclimator`,
              config: {
                ...RESCUE,
                functionName: 'makeAdoptionOfferToAddress',
                args: [token.id, BigInt(0), ACCLIMATOR_ADDRESS],
              },
              userAddress: owner,
            })
          )
        }

        // Then, batch wrap the MoonCats
        steps.push(
          constructContractStep({
            type: 'CONTRACT',
            label: `Acclimate MoonCats ${selectedTokens.map((t) => `#${t.extra!.rescueOrder})`).join(', ')}`,
            config: {
              ...ACCLIMATOR,
              functionName: 'batchWrap',
              args: [selectedTokens.map((t) => BigInt(t.extra!.rescueOrder))],
            },
            userAddress: owner,
          })
        )
      }
    }

    const action: AcclimateAction = {
      label: 'Acclimate the MoonCats',
      id: `acclimate-${crypto.getRandomValues(new Uint32Array(3)).join('')}`,
      fromAddress: owner,
      steps: steps,
    }

    dispatch({
      type: 'UPDATE',
      payload: action,
    })

    // Do the action now
    doAction(action)
  }

  let txDescription: string = '' // a description of what transactions the user will be prompted for, for the current token selection
  if (selectionMode == 'old-wrapped') {
    // Old-wrapped MoonCats
    if (selectedTokens.length == 1) {
      txDescription =
        'You will be prompted for a single transaction, to transfer this Wrapped MoonCat to the Acclimator.'
    } else {
      txDescription = `You will be prompted for two transactions: first to approve the Acclimator to be able to transfer your Wrapped MoonCats, and then to transfer them.`
    }
  } else if (selectionMode == 'original') {
    // Original MoonCats
    if (selectedTokens.length == 1) {
      txDescription =
        'You will be prompted for two transactions: first to offer your MoonCat to the Acclimator, and then to accept the offer.'
    } else {
      txDescription = `You will be prompted for one transaction for each MoonCat to first offer it to the Acclimator, and then a final transaction to accept all the offers.`
    }
  }

  const moonCatList = selectedTokens.map((t) => hexToNumber(t.extra!.rescueOrder)).sort((a, b) => a - b)
  if (isComplete) {
    return (
      <>
        <h2>Acclimation Complete</h2>
        <AcclimatedCatTree moonCats={moonCatList} />
        <p>
          The acclimation process has been completed successfully. Your newly-Acclimated friends will be arriving in{' '}
          <Link href={`/acclimator/lounge/${owner}`}>the lounge</Link> shortly.
        </p>
        <p>
          <button
            onClick={(e) => {
              e.preventDefault()
              e.stopPropagation()
              onDone()
            }}
          >
            Close
          </button>
        </p>
      </>
    )
  }

  return (
    <>
      <h2>Acclimate</h2>
      <p>
        You have selected {selectedTokens.length} {selectedTokens.length == 1 ? 'MoonCat' : 'MoonCats'} to Acclimate.
      </p>
      <AcclimatedCatTree moonCats={moonCatList} />
      <p style={{ marginTop: '1em' }}>{txDescription}</p>
      <p>
        <ShinyButton
          onClick={(e) => {
            e.preventDefault()
            e.stopPropagation()
            startAcclimation()
          }}
          disabled={isProcessing}
        >
          Acclimate!
          <Icon name="rocket" style={{ verticalAlign: -3, marginLeft: '0.5em' }} />
        </ShinyButton>
      </p>
      {viewMessage}
    </>
  )
}

const PickForAcclimation = () => {
  const { verifiedAddresses } = useSignedIn()
  const [selectedTokens, setSelectedTokens] = useState<TokenMeta[]>([])
  const { ref, setIsOpen } = useDialog()

  const targetAddress = verifiedAddresses[0].address

  const { status, data: tokenMeta } = useQuery({
    queryKey: ['not-acclimated-tokens', targetAddress],
    queryFn: async (): Promise<TokenMeta[]> => {
      // Enumerate owned tokens in old wrapper contract
      const wrappedTokenCount = await readContract(config, {
        ...OLD_WRAPPER,
        functionName: 'balanceOf',
        args: [targetAddress],
      })
      // Get list of token IDs from old wrapper contract
      const wrappedTokenIds = await readContracts(config, {
        contracts: Array.from(
          { length: Number(wrappedTokenCount) },
          (_, i) =>
            ({
              ...OLD_WRAPPER,
              functionName: 'tokenOfOwnerByIndex',
              args: [targetAddress, BigInt(i)],
            } as const)
        ),
        allowFailure: false,
      })

      // Convert old wrapper token IDs to original contract Hex IDs
      const wrappedTokenHexIds = await readContracts(config, {
        contracts: wrappedTokenIds.map((id) => ({
          ...OLD_WRAPPER,
          functionName: '_tokenIDToCatID',
          args: [id],
        })),
        allowFailure: false,
      })
      const oldWrappedMoonCats = getMoonCatData(wrappedTokenHexIds)
        .map(
          (mc, i): TokenMeta => ({
            collection: {
              address: OLD_WRAPPER.address,
              label: 'Wrapped MoonCat',
            },
            id: numberToHex(wrappedTokenIds[i]),
            name: `#${wrappedTokenIds[i]} (rescue order ${mc.rescueOrder})`,
            imageSrc: `${API_SERVER_ROOT}/image/${mc.rescueOrder}?scale=3&padding=5&costumes=true`,
            extra: {
              rescueOrder: mc.rescueOrder,
            },
          })
        )
        .sort((a, b) => a.extra!.rescueOrder - b.extra!.rescueOrder)

      // Enumerate tokens in original MoonCatRescue contract
      const rs = await fetch(`${API2_SERVER_ROOT}/owner-profile/${targetAddress}`)
      if (!rs.ok) {
        console.error('Failed to fetch owner information', rs)
        // Just return the list of MoonCats we have data for
        return oldWrappedMoonCats
      }

      const ownerProfile = await rs.json()
      const originalMoonCats: TokenMeta[] = getMoonCatData(
        (ownerProfile.ownedMoonCats as OwnedMoonCat[])
          .filter((mc) => mc.collection.name == 'Original')
          .map((mc) => mc.rescueOrder)
      )
        .sort((a, b) => a.rescueOrder - b.rescueOrder)
        .map(
          (mc): TokenMeta => ({
            collection: {
              address: RESCUE_ADDRESS,
              label: 'MoonCat',
            },
            id: mc.catId,
            name: `#${mc.rescueOrder}`,
            imageSrc: `${API_SERVER_ROOT}/image/${mc.rescueOrder}?scale=3&padding=5&costumes=true`,
            extra: {
              rescueOrder: mc.rescueOrder,
            },
          })
        )

      // Merge lists and sort by rescue order
      return [...oldWrappedMoonCats, ...originalMoonCats]
    },
    enabled: !!targetAddress,
  })

  const handleGlobalEvent: AppEventHandler = useCallback(
    (e) => {
      if (e.type !== AppEventType.CLOSE_DIALOGS) return
      setIsOpen(false)
    },
    [setIsOpen]
  )
  useGlobalEvents(handleGlobalEvent)

  if (status == 'pending') {
    return <LoadingIndicator />
  }
  if (status == 'error') {
    return <WarningIndicator message="Meteor shower! Failed to enumerate owned MoonCats" />
  }

  if (tokenMeta.length == 0) {
    return (
      <section className="card">
        <p>You have no MoonCats able to be acclimated.</p>
      </section>
    )
  }

  const selectionMode = getSelectionMode(selectedTokens)
  let selectionDescription: React.ReactNode = null // a description of the current token selection
  if (selectionMode == 'old-wrapped') {
    // Old-wrapped MoonCats
    selectionDescription = <p>Acclimating MoonCats from the old wrapper contract</p>
  } else if (selectionMode == 'original') {
    // Original MoonCats
    selectionDescription = <p>Acclimating MoonCats from the original MoonCat{ZWS}Rescue contract</p>
  }

  return (
    <section className="card">
      <p>
        You have {tokenMeta.length} {tokenMeta.length == 1 ? 'MoonCat' : 'MoonCats'} able to be acclimated.
      </p>
      <TokenPicker
        style={{ fontSize: '0.8rem', columnWidth: '20em' }}
        tokens={tokenMeta.map((t) => ({
          ...t,
          isSelected: !!selectedTokens.find((s) => t.id == s.id),
          isDisabled:
            (selectionMode == 'old-wrapped' && t.collection.label == 'MoonCat') ||
            (selectionMode == 'original' && t.collection.label == 'Wrapped MoonCat'),
        }))}
        onClick={(t) => {
          setSelectedTokens((curVal) => {
            if (curVal.find((s) => s.id == t.id)) {
              return curVal.filter((s) => s.id != t.id)
            }
            return [...curVal, t]
          })
        }}
      />
      <div style={{ marginTop: '2em', textAlign: 'center' }}>
        {selectionDescription}
        {selectedTokens.length > MAX_ACCLIMATION_COUNT && (
          <WarningIndicator
            message={`You cannot acclimate more than ${MAX_ACCLIMATION_COUNT} MoonCats in one batch.`}
          />
        )}
        <ShinyButton
          onClick={(e) => {
            e.preventDefault()
            e.stopPropagation()
            console.log('selectedTokens', selectedTokens)
            setIsOpen(true)
          }}
          disabled={selectedTokens.length == 0 || selectedTokens.length > MAX_ACCLIMATION_COUNT}
        >
          Engage The Acclimator!
        </ShinyButton>
      </div>
      {selectedTokens.length > 0 && (
        <dialog
          className="wizard"
          ref={ref}
          style={{ textAlign: 'center' }}
          onCancel={() => setIsOpen(false)}
          onClick={(e) => e.stopPropagation()}
        >
          <AcclimationWizard
            owner={targetAddress}
            selectedTokens={selectedTokens}
            onDone={() => {
              setSelectedTokens([])
              setIsOpen(false)
            }}
          />
          <div
            style={{ position: 'absolute', top: 0, right: 0, padding: '1em', cursor: 'pointer' }}
            onClick={() => setIsOpen(false)}
          >
            X
          </div>
        </dialog>
      )}
    </section>
  )
}

const Acclimator = () => {
  return (
    <SignInFirst>
      <PickForAcclimation />
    </SignInFirst>
  )
}
export default Acclimator
