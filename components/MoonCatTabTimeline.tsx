import React from 'react'
import {
  ACCESSORIES_ADDRESS,
  ACCLIMATOR_ADDRESS,
  API2_SERVER_ROOT,
  FIVE_MINUTES,
  JUMPPORT_ADDRESS,
  MOMENTS_ADDRESS,
  RESCUE_ADDRESS,
} from 'lib/util'
import { TabProps } from './MoonCatDetail'
import LoadingIndicator from './LoadingIndicator'
import { Moment } from 'lib/types'
import { BlockchainEvent } from 'lib/blockchainEvents'
import { zeroAddress } from 'viem'
import EventTimeline from './EventTimeline'
import { useInfiniteQuery } from '@tanstack/react-query'

import momentsData from 'lib/moments_meta.json'
const moments = momentsData as Moment[]

/**
 * Reformat Moment minting data into the shape needed for timeline display
 */
const MOMENT_RELEASES: BlockchainEvent[] = moments.map((m) => ({
  args: {
    momentId: m.momentId,
    URI: m.tokenURI,
    rescueOrders: m.moonCats,
  },
  eventContract: MOMENTS_ADDRESS,
  logIndex: 0,
  tx: {
    data: '0xac8ad13e',
    from: m.tx.from,
    to: m.tx.to,
    value: '0',
    hash: m.tx.hash,
  },
  blockNumber: m.tx.blockNumber,
  eventName: 'MomentMint',
  timestamp: m.tx.timestamp,
}))

const PAGE_SIZE = 50
const MoonCatTabTimeline = ({ moonCat, details }: TabProps) => {
  const {
    data: allEvents,
    fetchNextPage,
    hasNextPage,
  } = useInfiniteQuery({
    queryKey: ['events', `moonCat:${moonCat.rescueOrder}`, `limit:${PAGE_SIZE}`],
    queryFn: async ({ pageParam }: { pageParam: string | null }): Promise<BlockchainEvent[]> => {
      const params = new URLSearchParams({
        limit: String(PAGE_SIZE),
        mooncat: String(moonCat.rescueOrder),
      })
      if (pageParam != null) params.set('start_after', pageParam)
      const rs = await fetch(`${API2_SERVER_ROOT}/events?${params}`)
      if (!rs.ok) throw new Error('Failed to get blockchain event data')
      return await rs.json()
    },
    staleTime: FIVE_MINUTES,
    initialPageParam: null,
    getNextPageParam: (lastPage) => {
      if (lastPage.length == 0) return null
      const lastEntry = lastPage[lastPage.length - 1]
      return `${lastEntry.blockNumber}-${lastEntry.logIndex}`
    },
  })

  const events = allEvents
    ? allEvents.pages.flat().filter((e) => {
        // Skip transfer events on the Acclimator contract that are duplicated by more-specific events
        if (
          e.eventName == 'Transfer' &&
          e.eventContract == ACCLIMATOR_ADDRESS &&
          (e.args.from == zeroAddress ||
            e.args.to == zeroAddress ||
            e.args.from == JUMPPORT_ADDRESS ||
            e.args.to == JUMPPORT_ADDRESS)
        )
          return false

        // Skip transfer events that didn't actually go anywhere
        if (e.eventName == 'CatAdopted' && e.eventContract == RESCUE_ADDRESS && e.args.from == e.args.to) return false
        if (e.eventName == 'Transfer' && e.eventContract == ACCLIMATOR_ADDRESS && e.args.from == e.args.to) return false

        // Skip accessory applied events
        if (e.eventName == 'AccessoryApplied' && e.eventContract == ACCESSORIES_ADDRESS) return false

        return true
      })
    : []

  if (events.length > 0) {
    const maxTimestamp = events[0].timestamp
    const minTimestamp = events[events.length - 1].timestamp

    // Find if any MoonCatMoments include them
    let doSort = false
    MOMENT_RELEASES.filter((e) => e.timestamp >= minTimestamp && e.timestamp <= maxTimestamp).forEach((e) => {
      if (e.args.rescueOrders.includes(moonCat.rescueOrder)) {
        events.push(e)
        doSort = true
      }
    })
    if (doSort) {
      events.sort((a, b) => b.timestamp - a.timestamp)
    }
  }

  return (
    <>
      <div className="text-container">
        <section className="card-help">
          MoonCats have been around on the blockchain for quite a while now! Here&rsquo;s a record of all the things
          this MoonCat has done, starting from the most recent:
        </section>
      </div>
      {events.length > 0 && (
        <EventTimeline
          events={events}
          subject={{ type: 'mooncat', id: moonCat.catId, rescueOrder: moonCat.rescueOrder }}
          loadNext={hasNextPage ? fetchNextPage : undefined}
        />
      )}
      {events.length == 0 && <LoadingIndicator className="text-scrim" />}
    </>
  )
}
export default MoonCatTabTimeline
