import { COLORS } from 'lib/util'

const ColorPicker = ({ highlighted, onClick }: { highlighted?: number; onClick?: (colorIndex: number) => void }) => {
  const ColorCell = ({ colorIndex }: { colorIndex: number }) => {
    const c = COLORS[colorIndex]
    const className = colorIndex == highlighted ? 'color-cell highlighted' : 'color-cell'
    return (
      <div
        className={className}
        title={c.label}
        style={{ cursor: 'pointer', backgroundColor: `rgb(${c.r}  ${c.g}  ${c.b} / ${(c.a / 255).toFixed(2)})` }}
        onClick={(e) => {
          if (typeof onClick == 'function') onClick(colorIndex)
        }}
      ></div>
    )
  }
  const ColorCells = ({ start, end }: { start: number; end: number }) => {
    let cells = []
    for (let i = start; i <= end; i++) {
      cells.push(<ColorCell key={i} colorIndex={i} />)
    }
    return <>{cells}</>
  }

  return (
    <section className="color-picker" style={{ maxWidth: 400 }}>
      <div className="color-picker-row">
        <div className="color-picker-row-label">Neutrals</div>
        <ColorCells start={1} end={7} />
        <ColorCells start={64} end={70} />
      </div>
      <hr />
      <div className="color-picker-row">
        <div className="color-picker-row-label">Pale</div>
        <ColorCells start={57} end={63} />
      </div>
      <div className="color-picker-row">
        <div className="color-picker-row-label">Light</div>
        <ColorCell colorIndex={8} />
        <ColorCell colorIndex={11} />
        <ColorCell colorIndex={14} />
        <ColorCell colorIndex={17} />
        <ColorCell colorIndex={20} />
        <ColorCell colorIndex={23} />
        <ColorCell colorIndex={26} />
        <ColorCell colorIndex={29} />
        <ColorCell colorIndex={32} />
        <ColorCell colorIndex={35} />
        <ColorCell colorIndex={38} />
        <ColorCell colorIndex={41} />
        <ColorCell colorIndex={44} />
        <ColorCell colorIndex={47} />
      </div>
      <div className="color-picker-row">
        <div className="color-picker-row-label">Normal</div>
        <ColorCell colorIndex={9} />
        <ColorCell colorIndex={12} />
        <ColorCell colorIndex={15} />
        <ColorCell colorIndex={18} />
        <ColorCell colorIndex={21} />
        <ColorCell colorIndex={24} />
        <ColorCell colorIndex={27} />
        <ColorCell colorIndex={30} />
        <ColorCell colorIndex={33} />
        <ColorCell colorIndex={36} />
        <ColorCell colorIndex={39} />
        <ColorCell colorIndex={42} />
        <ColorCell colorIndex={45} />
        <ColorCell colorIndex={48} />
      </div>
      <div className="color-picker-row">
        <div className="color-picker-row-label">Dark</div>
        <ColorCell colorIndex={10} />
        <ColorCell colorIndex={13} />
        <ColorCell colorIndex={16} />
        <ColorCell colorIndex={19} />
        <ColorCell colorIndex={22} />
        <ColorCell colorIndex={25} />
        <ColorCell colorIndex={28} />
        <ColorCell colorIndex={31} />
        <ColorCell colorIndex={34} />
        <ColorCell colorIndex={37} />
        <ColorCell colorIndex={40} />
        <ColorCell colorIndex={43} />
        <ColorCell colorIndex={46} />
        <ColorCell colorIndex={49} />
      </div>
      <div className="color-picker-row">
        <div className="color-picker-row-label">Deep</div>
        <ColorCells start={50} end={56} />
      </div>
      <hr />
      <div className="color-picker-row">
        <div className="color-picker-row-label">Greyed</div>
        <ColorCells start={71} end={77} />
      </div>
      <div className="color-picker-row">
        <div className="color-picker-row-label">Smoked</div>
        <ColorCells start={78} end={91} />
      </div>
      <div className="color-picker-row">
        <div className="color-picker-row-label">Stained</div>
        <ColorCells start={92} end={105} />
      </div>
      <div className="color-picker-row">
        <div className="color-picker-row-label">Tinted</div>
        <ColorCells start={106} end={112} />
      </div>
      <hr />
      <div className="color-picker-row">
        <div className="color-picker-row-label">MoonCat</div>
        <ColorCells start={114} end={127} />
      </div>
    </section>
  )
}
export default ColorPicker
