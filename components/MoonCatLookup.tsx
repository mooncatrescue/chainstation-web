'use client'
import { useRouter } from 'next/navigation'
import { CSSProperties, useState } from 'react'
import WarningIndicator from './WarningIndicator'

const MoonCatLookup = ({ style }: { style?: CSSProperties }) => {
  const [searchId, setSearchId] = useState<string>('')
  const [errorMessage, setErrorMessage] = useState<string>('')
  const router = useRouter()

  function doLookup() {
    setErrorMessage('')
    if (/^0x[0-9a-f]{10}$/i.test(searchId.toLowerCase())) {
      // searchId is a MoonCat Hex ID
      if (Number.isNaN(Number(searchId)) || searchId.length != 12) {
        setErrorMessage('Not a valid MoonCat Hex ID')
        return
      }
    } else {
      // searchId is numeric
      const rescueOrder = Number(searchId)
      if (Number.isNaN(rescueOrder)) {
        setErrorMessage('Search term needs to be numeric')
        return
      }
      if (rescueOrder < 0 || rescueOrder >= 25440) {
        setErrorMessage('Not a valid rescue order')
        return
      }
    }

    router.push(`/mooncats/${searchId}`)
  }

  return (
    <div className="mooncat-lookup" style={style}>
      <p style={{ textAlign: 'center' }}>
        <input
          type="text"
          value={searchId}
          onKeyDown={(e) => {
            if (e.key == 'Enter') doLookup()
          }}
          onChange={(e) => setSearchId(e.target.value)}
          style={{ marginRight: '1em' }}
          placeholder="MoonCat ID"
          autoCorrect="off"
          autoCapitalize="off"
          spellCheck="false"
        />
        <button
          onClick={(e) => {
            e.preventDefault()
            doLookup()
          }}
        >
          Lookup
        </button>
      </p>
      {errorMessage && <WarningIndicator style={{ textAlign: 'center' }} message={errorMessage} />}
    </div>
  )
}
export default MoonCatLookup
