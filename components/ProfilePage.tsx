'use client'
import useSignedIn from 'lib/useSignedIn'
import SignInFirst from 'components/SignInFirst'
import MyInfo from './MyInfo'

export interface TabProps {
  addresses: ReturnType<typeof useSignedIn>['verifiedAddresses']
}

const ProfilePage = () => {
  const { verifiedAddresses } = useSignedIn()

  return (
    <div id="content-container">
      <div className="text-container">
        <h1 className="hero">My Profile</h1>
        <SignInFirst className="card-help">
          <MyInfo addresses={verifiedAddresses} />
        </SignInFirst>
      </div>
    </div>
  )
}
export default ProfilePage
