import useTxStatus from 'lib/useTxStatus'
import {
  ACCESSORIES_ADDRESS,
  COSTUME_ACCESSORIES,
  formatAccessoryMetabyte,
  interleave,
  parseAccessoryMetabyte,
} from 'lib/util'
import { useRouter } from 'next/navigation'
import { ChangeEventHandler, useState } from 'react'
import { SelectField } from './FormFields'
import { AccessoryContractData, AccessoryTraits } from 'lib/types'
import { Address, formatEther, numberToHex, parseAbi, zeroAddress } from 'viem'
import EthereumAddress from './EthereumAddress'
import StarButton from './StarButton'
import EthAmount from './EthAmount'

const ACCESSORIES = {
  address: ACCESSORIES_ADDRESS as Address,
  chainId: 1,
  abi: parseAbi(['function setMetaByte(uint256 accessoryId, uint8 metabyte) external']),
}

/**
 * Form field for updating the metabyte value of an Accessory
 * Action that only the owner of the Accessories contract overall can do, to curate the collection
 */
const AdminTools = ({ accessoryId, metabyte }: { accessoryId: number; metabyte: number }) => {
  const [updatedMetabyte, setMetabyte] = useState(parseAccessoryMetabyte(metabyte))
  const { isProcessing, viewMessage, processTransaction } = useTxStatus()
  const router = useRouter()

  /**
   * Submit button click handler
   * Make the on-chain transaction to update this Accessory
   */
  async function updateMeta() {
    const rs = await processTransaction({
      ...ACCESSORIES,
      functionName: 'setMetaByte',
      args: [BigInt(accessoryId), formatAccessoryMetabyte(updatedMetabyte)],
    })
    if (rs) {
      router.refresh()
    }
  }

  /**
   * Form field change handler
   * A form field has been updated; update the local state value of pending metabyte changes.
   */
  const handleChange: ChangeEventHandler<HTMLSelectElement> = function (e) {
    const propName = e.target.name as keyof typeof updatedMetabyte
    const newValue = e.target.value
    setMetabyte((curVal) => {
      const newMeta = { ...curVal }
      switch (propName) {
        case 'audience':
          newMeta.audience = Number(newValue)
          break
        default:
          newMeta[propName] = newValue === 'yes'
      }
      return newMeta
    })
  }

  const boolOpts = {
    yes: 'Yes',
    no: 'No',
  }
  return (
    <div className="admin-tools">
      <div className="form-grid" style={{ gridTemplateColumns: 'auto auto' }}>
        <SelectField
          meta={{
            type: 'select',
            name: 'verified',
            label: 'Verified',
            options: boolOpts,
          }}
          currentValue={updatedMetabyte.verified ? 'yes' : 'no'}
          onChange={handleChange}
        />
        <SelectField
          meta={{
            type: 'select',
            name: 'audience',
            label: 'Audience',
            options: {
              0: 'Everyone',
              1: 'Teen',
              2: 'Mature',
              3: 'Adult',
            },
          }}
          currentValue={updatedMetabyte.audience}
          onChange={handleChange}
        />
        <SelectField
          meta={{
            type: 'select',
            name: 'mirrorPlacement',
            label: 'Mirror Placement',
            options: boolOpts,
          }}
          currentValue={updatedMetabyte.mirrorPlacement ? 'yes' : 'no'}
          onChange={handleChange}
        />
        <SelectField
          meta={{
            type: 'select',
            name: 'mirrorAccessory',
            label: 'Mirror Accessory',
            options: boolOpts,
          }}
          currentValue={updatedMetabyte.mirrorAccessory ? 'yes' : 'no'}
          onChange={handleChange}
        />
        <SelectField
          meta={{
            type: 'select',
            name: 'background',
            label: 'Is Background?',
            options: boolOpts,
          }}
          currentValue={updatedMetabyte.background ? 'yes' : 'no'}
          onChange={handleChange}
        />
        <div style={{ gridColumnEnd: 'span 2', textAlign: 'center' }}>
          <button disabled={isProcessing} onClick={updateMeta}>
            Update Metabyte
          </button>
          {viewMessage}
        </div>
      </div>
    </div>
  )
}

const AccessoryInfoCard = ({
  accessoryId,
  accessoryDetail,
  accessoryTraits,
  showAdminTools,
  onPaletteChanged,
}: {
  accessoryId: number
  accessoryDetail: AccessoryContractData
  accessoryTraits?: AccessoryTraits
  showAdminTools: boolean
  onPaletteChanged?: (paletteIndex: number) => void
}) => {
  const metadata = parseAccessoryMetabyte(accessoryDetail.metabyte)
  const audienceLabel = ['Everyone', 'Teen', 'Mature', 'Adult'][metadata.audience]
  const positionLabels = ['standing', 'sleeping', 'pouncing', 'stalking'] as const
  const positionsEnabled = accessoryDetail.positions.map((p) => p !== '0xffff')

  /**
   * Details about the Creator/Manager of the Accessory
   *
   * If the Creator and the current Manager are the same, no need to show both.
   * If the current manager is the Zero Address, the Accessory has been discontinued.
   */
  const managerDetail =
    accessoryDetail.manager == zeroAddress ? (
      <>Nobody (ownership has been revoked; no further changes can be made to this accessory)</>
    ) : (
      <EthereumAddress address={accessoryDetail.manager} />
    )
  const managerView =
    !accessoryTraits || accessoryTraits.creator == accessoryDetail.manager ? (
      <li>
        <strong>Manager</strong> {managerDetail}
      </li>
    ) : (
      <>
        <li itemProp="creator">
          <strong>Creator</strong> <EthereumAddress address={accessoryTraits.creator} />
        </li>
        <li itemProp="maintainer">
          <strong>Manager</strong> {managerDetail}
        </li>
      </>
    )

  const palettePicker: JSX.Element[] = []
  for (let i = 0; i < accessoryDetail.availablePalettes; i++) {
    palettePicker.push(
      <span
        key={i}
        style={{ cursor: 'pointer' }}
        onClick={(e) => {
          if (onPaletteChanged) onPaletteChanged(i)
        }}
      >
        {i + 1}
      </span>
    )
  }

  if (accessoryDetail.availableSupply == 0) {
    // This accessory has sold out
    const preamble =
      accessoryDetail.totalSupply == 1 ? (
        <p>
          This accessory is a <strong>unique item</strong> (only <em>one</em> in circulation) and it has been applied to
          an individual MoonCat already.
        </p>
      ) : (
        <p>
          There are {accessoryDetail.totalSupply.toLocaleString()} copies of this accessory in circulation. No further
          copies will ever be for sale, so only the MoonCats that currently own this accessory will ever own it.
        </p>
      )
    return (
      <>
        <div className="btn-bar">
          <StarButton
            targetAddress={ACCESSORIES_ADDRESS}
            chainName="eth"
            targetItem={numberToHex(accessoryId, { size: 32 })}
            itemLabel="Accessory"
            message={'I like this Accessory! \u{01F63B}'}
          />
        </div>
        {preamble}
        <ul>
          {managerView}
          {accessoryDetail.availablePalettes > 1 && (
            <li>
              <strong>Color Palettes</strong> {interleave(palettePicker, ', ')}
            </li>
          )}
          {metadata.audience > 0 && (
            <li>
              <strong>Rating</strong> {audienceLabel}
            </li>
          )}
          {!metadata.verified && (
            <li>
              <strong>Unverified</strong> This accessory will not appear in public marketplace representations of
              MoonCats. It is still an on-chain item and can be viewed through direct blockchain access.
            </li>
          )}
          {COSTUME_ACCESSORIES.includes(accessoryId) && (
            <li>
              <strong>Costume</strong> This accessory will not be visually seen by most marketplaces or generic NFT
              galleries to reduce user confusion about what a MoonCat looks like. It is still an on-chain item and can
              be viewed through direct blockchain access.
            </li>
          )}
        </ul>
        {showAdminTools && <AdminTools accessoryId={accessoryId} metabyte={accessoryDetail.metabyte} />}
      </>
    )
  } else {
    // There is still an available supply of this Accessory. Show purchase detail info

    // Parse the purchase cost
    let costView: React.ReactNode
    if (accessoryDetail.availableForPurchase) {
      if (accessoryDetail.price > 0) {
        costView = (
          <span itemProp="offers" itemScope itemType="https://schema.org/Offer">
            <link itemProp="availability" href="https://schema.org/InStock" />
            <meta itemProp="price" content={formatEther(accessoryDetail.price)} />
            <meta itemProp="priceCurrency" content="ETH" />
            <strong>Cost</strong> <EthAmount amount={accessoryDetail.price} />
          </span>
        )
      } else {
        costView = (
          <>
            <strong>Cost</strong> <em>Free!</em>
          </>
        )
      }
    } else {
      costView = <strong>Not for sale</strong>
    }

    // Parse the eligibility list
    const eligibleListIsActive = BigInt(accessoryDetail.eligibleList[99]) & 1n
    let eligibleView: React.ReactNode
    if (eligibleListIsActive) {
      const eligibleMoonCats = []
      for (let rescueOrder = 0; rescueOrder < 25440; rescueOrder++) {
        const wordIndex = Math.floor(rescueOrder / 256)
        const bitIndex = rescueOrder % 256
        const isEligible = BigInt(accessoryDetail.eligibleList[wordIndex]) & (1n << BigInt(255 - bitIndex))
        if (isEligible) eligibleMoonCats.push(rescueOrder)
      }
      eligibleView = (
        <>
          Only <strong>eligible</strong> MoonCats may purchase (
          <em>{eligibleMoonCats.length.toLocaleString()} MoonCats</em> are eligible)
        </>
      )
    } else {
      if (positionsEnabled[0] && positionsEnabled[1] && positionsEnabled[2] && positionsEnabled[3]) {
        eligibleView = 'All MoonCats are eligible to purchase'
      } else {
        const eligiblePoses = positionLabels.filter((_pose, i) => positionsEnabled[i])
        if (eligiblePoses.length == 3) {
          eligibleView = (
            <>
              Any <em>{eligiblePoses[0]}</em>, <em>{eligiblePoses[1]}</em>, or <em>{eligiblePoses[2]}</em> MoonCat is
              eligible to purchase
            </>
          )
        } else if (eligiblePoses.length == 2) {
          eligibleView = (
            <>
              Any <em>{eligiblePoses[0]}</em> or <em>{eligiblePoses[1]}</em> MoonCat is eligible to purchase
            </>
          )
        } else if (eligiblePoses.length == 1) {
          eligibleView = (
            <>
              Any <em>{eligiblePoses[0]}</em> MoonCat is eligible to purchase
            </>
          )
        }
      }
    }

    return (
      <>
        <div className="btn-bar">
          <StarButton
            targetAddress={ACCESSORIES_ADDRESS}
            chainName="eth"
            targetItem={numberToHex(accessoryId, { size: 32 })}
            itemLabel="Accessory"
            message={'I like this Accessory! \u{01F63B}'}
          />
        </div>
        <ul>
          <li>{costView}</li>
          <li>{eligibleView}</li>
          <li>
            Only <em>{accessoryDetail.availableSupply.toLocaleString()} copies</em> of this accessory are still
            available
          </li>
          {managerView}
          {accessoryDetail.availablePalettes > 1 && (
            <li>
              <strong>Color Palettes</strong> {interleave(palettePicker, ', ')}
            </li>
          )}
          {metadata.audience > 0 && (
            <li>
              <strong>Rating</strong> {audienceLabel}
            </li>
          )}
          {!metadata.verified && (
            <li>
              <strong>Unverified</strong> This accessory will not appear in public marketplace representations of
              MoonCats. It is still an on-chain item and can be viewed through direct blockchain access.
            </li>
          )}
          {COSTUME_ACCESSORIES.includes(accessoryId) && (
            <li>
              <strong>Costume</strong> This accessory will not be visually seen by most marketplaces or generic NFT
              galleries to reduce user confusion about what a MoonCat looks like. It is still an on-chain item and can
              be viewed through direct blockchain access.
            </li>
          )}
        </ul>
        {showAdminTools && <AdminTools accessoryId={accessoryId} metabyte={accessoryDetail.metabyte} />}
      </>
    )
  }
}

export default AccessoryInfoCard
