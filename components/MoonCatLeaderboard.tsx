'use client'
import { MoonCatSummary } from 'lib/types'
import { API2_SERVER_ROOT, API_SERVER_ROOT } from 'lib/util'
import Link from 'next/link'
import Icon from './Icon'
import { useQuery } from '@tanstack/react-query'

const MoonCatLeaderboard = ({ moonCats: initialMoonCats }: { moonCats: MoonCatSummary[] }) => {
  const { data: moonCats } = useQuery({
    queryKey: ['mooncat-stars'],
    queryFn: async (): Promise<MoonCatSummary[]> => {
      const rs = await fetch(`${API2_SERVER_ROOT}/mooncats?sort=liked&limit=50`)
      if (!rs.ok) throw new Error('Failed to fetch popular MoonCat data from back-end')
      return await rs.json()
    },
    initialData: initialMoonCats,
  })

  return (
    <div id="content-container" className="leaderboard">
      <div className="text-container leaderboard-wrap">
        <h1 className="hero">Popular MoonCats</h1>
        <section className="card" style={{ marginTop: 140 }}>
          <p>
            All cats are good cats, though some MoonCats have collected quite an entourage! Check out which MoonCats
            have collected the most &ldquo;stars&rdquo; from the Etherians they&rsquo;ve met:
          </p>
        </section>
        <section className="card">
          <ul className="zebra">
            {moonCats.map((d) => (
              <li key={d.catId}>
                <Link href={`/mooncats/${d.rescueOrder}`}>
                  <picture>
                    <img
                      className="inline"
                      alt=""
                      src={`${API_SERVER_ROOT}/image/${d.rescueOrder}?scale=1&padding=2&costumes=true`}
                    />
                  </picture>
                  <h3 style={{ flex: '1 1 auto' }}>
                    MoonCat #{d.rescueOrder}
                    {d.name.isNamed && `: ${d.name.value == '\ufffd' ? <Icon name="question" /> : d.name.value}`}
                  </h3>
                  {d.likesCount > 0 && (
                    <div className="icon-pill">
                      {d.likesCount}
                      <Icon style={{ marginLeft: '0.25rem', verticalAlign: '-1px' }} name="star-empty" />
                    </div>
                  )}
                </Link>
              </li>
            ))}
          </ul>
        </section>
      </div>
    </div>
  )
}
export default MoonCatLeaderboard
