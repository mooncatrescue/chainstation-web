'use client'
import { MOONCAT_TRAITS_ARB, formatAsTime, stringifyBigints, switchToChain } from 'lib/util'
import { useState } from 'react'
import Icon from './Icon'
import Drawer from './Drawer'
import WarningIndicator from './WarningIndicator'
import { useAccount } from 'wagmi'
import { useAppKit } from '@reown/appkit/react'
import { AttestationDoc, BasicAttestation, SCHEMAS, compressAttestation, useEas } from 'lib/eas'
import useIsMounted from 'lib/useIsMounted'
import { useQueryClient } from '@tanstack/react-query'

// The "GM" schema on Arbitrum
// https://arbitrum.easscan.org/schema/view/0x85500e806cf1e74844d51a20a6d893fe1ed6f6b0738b50e43d774827d08eca61
const SCHEMA_ID = SCHEMAS.GM

const EASGM = ({ existing }: { existing: AttestationDoc[] }) => {
  const [attestation, setAttestation] = useState<Omit<AttestationDoc, 'data'> | null>(null)
  const [error, setError] = useState<string | null>(null)
  const { offchainSign } = useEas(42161)
  const isMounted = useIsMounted()
  const { address, isConnected } = useAccount()
  const { open } = useAppKit()
  const queryClient = useQueryClient()

  if (!isMounted || !isConnected) {
    return (
      <p>
        <button onClick={(e) => open()}>Connect</button>
      </p>
    )
  }

  async function doGM() {
    if (!address) return // Must be connected

    // Must be on the Arbitrum network
    if (!(await switchToChain(42161))) {
      setError('Failed to switch to Arbitrum')
      return
    }

    setError(null)
    let attestation: BasicAttestation | null
    try {
      attestation = await offchainSign({
        schemaId: SCHEMA_ID as `0x${string}`,
        recipient: MOONCAT_TRAITS_ARB,
        revocable: false,
        data: [{ name: 'gm', value: true, type: 'bool' }],
      })
    } catch (err: any) {
      if (err.code == 'ACTION_REJECTED') {
        setError('Message signature rejected')
      } else if (typeof err.shortMessage != 'undefined') {
        setError(err.shortMessage)
      } else {
        setError((err as Error).message)
      }
      return
    }
    if (attestation == null) return

    setAttestation({
      ...attestation,
      compressed: await compressAttestation(attestation.sig, attestation.signer),
    })
    //const isValidAttestation = offchain.verifyOffchainAttestationSignature(signerAddress, attestation)
    queryClient.invalidateQueries({
      queryKey: ['gm-attestations'],
    })
  }

  let existingAttestation
  if (attestation) {
    // Visitor just signed an attestation; use that one
    existingAttestation = attestation
  } else {
    // Look through existing attestations to find if there's any from this address
    const pastAttestations = existing.filter((a) => a.signer == address)
    if (pastAttestations.length > 0) {
      const mostRecentTime = new Date(Number(pastAttestations[0].sig.message.time) * 1000)
      const today = new Date(new Date().setUTCHours(0, 0, 0, 0))
      if (mostRecentTime.getUTCDate() == today.getUTCDate() && mostRecentTime.getUTCMonth() == today.getUTCMonth()) {
        // This attestation is from today
        existingAttestation = pastAttestations[0]
      }
    }
  }

  if (existingAttestation) {
    const raw = {
      sig: existingAttestation.sig,
      signer: existingAttestation.signer,
    }

    return (
      <>
        <p>
          An attestation was signed from this address today{' '}
          <em>at {formatAsTime(Number(existingAttestation.sig.message.time))} (UTC time)</em>
        </p>
        <Drawer title="Raw Attestation">
          <p className="code-block" style={{ fontSize: '0.8rem' }}>
            {JSON.stringify(raw, stringifyBigints)}
          </p>
        </Drawer>
        <p>
          <a
            href={
              'https://arbitrum.easscan.org/offchain/url/#attestation=' +
              encodeURIComponent(existingAttestation.compressed)
            }
            target="_blank"
            rel="noreferrer"
          >
            Verify on EAS
            <Icon name="link-external" style={{ marginLeft: '0.5em', verticalAlign: '-0.2em' }} />
          </a>{' '}
          to see more details about this Attestation, including broadcasting it for the broader EAS community to see, or
          saving it as a QR code.
        </p>
      </>
    )
  }

  return (
    <>
      <p>
        <button onClick={doGM}>GM!</button>
      </p>
      {error != null && <WarningIndicator message={error} />}
    </>
  )
}

export default EASGM
