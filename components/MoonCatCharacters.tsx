'use client'

import { AppVisitorContext } from 'lib/AppVisitorProvider'
import { AppEventHandler, AppEventType, useGlobalEvents } from 'lib/useGlobalEvents'
import { AppGlobalActionType } from 'lib/util'
import { useCallback, useContext, useEffect, useRef, useState } from 'react'

const SPEED = 8 // pixels per step
const FRAME_TIME = 120 // milliseconds per frame
const BEHAVIOR_MIN_TIME = 5000 // minimum milliseconds per behavior
const SPRITE_SIZE = 96
const ROW_INDEX = {
  'right-walk': 0,
  'up-walk': 1,
  'left-walk': 2,
  'down-walk': 3,
  'right-idle': 4,
  'up-idle': 5,
  'left-idle': 6,
  'down-idle': 7,
}

type Behavior = 'idle' | 'wander' | 'follow' | 'warp-in'
type AnimationDirection = 'right' | 'up' | 'left' | 'down'
type AnimationCycle = 'walk' | 'idle'
type AnimationLabel = `${AnimationDirection}-${AnimationCycle}`

interface PetState {
  behavior: Behavior
  animation: AnimationLabel
  loopProgress: number // 0-79, showing sub-frame progress through frames 0-7
  x: number
  y: number
  behaviorChange: number
  goalPos?: { x: number; y: number }
}

interface MouseState {
  x: number
  y: number
  moveCount: number
  moveTime: number
}

const BEHAVIOR_CONTROLLERS: Record<string, (state: PetState, mouse: MouseState) => PetState> = {
  'warp-in': function (state: PetState) {
    // Show animation warping the MoonCat in
    const newState = { ...state }
    newState.loopProgress += 10
    if (newState.loopProgress > 80) {
      newState.behavior = 'idle'
    }
    return newState
  },
  'standard': function (state: PetState, mouse: MouseState) {
    const newState = { ...state }
    const now = Date.now()

    // MoonCat is wandering about the screen
    const progressStep = newState.behavior == 'idle' ? 5 : 10 // How much should the `loopProgress` value advance each tick?
    newState.loopProgress = (newState.loopProgress + progressStep) % 80 // Constrain `loopProgress` to 0-79, which relates to frames 0-7

    // Is the user's mouse being interesting?
    if (mouse.moveCount > 5) {
      // User has been moving the mouse; make a random chance that the MoonCat gets interested in the mouse and follows it
      if (Math.random() < 0.001) {
        newState.behavior = 'follow'
        newState.behaviorChange = now
        console.debug('Got interested in the mouse', mouse, now)
      }
    }

    // Check if it's time to change behavior
    if (now - state.behaviorChange > BEHAVIOR_MIN_TIME) {
      newState.behavior = (['idle', 'wander', 'follow'] as Behavior[])[Math.floor(Math.random() * 3)]
      if (newState.behavior == 'idle') {
        newState.animation = 'down-idle'
        newState.loopProgress = 0
      } else if (newState.behavior == 'wander') {
        newState.goalPos = {
          x: Math.floor(Math.random() * window.innerWidth),
          y: Math.floor(Math.random() * window.innerHeight),
        }
      }
      newState.behaviorChange = now
      console.debug('new behavior', newState.behavior, newState.x, newState.y, newState.goalPos, mouse, now)
    }

    // Act upon current behavior
    switch (newState.behavior) {
      case 'wander': {
        // Compare current position to goal position
        if (!newState.goalPos || (newState.goalPos.x == newState.x && newState.goalPos.y == newState.y)) {
          const [dir, cycle] = newState.animation.split('-') as [AnimationDirection, AnimationCycle]
          newState.animation = `${dir}-idle`
        } else {
          const parsed = parseGoal(newState.x, newState.y, newState.goalPos.x, newState.goalPos.y)
          newState.animation = parsed.animation
          newState.x = parsed.x
          newState.y = parsed.y
        }
        break
      }
      case 'follow': {
        // Compare current position to goal position
        if (mouse.x == newState.x && mouse.y == newState.y) {
          const [dir, cycle] = newState.animation.split('-') as [AnimationDirection, AnimationCycle]
          newState.animation = `${dir}-idle`
        } else {
          const parsed = parseGoal(newState.x, newState.y, mouse.x, mouse.y)
          newState.animation = parsed.animation
          newState.x = parsed.x
          newState.y = parsed.y
        }
        break
      }
    }
    return newState
  },
}

function parseGoal(x: number, y: number, rawGoalX: number, rawGoalY: number) {
  const parsed: { x: number; y: number; animation: AnimationLabel } = {
    x: x,
    y: y,
    animation: 'down-idle',
  }
  const goalX = rawGoalX - SPRITE_SIZE / 2
  const goalY = rawGoalY - SPRITE_SIZE
  if (goalX > x) {
    // Goal is somewhere to the right
    if (goalY > y) {
      // Goal is down and to the right
      if (goalY - y > (goalX - x) * 5) {
        // Walk down, but slide right to approach goal
        parsed.animation = 'down-walk'
        parsed.y = goalY - y < SPEED ? goalY : y + SPEED
        parsed.x = x + 1
      } else {
        // Walk right, but slide down to approach goal
        parsed.animation = 'right-walk'
        parsed.x = goalX - x < SPEED ? goalX : x + SPEED
        parsed.y = y + 1
      }
    } else if (goalY < y) {
      // Goal is up and to the right
      if (y - goalY > (goalX - x) * 5) {
        // Walk up, but slide right to approach goal
        parsed.animation = 'up-walk'
        parsed.y = y - goalY < SPEED ? goalY : y - SPEED
        parsed.x = x + 1
      } else {
        // Walk right, but slide up to approach goal
        parsed.animation = 'right-walk'
        parsed.x = goalX - x < SPEED ? goalX : x + SPEED
        parsed.y = y - 1
      }
    } else {
      // Goal is directly to the right
      parsed.animation = 'right-walk'
      parsed.x = goalX - x < SPEED ? goalX : x + SPEED
    }
  } else if (goalX < x) {
    // Goal is somewhere to the left
    if (goalY > y) {
      // Goal is down and to the left
      if (goalY - y > x - goalX) {
        // Walk down, but slide left to approach goal
        parsed.animation = 'down-walk'
        parsed.y = goalY - y < SPEED ? goalY : y + SPEED
        parsed.x = x - 1
      } else {
        // Walk left, but slide down to approach goal
        parsed.animation = 'left-walk'
        parsed.x = x - goalX < SPEED ? goalX : x - SPEED
        parsed.y = y + 1
      }
    } else if (goalY < y) {
      // Goal is up and to the left
      if (y - goalY < SPEED) {
        // Walk up, but slide left to approach goal
        parsed.animation = 'up-walk'
        parsed.y = y - goalY < SPEED ? goalY : y - SPEED
        parsed.x = x - 1
      } else {
        // Walk left, but slide up to approach goal
        parsed.animation = 'left-walk'
        parsed.x = x - goalX < SPEED ? goalX : x - SPEED
        parsed.y = y - 1
      }
    } else {
      // Goal is directly to the left
      parsed.animation = 'left-walk'
      parsed.x = x - goalX < SPEED ? goalX : x - SPEED
    }
  } else {
    // Goal is directly above or below
    if (goalY > y) {
      parsed.animation = 'down-walk'
      parsed.y = goalY - y < SPEED ? goalY : y + SPEED
    } else if (goalY < y) {
      parsed.animation = 'up-walk'
      parsed.y = y - goalY < SPEED ? goalY : y - SPEED
    }
  }

  return parsed
}

const MoonCatCharacters = () => {
  const [petStates, setPetStates] = useState<Record<number, PetState>>({})
  const { dispatch } = useContext(AppVisitorContext)
  const [isActive, setIsActive] = useState<boolean>(false)

  /**
   * Listen for events to add more MoonCats to those that are awakened
   */
  const handleGlobalEvent: AppEventHandler = useCallback(
    (e) => {
      // A global event is a request from some other component on the page, intended to be acted-upon by some other component
      if (e.type !== AppEventType.AWAKEN) return
      // This event is an AWAKEN event, which is intended for this component to act-upon
      setPetStates((prev) => {
        const newState = { ...prev }
        newState[e.payload.rescueOrder] = {
          behavior: 'warp-in',
          animation: 'down-idle',
          loopProgress: 0,
          x: e.payload.x ? e.payload.x - SPRITE_SIZE / 2 : window.innerWidth / 2,
          y: e.payload.y ? e.payload.y - SPRITE_SIZE : window.innerHeight / 2,
          behaviorChange: Date.now(),
        }
        return newState
      })
      // This dispatch is into the global application state, to inform all other components that we have
      // taken the appropriate action, and so all components can known which MoonCats are awoken (global state update)
      dispatch({
        type: AppGlobalActionType.AWAKEN,
        payload: e.payload.rescueOrder,
      })

      // Activate animation loops if they aren't already
      setIsActive((prev) => {
        if (prev === true) return prev
        return true
      })
    },
    [setPetStates, dispatch]
  )
  useGlobalEvents(handleGlobalEvent)

  const mousePos = useRef<MouseState>({ x: 0, y: 0, moveCount: 0, moveTime: 0 })

  function setToMode(rescueOrder: number, newMode: Behavior) {
    setPetStates((prev) => {
      if (prev[rescueOrder].behavior == newMode) return prev
      const newState = { ...prev }
      newState[rescueOrder].behavior = newMode
      newState[rescueOrder].behaviorChange = Date.now()
      return newState
    })
  }

  /**
   * Main animation loop
   * Updates character goals/behaviors, and advances through animation frames
   */
  const tick = useCallback(() => {
    setPetStates((prev) => {
      const newState = { ...prev }
      const now = Date.now()

      if (now - mousePos.current.moveTime > 2000) {
        // User has stopped moving the mouse; idle out the interaction counts
        mousePos.current = { ...mousePos.current, moveCount: 0, moveTime: 0 }
      }

      for (const k of Object.keys(newState)) {
        const rescueOrder = Number(k)

        if (prev[rescueOrder].behavior == 'warp-in') {
          newState[rescueOrder] = BEHAVIOR_CONTROLLERS['warp-in'](prev[rescueOrder], mousePos.current)
        } else {
          newState[rescueOrder] = BEHAVIOR_CONTROLLERS['standard'](prev[rescueOrder], mousePos.current)
        }
      }

      return newState
    })
  }, [setPetStates])

  /**
   * Listen for mouse movements
   */
  useEffect(() => {
    if (!isActive) return
    function handleMouseMove(e: MouseEvent) {
      const now = Date.now()
      if (now - mousePos.current.moveTime > 2000) {
        // It's been a pause since the last mouse movement
        mousePos.current = {
          x: e.pageX,
          y: e.pageY,
          moveCount: 1,
          moveTime: now,
        }
      } else {
        // In the midst of a mouse movement
        mousePos.current = {
          x: e.pageX,
          y: e.pageY,
          moveCount: mousePos.current.moveCount + 1,
          moveTime: mousePos.current.moveTime,
        }
      }
    }
    document.addEventListener('mousemove', handleMouseMove)

    // Stop listening to mouse events when unmounting
    return () => {
      document.removeEventListener('mousemove', handleMouseMove)
    }
  }, [isActive])

  /**
   * Start the animation loop on render
   */
  useEffect(() => {
    if (!isActive) return
    let lastTime = performance.now()
    function doTick(currentTime: number) {
      if (!isActive) return
      const delta = currentTime - lastTime
      if (delta >= FRAME_TIME) {
        // Time to update frames
        lastTime = currentTime
        tick()
      }

      // Call self again, to animate the sprites
      requestAnimationFrame(doTick)
    }
    requestAnimationFrame(doTick)

    return () => {
      setIsActive(false)
    }
  }, [isActive, tick])

  return Object.entries(petStates).map(([rescueOrder, petState]) => {
    if (petState.behavior == 'warp-in') {
      // Show the glowing, warping bits
      const MAX_SCALE = 3
      const MIN_SCALE = 1
      const progressPercent = petState.loopProgress / 80
      const size = (((80 - petState.loopProgress) / 80) * (MAX_SCALE - MIN_SCALE) + MIN_SCALE) * SPRITE_SIZE
      const delta = size - SPRITE_SIZE
      const opacity = 0.2 + progressPercent * 0.8
      const stop1 = progressPercent * 0.2
      const stop2 = 0.7 - progressPercent * 0.2

      return (
        <div
          key={rescueOrder}
          className="sprite-animation"
          onMouseUp={() => setToMode(Number(rescueOrder), 'follow')}
          style={{
            left: `${petState.x - delta / 2}px`,
            top: `${petState.y - delta / 2 + 10}px`,
            width: `${size}px`,
            height: `${size}px`,
            backgroundImage: `radial-gradient(rgba(255,255,255, ${opacity * 100}%) ${
              stop1 * 100
            }%, rgba(255,255,255, 0%) ${stop2 * 100}%)`,
            backgroundSize: 'cover',
            zIndex: 100,
          }}
        />
      )
    } else {
      // Show the MoonCat
      return (
        <div
          key={rescueOrder}
          className="sprite-animation"
          onMouseUp={() => setToMode(Number(rescueOrder), 'follow')}
          style={{
            left: `${petState.x}px`,
            top: `${petState.y}px`,
            backgroundImage: `url('https://ipfs.io/ipfs/bafybeib5iedrzr7unbp4zq6rkrab3caik7nw7rfzlcfvu4xqs6bfk7dgje/${rescueOrder}.png')`,
            backgroundPosition: `-${Math.floor(petState.loopProgress / 10) * SPRITE_SIZE}px -${
              ROW_INDEX[petState.animation] * SPRITE_SIZE
            }px`,
            zIndex: 100,
          }}
        />
      )
    }
  })
}
export default MoonCatCharacters
