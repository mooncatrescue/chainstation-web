'use client'
import { useGMs } from 'lib/attestations'
import { AttestationDoc } from 'lib/eas'
import { formatAsTime, formatAsDate } from 'lib/util'
import Link from 'next/link'
import React from 'react'
import EASGM from './EASGM'
import EthereumAddress from './EthereumAddress'
import Icon from './Icon'
import LoadingIndicator from './LoadingIndicator'
import WarningIndicator from './WarningIndicator'
import { Address } from 'viem'

/**
 * How much time has progressed in the day so far.
 * Given a date object, give a relative number of seconds, minutes, hours, or combination of hours and minutes that the day has elapsed so far.
 */
function formatElapsed(d: Date): string {
  const hours = d.getUTCHours()
  const minutes = d.getUTCMinutes()
  const seconds = d.getUTCSeconds()

  if (hours == 0) {
    if (minutes == 0) {
      // Measure in seconds
      if (seconds == 1) return '1 second'
      return `${seconds} seconds`
    }
    // Measure in minutes
    if (minutes == 1) return '1 minute'
    return `${minutes} minutes`
  }
  const parts: string[] = []
  if (hours == 1) {
    parts.push('1 hour')
  } else {
    parts.push(`${hours} hours`)
  }
  if (minutes > 0) {
    if (minutes == 1) {
      parts.push('1 minute')
    } else {
      parts.push(`${minutes} minutes`)
    }
  }
  return parts.join(' ')
}

/**
 * Show an individual Attestation in the list.
 * Shows the time (hours and minutes) the attestation was signed, and the Ethereum address that signed the attestation.
 */
const AttestationListItem = ({ attestation }: { attestation: AttestationDoc }): React.ReactElement => {
  const date = new Date(Number(attestation.sig.message.time) * 1000)
  return (
    <li>
      <a
        href={'https://arbitrum.easscan.org/offchain/url/#attestation=' + encodeURIComponent(attestation.compressed)}
        target="_blank"
        title="Verify this attestation on EAS"
        rel="noreferrer"
      >
        {formatAsTime(date)}
      </a>{' '}
      - <EthereumAddress address={attestation.signer as Address} />
    </li>
  )
}

/**
 * Show a listing of recent GMs.
 */
const DailyLog = ({ gmList }: { gmList?: AttestationDoc[] }): React.ReactElement => {
  if (typeof gmList == 'undefined') return <LoadingIndicator message="Scanning the spacewaves..." />

  // Bucket into UTC days
  const bucketGMs: Record<string, AttestationDoc[]> = {}
  gmList.forEach((attestation) => {
    const dayLabel = formatAsDate(Number(attestation.sig.message.time))
    if (typeof bucketGMs[dayLabel] == 'undefined') {
      bucketGMs[dayLabel] = []
    }
    bucketGMs[dayLabel].push(attestation)
  })

  const now = new Date()
  const today = new Date(new Date().setUTCHours(0, 0, 0, 0))
  const todayLabel = formatAsDate(today)

  const dailyGMs = typeof bucketGMs[todayLabel] != 'undefined' ? bucketGMs[todayLabel] : []
  const RECENT_DAYS = 30 * 24 * 60 * 60
  const nowTimestamp = today.getTime() / 1000
  const recentDays = Object.keys(bucketGMs).filter(
    (dayLabel) => nowTimestamp - Number(bucketGMs[dayLabel][0].sig.message.time) < RECENT_DAYS
  )
  if (recentDays[0] == todayLabel) {
    recentDays.shift()
  }

  return (
    <section className="card">
      <h2>GMs today</h2>
      <p>
        {todayLabel} (UTC) is {formatElapsed(now)} old. In that time, there&rsquo;s been {dailyGMs.length} GMs sent:
      </p>
      <ul style={{ columnWidth: '20em' }}>
        {dailyGMs.map((attestation) => (
          <AttestationListItem key={attestation.sig.uid} attestation={attestation} />
        ))}
      </ul>
      <nav className="breadcrumb">
        <Link href="/gm-leaderboard">
          <>
            Browse more GMs <Icon name="arrow-right" style={{ verticalAlign: '-0.2em' }} />
          </>
        </Link>
      </nav>
      <h2>Recent logs</h2>
      {recentDays.map((dayLabel) => {
        return (
          <React.Fragment key={dayLabel}>
            <h3>{dayLabel}</h3>
            <ul style={{ columnWidth: '20em' }}>
              {bucketGMs[dayLabel].reverse().map((attestation) => (
                <AttestationListItem key={attestation.sig.uid} attestation={attestation} />
              ))}
            </ul>
          </React.Fragment>
        )
      })}
    </section>
  )
}

const GMPage = () => {
  const { data: GMs, status } = useGMs()

  let attestView: React.ReactNode
  if (typeof GMs == 'undefined') {
    attestView = <LoadingIndicator message="Fetching attestations..." />
  } else if (status == 'error') {
    attestView = (
      <WarningIndicator message="Unexpected black hole! Unable to get the back-end data needed to render this form" />
    )
  } else {
    attestView = <EASGM existing={GMs} />
  }

  return (
    <>
      <section className="card">
        <p>
          It&rsquo;s always morning somewhere, let&rsquo;s make it a good one! Using the{' '}
          <a href="https://attest.sh/">Ethereum Attestation Service (EAS)</a>, send a verifiable &ldquo;GM&rdquo; to the
          whole network by pressing the button below. This integration uses the Arbitrum rollup blockchain, so your
          wallet will prompt you to change to that network. But it won&rsquo;t cost you any gas as this tool uses the
          offchain attestation method. Your wallet will prompt you to sign the message payload (to show the network it
          really was from you), and then you&rsquo;ll see the resulting verified Attestation here.
        </p>
        {attestView}
      </section>
      <DailyLog gmList={GMs} />
    </>
  )
}
export default GMPage
