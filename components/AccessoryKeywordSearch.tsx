'use client'
import { useQuery } from '@tanstack/react-query'
import useDebounce from 'lib/useDebounce'
import { API2_SERVER_ROOT, API_SERVER_ROOT } from 'lib/util'
import { useState } from 'react'
import { AccessorySummary } from 'lib/types'
import Link from 'next/link'
import LoadingIndicator from './LoadingIndicator'
import useSignedIn from 'lib/useSignedIn'
import IconButton from './IconButton'

const AccessoryKeywordSearch = () => {
  const [keyword, setKeyword] = useState<string>('')
  const debouncedKeyword = useDebounce(keyword)
  const { isSignedIn } = useSignedIn()

  const { data, isFetching, refetch } = useQuery({
    queryKey: ['accessory-keyword-search', debouncedKeyword],
    queryFn: async (): Promise<AccessorySummary[]> => {
      const rs = await fetch(
        `${API2_SERVER_ROOT}/accessories-search?${new URLSearchParams({
          search: debouncedKeyword,
          limit: '30',
        })}`
      )
      if (!rs.ok) throw new Error('Failed to fetch')
      return await rs.json()
    },
    enabled: debouncedKeyword.length >= 2,
  })

  const shownResults = data && data.length > 0 ? data.filter((a) => a.isVerified || isSignedIn).slice(0, 10) : []

  return (
    <section className="card">
      <h2>Accessory Search</h2>
      <section style={{ display: 'flex', alignItems: 'baseline' }}>
        <div style={{ paddingRight: '1rem' }}>
          <label htmlFor="accessory-search">Search accessory names and descriptions:</label>
        </div>
        <input
          id="accessory-search"
          style={{ flex: '1 1 auto' }}
          type="text"
          value={keyword}
          onChange={(e) => setKeyword(e.target.value)}
        />
        <div style={{ alignSelf: 'stretch', paddingLeft: '0.5rem' }}>
          <IconButton
            name={'search'}
            title="Perform search"
            style={{ padding: '0.3rem' }}
            onClick={(e) => {
              refetch()
            }}
          />
        </div>
      </section>
      {isFetching && <LoadingIndicator message="Searching..." />}
      {shownResults.length > 0 && (
        <div style={{ columnWidth: '21em', marginBottom: '1.5em' }}>
          <table cellSpacing="0" cellPadding="0" className="zebra" style={{ margin: '0 auto', fontSize: '0.8rem' }}>
            <tbody>
              {shownResults.map((a) => {
                return (
                  <tr key={a.id}>
                    <td style={{ textAlign: 'center', lineHeight: 1 }}>
                      <picture>
                        <img alt="" style={{ height: '4em' }} src={`${API_SERVER_ROOT}/accessory-image/${a.id}`} />
                      </picture>
                    </td>
                    <td style={{ breakInside: 'avoid-column' }}>
                      <Link href={'/accessories/' + a.id}>{a.name}</Link>{' '}
                      {a.description && a.description.length > 75
                        ? a.description?.substring(0, 70) + '...'
                        : a.description}
                    </td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </div>
      )}
    </section>
  )
}
export default AccessoryKeywordSearch
