import React from 'react'
import Link from 'next/link'

interface Props {
  href?: string
  children: string
}

const HexString = ({ children: hex, href }: Props) => {
  const shortHex = hex.substring(0, 8) + '...' + hex.substring(hex.length - 6)
  if (typeof href == 'undefined') {
    return (
      <span title={hex} className="hex-string">
        {shortHex}
      </span>
    )
  }
  return (
    <Link href={href} title={hex} className="hex-string">
      {shortHex}
    </Link>
  )
}
export default HexString
