import { SCHEMAS, attestHelpText, useEas } from 'lib/eas'
import { API2_SERVER_ROOT, FIVE_MINUTES, switchToChain } from 'lib/util'
import Image from 'next/image'
import { CSSProperties, useState } from 'react'
import { useAccount } from 'wagmi'
import Icon from './Icon'
import { OwnerProfile } from 'lib/types'
import useDialog from 'lib/useDialog'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import { Address } from 'viem'

// https://arbitrum.easscan.org/schema/view/0x58de78ba175d13cb1699fdbc6c25a80ba2b21a943b5aac252ec367c878808d09
const SCHEMA_TOKEN_LIST = SCHEMAS.TOKEN_LIST as `0x${string}`

// https://arbitrum.easscan.org/schema/view/0xeae0ff58b769be065ba9d54f4dd9b88c07de93a00e1e3b889a7099de02dd5549
const SCHEMA_REVIEW_LIST = SCHEMAS.PRODUCT_REVIEW

interface Props {
  targetAddress: Address
  targetItems: `0x${string}`[]
  onAdd?: () => {}
  style?: CSSProperties
  title?: string
}
const AddToListButton = ({ targetAddress, targetItems, onAdd, style, title }: Props) => {
  const { address } = useAccount()
  const { offchainSign } = useEas(42161)
  const queryClient = useQueryClient()
  const [chosenList, setChosenList] = useState<string | null>(null)
  const [newListTitle, setNewListTitle] = useState<string>('')
  const { ref: dialogRef, setIsOpen: setDialogIsOpen } = useDialog()

  const existingLists = useQuery({
    queryKey: ['owner-profile', address],
    queryFn: async (): Promise<OwnerProfile> => {
      const rs = await fetch(`${API2_SERVER_ROOT}/owner-profile/${address}`)
      if (!rs.ok) throw new Error('Failed to fetch owner profile')
      return await rs.json()
    },
    staleTime: FIVE_MINUTES,
    select: (ownerDetails) => {
      const tokenLists = ownerDetails.tokenLists.map((l) => l.title)
      if (tokenLists.indexOf('Wishlist') < 0) tokenLists.push('Wishlist') // Add 'Wishlist' as a default list for all users
      return tokenLists.sort((a, b) => a.localeCompare(b))
    },
  })

  /**
   * Sign a new Attestation, adding the target token to a list
   */
  async function addToken(targetList: string) {
    if (typeof address == 'undefined') return // Must be connected
    if (!(await switchToChain(42161))) return // Must be on the Arbitrum network
    setChosenList(targetList)
    setNewListTitle('')
    setDialogIsOpen(true)
    console.debug(
      `Signing attestation to add ${targetAddress}:${targetItems.join(', ')} to list ${targetList} for ${address}`
    )

    try {
      await offchainSign({
        schemaId: SCHEMA_TOKEN_LIST,
        recipient: targetAddress,
        revocable: true,
        data: [
          { name: 'tokenIds', value: targetItems, type: 'bytes32[]' },
          { name: 'name', value: targetList, type: 'bytes32' },
        ],
      })
    } catch (err: any) {
      console.error(err)
      setChosenList(null)
      setDialogIsOpen(false)
      return
    }

    existingLists.refetch() // Update which lists this address has
    queryClient.invalidateQueries({ queryKey: ['user-list-tokens', address, targetList] }) // Update which toekns are in this list
    if (onAdd) onAdd()
    setChosenList(null)
    setDialogIsOpen(false)
  }

  if (existingLists.status != 'success') {
    return (
      <button className="btn-small" style={style}>
        <div style={{ position: 'relative', width: '1.5em', height: '1.5em' }}>
          <Image src="/img/loading.svg" alt="" fill={true} />
        </div>
      </button>
    )
  }

  let dialogView: React.ReactNode
  if (chosenList == null) {
    // Show list-picking form
    const tokenLabel = targetItems.length == 1 ? 'this MoonCat' : 'these MoonCats'
    const headerLabel = targetItems.length == 1 ? 'MoonCat' : 'MoonCats'
    dialogView = (
      <>
        <h3>Add {headerLabel} to a list</h3>
        <p style={{ margin: 0 }}>Pick an existing list of yours to add {tokenLabel} to:</p>
        <ul className="list-picker">
          {existingLists.data.map((l) => (
            <li key={l} onClick={(e) => addToken(l)}>
              {l}
            </li>
          ))}
        </ul>
        <p style={{ margin: 0 }}>Or, create a new list and add them to it:</p>
        <div style={{ display: 'flex', alignItems: 'baseline', gap: '1rem' }}>
          <input
            type="text"
            placeholder="New list title"
            style={{ flexGrow: 2, flexShrink: 1 }}
            onChange={(e) => setNewListTitle(e.target.value.substring(0, 20))}
            value={newListTitle}
          />
          <button
            className="btn-small"
            style={{ flexGrow: 0 }}
            onClick={(e) => {
              // Validate input
              if (newListTitle == '') return null

              addToken(newListTitle)
            }}
          >
            Create List
          </button>
        </div>
        {newListTitle.length >= 17 && (
          <p style={{ margin: 0, fontSize: '0.8rem', lineHeight: '1.2em', color: '#777', maxWidth: '40em' }}>
            List titles need to be 20 characters or less. If you have more to say about your list, you can add a
            description to it later.
          </p>
        )}
        <p style={{ marginTop: '2rem', textAlign: 'right' }}>
          <button className="btn-cancel" onClick={(e) => setDialogIsOpen(false)}>
            Cancel
          </button>
        </p>
      </>
    )
  } else {
    // Show attestation help
    dialogView = (
      <>
        <h3>Check your wallet to confirm this Attestation</h3>
        {attestHelpText}
      </>
    )
  }

  let parsedTitle: string
  if (typeof title != 'undefined') {
    parsedTitle = title
  } else if (targetItems.length == 1) {
    parsedTitle = 'Click to add this token to your saved lists'
  } else {
    parsedTitle = 'Click to add these tokens to your saved lists'
  }

  return (
    <>
      <button className="btn-small" style={style} onClick={(e) => setDialogIsOpen(true)} title={parsedTitle}>
        <Icon name="bookmarks" />
      </button>
      <dialog ref={dialogRef} aria-modal="true" onCancel={(e) => setDialogIsOpen(false)}>
        {dialogView}
      </dialog>
    </>
  )
}
export default AddToListButton
