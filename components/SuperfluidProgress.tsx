'use client'
import { Address } from 'viem'
import LoadingIndicator from './LoadingIndicator'
import { useQuery } from '@tanstack/react-query'
import { CSSProperties } from 'react'
import { ONE_HOUR } from 'lib/util'

interface QueryResponse {
  data: {
    streams: {
      currentFlowRate: number
      token: {
        symbol: string
        underlyingAddress: Address
      }
      sender: {
        id: Address
      }
    }[]
  }
}
const query = `query getFlows {
  streams(where: {receiver: "0xf45349435c4a7cbb519987bd4d7460479568fd63", currentFlowRate_gt: 0}) {
    currentFlowRate
    token { symbol, underlyingAddress }
    sender { id }
  }
}`

const GOAL = 250

const SuperfluidProgress = ({ style }: { style?: CSSProperties }) => {
  const { status, data } = useQuery({
    queryKey: ['superfluid-sponsors'],
    queryFn: async (): Promise<QueryResponse> => {
      const rs = await fetch('https://subgraph-endpoints.superfluid.dev/arbitrum-one/protocol-v1', {
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
        },
        method: 'POST',
        body: JSON.stringify({
          query: query,
        }),
      })
      if (!rs.ok) throw new Error('Failed to query GraphQL endpoint')
      return await rs.json()
    },
    staleTime: ONE_HOUR,
  })

  const showData = status != 'pending' && data

  const flowRate = showData
    ? data.data.streams.reduce((agg, s) => {
        switch (s.token.underlyingAddress) {
          case '0xda10009cbd5d07dd0cecc66161fc93d7c9000da1': // DAI
          case '0xaf88d065e77c8cC2239327C5EDb3A432268e5831': // USDC
            // value is 1:1
            return BigInt(s.currentFlowRate) + agg
          default:
            // Unknown token; skip valuing
            return agg
        }
      }, 0n) // Sum up the flows
    : 0n
  const monthlyTotal = Number((flowRate * 60n * 60n * 24n * 30n) / 1_000_000_000_000_000n) / 1000 // Use three decimals of precision

  let pctDone = Math.floor((monthlyTotal / GOAL) * 1000) / 10 // Use one decimal of precision
  if (pctDone > 100) pctDone = 100
  if (pctDone < 1) pctDone = 1

  const label = showData ? (
    <>
      ${monthlyTotal} of ${GOAL}/month complete
    </>
  ) : (
    <LoadingIndicator message="Checking sponsors..." />
  )

  return (
    <div
      style={{
        display: 'flex',
        gap: '0.5rem',
        justifyContent: 'center',
        flexWrap: 'wrap',
        maxWidth: '40em',
        margin: '0 auto',
        ...style,
      }}
    >
      <div className="progress-bar" style={{ flex: '0 0 20em' }} title={`${pctDone}% of $${GOAL}`}>
        <div style={{ width: pctDone + '%' }} />
      </div>
      <div style={{ flex: '0 5 auto', minWidth: '15em' }}>{label}</div>
    </div>
  )
}
export default SuperfluidProgress
