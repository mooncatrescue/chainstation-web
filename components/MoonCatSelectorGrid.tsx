import React, { useState } from 'react'
import MoonCatThumb from './MoonCatThumb'
import Pagination from './Pagination'
import { MoonCatData, MoonCatFilterSettings } from 'lib/types'
import { keepPreviousData, useQuery } from '@tanstack/react-query'
import LoadingIndicator from './LoadingIndicator'
import { ONE_HOUR } from 'lib/util'

const DEFAULT_PER_PAGE = 50

interface FilterParams {
  pageNumber: number
  perPage: number
  moonCats: number[] | 'all'
  filters: MoonCatFilterSettings
}

interface FilteredApiPage {
  moonCats: MoonCatData[]
  length: number
  totalLength: number
}

interface Props {
  children?: Parameters<typeof MoonCatThumb>[0]['thumbHandler']
  moonCats: number[] | 'all'
  perPage?: number
  extraParams?: Record<string, string>
  disabledMoonCats?: number[]
  highlightedMoonCats?: number[]
  onClick?: (moonCat: MoonCatData) => any
  minCellWidth?: number
}

const MoonCatGrid = ({
  children: thumbHandler,
  moonCats: allMoonCats,
  perPage,
  extraParams,
  disabledMoonCats = [],
  highlightedMoonCats = [],
  onClick,
  minCellWidth = 150,
}: Props) => {
  if (typeof perPage === 'undefined') {
    perPage = DEFAULT_PER_PAGE
  }
  const [filterProps, setFilterProps] = useState<FilterParams>({
    pageNumber: 0,
    perPage: perPage,
    moonCats: allMoonCats,
    filters: {},
  })

  const params = new URLSearchParams(filterProps.filters as Record<string, string>)
  params.set('limit', String(perPage!))
  params.set('offset', String(filterProps.pageNumber * perPage!))
  if (Array.isArray(filterProps.moonCats)) {
    params.set('mooncats', filterProps.moonCats.join(','))
  } else {
    params.set('mooncats', filterProps.moonCats)
  }
  if (extraParams) {
    for (const key of Object.keys(extraParams)) {
      params.set(key, extraParams[key])
    }
  }

  const filteredData = useQuery({
    queryKey: ['filtered-mooncats', params.toString()],
    queryFn: async (): Promise<FilteredApiPage> => {
      const rs =
        params.get('mooncats') == 'all'
          ? await fetch(`/api/mooncats?${params.toString()}`)
          : await fetch(`/api/mooncats`, {
              method: 'POST',
              headers: { 'Content-Type': 'application/json' },
              body: JSON.stringify(Object.fromEntries(params)),
            })
      if (!rs.ok) throw new Error('Failed to fetch filtered data page')
      return (await rs.json()) as FilteredApiPage
    },
    staleTime: ONE_HOUR,
    placeholderData: keepPreviousData,
  })

  // Event handler for updates from Pagination component when user navigates to a new page
  function handlePageUpdate(newPage: number) {
    setFilterProps((curProps) => {
      if (curProps.pageNumber == newPage) {
        // Already set to that value
        return curProps
      }

      return { ...curProps, pageNumber: newPage }
    })
  }

  if (!filteredData.data) return <LoadingIndicator message="Herding all the cats..." />

  return (
    <div className="mooncat-grid">
      <div
        className="item-grid"
        style={{
          '--thumb-height': `${minCellWidth * 0.8}px`,
          margin: '0 0 2rem',
          gridTemplateColumns: `repeat(auto-fill, minmax(${minCellWidth}px, 1fr))`,
        }}
      >
        {filteredData.data.moonCats.map((mc) => {
          let classes = []
          let clickHandler = onClick
          if (highlightedMoonCats.includes(mc.rescueOrder)) classes.push('highlight')
          if (disabledMoonCats.includes(mc.rescueOrder)) {
            classes.push('disabled')
            clickHandler = () => {}
          }

          return (
            <MoonCatThumb
              key={mc.rescueOrder}
              className={classes.join(' ')}
              moonCat={mc}
              isEventActive={false}
              thumbHandler={thumbHandler}
              onClick={clickHandler}
            />
          )
        })}
      </div>
      <Pagination
        currentPage={filterProps.pageNumber}
        maxPage={Math.ceil(filteredData.data.length / perPage) - 1}
        setCurrentPage={handlePageUpdate}
      />
    </div>
  )
}
export default MoonCatGrid
