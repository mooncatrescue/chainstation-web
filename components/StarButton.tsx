import { AttestationDoc, SCHEMAS, attestHelpText, revokeHelpText, useEas } from 'lib/eas'
import useSignedIn from 'lib/useSignedIn'
import useDialog from 'lib/useDialog'
import { API2_SERVER_ROOT, CHAIN_IDS, ChainName, switchToChain } from 'lib/util'
import Image from 'next/image'
import { Transaction } from '@ethereum-attestation-service/eas-sdk/dist/transaction'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import Icon from './Icon'
import { Address } from 'viem'

// https://arbitrum.easscan.org/schema/view/0xeae0ff58b769be065ba9d54f4dd9b88c07de93a00e1e3b889a7099de02dd5549
const SCHEMA_ID = SCHEMAS.PRODUCT_REVIEW

interface Props {
  targetAddress: Address
  chainName?: ChainName
  targetItem: `0x${string}`
  itemLabel: string
  message: string
  readOnly?: boolean
  onVote?: () => void
}
const StarButton = ({
  targetAddress,
  chainName = 'arb',
  targetItem,
  itemLabel,
  message,
  readOnly = false,
  onVote,
}: Props) => {
  const { isSignedIn, connectedAddress } = useSignedIn()
  const { eas, offchainSign } = useEas(CHAIN_IDS[chainName].chainId)
  const { ref: dialogRef, setIsOpen: setDialogIsOpen } = useDialog()
  const queryClient = useQueryClient()

  const { data: existingVotes, refetch } = useQuery({
    queryKey: ['token-stars', chainName, targetAddress, targetItem],
    queryFn: async () => {
      const rs = await fetch(
        `${API2_SERVER_ROOT}/attestations?&${new URLSearchParams({
          schema: `${chainName}:${SCHEMA_ID}`,
          recipient: `${chainName}:${targetAddress}`,
          data_key: 'productName',
          data_value: targetItem,
          limit: '100',
        })}`
      )
      if (!rs.ok) {
        console.error('Failed to fetch', rs)
        throw new Error('Failed to fetch')
      }
      return (await rs.json()) as AttestationDoc[]
    },
    select: (raw) =>
      raw
        .filter((att) => att.data.rating.value == 7) // Only keep those that are generic 'Likes'
        .map((att) => ({ uid: att.sig.uid, signer: att.signer })), // Only keep the data bits that are needed for this action
  })

  if (typeof existingVotes == 'undefined') {
    return (
      <button className="btn-small" disabled={true}>
        <div style={{ position: 'relative', width: '1.5em', height: '1.5em' }}>
          <Image src="/img/loading.svg" alt="" fill={true} />
        </div>
      </button>
    )
  }

  /**
   * Sign a new Attestation indicating a favorable vote for this token
   */
  async function castVote() {
    if (typeof connectedAddress == 'undefined') return // Must be connected
    if (!(await switchToChain(CHAIN_IDS[chainName].chainId))) return // Must be on the target network

    setDialogIsOpen(true)
    try {
      await offchainSign({
        schemaId: SCHEMA_ID as `0x${string}`,
        recipient: targetAddress,
        revocable: true,
        data: [
          { name: 'productName', value: targetItem, type: 'bytes32' },
          { name: 'review', value: message, type: 'string' },
          { name: 'rating', value: 7n, type: 'uint8' },
        ],
      })
    } catch (err) {
      console.error(err)
      setDialogIsOpen(false)
      return
    }

    if (onVote) onVote()
    queryClient.invalidateQueries({ queryKey: ['user-stars', connectedAddress] })
    setDialogIsOpen(false)
    refetch()
  }

  const existingVote = existingVotes.find((v) => v.signer == connectedAddress)
  const hasVoted: boolean = typeof connectedAddress !== 'undefined' && typeof existingVote != 'undefined'
  const voteCount = existingVotes.length

  async function revokeVote() {
    if (!existingVote || !connectedAddress) return

    // Check if the current Attestation was revoked out-of-band
    const precheck = await fetch(`${API2_SERVER_ROOT}/check-attestation?id=${existingVote.uid}`, { method: 'POST' })
    if (precheck.ok) {
      const data = await precheck.json()
      if (data.isRevoked > 0) {
        // The current Attestation for this address was revoked. Our view must be out-of-date; refresh all votes
        refetch()
        return
      }
    }

    setDialogIsOpen(true)
    let rs: Transaction<bigint>
    try {
      rs = await eas.revokeOffchain(existingVote.uid) // Await user signing transaction
    } catch (err) {
      console.error(err)
      setDialogIsOpen(false)
      return
    }
    setDialogIsOpen(false)

    await rs.wait() // Await transaction finalized in blockchain
    // Update local cache of attestation
    await fetch(`${API2_SERVER_ROOT}/check-attestation?id=${existingVote.uid}`, { method: 'POST' })
    queryClient.invalidateQueries({ queryKey: ['user-stars', connectedAddress] })
    refetch()
  }

  const iconName = hasVoted ? 'star-full' : 'star-empty'
  const countView =
    voteCount >= 100 ? (
      <>
        <Icon name={iconName} />
        <Icon name={iconName} />
        <Icon name={iconName} />
      </>
    ) : (
      <>
        {voteCount.toLocaleString()}
        <Icon style={{ paddingLeft: '0.3rem' }} name={iconName} />
      </>
    )

  if (!isSignedIn || readOnly) {
    // Show a read-only view of the current star-count
    const notice = readOnly ? '' : `Sign in to vote for this ${itemLabel}`
    return (
      <button
        className="btn-small read-only"
        title={notice}
        itemProp="interactionStatistic"
        itemScope
        itemType="https://schema.org/InteractionCounter"
      >
        <meta itemProp="interactionType" content="https://schema.org/LikeAction" />
        <meta itemProp="userInteractionCount" content={String(voteCount)} />
        {countView}
      </button>
    )
  }

  if (hasVoted) {
    return (
      <>
        <button
          className="btn-small highlight"
          onClick={revokeVote}
          title={`Click to undo your like of this ${itemLabel}`}
        >
          {countView}
        </button>
        <dialog ref={dialogRef} aria-modal="true" onCancel={(e) => e.preventDefault()}>
          <h3>Check your wallet to confirm revoking this Attestation</h3>
          {revokeHelpText}
        </dialog>
      </>
    )
  }

  // Visitor has not voted for this item
  return (
    <>
      <button className="btn-small" onClick={castVote} title={`Click to attest you like this ${itemLabel}`}>
        {countView}
      </button>
      <dialog ref={dialogRef} aria-modal="true" onCancel={(e) => e.preventDefault()}>
        <h3>Check your wallet to confirm this Attestation</h3>
        {attestHelpText}
      </dialog>
    </>
  )
}
export default StarButton
