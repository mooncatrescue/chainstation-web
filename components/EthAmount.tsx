import { formatEther } from 'viem'
import Icon from './Icon'

const EthAmount = ({ amount, truncateTo = 25 }: { amount: bigint | number | string; truncateTo?: number }) => {
  let formatted = formatEther(BigInt(amount)).slice(0, truncateTo)
  if (formatted.indexOf('.') > 0) {
    // If the formatted value is a fractional amount, trim any trailing zeroes
    formatted = formatted
      .replace(/0+$/, '') // Trim trailing zeroes
      .replace(/\.$/, '') // Trim off the decimal point if it's now the last character in the string
  }
  return (
    <span style={{ whiteSpace: 'nowrap' }}>
      {[
        <Icon key="icon" name="ethereum" style={{ marginRight: '0.1em', verticalAlign: '-0.15em' }} />,
        formatted + ' ETH',
      ]}
    </span>
  )
}
export default EthAmount
