'use client'
import { MoonCatData } from "lib/types"
import Icon from "./Icon"
import MoonCatGrid from "./MoonCatGrid"

const MoonCatsNamedHome = () => {
  return (
    <MoonCatGrid moonCats="all" extraParams={{ onlynamed: 'yes' }}>
    {(moonCat: MoonCatData) => {
      let name = null
      if (moonCat.name === true) {
        name = (
          <p>
            <Icon name="question" />
          </p>
        )
      } else {
        name = <p>{moonCat.name}</p>
      }

      return (
        <>
          <p>Named #{moonCat.namedOrder}</p>
          <p>Rescued #{moonCat.rescueOrder}</p>
          <p>
            <code>{moonCat.catId}</code>
          </p>
          {name}
        </>
      )
    }}
  </MoonCatGrid>
  )
}
export default MoonCatsNamedHome