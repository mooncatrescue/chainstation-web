'use client'
import { useQuery } from '@tanstack/react-query'
import {
  AppGlobalActionType,
  AppGlobalViewAction,
  EcosystemEvent,
  getCurrentEvent,
  LOCALSTORAGE_VIEWCHANGE_KEY,
  ZWS,
} from 'lib/util'
import MoonCatGrid from './MoonCatGrid'
import { AppVisitorContext } from 'lib/AppVisitorProvider'
import { useContext, useEffect } from 'react'

const EventBanner = ({ currentEvent }: { currentEvent: EcosystemEvent | null | undefined }): JSX.Element | null => {
  const {
    state: { viewPreference },
  } = useContext(AppVisitorContext)

  if (currentEvent == null || viewPreference != 'event') return null
  switch (currentEvent.label.toLowerCase()) {
    case 'anniversary': {
      return (
        <div className="text-container">
          <section className="card">
            <h3>Happy Anniversary MoonCats!</h3>
            <p>
              It&rsquo;s party season! The MoonCat{ZWS}Rescue Insanely Cute Operation launched August 9, 2017, and so for the
              month of August, the MoonCats are in a partying mood. Take a peek at which MoonCats have anniversary
              celebration accessories from across the six years of this project&rsquo;s existence.
            </p>
          </section>
        </div>
      )
    }
  }
  return null
}

const MoonCatsHome = () => {
  const { status, data: currentEvent } = useQuery({
    queryKey: ['current-event'],
    queryFn: getCurrentEvent,
  })

  const { dispatch } = useContext(AppVisitorContext)

  useEffect(() => {
    if (!currentEvent) return
    const lastViewChange = localStorage.getItem(LOCALSTORAGE_VIEWCHANGE_KEY)
    const lastViewDate = lastViewChange ? new Date(Number(lastViewChange)) : null
    const eventStartDate = new Date(currentEvent.start)
    if (!lastViewDate || eventStartDate > lastViewDate) {
      // Visitor has not changed views while the current event has been active
      dispatch({
        type: AppGlobalActionType.SET_VIEW_PREFERENCE,
        payload: 'event',
      } as AppGlobalViewAction)
    }
  }, [dispatch, currentEvent])

  return (
    <>
      <EventBanner currentEvent={currentEvent} />
      <MoonCatGrid moonCats="all" isEventActive={currentEvent != null} />
    </>
  )
}
export default MoonCatsHome
