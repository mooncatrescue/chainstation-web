'use client'
import { useState } from 'react'
import Link from 'next/link'
import { Address, formatEther, parseAbi } from 'viem'
import { useReadContract, useReadContracts } from 'wagmi'
import { AccessoryContractData, AccessoryTraits } from 'lib/types'
import { Listing } from 'lib/reservoirData'
import { ACCESSORIES_ADDRESS, API_SERVER_ROOT, bytes32ToString, parseAccessoryMetabyte, pluck } from 'lib/util'
import parseDescription from 'lib/parseDescription'
import useIsMounted from 'lib/useIsMounted'
import useSignedIn from 'lib/useSignedIn'
import AccessorizedMoonCat from 'components/AccessorizedMoonCat'
import AccessoryInfoCard from 'components/AccessoryInfoCard'
import Icon from 'components/Icon'
import JsonLd from 'components/JsonLd'
import LoadingIndicator from 'components/LoadingIndicator'
import MoonCatAccessoryOwners from 'components/MoonCatAccessoryOwners'
import PurchaseAccessory from 'components/PurchaseAccessory'
import SignInFirst from 'components/SignInFirst'
import WarningIndicator from 'components/WarningIndicator'

const ACCESSORIES = {
  address: ACCESSORIES_ADDRESS as Address,
  chainId: 1,
  abi: parseAbi([
    'function accessoryInfo(uint256 accessoryId) external view returns (uint16 totalSupply, uint16 availableSupply, bytes28 name, address manager, uint8 metabyte, uint8 availablePalettes, bytes2[4] memory positions, bool availableForPurchase, uint256 price)',
    'function accessoryEligibleList(uint256 accessoryId) external view returns (bytes32[100] memory)',
    'function owner() external view returns (address)',
  ]),
}

type Props =
  | {
      accessory: number
      traits: AccessoryTraits
      listings: Listing[]
    }
  | {
      accessory: number
      traits: false
      listings: false
    }

/**
 * Show detail page for a specific Accessory
 */
const AccessoryDetail = ({ accessory, traits, listings }: Props) => {
  const {
    data: accessoryDetail,
    error,
    status,
  } = useReadContracts({
    contracts: [
      {
        ...ACCESSORIES,
        functionName: 'accessoryInfo',
        args: [BigInt(accessory)],
      },
      {
        ...ACCESSORIES,
        functionName: 'accessoryEligibleList',
        args: [BigInt(accessory)],
      },
    ],
    allowFailure: false,
    query: {
      select: (data): AccessoryContractData => {
        return {
          totalSupply: data[0][0],
          availableSupply: data[0][1],
          name: bytes32ToString(data[0][2]),
          manager: data[0][3],
          metabyte: data[0][4],
          availablePalettes: data[0][5],
          positions: data[0][6] as readonly `0x${string}`[],
          availableForPurchase: data[0][7],
          price: data[0][8],
          eligibleList: data[1] as `0x${string}`[],
        }
      },
    },
  })
  const { data: owner } = useReadContract({
    ...ACCESSORIES,
    functionName: 'owner',
  })

  const mounted = useIsMounted()
  const { verifiedAddresses, isSignedIn, doSignIn } = useSignedIn()
  const isOwner = verifiedAddresses.findIndex((a) => a.address == owner) >= 0

  const [heroMoonCat, setHeroMoonCat] = useState<false | number>(
    traits && traits.ownedBy.list.length > 0 ? pluck(traits.ownedBy.list).rescueOrder : false
  )
  const [activePalette, setActivePalette] = useState<number>(0)

  if (!mounted || !accessoryDetail) {
    // Loading view
    const pageTitle = `Accessory #${accessory}`

    // Assemble a JSONLD-formatted details object
    const metaObj: any = {
      '@context': 'https://schema.org',
      '@type': 'ItemPage',
      'isFamilyFriendly': true,
      'breadcrumb': {
        '@type': 'BreadcrumbList',
        'itemListElement': [
          {
            '@type': 'ListItem',
            'position': 1,
            'name': 'Accessories',
            'item': 'https://chainstation.mooncatrescue.com/accessories',
          },
          {
            '@type': 'ListItem',
            'position': 2,
            'name': traits ? traits.name : pageTitle,
          },
        ],
      },
      'mainEntity': {
        '@type': 'Product',
        'productID': accessory,
        'name': traits ? traits.name : pageTitle,
        'description': traits ? traits?.description : undefined,
        'image': `${API_SERVER_ROOT}/accessory-image/${accessory}`,
      },
    }
    if (traits) {
      metaObj.mainEntity.creator = {
        'identifier': traits.creator,
      }
      if (traits.created.timestamp)
        metaObj.mainEntity.releaseDate = new Date(traits.created.timestamp * 1000).toISOString()
      if (traits.availableSupply.value > 0) {
        metaObj.mainEntity.offers = {
          '@type': 'Offer',
          'availability': 'https://schema.org/InStock',
          'price': formatEther(BigInt(traits.price.value)),
          'priceCurrency': 'ETH',
        }
      }
      if (traits.likesCount && traits.likesCount > 0) {
        metaObj.interactionStatistic = {
          '@type': 'InteractionCounter',
          'interactionType': 'https://schema.org/LikeAction',
          'userInteractionCount': traits.likesCount,
        }
      }
    }
    return (
      <div id="content-container">
        <JsonLd data={metaObj} />
        <nav className="breadcrumb">
          <Link href="/accessories">
            <>
              <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
              Browse all Accessories
            </>
          </Link>
        </nav>
        <div className="text-container">
          <h1 className="hero">{pageTitle}</h1>
          <LoadingIndicator />
        </div>
      </div>
    )
  }

  if (!traits) {
    // Error view
    const pageTitle = `Accessory #${accessory}`
    return (
      <div id="content-container">
        <nav className="breadcrumb">
          <Link href="/accessories">
            <>
              <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
              Browse all Accessories
            </>
          </Link>
        </nav>
        <div className="text-container">
          <h1 className="hero">{pageTitle}</h1>
          <WarningIndicator message="Failed to load Accessory metadata" />
        </div>
      </div>
    )
  }

  const pageTitle = `${accessoryDetail.name} - Accessory #${accessory}`

  // Parse metabyte
  const metadata = parseAccessoryMetabyte(accessoryDetail.metabyte)

  if (metadata.audience >= 2 && !isSignedIn) {
    // Redacted view
    const pageTitle = `Accessory #${accessory}`
    return (
      <div id="content-container">
        <nav className="breadcrumb">
          <Link href="/accessories">
            <>
              <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
              Browse all Accessories
            </>
          </Link>
        </nav>
        <div className="text-container">
          <h1 className="hero">{pageTitle}</h1>
          <section className="card" style={{ textAlign: 'center' }}>
            <p>There is an accessory with this ID. Log in to see it.</p>
            <p>
              <button onClick={(e) => doSignIn()}>Sign In</button>
            </p>
          </section>
        </div>
      </div>
    )
  }

  let accHeroImage: JSX.Element
  if (traits) {
    const moonCatOwners = traits.ownedBy.list
    // We know which MoonCats own this accessory; use one of them as the example
    if (moonCatOwners.length == 1) {
      // This is a "one of one" unique accessory
      const moonCat = moonCatOwners[0]
      accHeroImage = (
        <div style={{ flex: '1 1 30%' }}>
          <a href={'/mooncats/' + moonCat.rescueOrder}>
            <AccessorizedMoonCat
              moonCat={moonCat.rescueOrder}
              accessories={[accessory + ':' + activePalette]}
              style={{ maxHeight: 350, maxWidth: '95vw', display: 'block', margin: '0 auto' }}
            />
            <p style={{ textAlign: 'center' }}>MoonCat #{moonCat.rescueOrder}</p>
          </a>
        </div>
      )
    } else if (moonCatOwners.length == 0) {
      // No purchases of this accessory yet
      accHeroImage = (
        <picture style={{ flex: '1 1 30%' }}>
          <img
            src={`${API_SERVER_ROOT}/accessory-image/${accessory}`}
            alt={`Accessory #${accessory})`}
            style={{ maxHeight: 350, maxWidth: '95vw', display: 'block', margin: '0 auto' }}
            itemProp="image"
          />
        </picture>
      )
    } else {
      // There's several MoonCats to pick from to show as the example
      accHeroImage = (
        <picture style={{ flex: '1 1 30%' }}>
          <img
            src={`${API_SERVER_ROOT}/image/${heroMoonCat}?costumes=true&acc=${accessory}:${activePalette}`}
            alt={`Accessory #${accessory})`}
            style={{ maxHeight: 350, maxWidth: '95vw', display: 'block', margin: '0 auto' }}
            itemProp="image"
          />
        </picture>
      )
    }
  } else {
    // Use a random MoonCat as the base for the accessory preview
    accHeroImage = (
      <picture style={{ flex: '1 1 30%' }}>
        <img
          src={`${API_SERVER_ROOT}/accessory-image/${accessory}`}
          alt={`Accessory #${accessory})`}
          style={{ maxHeight: 350, maxWidth: '95vw', display: 'block', margin: '0 auto' }}
          itemProp="image"
        />
      </picture>
    )
  }

  const moonCatOwners = traits.ownedBy.list.sort((a, b) => a.rescueOrder - b.rescueOrder)

  const ownersView = moonCatOwners ? (
    <MoonCatAccessoryOwners accessory={accessory} moonCatOwners={moonCatOwners} listings={listings} />
  ) : (
    <LoadingIndicator />
  )

  return (
    <div id="content-container" itemScope itemType="https://schema.org/ItemPage">
      <meta itemProp="isFamilyFriendly" content="true" />
      <nav className="breadcrumb">
        <Link href="/accessories">
          <>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Browse all Accessories
          </>
        </Link>
      </nav>
      <JsonLd
        data={{
          '@context': 'https://schema.org',
          '@type': 'BreadcrumbList',
          'itemListElement': [
            {
              '@type': 'ListItem',
              'position': 1,
              'name': 'Accessories',
              'item': 'https://chainstation.mooncatrescue.com/accessories',
            },
            {
              '@type': 'ListItem',
              'position': 2,
              'name': accessoryDetail.name,
            },
          ],
        }}
      />
      <div className="text-container" itemProp="mainEntity" itemScope itemType="https://schema.org/Product">
        {traits.created.timestamp && (
          <meta itemProp="releaseDate" content={new Date(traits.created.timestamp * 1000).toISOString()} />
        )}
        <h1 className="hero" itemProp="name">
          {accessoryDetail.name}
        </h1>
        <h3 style={{ textAlign: 'center', marginTop: '-2rem' }}>Accessory #{accessory}</h3>
        <div style={{ margin: '2rem 0', display: 'flex', flexWrap: 'wrap' }}>
          {accHeroImage}
          <section className="card" style={{ flex: '1 1 30em' }}>
            {isSignedIn && verifiedAddresses.filter((a) => a.address == accessoryDetail.manager).length > 0 && (
              <div style={{ float: 'right', display: 'flex', margin: '-0.5rem -0.5rem 0 0' }}>
                <Link href={`/accessories/${accessory}/manage`} className="btn btn-small">
                  Manage
                </Link>
              </div>
            )}
            <AccessoryInfoCard
              accessoryId={accessory}
              accessoryDetail={accessoryDetail}
              accessoryTraits={traits}
              showAdminTools={isOwner}
              onPaletteChanged={(paletteIndex) => {
                if (traits.ownedBy.list.length > 0) setHeroMoonCat(pluck(traits.ownedBy.list).rescueOrder)
                setActivePalette(paletteIndex)
              }}
            />
          </section>
        </div>
        {traits.description && (
          <section className="card" itemProp="description">
            {parseDescription(traits.description)}
          </section>
        )}
        {accessoryDetail.availableForPurchase && (
          <SignInFirst preamble={<h3>Buy this Accessory</h3>}>
            {(didInteract, preamble) => (
              <PurchaseAccessory
                didInteract={didInteract}
                preamble={preamble}
                accessoryId={accessory}
                accessoryDetail={accessoryDetail}
                name={accessoryDetail.name}
                background={metadata.background}
                price={accessoryDetail.price}
              />
            )}
          </SignInFirst>
        )}
      </div>
      {ownersView}
    </div>
  )
}

export default AccessoryDetail
