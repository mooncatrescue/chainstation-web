'use client'
import { useReadContract } from 'wagmi'
import AccessoriesStrip from './AccessoriesStrip'
import { ACCESSORIES_ADDRESS } from 'lib/util'
import { Address, parseAbi } from 'viem'
import useSignedIn from 'lib/useSignedIn'

const ACCESSORIES = {
  address: ACCESSORIES_ADDRESS as Address,
  abi: parseAbi(['function owner() external view returns (address)']),
  chainId: 1,
} as const

const RecentlyLaunchedAccessories = () => {
  const { data: owner } = useReadContract({
    ...ACCESSORIES,
    functionName: 'owner',
  })
  const { connectedAddress } = useSignedIn()
  return (
    <section className="card">
      <h2>Recently Launched</h2>
      <p>The newest, shiniest, brightest newcomers to the Boutique!</p>
      <AccessoriesStrip
        filters={connectedAddress == owner ? {
          verified: 'any',
        } : {
          verified: 'yes'
        }}
      />
    </section>
  )
}
export default RecentlyLaunchedAccessories
