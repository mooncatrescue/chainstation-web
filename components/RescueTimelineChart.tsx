'use client'
import React, { useEffect, useRef } from 'react'
import * as d3 from 'd3'
import _rawRescues from 'lib/mooncat_rescues.json'
import _rawDateBlocks from 'lib/date_blocks.json'
import { API_SERVER_ROOT, debounce } from 'lib/util'

interface RescueMeta {
  txHash: `0x${string}`
  blockHeight: number
  timestamp: number
  date: Date
  rescueOrder: number
  seed: `0x${string}`
  catId: `0x${string}`
  rescuer: `0x${string}`
}

// Convert raw rescue data into the data we need for charting easily
const rescueTxs: RescueMeta[] = Object.values(_rawRescues).map(
  (d): RescueMeta =>
    ({
      ...d,
      date: new Date(d.timestamp * 1000),
    } as RescueMeta)
)

const dateBlocks = _rawDateBlocks
  // Reformat date to a JavaScript Date object
  .map((b) => ({
    blockNumber: b.blockNumber,
    timestamp: b.timestamp,
    date: new Date(b.timestamp * 1000),
  }))
  // Only keep days that are the first day of a calendar month
  .filter((b) => {
    return b.date.getUTCDate() == 1
  })
// Keep just the January months to represent the start of a calendar year
const yearBlocks = dateBlocks.filter((b) => b.date.getUTCMonth() == 0)
// For each calendar year, what block number started and ended it?
let yearRanges: { year: number; startBlock: number; endBlock: number }[] = []
for (let i = 0; i < yearBlocks.length - 2; i++) {
  yearRanges.push({
    year: yearBlocks[i].date.getUTCFullYear(),
    startBlock: yearBlocks[i].blockNumber,
    endBlock: yearBlocks[i + 1].blockNumber - 1,
  })
}

const chartConfig = {
  height: 1000,
  width: 800,
  margin: {
    top: 30,
    right: 5,
    bottom: 15,
    left: 180,
  },
}
const plotHeight = chartConfig.height - chartConfig.margin.top - chartConfig.margin.bottom

/**
 * Create and update background boxes that shade the year ranges
 */
function drawYearBlocks(y: d3.ScaleLinear<number, number>, svg: d3.Selection<SVGSVGElement, any, any, any>) {
  function calcOffset(r: (typeof yearRanges)[0]): number {
    if (y(r.endBlock) < -10) return -10 // Year ends far above the chart
    if (y(r.endBlock) > chartConfig.height + 10) return chartConfig.height + 10 // Year ends far below the chart
    return Math.floor(y(r.endBlock + 0.5))
  }
  function calcHeight(r: (typeof yearRanges)[0]): number {
    if (y(r.startBlock) < -10) return 0 // Year begins and ends above the chart
    if (y(r.startBlock) > chartConfig.height + 10) return chartConfig.height + 10 // Year extends far below the chart
    return Math.floor(y(r.startBlock - 0.5) - calcOffset(r))
  }
  svg
    .select('#year-blocks')
    .selectAll<d3.BaseType, (typeof yearRanges)[0]>('g')
    .data(yearRanges, (d) => d.year)
    .join(
      (enter) =>
        enter
          .append('g')
          .attr('transform', (d) => `translate(0,${calcOffset(d)})`)
          .append('rect')
          .style('fill', (d, i) => (i % 2 == 0 ? 'rgba(255, 255, 0, 0.1)' : 'transparent'))
          .attr('width', chartConfig.width)
          .attr('height', calcHeight),
      (update) =>
        update
          .attr('transform', (d) => `translate(0,${calcOffset(d)})`)
          .select('rect')
          .attr('height', calcHeight)
    )
}

/**
 * Create and update the timeline ticks
 */
function drawBlockTicks(y: d3.ScaleLinear<number, number>, svg: d3.Selection<SVGSVGElement, any, any, any>) {
  const blockTicks = y.ticks().filter(Number.isInteger)
  const TICK_SIZE = 100
  svg
    .select('#axis')
    .selectAll<d3.BaseType, number[]>('g')
    .data(blockTicks)
    .join(
      (enter) => {
        const g = enter.append('g').attr('transform', (d) => `translate(0,${Math.floor(y(d))})`)
        g.append('text')
          .attr('x', -1 * TICK_SIZE - 5)
          .attr('y', '0.4em')
          .text((d) => d.toLocaleString())
        g.append('line')
          .style('stroke', '#FFF')
          .style('opacity', 0.5)
          .attr('y1', 0.5)
          .attr('x1', -1 * TICK_SIZE)
          .attr('x2', 0)
          .attr('y2', 0.5)
        return g
      },
      (update) =>
        update
          .attr('transform', (d) => `translate(0,${Math.floor(y(d))})`)
          .select('text')
          .text((d) => d.toLocaleString())
    )
}

const formatMonthYear = d3.utcFormat('%b %Y')
const formatYear = d3.utcFormat('%Y')
/**
 * Create and update labels for calendar dates
 */
function drawMonthYearTicks(y: d3.ScaleLinear<number, number>, svg: d3.Selection<SVGSVGElement, any, any, any>) {
  const domain = y.domain()
  svg
    .select('#dates')
    .selectAll<d3.BaseType, (typeof dateBlocks)[0]>('g')
    .data(
      dateBlocks.filter((d) => d.blockNumber > domain[0] && d.blockNumber < domain[1]),
      (d) => d.blockNumber
    )
    .join(
      (enter) => {
        const g = enter.append('g').attr('transform', (d) => `translate(-40,${Math.floor(y(d.blockNumber))})`)
        g.append('text')
          .attr('y', -2)
          .text((d) => formatMonthYear(d.date))
        g.append('line')
          .style('stroke', '#FFF')
          .style('opacity', 0.3)
          .style('stroke-width', '1px')
          .attr('x1', -50)
          .attr('y1', 0.5)
          .attr('x2', 40)
          .attr('y2', 0.5)
        return g
      },
      (update) => {
        update
          .attr('transform', (d) => `translate(-40,${Math.floor(y(d.blockNumber))})`)
          .select('text')
          .text((d) => formatMonthYear(d.date))
        update.select('line').attr('x1', -50)
        return update
      }
    )
}
function drawYearTicks(y: d3.ScaleLinear<number, number>, svg: d3.Selection<SVGSVGElement, any, any, any>) {
  const domain = y.domain()
  svg
    .select('#dates')
    .selectAll<d3.BaseType, (typeof dateBlocks)[0]>('g')
    .data(
      dateBlocks.filter((d) => d.date.getUTCMonth() == 0 && d.blockNumber > domain[0] && d.blockNumber < domain[1]),
      (d) => d.blockNumber
    )
    .join(
      (enter) => {
        const g = enter.append('g').attr('transform', (d) => `translate(-40,${Math.floor(y(d.blockNumber))})`)
        g.append('text')
          .attr('y', -2)
          .text((d) => formatMonthYear(d.date))
        g.append('line')
          .style('stroke', '#FFF')
          .style('opacity', 0.3)
          .style('stroke-width', '1px')
          .attr('x1', -30)
          .attr('y1', 0.5)
          .attr('x2', 40)
          .attr('y2', 0.5)
        return g
      },
      (update) => {
        update
          .attr('transform', (d) => `translate(-40,${Math.floor(y(d.blockNumber))})`)
          .select('text')
          .text((d) => formatYear(d.date))
        update.select('line').attr('x1', -30)
        return update
      }
    )
}

/**
 * Create and update circles to represent rescue clusters
 */
function drawTxs(
  y: d3.ScaleLinear<number, number>,
  svg: d3.Selection<SVGSVGElement, any, any, any>,
  data: d3.Bin<RescueMeta, number>[]
) {
  function radiusSize(d: d3.Bin<RescueMeta, number>) {
    const clusterSize = d.length
    if (clusterSize == 1) {
      return 3
    } else if (clusterSize <= 10) {
      return 5
    } else if (clusterSize <= 30) {
      return 10
    } else if (clusterSize <= 50) {
      return 12
    } else {
      return 15
    }
  }
  function color(d: d3.Bin<RescueMeta, number>) {
    const clusterSize = d.length
    if (clusterSize <= 60) {
      return 'rgb(74 35 90 / 70%)'
    } else if (clusterSize <= 100) {
      return 'rgb(120 35 100 / 70%)'
    } else if (clusterSize <= 300) {
      return 'rgb(190 45 140 / 70%)'
    } else if (clusterSize <= 600) {
      return 'rgb(221 41 152 / 70%)'
    } else {
      return 'rgb(255 17 99 / 70%)'
    }
  }

  function midpoint(d: d3.Bin<RescueMeta, number>) {
    if (d.length == 1) return d[0].blockHeight
    const max = d[d.length - 1].blockHeight
    const min = d[0].blockHeight
    return min + (max - min) / 2
  }

  svg
    .select('#data')
    .selectAll<d3.BaseType, d3.Bin<RescueMeta, number>>('g')
    .data(
      data.filter((d) => d.length > 0),
      (d) => `${d.x0}-${d.x1}`
    )
    .join(
      (enter) =>
        enter
          .append('g')
          .attr('transform', (d: d3.Bin<RescueMeta, number>) => `translate(0,${Math.floor(y(midpoint(d)))})`)
          .append('circle')
          .style('fill', color)
          .style('stroke', '#FFF')
          .style('stroke-width', '2px')
          .attr('r', radiusSize)
          .attr('title', (d) => `${d.length} rescues`),
      (update) =>
        update
          .attr('transform', (d: d3.Bin<RescueMeta, number>) => `translate(0,${Math.floor(y(midpoint(d)))})`)
          .select('circle')
          .attr('r', radiusSize)
          .style('fill', color)
    )
}

const formatDate = d3.utcFormat('%Y.%m.%d at %H:%M:%S')

const THUMBNAIL_SIZE = 30
const GAP_SIZE = 5

const drawRescueImages = debounce((svg: d3.Selection<SVGSVGElement, any, any, any>) => {
  svg
    .select('#rescues')
    .selectAll<d3.BaseType, RescueMeta>('g')
    .append('image')
    .attr('x', 100)
    .attr('y', THUMBNAIL_SIZE / -2)
    .attr('width', THUMBNAIL_SIZE)
    .attr('height', THUMBNAIL_SIZE)
    .attr('preserveAspectRatio', 'xMidYMid')
    .attr('href', (d) => `${API_SERVER_ROOT}/image/${d.rescueOrder}`)
}, 200)

/**
 * Create and update the individual rescue event details.
 * Because the rescues are overlapping at most zoom levels, only a few rescues can be shown without overlapping the labels.
 * This function ensures the first and last data elements are drawn, and then fills in as many as possible between them for the current zoom level.
 * It calculates from the first data element even if it's offscreen, to prevent flicker while panning.
 */
function drawRescues(
  y: d3.ScaleLinear<number, number>,
  svg: d3.Selection<SVGSVGElement, any, any, any>,
  data: RescueMeta[]
) {
  // Figure out which specific Rescues to enumerate
  let showDates: RescueMeta[] = []
  let maxY = plotHeight
  let minY = 0
  if (data.length > 0) {
    showDates.push(data[0])
    maxY = y(data[0].blockHeight) - THUMBNAIL_SIZE / 2
  }
  if (data.length > 1 && y(data[data.length - 1].blockHeight) <= maxY - THUMBNAIL_SIZE / 2 - GAP_SIZE) {
    showDates.push(data[data.length - 1])
    minY = y(data[data.length - 1].blockHeight) + THUMBNAIL_SIZE / 2
  }
  data.forEach((d) => {
    const plotPoint = y(d.blockHeight)
    if (plotPoint <= maxY - THUMBNAIL_SIZE / 2 - GAP_SIZE && plotPoint >= minY + THUMBNAIL_SIZE / 2 + GAP_SIZE) {
      showDates.push(d)
      maxY = plotPoint - THUMBNAIL_SIZE / 2
    }
  })

  const domain = y.domain()
  const txInBounds = showDates.filter((d) => d.blockHeight >= domain[0] - 10 && d.blockHeight <= domain[1] + 10)

  svg
    .select('#rescues')
    .selectAll<d3.BaseType, RescueMeta>('g')
    .data(txInBounds, (d) => d.rescueOrder)
    .join(
      (enter) => {
        const g = enter.append('g').attr('transform', (d: RescueMeta) => `translate(0,${Math.floor(y(d.blockHeight))})`)
        g.append('line')
          .attr('x1', 0)
          .attr('y1', 0)
          .attr('x2', 90)
          .attr('y2', 0)
          .style('stroke', '#8e8')
          .style('stroke-width', '2px')
          .style('opacity', 0.5)
        g.append('text')
          .attr('x', 105 + THUMBNAIL_SIZE)
          .attr('y', '0.4em')
          .style('fill', '#8e8')
          .text((d) => {
            return `MoonCat #${d.rescueOrder}, rescued on ${formatDate(d.date)} UTC`
          })
        return g
      },
      (update) => update.attr('transform', (d: RescueMeta) => `translate(0,${Math.floor(y(d.blockHeight))})`)
    )

  drawRescueImages(svg)
}

const BIN_COUNT = 70
const RescueTimelineChart = () => {
  const ref = useRef<SVGSVGElement>(null)

  let y = d3
    .scaleLinear()
    .domain(d3.extent(rescueTxs, (d: RescueMeta) => d.blockHeight) as [number, number])
    .range([plotHeight, 0])
    .nice()

  useEffect(() => {
    if (ref.current == null) return
    const svg = d3.select<SVGSVGElement, null>(ref.current)
    drawYearBlocks(y, svg)
    drawBlockTicks(y, svg)
    drawYearTicks(y, svg)

    const domain = y.domain() as [number, number]
    let txInBounds = rescueTxs.filter((d) => d.blockHeight >= domain[0] && d.blockHeight <= domain[1])
    const binned = d3
      .bin<RescueMeta, number>()
      .domain(domain)
      .thresholds(y.ticks(BIN_COUNT))
      .value((d) => d.blockHeight)(txInBounds)

    drawTxs(y, svg, binned)
    drawRescues(y, svg, txInBounds)

    const zoom = d3
      .zoom<SVGSVGElement, null>()
      .scaleExtent([1, 500000])
      .translateExtent([
        [0, -50],
        [0, plotHeight + 50],
      ])
      .on('zoom', ({ transform }) => {
        const zoomedY = transform.rescaleY(y)
        drawYearBlocks(zoomedY, svg)
        drawBlockTicks(zoomedY, svg)
        if (transform.k > 2) {
          drawMonthYearTicks(zoomedY, svg)
        } else {
          drawYearTicks(zoomedY, svg)
        }

        const domain = zoomedY.domain() as [number, number]
        let txInBounds = rescueTxs.filter((d) => d.blockHeight >= domain[0] - 10 && d.blockHeight <= domain[1] + 10)
        const binned = d3
          .bin<RescueMeta, number>()
          .domain(domain)
          .thresholds(zoomedY.ticks(BIN_COUNT))
          .value((d) => d.blockHeight)(txInBounds)

        drawTxs(zoomedY, svg, binned)
        drawRescues(zoomedY, svg, rescueTxs)
      })
    svg.call(zoom)
  }, [y])

  return (
    <svg style={{ width: chartConfig.width, height: chartConfig.height, background: 'rgba(0,0,0, 0.7)' }} ref={ref}>
      <g
        id="year-blocks"
        style={{ fontSize: '7pt', fill: '#FFF', textAnchor: 'end' }}
        transform={`translate(0,${chartConfig.margin.top})`}
      ></g>
      <text
        x={chartConfig.margin.left - 105}
        y="17"
        style={{ fontSize: '12pt', fontWeight: 'bold', fill: '#fff', textAnchor: 'end' }}
      >
        Block
      </text>
      <text
        x={chartConfig.margin.left - 40}
        y="17"
        style={{ fontSize: '12pt', fontWeight: 'bold', fill: '#fff', textAnchor: 'end' }}
      >
        Date
      </text>
      <g
        id="axis"
        style={{ fontSize: '8pt', fill: '#FFF', textAnchor: 'end' }}
        transform={`translate(${chartConfig.margin.left},${chartConfig.margin.top})`}
      >
        <line y2={plotHeight} style={{ stroke: '#FFF', strokeWidth: 2 }} />
      </g>
      <g
        id="dates"
        style={{ fontSize: '7pt', fill: '#FFF', textAnchor: 'end' }}
        transform={`translate(${chartConfig.margin.left},${chartConfig.margin.top})`}
      ></g>
      <g
        id="rescues"
        transform={`translate(${chartConfig.margin.left},${chartConfig.margin.top})`}
        style={{ fontSize: '8pt', fill: '#FFF', textAnchor: 'start' }}
      ></g>
      <g id="data" transform={`translate(${chartConfig.margin.left},${chartConfig.margin.top})`}></g>
    </svg>
  )
}
export default RescueTimelineChart
