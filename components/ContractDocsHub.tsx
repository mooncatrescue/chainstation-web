'use client'
import { Abi, Address, parseAbi } from 'viem'
import { usePublicClient, useReadContract, useReadContracts } from 'wagmi'
import LoadingIndicator from './LoadingIndicator'
import WarningIndicator from './WarningIndicator'
import EthereumAddress from './EthereumAddress'
import useSignedIn from 'lib/useSignedIn'
import { useState } from 'react'
import { TextField } from './FormFields'
import useTxStatus from 'lib/useTxStatus'
import { formatAsDate } from 'lib/util'
import { useQuery } from '@tanstack/react-query'
import EthereumTransaction from './EthereumTransaction'
import parseDescription from 'lib/parseDescription'
import Link from 'next/link'

const DOC_CONTRACT = {
  address: `0x0B78C64bCE6d6d4447e58b09E53F3621f44A2a48` as Address,
  abi: parseAbi([
    'function owner() external view returns (address)',
    'function totalContracts() external view returns (uint256)',
    'function doc(uint256 index) external view returns (string name, string description, string details, address contractAddress)',
    'function setDoc(address contractAddress, string name, string description, string details) external',
  ]),
  chainId: 1,
}

/**
 * Stucture of metadata object stored on Arweave by the project maintainers, to give info about a smart contract
 */
interface ContractDetails {
  address: Address,
  abi: Abi,
  name: string,
  deployed: `0x${string}`,
  description: string
}

const ContractDocumentation = ({
  index,
  name,
  description,
  details,
  address,
  actionButton,
}: {
  index: number
  name: string
  description: string
  details: string
  address: Address
  actionButton: React.ReactNode
}) => {
  const publicClient = usePublicClient()
  const { data: contractDetails } = useQuery({
    queryKey: ['ardrive', details],
    queryFn: async (): Promise<ContractDetails> => {
      const rs = await fetch(`https://arweave.net/${details.substring(5)}`)
      if (!rs.ok) throw new Error('Failed to get detail content')
      return (await rs.json())
    },
    enabled: details.indexOf('ar://') === 0
  })

  const { data: deployment, status } = useQuery({
    queryKey: ['contract-deployment', address],
    queryFn: async () => {
      const tx = await publicClient!.getTransaction({ hash: contractDetails!.deployed })
      const block = await publicClient!.getBlock({ blockNumber: tx.blockNumber })
      return {
        blockNumber: block.number,
        timestamp: block.timestamp,
        tx,
      }
    },
    enabled: publicClient && typeof contractDetails != 'undefined',
    staleTime: Infinity,
  })

  const deployDetails =
    status == 'success' && deployment ? (
      <p style={{ fontSize: '0.7em' }}>
        Deployed {formatAsDate(Number(deployment.timestamp))} in transaction{' '}
        <EthereumTransaction hash={deployment.tx.hash} /> by <EthereumAddress address={deployment.tx.from} />. Metadata saved to <Link target="_blank" rel="noreferrer" href={`https://arweave.net/${details.substring(5)}`}>{details}</Link>.
      </p>
    ) : (
      <p>
        <LoadingIndicator message="Finding deployment details..." />
      </p>
    )

  return (
    <section>
      <div style={{ display: 'flex', marginTop: '4em', alignItems: 'center' }}>
        <h3 style={{ textWrap: 'nowrap', margin: 0, flexGrow: '3' }}>
          #{index} {name}
        </h3>
        <EthereumAddress address={address} />
        {actionButton}
      </div>
      {deployDetails}
      <p><strong>TL;DR</strong> {description}</p>
      {contractDetails && parseDescription(contractDetails.description)}
    </section>
  )
}

const ContractDocsHub = () => {
  const { verifiedAddresses } = useSignedIn()
  const [editingData, setEditingData] = useState<{
    name: string
    description: string
    details: string
    address: string
  }>({ name: '', description: '', details: '', address: '' })
  const { isProcessing, viewMessage, setStatus, processTransaction } = useTxStatus()

  const totalRs = useReadContract({
    ...DOC_CONTRACT,
    functionName: 'totalContracts',
  })

  const ownerRs = useReadContract({
    ...DOC_CONTRACT,
    functionName: 'owner',
  })

  const docDetails = useReadContracts({
    contracts: [...Array(Number(totalRs.data ?? 0))].map(
      (m, i) =>
      ({
        ...DOC_CONTRACT,
        functionName: 'doc',
        args: [BigInt(i)],
      } as const)
    ),
    allowFailure: false,
  })

  const isOwner =
    ownerRs.isSuccess && ownerRs.data && verifiedAddresses.findIndex((a) => a.address == ownerRs.data) >= 0

  if (totalRs.isLoading) {
    return <LoadingIndicator message="Finding total number of documented contracts..." />
  }
  if (totalRs.isError) {
    return <WarningIndicator message="Failed to get total documentation count" />
  }

  if (docDetails.isLoading) {
    return (
      <section className="card-help">
        <p>
          The documentation contract at <EthereumAddress address={DOC_CONTRACT.address} /> has data for{' '}
          {Number(totalRs.data)} smart contracts.
        </p>
        <LoadingIndicator message="Updating documentation info from the blockchain..." />
      </section>
    )
  }
  if (docDetails.isError || typeof docDetails.data == 'undefined') {
    return (
      <section className="card-help">
        <p>
          The documentation contract at <EthereumAddress address={DOC_CONTRACT.address} /> has data for{' '}
          {Number(totalRs.data)} smart contracts.
        </p>
        <WarningIndicator message="Failed to get documentation info" />
      </section>
    )
  }

  return (
    <section className="card-help">
      <p>
        The documentation contract at <EthereumAddress address={DOC_CONTRACT.address} /> has data for{' '}
        {Number(totalRs.data)} smart contracts:
      </p>
      {docDetails.data.map((d, i) => {
        const actionButton = isOwner && (
          <button
            style={{ marginLeft: '1em' }}
            className="btn btn-small btn-admin"
            onClick={() => {
              setEditingData({
                name: d[0],
                description: d[1],
                details: d[2],
                address: d[3],
              })
            }}
          >
            Edit
          </button>
        )
        return (
          <ContractDocumentation
            key={d[3]}
            index={i}
            name={d[0]}
            description={d[1]}
            details={d[2]}
            address={d[3]}
            actionButton={actionButton}
          />
        )
      })}
      {isOwner && (
        <>
          <hr />
          <div className="admin-tools">
            <div className="form-grid" style={{ gridTemplateColumns: 'auto 30em' }}>
              <TextField
                meta={{ type: 'text', label: 'Address', name: 'address' }}
                currentValue={editingData.address}
                onChange={(e) => setEditingData((curVal) => ({ ...curVal, address: e.target.value }))}
              />
              <TextField
                meta={{ type: 'text', label: 'Name', name: 'name' }}
                currentValue={editingData.name}
                onChange={(e) => setEditingData((curVal) => ({ ...curVal, name: e.target.value }))}
              />
              <TextField
                meta={{ type: 'text', label: 'Description', name: 'description' }}
                currentValue={editingData.description}
                onChange={(e) => setEditingData((curVal) => ({ ...curVal, description: e.target.value }))}
              />
              <TextField
                meta={{ type: 'text', label: 'Details', name: 'details' }}
                currentValue={editingData.details}
                onChange={(e) => setEditingData((curVal) => ({ ...curVal, details: e.target.value }))}
              />
              <div style={{ gridColumnStart: '2', width: 'auto' }}>
                <button
                  disabled={isProcessing}
                  onClick={async () => {
                    if (editingData.address == '') {
                      setStatus('error', 'Need to specify an address')
                      return
                    }
                    const rs = await processTransaction({
                      ...DOC_CONTRACT,
                      functionName: 'setDoc',
                      args: [
                        editingData.address as `0x${string}`,
                        editingData.name,
                        editingData.description,
                        editingData.details,
                      ],
                    })
                    if (rs) {
                      setEditingData({ name: '', description: '', details: '', address: '' })
                      totalRs.refetch()
                      docDetails.refetch()
                    }
                  }}
                >
                  Save
                </button>
              </div>
            </div>
            {viewMessage}
          </div>
        </>
      )}
    </section>
  )
}
export default ContractDocsHub
