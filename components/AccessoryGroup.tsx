import { OwnedAccessory } from 'lib/useMoonCatAccessories'
import IconButton from './IconButton'
import Icon from './Icon'

function between(z1: number, z2: number) {
  if (z2 == z1) return null
  if (z2 > z1) {
    // z2 is bigger
    if (z1 + 1 == z2) return null
    return Math.floor((z2 - z1) / 2) + z1
  } else {
    // z1 is bigger
    if (z2 + 1 == z1) return null
    return Math.floor((z1 - z2) / 2) + z2
  }
}

function reorderSet(accessories: LockableAccessory[]) {
  const isBackground = accessories[0].isBackground // This is an assumption that all the supplied accessories are the same type
  const visible = accessories.filter((a) => a.zIndex > 0)
  const hidden = accessories.filter((a) => a.zIndex == 0)

  if (visible.length == 1) {
    visible[0].zIndex = 32768
    return [...visible, ...hidden]
  }
  if (isBackground) {
    // Array order goes from 1000 -> 64536
    if (visible.length == 2) {
      visible[0].zIndex = 22178
      visible[1].zIndex = 43357
      return [...hidden, ...visible]
    }
    const delta = 63536 / (visible.length - 1)
    for (let i = 0; i < visible.length; i++) {
      visible[i].zIndex = 1000 + Math.floor(i * delta)
    }
    return [...hidden, ...visible]
  } else {
    // Array order goes from 64536 -> 1000
    if (visible.length == 2) {
      visible[0].zIndex = 43357
      visible[1].zIndex = 22178
      return [...visible, ...hidden]
    }
    const delta = 63536 / (visible.length - 1)
    for (let i = 0; i < visible.length; i++) {
      visible[i].zIndex = 64536 - Math.floor(i * delta)
    }
    return [...visible, ...hidden]
  }
}

type LockableAccessory = OwnedAccessory & { isLocked?: boolean }

const AccessoryGroup = ({
  accessories,
  label,
  onChange,
}: {
  accessories: LockableAccessory[]
  label?: 'foreground' | 'background'
  onChange: (list: LockableAccessory[], accessoryId: bigint) => any
}) => {
  if (accessories.length == 0) return <p>No {label} accessories owned</p>
  const isBackground = accessories[0].isBackground // This is an assumption that all the supplied accessories are the same type

  // Separate accessories into worn and hidden
  const accClone = accessories.map((a) => Object.assign({}, a)) // Don't clobber the input objects
  let wornAccessories = accClone.filter((a) => a.zIndex > 0)
  let hiddenAccessories = accClone.filter((a) => a.zIndex == 0)
  hiddenAccessories.sort((a, b) => a.name.localeCompare(b.name))

  /**
   * A button on an individual Accessory row was clicked.
   * Modify the `accessories` array according to that action type, and return the transformed
   * `accessories` array to the parent component that included this one.
   */
  function handleClick(index: number, action: 'up' | 'down' | 'show' | 'hide'): void {
    let targetId: bigint = -1n
    let newStack: LockableAccessory[] = []
    switch (action) {
      case 'up':
        targetId = wornAccessories[index].accessoryId
        if (index == 0) break
        ;[wornAccessories[index], wornAccessories[index - 1]] = [wornAccessories[index - 1], wornAccessories[index]]
        if (index - 1 == 0) {
          // Moving item is now at the top
          const z1 = wornAccessories[index].zIndex
          if (isBackground) {
            if (z1 <= 1000) {
              wornAccessories[index - 1].zIndex = z1 - 100
            } else {
              const newZ = between(z1, 1000)
              if (newZ == null) {
                console.error('cannot place between', z1, 1000)
                wornAccessories = reorderSet(wornAccessories)
              } else {
                wornAccessories[index - 1].zIndex = newZ
              }
            }
          } else {
            if (z1 >= 64536) {
              wornAccessories[index - 1].zIndex = z1 + 100
            } else {
              const newZ = between(z1, 64536)
              if (newZ == null) {
                console.error('cannot place between', z1, 64536)
                wornAccessories = reorderSet(wornAccessories)
              } else {
                wornAccessories[index - 1].zIndex = newZ
              }
            }
          }
        } else {
          // Moving item somewhere in the middle of the stack
          const z1 = wornAccessories[index - 2].zIndex
          const z2 = wornAccessories[index].zIndex
          const newZ = between(z1, z2)
          if (newZ == null) {
            console.error('cannot place between', z1, z2)
            wornAccessories = reorderSet(wornAccessories)
          } else {
            wornAccessories[index - 1].zIndex = newZ
          }
        }
        newStack = isBackground
          ? [...hiddenAccessories, ...wornAccessories]
          : [...wornAccessories, ...hiddenAccessories]
        break
      case 'down':
        targetId = wornAccessories[index].accessoryId
        if (index >= wornAccessories.length - 1) break
        ;[wornAccessories[index + 1], wornAccessories[index]] = [wornAccessories[index], wornAccessories[index + 1]]
        if (index + 1 == wornAccessories.length - 1) {
          // Moving item is now at the bottom
          const z1 = wornAccessories[index].zIndex
          if (isBackground) {
            if (z1 >= 64536) {
              wornAccessories[index + 1].zIndex = z1 + 100
            } else {
              const newZ = between(z1, 64536)
              if (newZ == null) {
                console.error('cannot place between', z1, 64536)
                wornAccessories = reorderSet(wornAccessories)
              } else {
                wornAccessories[index + 1].zIndex = newZ
              }
            }
          } else {
            if (z1 <= 1000) {
              wornAccessories[index + 1].zIndex = z1 - 100
            } else {
              const newZ = between(z1, 1000)
              if (newZ == null) {
                console.error('cannot place between', z1, 1000)
                wornAccessories = reorderSet(wornAccessories)
              } else {
                wornAccessories[index + 1].zIndex = newZ
              }
            }
          }
        } else {
          // Moving item is somewhere in the middle of the stack
          const z1 = wornAccessories[index + 2].zIndex
          const z2 = wornAccessories[index].zIndex
          const newZ = between(z1, z2)
          if (newZ == null) {
            console.error('cannot place between', z1, z2)
            wornAccessories = reorderSet(wornAccessories)
          } else {
            wornAccessories[index + 1].zIndex = newZ
          }
        }
        newStack = isBackground
          ? [...hiddenAccessories, ...wornAccessories]
          : [...wornAccessories, ...hiddenAccessories]
        break
      case 'show': {
        // Remove this element from the set of hidden accessories
        targetId = hiddenAccessories[index].accessoryId
        const movingAccessory = hiddenAccessories.splice(index, 1)[0]
        if (isBackground) {
          // Place item just above the highest visible item in the stack
          if (wornAccessories.length == 0) {
            // No other items are visible
            wornAccessories.push({
              ...movingAccessory,
              zIndex: 32768,
            })
          } else {
            const z1 = wornAccessories[0].zIndex
            if (z1 < 1000) {
              wornAccessories.push({
                ...movingAccessory,
                zIndex: z1 - 100,
              })
            } else {
              const newZ = between(z1, 1000)
              if (newZ == null) {
                console.error('cannot place between', z1, 1000)
                wornAccessories.push({
                  ...movingAccessory,
                  zIndex: 1,
                })
                wornAccessories = reorderSet(wornAccessories)
              } else {
                wornAccessories.push({
                  ...movingAccessory,
                  zIndex: newZ,
                })
              }
            }
          }
          // Reassemble the accessories
          newStack = [...hiddenAccessories, ...wornAccessories]
        } else {
          // Place item just below the lowest visible item in the stack
          if (wornAccessories.length == 0) {
            // No other items are visible
            wornAccessories.push({
              ...movingAccessory,
              zIndex: 32768,
            })
          } else {
            const z1 = wornAccessories[wornAccessories.length - 1].zIndex
            if (z1 < 1000) {
              wornAccessories.push({
                ...movingAccessory,
                zIndex: z1 - 100,
              })
            } else {
              const newZ = between(z1, 1000)
              if (newZ == null) {
                console.error('cannot place between', z1, 1000)
                wornAccessories.push({
                  ...movingAccessory,
                  zIndex: 1,
                })
                wornAccessories = reorderSet(wornAccessories)
              } else {
                wornAccessories.push({
                  ...movingAccessory,
                  zIndex: newZ,
                })
              }
            }
          }
          // Reassemble the accessories
          newStack = [...wornAccessories, ...hiddenAccessories]
        }
        break
      }
      case 'hide': {
        targetId = wornAccessories[index].accessoryId
        const movingAccessory = wornAccessories.splice(index, 1)[0]
        hiddenAccessories.push({
          ...movingAccessory,
          zIndex: 0,
        })
        hiddenAccessories.sort((a, b) => a.name.localeCompare(b.name))
        newStack = isBackground
          ? [...hiddenAccessories, ...wornAccessories]
          : [...wornAccessories, ...hiddenAccessories]
        break
      }
    }
    onChange(newStack, targetId)
  }

  function handlePaletteChange(index: number, newPalette: number) {
    accessories[index].paletteIndex = newPalette
    onChange(accessories, accessories[index].accessoryId)
  }

  const hiddenView = hiddenAccessories.map((a, i) => (
    <li key={Number(a.accessoryId)} className="hidden">
      <span className="label">{a.name}</span>
      <span className="buttons">
        <IconButton key="show" title="Show" onClick={(e) => handleClick(i, 'show')} name="eye-blocked" />
      </span>
    </li>
  ))

  return (
    <ul>
      {isBackground && hiddenView}
      {wornAccessories.map((a, i) => {
        let buttons: React.ReactNode[] = []
        if (a.isLocked) {
          buttons.push(<Icon key="lock" name="lock" style={{ opacity: 0.3 }} />)
        } else {
          if (a.availablePalettes > 1) {
            buttons.push(
              <select
                key="palette"
                value={a.paletteIndex}
                onChange={(e) => handlePaletteChange(i, parseInt(e.target.value))}
              >
                {[...Array(a.availablePalettes)].map((m, i) => (
                  <option key={i}>{i}</option>
                ))}
              </select>
            )
          }
          buttons.push(
            <IconButton
              key="up"
              title="Move Up"
              disabled={i == 0}
              onClick={(e) => handleClick(i, 'up')}
              name="arrow-up-light"
            />
          )
          buttons.push(
            <IconButton
              key="down"
              title="Move Down"
              disabled={i == wornAccessories.length - 1}
              onClick={(e) => handleClick(i, 'down')}
              name="arrow-down-light"
            />
          )
          buttons.push(<IconButton key="hide" title="Hide" onClick={(e) => handleClick(i, 'hide')} name="eye" />)
        }

        return (
          <li key={Number(a.accessoryId)}>
            <span className="label">{a.name}</span>
            <span className="buttons">{buttons}</span>
          </li>
        )
      })}
      {!isBackground && hiddenView}
    </ul>
  )
}
export default AccessoryGroup
