'use client'
import { CSSProperties } from 'react'
import { FIVE_MINUTES, switchToChain } from 'lib/util'
import useTxStatus from 'lib/useTxStatus'
import LoadingIndicator from 'components/LoadingIndicator'
import { customEvent } from 'lib/analytics'
import { Address, BaseError, formatEther, parseAbi } from 'viem'
import { useAccount } from 'wagmi'
import { waitForTransactionReceipt, writeContract } from 'wagmi/actions'
import { useAppKit } from '@reown/appkit/react'
import EthAmount from './EthAmount'
import { useQuery } from '@tanstack/react-query'
import { config } from 'lib/wagmi-config'

interface PriceQuote {
  calldata: `0x${string}`
  sellAmount: string
}

interface Props {
  tokenId?: number
  quoteType: string
  style?: CSSProperties
}

const NFTXAdopt = ({ tokenId, quoteType, style }: Props) => {
  const { status: adoptStatus, viewMessage: adoptMessage, setStatus: setAdoptStatus } = useTxStatus()
  const { address, isConnected } = useAccount()
  const { open } = useAppKit()

  const { data: priceQuote, status } = useQuery({
    queryKey: ['nftx-price', quoteType],
    queryFn: async (): Promise<PriceQuote> => {
      const rs = await fetch(`/api/nftx-price/${quoteType}`)
      if (!rs.ok) throw new Error('Failed to fetch NFTX current price')
      return (await rs.json()).WETH
    },
    staleTime: FIVE_MINUTES,
  })

  if (status != 'success') {
    return <LoadingIndicator style={style} />
  }
  // Need to pad the amount, in case slippage happens before the swap confirms
  const actualPrice = (BigInt(priceQuote.sellAmount) * 102n) / 100n
  if (!isConnected) {
    const target = typeof tokenId == 'undefined' ? 'adopt now' : 'adopt them now'
    return (
      <section className="sub-card" style={style} itemScope itemType="https://schema.org/Product">
        <span itemProp="offers" itemScope itemType="https://schema.org/Offer">
          <link itemProp="availability" href="https://schema.org/InStock" />
          <meta itemProp="price" content={formatEther(actualPrice)} />
          <meta itemProp="priceCurrency" content="ETH" />
          <p>
            <button onClick={(e) => open()}>Connect</button>
          </p>
          <p>
            Connect your wallet if you&rsquo;d like to {target} for <EthAmount amount={actualPrice} truncateTo={7} />.
          </p>
        </span>
      </section>
    )
  }

  const doAdopt = async () => {
    // User has clicked the Adopt button; prepare and send a transaction to adopt that MoonCat
    let eventConfig: Record<string, any> = {
      currency: 'ETH',
      value: actualPrice,
    }
    if (typeof tokenId == 'undefined') {
      eventConfig.items = [
        {
          item_name: 'Random MoonCat',
          affiliation: 'NFTX',
          item_brand: 'MoonCatRescue',
          item_category: 'MoonCat',
          item_variant: quoteType,
          quantity: 1,
        },
      ]
    } else {
      eventConfig.items = [
        {
          item_id: tokenId,
          item_name: 'MoonCat ' + tokenId,
          affiliation: 'NFTX',
          item_brand: 'MoonCatRescue',
          item_category: 'MoonCat',
          item_variant: quoteType,
          quantity: 1,
        },
      ]
    }
    customEvent('begin_checkout', eventConfig)

    // NFTXMarketplace0xZap contract: https://docs.nftx.io/smart-contracts/integrations#buyandredeem
    const NFTX_MARKET = {
      address: '0x941A6d105802CCCaa06DE58a13a6F49ebDCD481C' as Address,
      chainId: 1,
      abi: parseAbi([
        'function buyAndRedeem(uint256 vaultId, uint256 amount, uint256[] specificIds, bytes swapCallData, address to) external payable',
      ]),
    }
    setAdoptStatus('building')
    if (!(await switchToChain(1))) {
      setAdoptStatus('error', 'Wrong network chain')
      return
    }
    const specificIds = typeof tokenId == 'undefined' ? [] : [BigInt(tokenId)]
    const adoptPromise = writeContract(config, {
      ...NFTX_MARKET,
      functionName: 'buyAndRedeem',
      args: [25n, 1n, specificIds, priceQuote.calldata, address!],
      value: actualPrice,
    })
    setAdoptStatus('authorizing')
    try {
      const hash = await adoptPromise
      setAdoptStatus('confirming')
      await waitForTransactionReceipt(config, { hash })
      setAdoptStatus(
        'done',
        <p>
          <strong>Adopted!</strong> This page will update shortly to mark you as the new owner.
        </p>
      )
      eventConfig.transaction_id = hash
      customEvent('purchase', eventConfig)
    } catch (err) {
      if (err instanceof BaseError) {
        console.error('Transaction failure', {
          name: err.name,
          shortMessage: err.shortMessage,
          cause: err.cause,
        })
        setAdoptStatus('error', err.shortMessage)
      } else {
        console.error('Transaction unknown error', err)
        setAdoptStatus('error', 'Transaction error')
      }
      return
    }
  }

  const isButtonDisabled = ['building', 'authorizing', 'confirming'].includes(adoptStatus)
  const target = typeof tokenId == 'undefined' ? 'Adopt!' : 'Adopt Me!'

  return (
    <section className="sub-card" style={style} itemScope itemType="https://schema.org/Product">
      <span itemProp="offers" itemScope itemType="https://schema.org/Offer">
        <link itemProp="availability" href="https://schema.org/InStock" />
        <meta itemProp="price" content={formatEther(actualPrice)} />
        <meta itemProp="priceCurrency" content="ETH" />
        <p>
          <button onClick={doAdopt} disabled={isButtonDisabled}>
            {target}
          </button>{' '}
          for <EthAmount amount={actualPrice} truncateTo={7} />
        </p>
        {adoptMessage}
      </span>
    </section>
  )
}

export default NFTXAdopt
