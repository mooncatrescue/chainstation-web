'use client'
import { useAppKit } from '@reown/appkit/react'
import { customEvent } from 'lib/analytics'
import { Moment, MoonCatData } from 'lib/types'
import useIsMounted from 'lib/useIsMounted'
import useTxStatus from 'lib/useTxStatus'
import { MOMENTS_ADDRESS, switchToChain } from 'lib/util'
import { useState, useEffect } from 'react'
import { useAccount, useReadContract } from 'wagmi'
import LoadingIndicator from './LoadingIndicator'
import WarningIndicator from './WarningIndicator'
import { Address, parseAbi } from 'viem'
import MoonCatGrid from './MoonCatSelectorGrid'

const MOMENTS = {
  address: MOMENTS_ADDRESS as Address,
  abi: parseAbi([
    'function isClaimable(uint256 momentId, uint256[] rescueOrders) external view returns (bool[])',
    'function batchClaim(uint256 momentId, uint256[] rescueOrders) external',
  ]),
  chainId: 1,
} as const

const MomentClaim = ({ moment }: { moment: Moment }) => {
  const moonCats = moment.moonCats
  const isMounted = useIsMounted()
  const { isConnected } = useAccount()
  const { open } = useAppKit()
  const [claimableMoonCats, setClaimableMoonCats] = useState<number[]>([])
  const [selectedMoonCats, setSelectedMoonCats] = useState<Set<bigint>>(new Set())
  const { viewMessage, setStatus, isProcessing, processTransaction } = useTxStatus()

  const {
    data,
    status: claimableStatus,
    refetch,
  } = useReadContract({
    ...MOMENTS,
    functionName: 'isClaimable',
    args: [BigInt(moment.momentId), moonCats.map((m) => BigInt(m))],
  })

  useEffect(() => {
    if (typeof data == 'undefined') return
    // Match MoonCat rescue order to their status
    let claimable = []
    for (let i = 0; i < moonCats.length; i++) {
      if (data[i]) {
        claimable.push(moonCats[i])
      }
    }
    setClaimableMoonCats(claimable)
  }, [moonCats, data])

  async function handleClaim() {
    if (selectedMoonCats.size == 0) {
      setStatus('error', 'No MoonCats selected')
      return
    }
    setStatus('building')

    let rs = await processTransaction({
      ...MOMENTS,
      functionName: 'batchClaim',
      args: [BigInt(moment!.momentId), Array.from(selectedMoonCats)],
    })
    if (rs) {
      customEvent('moments_claim', {
        'mooncats': Array.from(selectedMoonCats).map((num) => Number(num)),
        'moment': moment!.momentId,
      })
      setSelectedMoonCats(new Set())
      refetch()
    }
  }
  function handleClick(mc: MoonCatData) {
    const rescueOrder = BigInt(mc.rescueOrder)
    setSelectedMoonCats((current) => {
      let next = new Set(current)
      if (next.has(rescueOrder)) {
        next.delete(rescueOrder)
      } else {
        next.add(rescueOrder)
      }
      return next
    })
  }

  if (!isMounted || claimableStatus == 'pending') {
    return <LoadingIndicator />
  }
  if (claimableMoonCats.length == 0) {
    return <WarningIndicator message="All tokens have been claimed for this Moment" />
  }

  const actionButton = isConnected ? (
    <button disabled={isProcessing} onClick={handleClaim}>
      Claim
    </button>
  ) : (
    <button onClick={() => open()}>Connect</button>
  )

  const label = selectedMoonCats.size == 1 ? 'MoonCat' : 'MoonCats'
  const selectionView =
    selectedMoonCats.size > 0 ? (
      <>
        <p>
          {selectedMoonCats.size} {label} selected for Claiming
        </p>
        <p>{actionButton}</p>
        {viewMessage}
      </>
    ) : (
      <p>Click to select some MoonCats to claim this Moment for</p>
    )

  return (
    <>
      <MoonCatGrid
        moonCats={claimableMoonCats}
        minCellWidth={100}
        highlightedMoonCats={Array.from(selectedMoonCats).map((i) => Number(i))}
        onClick={handleClick}
      >
        {(mc: MoonCatData) => <p>#{mc.rescueOrder}</p>}
      </MoonCatGrid>
      <div className="text-container" style={{ textAlign: 'center' }}>
        <section className="card">{selectionView}</section>
      </div>
    </>
  )
}
export default MomentClaim
