import { useQuery } from '@tanstack/react-query'
import { API_SERVER_ROOT, ONE_DAY } from 'lib/util'
import Link from 'next/link'

/**
 * Show an inline reference to a MoonCat.
 * If only the MoonCat's Hex ID is known, look up its rescue order, to make a good link.
 */
const MoonCatLink = ({ rescueOrder, catId }: { rescueOrder?: number; catId?: string }) => {
  const { data: fetchedOrder } = useQuery({
    queryKey: ['mooncat-meta', catId],
    queryFn: async () => {
      const rs = await fetch(`/api/mooncats?mooncats=${catId}`)
      if (!rs.ok) throw new Error('Failed to fetch data from back-end')
      const data = await rs.json()
      return data.moonCats[0]
    },
    select: (meta) => meta.rescueOrder,
    staleTime: ONE_DAY,
    enabled: typeof rescueOrder === 'undefined' && typeof catId !== 'undefined',
  })

  const renderedOrder = fetchedOrder ?? rescueOrder
  if (typeof renderedOrder !== 'undefined') {
    return (
      <Link href={'/mooncats/' + renderedOrder}>
        <picture>
          <img
            className="inline"
            alt=""
            src={`${API_SERVER_ROOT}/image/${renderedOrder}?scale=1&padding=2&costumes=true`}
          />
        </picture>{' '}
        {`MoonCat #${renderedOrder}`}
      </Link>
    )
  }
  return (
    <>
      <picture>
        <img className="inline" alt="" src={`${API_SERVER_ROOT}/image/${catId}?scale=1&padding=2&costumes=true`} />
      </picture>{' '}
      MoonCat <code>{catId}</code>
    </>
  )
}
export default MoonCatLink
