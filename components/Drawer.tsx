import { CSSProperties, useState } from 'react'
import Icon from './Icon'

interface Props {
  title: React.ReactNode
  style?: CSSProperties
  height?: number
  startOpen?: boolean
  children: React.ReactNode
}

const Drawer = ({ title, style, height = 1000, startOpen = false, children }: Props) => {
  const [isOpen, setIsOpen] = useState<boolean>(startOpen)
  const chevron = isOpen ? 'circle-down' : 'circle-up'
  return (
    <div className="drawer" style={{ ...style, '--drawer-height': height + 'px' }}>
      <div
        className="drawer-title"
        onClick={(e) => {
          setIsOpen((current) => !current)
        }}
      >
        <Icon name={chevron} style={{ marginRight: '0.5rem', verticalAlign: '-2px' }} />
        {title}
      </div>
      <div className="drawer-body" aria-expanded={isOpen}>
        {children}
      </div>
    </div>
  )
}
export default Drawer
