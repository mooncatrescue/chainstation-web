'use client'
import { useQuery } from '@tanstack/react-query'
import { AttestationDoc, SCHEMAS } from 'lib/eas'
import useSignedIn from 'lib/useSignedIn'
import {
  ACCESSORIES_ADDRESS,
  API2_SERVER_ROOT,
  API_SERVER_ROOT,
  codeForChainId,
  MOONCAT_TRAITS_ARB,
  nameToUriSlug,
} from 'lib/util'
import LoadingIndicator from './LoadingIndicator'
import WarningIndicator from './WarningIndicator'
import React, { useState } from 'react'
import { Address, hexToNumber, hexToString, pad, trim } from 'viem'
import AccessoryLink from './AccessoryLink'
import MoonCatLink from './MoonCatLink'
import EthereumAddress from './EthereumAddress'
import Link from 'next/link'
import Icon from './Icon'

const VerifyLink = ({ att }: { att: string }) => {
  return (
    <a
      href={'https://arbitrum.easscan.org/offchain/url/#attestation=' + encodeURIComponent(att)}
      target="_blank"
      rel="noreferrer"
      title="Verify on EAS"
      className="alt-link"
      style={{ display: 'block', padding: '0.5em' }}
    >
      <Icon name="link-external" style={{ verticalAlign: '-0.2em' }} />
    </a>
  )
}

const PER_PAGE = 20
const MoonCatStars = ({ atts }: { atts: AttestationDoc[] }) => {
  const [page, setPage] = useState<number>(1)
  return (
    <>
      <h2>MoonCats</h2>
      <div style={{ columnWidth: '22em', margin: '0 auto' }}>
        <table cellSpacing="0" cellPadding="0" className="zebra" style={{ width: '100%' }}>
          <tbody>
            {atts.slice(0, page * PER_PAGE).map((att) => {
              return (
                <tr key={att.sig.uid}>
                  <td>
                    <MoonCatLink catId={pad(trim(att.data.productName.value as `0x${string}`), { size: 5 })} />
                  </td>
                  <td style={{ width: 1 }}>
                    <VerifyLink att={att.compressed} />
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
      {page * PER_PAGE < atts.length && (
        <p>
          <button onClick={() => setPage((currentValue) => currentValue + 1)}>Explore further</button>
        </p>
      )}
    </>
  )
}

const AccessoryStars = ({ atts }: { atts: AttestationDoc[] }) => {
  const [page, setPage] = useState<number>(1)
  return (
    <>
      <h2>Accessories</h2>
      <div style={{ columnWidth: '22em' }}>
        <table cellSpacing="0" cellPadding="0" className="zebra" style={{ width: '100%' }}>
          <tbody>
            {atts
              .slice(0, page * PER_PAGE)
              .sort((a, b) => hexToNumber(a.data.productName.value) - hexToNumber(b.data.productName.value))
              .map((att) => {
                const accessoryId = hexToNumber(att.data.productName.value)
                return (
                  <tr key={att.sig.uid}>
                    <td style={{ textAlign: 'center', lineHeight: 1 }}>
                      <picture style={{ marginRight: '0.5em' }}>
                        <img
                          className="inline"
                          alt=""
                          src={`${API_SERVER_ROOT}/accessory-image/${accessoryId}?scale=2&padding=2`}
                        />
                      </picture>
                    </td>
                    <td>
                      <AccessoryLink id={accessoryId} />
                    </td>
                    <td style={{ width: 1 }}>
                      <VerifyLink att={att.compressed} />
                    </td>
                  </tr>
                )
              })}
          </tbody>
        </table>
      </div>
      {page * PER_PAGE < atts.length && (
        <p>
          <button onClick={() => setPage((currentValue) => currentValue + 1)}>Explore further</button>
        </p>
      )}
    </>
  )
}

const MyStars = () => {
  const { connectedAddress } = useSignedIn()

  const { data, status } = useQuery({
    queryKey: ['user-stars', connectedAddress],
    queryFn: async (): Promise<AttestationDoc[]> => {
      const rs = await Promise.all([
        fetch(
          `${API2_SERVER_ROOT}/attestations?&${new URLSearchParams({
            schema: `arb:${SCHEMAS.PRODUCT_REVIEW}`,
            sender: `arb:${connectedAddress!}`,
            limit: '200',
          })}`
        ),
        fetch(
          `${API2_SERVER_ROOT}/attestations?&${new URLSearchParams({
            schema: `eth:${SCHEMAS.PRODUCT_REVIEW}`,
            sender: `eth:${connectedAddress!}`,
            limit: '200',
          })}`
        ),
      ])
      if (!rs[0].ok || !rs[1].ok) {
        console.error('Failed to fetch', rs)
        throw new Error('Failed to fetch')
      }
      return await Promise.all(
        rs.map((r) =>
          r.json().then((data) =>
            data.map((att: any) => {
              // JSON format does not have BigInts, so those values are stored as strings. Convert them back to BigInts to adhere to EAS-SDK type
              att.sig.message.time = BigInt(att.sig.message.time)
              att.sig.message.expirationTime = BigInt(att.sig.message.expirationTime)
              return att
            })
          )
        )
      )
    },
    enabled: typeof connectedAddress !== 'undefined',
    staleTime: 60 * 60 * 1000,
    select: (raw) =>
      raw
        .flat()
        .filter((att) => att.data.rating.value == 7)
        .reduce((agg, att) => {
          const target = codeForChainId(att.sig.domain.chainId) + ':' + att.sig.message.recipient
          if (typeof agg[target] == 'undefined') {
            agg[target] = [att]
          } else {
            agg[target].push(att)
          }
          return agg
        }, {} as Record<string, AttestationDoc[]>), // Only keep those that are generic 'Likes'
  })

  const preamble = (
    <p>
      As you journey around ChainStation Alpha, there&rsquo;s several opportunities for you to signal your appreciation
      of items by <strong>starring</strong> them. Forgotten all the things you&rsquo;ve given a star to? Let&rsquo;s
      list them all out:
    </p>
  )

  if (status == 'pending') {
    return (
      <section className="card-help">
        {preamble}
        <LoadingIndicator message="Let us see what we can find that you starred in the past..." />
      </section>
    )
  }
  if (status == 'error') {
    return (
      <section className="card-help">
        {preamble}
        <WarningIndicator message="Failed to fetch past Stars for this address" />
      </section>
    )
  }

  if (Object.entries(data).length == 0) {
    return (
      <>
        <section className="card-help">{preamble}</section>
        <section className="card-notice">
          <p>
            You don&rsquo;t appear to have starred anything yet. Get out there and browse some{' '}
            <Link href="/mooncats">MoonCats</Link> or <Link href="/accessories">Accessories</Link> and{' '}
            <strong>star</strong> something you enjoy!
          </p>
        </section>
      </>
    )
  }

  // Group User List stars
  const userListStars = Object.entries(data)
    .filter(([recipient, atts]) => {
      return recipient !== 'arb:' + MOONCAT_TRAITS_ARB && recipient !== 'eth:' + ACCESSORIES_ADDRESS
    })
    .reduce((agg, [recipient, atts]) => {
      for (const att of atts) {
        const productName = hexToString(att.data.productName.value)
        if (productName.indexOf('Token List:') === 0) {
          agg.push({
            title: productName.substring(11).trim(),
            owner: att.sig.message.recipient as Address,
            uid: att.sig.uid,
            compressed: att.compressed,
          })
        }
      }
      return agg
    }, [] as { title: string; owner: Address; uid: string; compressed: string }[])
    .sort((a, b) => a.title.localeCompare(b.title))

  return (
    <>
      <section className="card-help">{preamble}</section>
      <section className="card">
        <MoonCatStars atts={data['arb:' + MOONCAT_TRAITS_ARB]} />
        <AccessoryStars atts={data['eth:' + ACCESSORIES_ADDRESS]} />
        <h2>User Lists</h2>
        <table cellSpacing="0" cellPadding="0" className="zebra" style={{ width: '100%' }}>
          <tbody>
            {userListStars.map((l) => (
              <tr key={l.uid}>
                <td>
                  <Link href={`/owners/${l.owner}/lists/${nameToUriSlug(l.title)}`}>{l.title}</Link> by{' '}
                  <EthereumAddress address={l.owner} />
                </td>
                <td style={{ width: 1 }}>
                  <VerifyLink att={l.compressed} />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </section>
    </>
  )
}
export default MyStars
