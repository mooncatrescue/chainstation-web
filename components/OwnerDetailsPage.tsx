'use client'
import React from 'react'
import MoonCatGrid from 'components/MoonCatGrid'
import EthereumAddress from 'components/EthereumAddress'
import SignIn from 'components/SignIn'
import {
  ADDRESS_DETAILS,
  API2_SERVER_ROOT,
  ACCLIMATOR_ADDRESS,
  JUMPPORT_ADDRESS,
  RESCUE_ADDRESS,
  ACCESSORIES_ADDRESS,
  FIVE_MINUTES,
} from 'lib/util'
import { Address, zeroAddress } from 'viem'
import { OwnerProfile } from 'lib/types'
import { MomentsOwned } from 'components/MomentsOwned'
import useTabHandler from 'lib/useTabHandler'
import Icon from 'components/Icon'
import useSignedIn from 'lib/useSignedIn'
import { BlockchainEvent } from 'lib/blockchainEvents'
import LoadingIndicator from 'components/LoadingIndicator'
import EventTimeline from 'components/EventTimeline'
import OwnerTabInfo from 'components/OwnerTabInfo'
import OwnerTabLists from 'components/OwnerTabLists'
import OwnerTabAccessories from 'components/OwnerTabAccessories'
import useIsMounted from 'lib/useIsMounted'
import { useInfiniteQuery } from '@tanstack/react-query'
import JsonLd from './JsonLd'
import TabRow, { Tab } from './TabRow'

export interface TabProps {
  ownerAddress: Address
  isOwnWallet: boolean
  details: OwnerProfile
}

const OwnerTabMoonCats = ({ ownerAddress, isOwnWallet, details }: TabProps) => {
  const moonCats = details.ownedMoonCats.map((moonCat) => {
    return moonCat.rescueOrder
  })

  let clowderSummary: React.ReactNode
  if (ADDRESS_DETAILS[ownerAddress]?.type == 'pool') {
    // This is an NFT pool address
    clowderSummary = (
      <>
        This is an NFT pool address; all of these MoonCats are up for adoption <em>right now</em>! If you&rsquo;re
        interested in adopting any of these {moonCats.length} MoonCats, head over to{' '}
        <a href={ADDRESS_DETAILS[ownerAddress].link} target="_blank" rel="noreferrer">
          the pool&rsquo;s website
        </a>{' '}
        to do so.
      </>
    )
  } else if (moonCats.length == 1) {
    if (isOwnWallet) {
      clowderSummary = <>You are the proud owner of a MoonCat!</>
    } else {
      clowderSummary = <>This address is the proud owner of a MoonCat!</>
    }
  } else if (moonCats.length > 0) {
    if (isOwnWallet) {
      clowderSummary = <>You are the proud owner of {moonCats.length} MoonCats!</>
    } else {
      clowderSummary = <>This address is the proud owner of {moonCats.length} MoonCats!</>
    }
  } else {
    if (isOwnWallet) {
      clowderSummary = <>You don&rsquo;t own any MoonCats, currently.</>
    } else {
      clowderSummary = <>This address doesn&rsquo;t own any MoonCats, currently.</>
    }
  }

  return (
    <>
      <section className="card">{clowderSummary}</section>
      {moonCats.length > 0 && <MoonCatGrid moonCats={moonCats} showListings={false} />}
    </>
  )
}

const OwnerTabMoments = ({ ownerAddress, isOwnWallet, details }: TabProps) => {
  return <MomentsOwned address={ownerAddress} />
}

const PAGE_SIZE = 50
const OwnerTabTimeline = ({ ownerAddress, isOwnWallet, details }: TabProps) => {
  const {
    data: allEvents,
    fetchNextPage,
    hasNextPage,
  } = useInfiniteQuery({
    queryKey: ['events', `address:${ownerAddress}`, `limit:${PAGE_SIZE}`],
    queryFn: async ({ pageParam }: { pageParam: string | null }): Promise<BlockchainEvent[]> => {
      const params = new URLSearchParams({
        limit: String(PAGE_SIZE),
        address: ownerAddress,
      })
      if (pageParam != null) params.set('start_after', pageParam)
      const rs = await fetch(`${API2_SERVER_ROOT}/events?${params}`)
      if (!rs.ok) throw new Error('Failed to get blockchain event data')
      return await rs.json()
    },
    staleTime: FIVE_MINUTES,
    initialPageParam: null,
    getNextPageParam: (lastPage) => {
      if (lastPage.length == 0) return null
      const lastEntry = lastPage[lastPage.length - 1]
      return `${lastEntry.blockNumber}-${lastEntry.logIndex}`
    },
  })

  const events = allEvents
    ? allEvents.pages.flat().filter((e) => {
        // Skip transfer events on the Acclimator contract that are duplicated by more-specific events
        if (
          e.eventName == 'Transfer' &&
          e.eventContract == ACCLIMATOR_ADDRESS &&
          (e.args.from == zeroAddress ||
            e.args.to == zeroAddress ||
            e.args.from == JUMPPORT_ADDRESS ||
            e.args.to == JUMPPORT_ADDRESS)
        )
          return false

        // Skip transfer events that didn't actually go anywhere
        if (e.eventName == 'CatAdopted' && e.eventContract == RESCUE_ADDRESS && e.args.from == e.args.to) return false
        if (e.eventName == 'Transfer' && e.eventContract == ACCLIMATOR_ADDRESS && e.args.from == e.args.to) return false

        // Skip accessory applied events
        if (e.eventName == 'AccessoryApplied' && e.eventContract == ACCESSORIES_ADDRESS) return false

        return true
      })
    : []

  if (events.length == 0) {
    return (
      <p className="text-scrim">
        <LoadingIndicator />
      </p>
    )
  }

  return (
    <EventTimeline
      events={events}
      subject={{ type: 'address', id: ownerAddress }}
      loadNext={hasNextPage ? fetchNextPage : undefined}
    />
  )
}

const tabs = {
  'info': {
    label: 'Info',
    content: OwnerTabInfo,
  } as Tab<TabProps>,
  'mooncats': {
    label: 'MoonCats',
    content: OwnerTabMoonCats,
  } as Tab<TabProps>,
  'moments': {
    label: 'Moments',
    content: OwnerTabMoments,
  } as Tab<TabProps>,
  'accessories': {
    label: 'Accessories',
    content: OwnerTabAccessories,
  } as Tab<TabProps>,
  'lists': {
    label: 'Lists',
    content: OwnerTabLists,
  } as Tab<TabProps>,
  'timeline': {
    label: 'Timeline',
    content: OwnerTabTimeline,
  } as Tab<TabProps>,
}
type TabName = keyof typeof tabs
const defaultTab: TabName = 'info'
function isTabName(name: string): name is TabName {
  return Object.keys(tabs).includes(name)
}

interface Props {
  ownerAddress: Address
  details: OwnerProfile
}

const OwnerDetailsPage = ({ ownerAddress, details }: Props) => {
  const { activeTab, toggleTab } = useTabHandler(defaultTab)
  const { connectedAddress, verifiedAddresses } = useSignedIn()
  const isOwnWallet = verifiedAddresses.filter((a) => a.address == ownerAddress).length > 0
  const isMounted = useIsMounted()

  if (!isTabName(activeTab)) console.debug('Invalid tab name', activeTab)
  let ownerPrompt: React.ReactNode
  if (isMounted && connectedAddress && connectedAddress == ownerAddress) {
    ownerPrompt = <SignIn />
  }

  return (
    <div id="content-container" itemScope itemType="https://schema.org/ProfilePage">
      <meta itemProp="isFamilyFriendly" content="true" />
      <JsonLd
        data={{
          '@context': 'https://schema.org',
          '@type': 'BreadcrumbList',
          'itemListElement': [
            {
              '@type': 'ListItem',
              'position': 1,
              'name': 'Owners',
              'item': 'https://chainstation.mooncatrescue.com/owners',
            },
            {
              '@type': 'ListItem',
              'position': 2,
              'name': ownerAddress,
            },
          ],
        }}
      />
      <div
        className="text-container"
        style={{ marginBottom: '1em' }}
        itemProp="mainEntity"
        itemScope
        itemType="https://schema.org/Person"
      >
        <meta itemProp="identifier" content={ownerAddress} />
        <h1 className="hero">Owner Profile</h1>
        <p
          style={{ textAlign: 'center', marginTop: '-1em', fontSize: '1.5rem' }}
          className="text-scrim"
          itemProp="name"
        >
          <EthereumAddress address={ownerAddress} mode="bar" linkProfile={false} />
        </p>
        {ownerPrompt}
      </div>
      <TabRow tabs={tabs} activeTab={isTabName(activeTab) ? activeTab : defaultTab} toggleTab={toggleTab}>
        {(TabContent) => {
          if (details == null)
            return (
              <p className="message-error">
                <Icon name="warning" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
                Failed to fetch Owner info
              </p>
            )
          return <TabContent ownerAddress={ownerAddress} isOwnWallet={isOwnWallet} details={details} />
        }}
      </TabRow>
    </div>
  )
}

export default OwnerDetailsPage
