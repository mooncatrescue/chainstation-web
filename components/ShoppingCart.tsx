'use client'
import {
  ACCESSORIES_ADDRESS,
  AccessoryCartItem,
  AccessoryImageDetails,
  API_SERVER_ROOT,
  CartItem,
  COSTUME_ACCESSORIES,
  getAccessoryImageDetails,
  parseAccessoryMetabyte,
  switchToChain,
} from 'lib/util'
import { useAccount } from 'wagmi'
import { waitForTransactionReceipt, writeContract } from 'wagmi/actions'
import { Address, BaseError, parseAbi } from 'viem'
import IconButton from './IconButton'
import AccessoryGroup from './AccessoryGroup'
import useMoonCatAccessories, { OwnedAccessory } from 'lib/useMoonCatAccessories'
import LoadingIndicator from './LoadingIndicator'
import { CSSProperties, useContext, useEffect, useState } from 'react'
import Link from 'next/link'
import EthAmount from './EthAmount'
import Script from 'next/script'
import useTxStatus from 'lib/useTxStatus'
import { customEvent } from 'lib/analytics'
import Drawer from './Drawer'
import Icon from './Icon'
import { CartContext } from 'lib/ShoppingCartProvider'
import { config } from 'lib/wagmi-config'

declare global {
  interface Window {
    LibMoonCat: any
  }
}

const AccessoryItem = ({
  cartItems,
  onEdit,
  onDelete,
}: {
  cartItems: AccessoryCartItem[]
  onEdit?: (item: AccessoryCartItem) => void
  onDelete?: (itemId: string) => void
}): JSX.Element => {
  const targetMoonCat = cartItems[0].moonCat
  const { status, ownedAccessories } = useMoonCatAccessories(targetMoonCat)
  const [accImageData, setAccImageData] = useState<Record<number, AccessoryImageDetails>>({})
  const cellStyle: CSSProperties = { padding: '0.2rem 1rem' }

  useEffect(() => {
    if (!window.LibMoonCat) return
    let ignore = false
    getAccessoryImageDetails([
      ...ownedAccessories.map((a) => Number(a.accessoryId)),
      ...cartItems.map((i) => i.accessory.id),
    ]).then((accImageDetails) => {
      if (ignore) return
      const map: Record<number, AccessoryImageDetails> = {}
      for (const a of accImageDetails) {
        map[Number(a.id)] = a
      }
      setAccImageData(map)
    })

    return () => {
      ignore = true
    }
  }, [ownedAccessories, cartItems])

  const invalidItems = cartItems.filter((i) => ownedAccessories.some((a) => a.accessoryId == BigInt(i.accessory.id)))
  const validItems = cartItems.filter((i) => !invalidItems.includes(i))

  const invalidView =
    invalidItems.length == 0 ? null : (
      <>
        <p>Some items in the cart are not valid to purchase and need to be removed from your cart:</p>
        <table style={{ marginBottom: '2rem', backgroundColor: 'rgba(255,0,0, 0.2)' }} className="zebra">
          <tbody>
            {invalidItems.map((i) => (
              <tr key={i.id}>
                <td style={cellStyle}>{i.accessory.name}</td>
                <td>
                  <IconButton
                    name="trash"
                    onClick={(e) => {
                      if (onDelete) onDelete(i.id)
                    }}
                  />
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </>
    )

  const label = validItems.length == 1 ? <>1 new accessory is</> : <>{validItems.length} new accessories are</>
  const showVisibilityBanner = cartItems.some((i) => {
    if (COSTUME_ACCESSORIES.includes(i.accessory.id)) return true
    const details = accImageData[i.accessory.id]
    if (!details) return false
    const meta = parseAccessoryMetabyte(details.meta)
    return !meta.verified
  })

  // Show the banner as expanded by default if this is the first visibility-limited Accessory this MoonCat owns
  const bannerStartsOpen =
    showVisibilityBanner &&
    !ownedAccessories.some(
      (a) =>
        COSTUME_ACCESSORIES.includes(Number(a.accessoryId)) ||
        !parseAccessoryMetabyte(accImageData[Number(a.accessoryId)].meta).verified
    )
  const itemSummary = (
    <>
      {invalidView}
      {validItems.length > 0 && (
        <>
          <p>{label} pending being added to this MoonCat&rsquo;s wardrobe:</p>
          {showVisibilityBanner && (
            <Drawer title="Accessory visibility limitations" startOpen={bannerStartsOpen}>
              <p style={{ padding: '1rem', background: 'rgba(255, 238, 204, 0.1)', color: '#FFE' }}>
                Accessories that are &ldquo;unverified&rdquo; or classified as &ldquo;costumes&rdquo; will not be
                visually seen by marketplaces or NFT galleries. Ownership of these accessoriwa will still be noted in
                the official metadata of the MoonCat. All accessories are still irreversibly stored on-chain and can be
                viewed by any accessories-compatible software. Buy at your own risk. Sales are final and refunds will
                not be offered.
              </p>
            </Drawer>
          )}
          <table style={{ marginBottom: '2rem' }} className="zebra">
            <tbody>
              {validItems
                .sort((a, b) => a.accessory.name.localeCompare(b.accessory.name))
                .map((i) => {
                  const details = accImageData[i.accessory.id]
                  let suffix
                  if (details && !parseAccessoryMetabyte(details.meta).verified) {
                    suffix = (
                      <Icon
                        name="warning"
                        title="Unverified"
                        style={{ verticalAlign: '-2px', marginLeft: '1rem', color: '#FF0' }}
                      />
                    )
                  } else if (COSTUME_ACCESSORIES.includes(i.accessory.id)) {
                    suffix = (
                      <Icon
                        name="warning"
                        title="Costume"
                        style={{ verticalAlign: '-2px', marginLeft: '1rem', color: '#FF0' }}
                      />
                    )
                  }
                  return (
                    <tr key={i.id}>
                      <td style={cellStyle}>
                        <Link href={'/accessories/' + i.accessory.id}>{i.accessory.name}</Link>
                        {suffix}
                      </td>
                      <td style={cellStyle}>
                        <EthAmount amount={i.accessory.price} />
                      </td>
                      <td>
                        <IconButton
                          name="trash"
                          onClick={(e) => {
                            if (onDelete) onDelete(i.id)
                          }}
                        />
                      </td>
                    </tr>
                  )
                })}
            </tbody>
          </table>
        </>
      )}
    </>
  )

  if (status != 'success') {
    return (
      <>
        {itemSummary}
        <LoadingIndicator />
      </>
    )
  }

  if (validItems.length == 0) {
    return (
      <>
        {itemSummary}
        <p>
          <picture>
            <img
              alt={'MoonCat ' + targetMoonCat}
              src={`${API_SERVER_ROOT}/image/${targetMoonCat}?scale=2&costumes=true`}
              style={{ maxWidth: 640 }}
            />
          </picture>
        </p>
      </>
    )
  }

  function handleAccessoryOrderChange(accessoryList: OwnedAccessory[], changedAccessory: bigint) {
    const acc = accessoryList.find((acc) => acc.accessoryId == changedAccessory)
    if (typeof acc == 'undefined') {
      console.error('Failed to find updated Accessory', accessoryList, changedAccessory)
      return
    }
    const cartItem = cartItems.find((i) => i.accessory.id == Number(changedAccessory))
    if (typeof cartItem == 'undefined') {
      console.error('Failed to find related cart item', cartItems, changedAccessory)
      return
    }
    if (onEdit) {
      onEdit({
        ...cartItem,
        palette: acc.paletteIndex,
        zIndex: acc.zIndex,
      })
    }
  }

  // Sort pending accessories into foreground and background
  let pendingForeground = []
  let pendingBackground = []
  for (const cartItem of validItems) {
    if (cartItem.accessory.background) {
      pendingBackground.push(cartItem)
    } else {
      pendingForeground.push(cartItem)
    }
  }

  let wornForeground: (OwnedAccessory & { isLocked?: boolean })[] = ownedAccessories
    .filter((a) => a.zIndex > 0 && !a.isBackground)
    .map((a) => ({ ...a, isLocked: true }))
  let wornBackground: (OwnedAccessory & { isLocked?: boolean })[] = ownedAccessories
    .filter((a) => a.zIndex > 0 && a.isBackground)
    .map((a) => ({ ...a, isLocked: true }))

  for (const cartItem of pendingForeground) {
    wornForeground.push({
      accessoryId: BigInt(cartItem.accessory.id),
      name: cartItem.accessory.name,
      paletteIndex: cartItem.palette,
      availablePalettes: accImageData[cartItem.accessory.id]
        ? accImageData[cartItem.accessory.id].palettes.filter((p) => p.join('') != '00000000').length
        : 0,
      isBackground: false,
      zIndex: cartItem.zIndex,
      isLocked: false,
      ownedIndex: -1,
    })
  }
  for (const cartItem of pendingBackground) {
    wornBackground.push({
      accessoryId: BigInt(cartItem.accessory.id),
      name: cartItem.accessory.name,
      paletteIndex: cartItem.palette,
      availablePalettes: accImageData[cartItem.accessory.id]
        ? accImageData[cartItem.accessory.id].palettes.filter((p) => p.join('') != '00000000').length
        : 0,
      isBackground: true,
      zIndex: cartItem.zIndex,
      isLocked: false,
      ownedIndex: -1,
    })
  }
  wornForeground.sort((a, b) => b.zIndex - a.zIndex)
  wornBackground.sort((a, b) => a.zIndex - b.zIndex)

  // Assemble preview image of what MoonCat would look like with the indicated accessory set
  let imgSrc
  if (window.LibMoonCat) {
    let accData: AccessoryImageDetails[] = []
    if (accImageData) {
      for (const a of wornForeground) {
        const imgData = accImageData[Number(a.accessoryId)]
        if (typeof imgData !== 'undefined') {
          accData.push({
            ...imgData,
            zIndex: a.zIndex,
            paletteIndex: a.paletteIndex,
          })
        }
      }
      for (const a of wornBackground) {
        const imgData = accImageData[Number(a.accessoryId)]
        if (typeof imgData !== 'undefined') {
          accData.push({
            ...imgData,
            zIndex: a.zIndex,
            paletteIndex: a.paletteIndex,
          })
        }
      }
    }
    imgSrc = window.LibMoonCat.generateImage(window.LibMoonCat.parseCatId(targetMoonCat), accData, { scale: 2 })
  } else {
    imgSrc = `${API_SERVER_ROOT}/image/${targetMoonCat}?scale=2&costumes=true`
  }

  return (
    <>
      {itemSummary}
      <p>
        Adjust the position of the new accessories below to modify how this MoonCat will be dressed following this
        purchase.
      </p>
      <div style={{ display: 'flex' }}>
        <div style={{ flex: '1 1 20%', textAlign: 'center' }}>
          <picture>
            <img alt={'MoonCat ' + targetMoonCat} src={imgSrc} style={{ maxWidth: 640 }} />
          </picture>
        </div>
        <div style={{ paddingLeft: '2rem', flex: '3 3 80%' }}>
          {pendingForeground.length > 0 && (
            <>
              <h3>Foreground</h3>
              <section className="accessory-picker">
                <AccessoryGroup accessories={wornForeground} onChange={handleAccessoryOrderChange} />
              </section>
            </>
          )}
          {pendingBackground.length > 0 && (
            <>
              <h3>Background</h3>
              <section className="accessory-picker">
                <AccessoryGroup accessories={wornBackground} onChange={handleAccessoryOrderChange} />
              </section>
            </>
          )}
        </div>
      </div>
    </>
  )
}

const ShoppingCart = (): JSX.Element => {
  const { address } = useAccount()
  const { cart, dispatch } = useContext(CartContext)
  const { status, viewMessage, setStatus } = useTxStatus()

  if (!address) return <section className="card-notice">No Cart</section>
  if (cart.length == 0 && status != 'start') {
    return <section className="card">{viewMessage}</section>
  }
  if (cart.length == 0) {
    return <section className="card-notice">No pending items in your cart</section>
  }

  function updateCartItem(item: CartItem) {
    dispatch({
      type: 'UPDATE',
      payload: item,
    })
  }
  function deleteCartItem(itemId: string) {
    dispatch({
      type: 'DELETE',
      payload: itemId,
    })
  }
  function clearCart() {
    dispatch({
      type: 'CLEAR',
    })
  }

  // All accessory purchases need to be grouped by which MoonCat is getting them, as they need to be edited in one interface
  const accMapping: Record<number, AccessoryCartItem[]> = {}
  for (const item of cart.filter((i): i is AccessoryCartItem => i.type == 'ACCESSORY')) {
    if (typeof accMapping[item.moonCat] == 'undefined') {
      accMapping[item.moonCat] = []
    }
    accMapping[item.moonCat].push(item)
  }

  const totalCost = cart.reduce((sum, item) => {
    switch (item.type) {
      case 'ACCESSORY':
        return sum + BigInt(item.accessory.price)
      default:
        return sum
    }
  }, 0n)

  async function doPurchase() {
    const ACCESSORIES = {
      address: ACCESSORIES_ADDRESS as Address,
      abi: parseAbi([
        'struct AccessoryBatchData { uint256 rescueOrder; uint232 ownedIndexOrAccessoryId; uint8 paletteIndex; uint16 zIndex; }',
        'function buyAccessories(AccessoryBatchData[] calldata orders, address referrer) external payable',
      ]),
      chainId: 1,
    } as const

    setStatus('building')
    if (!(await switchToChain(1))) {
      setStatus('error', 'Wrong network chain')
      return
    }
    let txPromise = writeContract(config, {
      ...ACCESSORIES,
      functionName: 'buyAccessories',
      args: [
        cart
          .filter((i): i is AccessoryCartItem => i.type == 'ACCESSORY')
          .map((i) => ({
            rescueOrder: BigInt(i.moonCat),
            ownedIndexOrAccessoryId: BigInt(i.accessory.id),
            paletteIndex: i.palette,
            zIndex: i.zIndex,
          })),
        '0xdf2E60Af57C411F848B1eA12B10a404d194bce27',
      ],
      value: totalCost,
      chainId: 1,
    })
    setStatus('authorizing')
    try {
      let hash = await txPromise
      setStatus('done', <p>Purchase submitted; waiting for blockchain confirmation...</p>)
      for (const i of cart) {
        if (i.type == 'ACCESSORY') {
          customEvent('mooncat_accessory_purchase', { mooncat: i.moonCat, mooncat_accessory: i.accessory.id })
        }
      }
      clearCart()
      await waitForTransactionReceipt(config, { hash })
      setStatus('done', <p>Confirmed on the blockchain!</p>)
    } catch (err) {
      if (err instanceof BaseError) {
        console.error('Transaction failure', {
          name: err.name,
          shortMessage: err.shortMessage,
          cause: err.cause,
        })
        setStatus('error', err.shortMessage)
      } else {
        console.error('Transaction unknown error', err)
        setStatus('error', 'Transaction error')
      }
    }
  }

  return (
    <>
      <Script src="/libmooncat.js" />
      {Object.entries(accMapping).map(([moonCat, items]) => (
        <section key={moonCat + '-accessories'} style={{ marginBottom: '3rem' }} className="card">
          <h3 style={{ margin: 0 }}>New Accessories for MoonCat #{moonCat}</h3>
          <AccessoryItem cartItems={items} onEdit={updateCartItem} onDelete={deleteCartItem} />
        </section>
      ))}
      <hr />
      <table style={{ width: '100%' }} className="text-scrim">
        <tbody>
          <tr>
            <th style={{ textAlign: 'right', paddingRight: '2rem' }}>Total Cost:</th>
            <td style={{ width: 1, whiteSpace: 'nowrap' }}>
              <EthAmount amount={totalCost} />
            </td>
          </tr>
        </tbody>
      </table>
      <p style={{ textAlign: 'right', margin: '1rem 0' }}>
        <button onClick={clearCart}>Clear Cart</button>
        <button onClick={doPurchase} style={{ marginLeft: '1rem' }}>
          Purchase Items
        </button>
      </p>
      {status != 'start' && (
        <div className="text-scrim" style={{ textAlign: 'right', marginBottom: '2rem' }}>
          {viewMessage}
        </div>
      )}
    </>
  )
}
export default ShoppingCart
