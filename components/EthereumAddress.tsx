'use client'
import React from 'react'
import Link from 'next/link'
import { useEnsName, useEnsAvatar } from 'wagmi'
import { ADDRESS_DETAILS } from 'lib/util'
import Icon from './Icon'
import useIsMounted from 'lib/useIsMounted'
import { Address } from 'viem'

interface Props {
  address: Address
  className?: string
  style?: React.CSSProperties
  mode?: 'bar' | 'short-bar' | 'text'
  linkProfile?: boolean
  explorerLink?: string
}

const EthereumAddress = ({
  address,
  className,
  style,
  mode = 'short-bar',
  linkProfile = true,
  explorerLink = 'https://etherscan.io/address/',
}: Props) => {
  const { data: ensName } = useEnsName({ address: address, chainId: 1 })
  const { data: avatar } = useEnsAvatar({ name: ensName ?? undefined, chainId: 1 })
  const isMounted = useIsMounted()

  let avatarView
  if (isMounted && !!avatar) {
    if (avatar.substring(0, 4) == 'http') {
      avatarView = (
        <picture>
          <img className="eth-avatar" alt="" src={avatar} />
        </picture>
      )
    }
  }

  let classes = 'eth-address eth-address-' + mode
  if (!!className) classes += ' ' + className

  const iconStyle: React.CSSProperties = { marginLeft: '0.5em', verticalAlign: '-0.2em' }

  switch (mode) {
    case 'text': {
      let label: React.ReactNode
      if (ADDRESS_DETAILS[address]) {
        label = <span className="address-label">{ADDRESS_DETAILS[address].label}</span>
      } else if (isMounted && !!ensName) {
        label = <span className="address-label">{ensName}</span>
      } else {
        label = (
          <span className="address-label">
            {address.substring(0, 8)}...{address.substring(address.length - 6)}
          </span>
        )
      }
      return (
        <span title={address} className={classes} style={style}>
          {avatarView}
          {label}
        </span>
      )
    }
    case 'bar':
    case 'short-bar':
      let label: React.ReactNode
      if (ADDRESS_DETAILS[address]) {
        label = <span className="address-label">{ADDRESS_DETAILS[address].label}</span>
      } else if (isMounted && !!ensName) {
        label = <span className="address-label">{ensName}</span>
      } else if (mode == 'short-bar') {
        label = (
          <span className="address-label">
            {address.substring(0, 8)}...{address.substring(address.length - 6)}
          </span>
        )
      } else {
        label = <span className="address-label hex-collapse">{address}</span>
      }

      let labelView
      if (linkProfile) {
        labelView = (
          <Link href={`/owners/${address}`}>
            <>
              {avatarView}
              {label}
            </>
          </Link>
        )
      } else {
        labelView = (
          <>
            {avatarView}
            {label}
          </>
        )
      }

      return (
        <span title={mode == 'short-bar' ? address : ''} className={classes} style={style}>
          {labelView}
          <a
            title="Copy address"
            className="alt-link"
            style={{ cursor: 'pointer' }}
            onClick={() => {
              navigator.clipboard.writeText(address)
            }}
          >
            <Icon name="copy" style={iconStyle} />
          </a>
          <a
            href={`${explorerLink}${address}`}
            className="alt-link"
            title="View on Etherscan"
            target="_blank"
            rel="noreferrer"
          >
            <Icon name="link-external" style={iconStyle} />
          </a>
        </span>
      )
    default:
      return <div>Unknown mode</div>
  }
}
export default EthereumAddress
