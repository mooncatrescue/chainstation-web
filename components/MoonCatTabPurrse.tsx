import React, { useState } from 'react'
import { useRouter } from 'next/navigation'
import Icon from './Icon'
import LoadingIndicator from './LoadingIndicator'
import { TabProps } from './MoonCatDetail'
import { MoonCatData, OwnedMoonCat, OwnerProfile } from 'lib/types'
import { ACCLIMATOR_ADDRESS, API2_SERVER_ROOT, FIVE_MINUTES, MOMENTS_ADDRESS, switchToChain } from 'lib/util'
import { multicall, readContract } from 'wagmi/actions'
import { Address, hexToBigInt, numberToHex, parseAbi } from 'viem'
import useSignedIn from 'lib/useSignedIn'
import useTxStatus from 'lib/useTxStatus'
import { TokenMeta, getTokenMeta, tokenDisplayLabel } from 'lib/tokens'
import TokenPicker from './TokenPicker'
import SignInFirst from './SignInFirst'
import { useQuery } from '@tanstack/react-query'
import WarningIndicator from './WarningIndicator'
import { config } from 'lib/wagmi-config'

const ACCLIMATOR = {
  address: ACCLIMATOR_ADDRESS as Address,
  chainid: 1,
  abi: parseAbi([
    'function totalChildContracts(uint256 tokenId) external view returns (uint256)',
    'function childContractByIndex(uint256 tokenId, uint256 index) external view returns (address childContract)',
    'function totalChildTokens(uint256 tokenId, address childContract) external view returns (uint256)',
    'function childTokenByIndex(uint256 tokenId, address childContract, uint256 index) external view returns (uint256 childTokenId)',
    'function safeTransferChild(uint256 fromTokenId, address to, address childContract, uint256 childTokenId) external',
  ]),
}

/**
 * Widget for donating an ERC721 token to a MoonCat
 */
const SendToMoonCat = ({
  didInteract,
  preamble,
  moonCat,
  owner,
}: {
  didInteract: boolean
  preamble: React.ReactNode
  moonCat: MoonCatData
  owner: Address | undefined
}) => {
  const [startProcess, setStartProcess] = useState<boolean>(didInteract)
  const { connectedAddress, verifiedAddresses } = useSignedIn()
  const { viewMessage, isProcessing, processTransaction } = useTxStatus()
  const router = useRouter()

  const [tokenChoice, setTokenChoice] = useState<TokenMeta | false>(false)

  // Fetch tokens owned by the current visitor
  const ownedTokens = useQuery({
    queryKey: ['owner-profile', connectedAddress],
    queryFn: async (): Promise<OwnerProfile> => {
      const rs = await fetch(`${API2_SERVER_ROOT}/owner-profile/${connectedAddress}`)
      if (!rs.ok) throw new Error('Failed to fetch owner information')
      return await rs.json()
    },
    select: (data) => {
      const tokens: TokenMeta[] = (data.ownedMoonCats as OwnedMoonCat[])
        .map((mc) =>
          getTokenMeta({
            collection: { address: ACCLIMATOR_ADDRESS },
            id: numberToHex(mc.rescueOrder),
          } as TokenMeta)
        )
        .concat(
          (data.ownedMoments as { moonCat?: number; momentId: number }[])
            .filter((m) => typeof m.moonCat == 'undefined')
            .map((m) =>
              getTokenMeta({
                collection: { address: MOMENTS_ADDRESS },
                id: numberToHex(m.momentId),
              } as TokenMeta)
            )
        )
      return tokens
    },
    staleTime: FIVE_MINUTES,
    enabled: startProcess && typeof connectedAddress !== 'undefined',
  })

  // Send the selected token to the current MoonCat
  async function sendToken() {
    if (!tokenChoice || !connectedAddress) return

    let rs = await processTransaction({
      address: tokenChoice.collection.address,
      chainId: 1,
      abi: parseAbi(['function safeTransferFrom(address from, address to, uint256 tokenId, bytes data) external']),
      functionName: 'safeTransferFrom',
      args: [
        connectedAddress,
        ACCLIMATOR_ADDRESS,
        hexToBigInt(tokenChoice.id),
        numberToHex(moonCat.rescueOrder, { size: 32 }),
      ],
    })
    if (rs) {
      router.refresh()
    }
  }

  if (!startProcess) {
    // The visitor is connected and signed-in, but hasn't given indication they want to give a gift
    // Wait for them to click a button to signal that intent before starting owned token enumeration
    const label = verifiedAddresses.filter((a) => a.address == owner).length > 0 ? 'Manage' : 'Find Tokens to Gift'
    return (
      <section className="card">
        {preamble}
        <p>
          <button onClick={(e) => setStartProcess(true)}>{label}</button>
        </p>
      </section>
    )
  }

  // If the visitor is connected and signed in, and the async process to enumerate their balances kicked off. Is it in progress?
  if (ownedTokens.isLoading || !ownedTokens.data) {
    return (
      <section className="card">
        {preamble}
        <LoadingIndicator message="Finding eligible tokens..." />
      </section>
    )
  }

  // The async process has finished; enumerate which tokens the visitor owns
  // If the visitor doesn't have any tokens from known collections, show error message
  if (ownedTokens.data.length == 0) {
    return (
      <section className="card">
        {preamble}
        <p>No tokens found in your address that could be gifted at this time.</p>
      </section>
    )
  }

  // The visitor has tokens that are possible to be gifted to a MoonCat; show widget for doing so
  const visitorIsOwner = verifiedAddresses.filter((a) => a.address == owner).length > 0
  let giftButton: React.ReactNode
  if (tokenChoice === false) {
    giftButton = (
      <section className="sub-card">
        <p>Click a token you wish to send to this MoonCat</p>
      </section>
    )
  } else {
    const [collectionLabel, tokenName] = tokenDisplayLabel(tokenChoice)
    giftButton = (
      <section className="sub-card">
        <p>
          <picture>
            <img
              style={{ maxHeight: '100px' }}
              src={tokenChoice.imageSrc}
              alt={`${tokenChoice.collection.address} ${tokenChoice.id}`}
            />
          </picture>
        </p>
        <p>
          Send {collectionLabel} {tokenName} to MoonCat #{moonCat.rescueOrder}:
        </p>
        <p>
          <button disabled={isProcessing} onClick={sendToken}>
            {visitorIsOwner ? 'Send' : 'Gift'}
          </button>
        </p>
        {viewMessage}
      </section>
    )
  }
  return (
    <section className="card">
      {preamble}
      <TokenPicker
        tokens={ownedTokens.data.map((t) => ({
          ...t,
          isSelected: tokenChoice && t.collection.address == tokenChoice.collection.address && t.id == tokenChoice.id,
        }))}
        onClick={(t) => {
          setTokenChoice(t)
        }}
      />
      {giftButton}
    </section>
  )
}

const MoonCatTabPurrse = ({ moonCat, details }: TabProps) => {
  const { connectedAddress, isSignedIn, verifiedAddresses } = useSignedIn()
  const { viewMessage, processTransaction, isProcessing, setStatus } = useTxStatus()
  const router = useRouter()

  const { data: childTokens, status } = useQuery({
    queryKey: ['mooncat-purrse-holdings', moonCat.rescueOrder],
    queryFn: async () => {
      const collectionCount = await readContract(config, {
        ...ACCLIMATOR,
        functionName: 'totalChildContracts',
        args: [BigInt(moonCat.rescueOrder)],
      })
      if (collectionCount == 0n) {
        return []
      }
      // Iterate up to that collection count to fetch which contracts they are
      let multicalls = []
      for (let i = 0n; i < collectionCount; i++) {
        multicalls.push({
          ...ACCLIMATOR,
          functionName: 'childContractByIndex',
          args: [BigInt(moonCat.rescueOrder), i],
        } as const)
      }
      const collectionAddresses = await multicall(config, {
        contracts: multicalls,
        allowFailure: false,
      })

      // For each collection, find out how many tokens from that collection
      multicalls = []
      for (let collectionAddress of collectionAddresses) {
        multicalls.push({
          ...ACCLIMATOR,
          functionName: 'totalChildTokens',
          args: [BigInt(moonCat.rescueOrder), collectionAddress],
        } as const)
      }
      const collectionCounts = await multicall(config, {
        contracts: multicalls,
        allowFailure: false,
      })

      // Next, change the child index values into token IDs
      let inputIndex = []
      multicalls = []
      for (let i = 0; i < collectionAddresses.length; i++) {
        const collectionAddress = collectionAddresses[i]
        const childCount = collectionCounts[i]
        for (let i = 0n; i < childCount; i++) {
          inputIndex.push(collectionAddress)
          multicalls.push({
            ...ACCLIMATOR,
            functionName: 'childTokenByIndex',
            args: [BigInt(moonCat.rescueOrder), collectionAddress, i],
          } as const)
        }
      }
      const childIds = await multicall(config, {
        contracts: multicalls,
        allowFailure: false,
      })

      // Merge the data together
      let data: TokenMeta[] = []
      for (let i = 0; i < inputIndex.length; i++) {
        data.push(
          getTokenMeta({
            collection: { address: inputIndex[i] as Address },
            id: numberToHex(childIds[i]),
          } as TokenMeta)
        )
      }
      // Sort the tokens to put them in ascending order, by collection
      data = data.sort((a, b) => {
        if (a.collection.address != b.collection.address) {
          return a.collection.address.localeCompare(b.collection.address)
        } else {
          return Number(BigInt(a.id) - BigInt(b.id))
        }
      })
      return data
    },
  })

  // Withdraw a token from the MoonCat, to the owner's wallet
  async function withdrawToken(childToken: TokenMeta) {
    if (!connectedAddress) {
      setStatus('error', 'No wallet connected')
      return
    }
    let rs = await processTransaction({
      ...ACCLIMATOR,
      functionName: 'safeTransferChild',
      args: [BigInt(moonCat.rescueOrder), connectedAddress, childToken.collection.address, hexToBigInt(childToken.id)],
    })
    if (rs) {
      router.refresh()
    }
  }

  let bodyView: React.ReactNode
  if (status == 'pending') {
    bodyView = <LoadingIndicator style={{ marginBottom: '1rem' }} />
  } else if (status == 'error') {
    bodyView = <WarningIndicator message="Crabby nebula discovered! Failed to fetch blockchain data." />
  } else if (childTokens.length == 0) {
    bodyView = (
      <section className="card">
        <p>This MoonCat doesn&rsquo;t have anything in its Purrse right now.</p>
      </section>
    )
  } else {
    // Assemble a grid to display child tokens
    bodyView = (
      <div className="item-grid" style={{ marginBottom: '1rem' }}>
        {childTokens.map((child) => {
          const key = child.collection.address + '::' + child.id
          const [collectionLabel, tokenName] = tokenDisplayLabel(child)
          let sendLink: React.ReactNode
          if (isSignedIn && connectedAddress == details.owner) {
            sendLink = (
              <button
                style={{
                  position: 'absolute',
                  top: 0,
                  right: 0,
                  margin: '0.2rem',
                  padding: '0.2rem',
                  borderWidth: 0,
                  lineHeight: 0,
                  fontSize: '0.8rem',
                }}
                title="Send"
                disabled={isProcessing}
                onClick={(e) => {
                  e.preventDefault()
                  withdrawToken(child)
                }}
              >
                <Icon name="rocket" />
              </button>
            )
          }
          if (!child.link) {
            return (
              <div key={key} className="item-thumb">
                {sendLink}
                <div className="thumb-img" style={{ backgroundImage: `url(${child.imageSrc})` }} />
                <p>{collectionLabel}</p>
                <p>{tokenName}</p>
              </div>
            )
          } else {
            return (
              <a key={key} href={child.link} className="item-thumb">
                {sendLink}
                <div className="thumb-img" style={{ backgroundImage: `url(${child.imageSrc})` }} />
                <p>{collectionLabel}</p>
                <p>{tokenName}</p>
              </a>
            )
          }
        })}
      </div>
    )
  }

  return (
    <div className="text-container">
      <section className="card-help">
        <p>
          MoonCats are able to contain/own other NFTs (following the{' '}
          <a href="https://eips.ethereum.org/EIPS/eip-998" target="_blank" rel="noreferrer">
            ERC998 standard
          </a>
          ), which then stay with the MoonCat even if the MoonCat is moved to a different wallet. Any ERC721-compliant
          token can be given to a MoonCat (put in their Purrse).
        </p>
        {!details.isAcclimated && (
          <p>
            MoonCats need to be Acclimated in order to interact with this feature, and this MoonCat is currently not
            Acclimated. Any tokens previously added to the MoonCat&rsquo;s Purrse will wait for them to be Acclimated
            again.
          </p>
        )}
      </section>
      {bodyView}
      {viewMessage}
      {details.isAcclimated && (
        <SignInFirst
          preamble={
            <p>
              <strong>Gift a token to this MoonCat:</strong> any ERC721 token can be gifted to a MoonCat. Once gifted,
              only the owner of that MoonCat can transfer that token elsewhere.
            </p>
          }
        >
          {(didInteract, preamble) => (
            <SendToMoonCat didInteract={didInteract} preamble={preamble} moonCat={moonCat} owner={details.owner} />
          )}
        </SignInFirst>
      )}
    </div>
  )
}
export default MoonCatTabPurrse
