'use client'
import { useState, useEffect } from 'react'
import Link from 'next/link'

const RandomMoonCat = (): JSX.Element => {
  const [rescueOrder, setRescueOrder] = useState<number | undefined>(undefined)
  useEffect(() => {
    setRescueOrder(Math.floor(Math.random() * 25440))
  }, [])

  if (!rescueOrder) return <></>

  let label = `MoonCat #${rescueOrder}`
  return (
    <Link href={`/mooncats/${rescueOrder}`} title={label}>
      <picture>
        <img
          className="mooncat-image"
          alt={label}
          title={label}
          src={'https://api.mooncat.community/image/' + rescueOrder + '?scale=3&padding=10'}
        />
      </picture>
    </Link>
  )
}
export default RandomMoonCat
