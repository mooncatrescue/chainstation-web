import React, { useContext } from 'react'
import Icon from './Icon'
import { API_SERVER_ROOT } from '../lib/util'
import { MoonCatData, MoonCatViewPreference } from '../lib/types'
import Link from 'next/link'
import { AppVisitorContext } from 'lib/AppVisitorProvider'
import { Listing } from 'lib/reservoirData'

interface ThumbProps {
  moonCat: MoonCatData
  isEventActive: boolean
  isAwoken?: boolean
  listing?: Listing
  className?: string
  id?: string
  thumbHandler?: (moonCat: MoonCatData) => React.ReactNode
  href?: string
  onClick?: (moonCat: MoonCatData, event: React.MouseEvent<HTMLElement, MouseEvent>) => any
}

const MoonCatThumb = ({
  moonCat,
  isEventActive,
  isAwoken = false,
  listing,
  className,
  id,
  thumbHandler,
  href,
  onClick,
}: ThumbProps) => {
  const {
    state: { viewPreference },
  } = useContext(AppVisitorContext)

  // If this instance of the MoonCatGrid is not showing special-event views, fall back to Accessorized view
  let parsedViewPreference: MoonCatViewPreference =
    viewPreference == 'event' && !isEventActive ? 'accessorized' : viewPreference

  let thumbStyle: React.CSSProperties = {}
  if (!isAwoken) {
    switch (parsedViewPreference) {
      case 'accessorized':
        thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5&costumes=true)`
        break
      case 'mooncat':
        thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5&acc=)`
        break
      case 'face':
        thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/image/${moonCat.rescueOrder}?scale=3&padding=5&acc=&headOnly)`
        break
      case 'event':
        thumbStyle.backgroundImage = `url(${API_SERVER_ROOT}/event-image/${moonCat.rescueOrder}?scale=3&padding=5)`
        break
    }
  }

  let details: React.ReactNode
  if (thumbHandler) {
    details = thumbHandler(moonCat)
  } else {
    let name = null
    if (typeof moonCat.name != 'undefined') {
      if (moonCat.name === true || moonCat.name == '\ufffd') {
        name = (
          <p>
            <Icon name="question" />
          </p>
        )
      } else {
        name = <p>{moonCat.name.trim()}</p>
      }
    }

    details = (
      <>
        <p>#{moonCat.rescueOrder}</p>
        <p>
          <code>{moonCat.catId}</code>
        </p>
        {name}
      </>
    )
  }

  const listingPill =
    typeof listing !== 'undefined' ? (
      <Icon
        title={`Available to adopt for ${listing.price.amount.decimal} ${listing.price.currency.symbol}`}
        className="icon-pill"
        style={{ position: 'absolute', top: '0.5rem', right: '0.5rem', zIndex: 15 }}
        name="rocket"
      />
    ) : null
  const listingPlatform = typeof listing !== 'undefined' ? <div className={`listed-platform ${moonCat.pose}`} /> : null

  let classes = ['item-thumb']
  if (className) classes.push(className)

  if (href) {
    // Use a standard link
    return (
      <Link href={href} className={classes.join(' ')} id={id}>
        <>
          {listingPill}
          <div className="thumb-img" style={thumbStyle} />
          {listingPlatform}
          {details}
        </>
      </Link>
    )
  }

  if (typeof onClick != 'function') {
    // Use default link
    return (
      <Link href={`/mooncats/${moonCat.rescueOrder}`} className={classes.join(' ')} id={id}>
        <>
          {listingPill}
          <div className="thumb-img" style={thumbStyle} />
          {listingPlatform}
          {details}
        </>
      </Link>
    )
  }

  // Do a custom onClick function
  const style = {
    cursor: classes.includes('disabled') ? '' : 'pointer',
  }
  return (
    <div className={classes.join(' ')} id={id} style={style} onClick={(e) => onClick(moonCat, e)}>
      {listingPill}
      <div className="thumb-img" style={thumbStyle} />
      {details}
    </div>
  )
}
export default MoonCatThumb
