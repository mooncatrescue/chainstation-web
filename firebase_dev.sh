#!/usr/bin/env bash

export NODE_ENV="development"
source ~/.profile

firebase experiments:enable webframeworks
firebase --debug emulators:start