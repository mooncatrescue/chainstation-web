require('dotenv').config()
const { initializeApp } = require('firebase-admin/app')
const { getFirestore } = require('firebase-admin/firestore')

initializeApp({ projectId: 'mooncatrescue-25600' })
const db = getFirestore()

;(async () => {
  const IS_EMULATED =
    typeof process.env.FIRESTORE_EMULATOR_HOST !== 'undefined' && process.env.FIRESTORE_EMULATOR_HOST != ''

  if (IS_EMULATED) {
    console.log('Using Firebase Emulator hosted at', process.env.FIRESTORE_EMULATOR_HOST)
  }
  console.log('Connecting to Firebase project', await db.projectId)

  const collection = db.collection('treasures')

  // Create a Treasure that gives out a specific link to each classification of MoonCatMoment tokens
  await collection.doc().set({
    type: 'mapped',
    label: 'MoonCatMoments Additional Variants',
    icon: null,
    details: 'Each MoonCat Moment grants access to a high-res and animated version of the visual.',
    mapping: {
      moment: {
        0: 'bafybeig2ea5p2xw2d5c552x3ocxj2xavsb6nglwczv2hyza7yhu2wvny5a',
        1: 'bafybeifnwnnyi4ddntzmlo3fn3a5n2hedu5ulysl2xfmac4x5soreeaxdu',
        2: 'bafybeidexf6rw7s2dvrddqkwtzjqs5esuoope7iqvyxdcsll7qswnzqfua',
        3: 'bafybeiawcrs4cnwj4wglynurogop34qan7o7cqq7odzykioys45k5fyosi',
        4: 'bafybeiclp4inwmfeyyqc5sleljnb4xbbl7u4nbcg3dqudrklgbi7u6if6y',
      },
    },
  })

  console.log('Done!')
})()
