import { useCallback, useEffect, useRef } from "react"
import { MoonCatData } from "./types"
import { AppEventType, emit } from "./useGlobalEvents"
import { pluck } from "./util"

/**
 * Monitor keypress events, and look for a `TAB + B` key combination.
 * Because the `TAB` key is not a typical "modifier" key, this cannot be done in a single `keydown` event,
 * but instead must monitor each individual key being pressed and released.
 */
const useTabbieHook = (moonCatList: MoonCatData[] | undefined, awokenMoonCats: Set<number>, getEnterCoords: (mc: MoonCatData) => { x: number, y: number }) => {
  const activeKeys = useRef(new Set<string>())

  const cb = useCallback(() => {
    if (!moonCatList || moonCatList.length == 0) return

    // TAB + B? Sure, let's summon a Tabbie!
    const available = moonCatList.filter((m) => !awokenMoonCats.has(m.rescueOrder))
    const tabbiesAvailable = available.filter((m) => m.pattern == 'tabby')
    if (available.length == 0) return

    // Prefer to awaken a Tabbie, otherwise any of the others.
    const targetMoonCat = tabbiesAvailable.length > 0 ? pluck(tabbiesAvailable) : pluck(available)
    const coords = getEnterCoords(targetMoonCat)

    emit({
      type: AppEventType.AWAKEN,
      payload: {
        rescueOrder: targetMoonCat.rescueOrder,
        x: coords.x,
        y: coords.y
      },
    })
  }, [awokenMoonCats, getEnterCoords, moonCatList])

  const handleKeyDown = useCallback(
    (event: KeyboardEvent) => {
      activeKeys.current.add(event.code)
      if (activeKeys.current.has('Tab') && activeKeys.current.has('KeyB')) {
        event.preventDefault() // We're going to do something special; don't move the focus around like TAB normally does...
        cb()
      }
    }, [cb])

  useEffect(() => {
    const handleKeyUp = (event: KeyboardEvent) => {
      activeKeys.current.delete(event.code)
    }

    // Add event listener
    window.addEventListener('keydown', handleKeyDown)
    window.addEventListener('keyup', handleKeyUp)

    // Cleanup function to remove event listener
    return () => {
      window.removeEventListener('keydown', handleKeyDown)
      window.removeEventListener('keyup', handleKeyUp)
    }
  }, [handleKeyDown]) // Empty dependency array ensures this effect runs only once on mount
}
export default useTabbieHook