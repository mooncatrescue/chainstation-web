import {
  EAS,
  SchemaEncoder,
  SchemaRegistry,
  SignedOffchainAttestation,
  Transaction,
} from '@ethereum-attestation-service/eas-sdk'
import { useSigner } from './wagmi-ethers-utils'
import { API2_SERVER_ROOT, stringifyBigints } from './util'
import { Address } from 'viem'

export const SCHEMAS = {
  GM: '0x85500e806cf1e74844d51a20a6d893fe1ed6f6b0738b50e43d774827d08eca61',
  PRODUCT_REVIEW: '0xeae0ff58b769be065ba9d54f4dd9b88c07de93a00e1e3b889a7099de02dd5549',
  TOKEN_LIST: '0x58de78ba175d13cb1699fdbc6c25a80ba2b21a943b5aac252ec367c878808d09',
}

// https://docs.attest.sh/docs/quick--start/contracts#arbitrum-one
const REGISTRY_ADDRESSES: Record<number, { eas: Address; schemas: Address }> = {
  1: {
    eas: '0xA1207F3BBa224E2c9c3c6D5aF63D0eb1582Ce587',
    schemas: '0xA7b39296258348C78294F95B872b282326A97BDF',
  },
  42161: {
    eas: '0xbD75f629A22Dc1ceD33dDA0b68c546A1c035c458',
    schemas: '0xA310da9c5B885E7fb3fbA9D66E9Ba6Df512b78eB',
  },
}

export interface BasicAttestation {
  sig: SignedOffchainAttestation
  signer: string
}

/**
 * Structure for records returned from API endpoint
 */
export interface AttestationDoc extends BasicAttestation {
  data: Record<string, { type: string; value: any }>
  compressed: string
}

export interface AttestationDraft {
  /** UID of Schema to use (32 bytes) */
  schemaId: `0x${string}`
  /** Ethereum address the Attestation is for (20 bytes) */
  recipient: Address
  /** Timestamp when this Attestation expires */
  expiration?: bigint
  /** Should this Attestation be able to be revoked in the future? */
  revocable?: boolean
  /** UID of an Attestation to reference (reply to; 32 bytes) */
  reference?: `0x${string}`
  /** Data fields for the attestation, matching the schema */
  data: any[]
}

/**
 * Compress an Attestation for verification on easscan.org
 *
 * The verification tool at https://arbitrum.easscan.org/offchain, if delivering an Attestation as a query parameter, it
 * does not accept the full Attestation as payload. Instead, it accepts a custom format where the key bits of an
 * Attestation are switched to a JSON array to be a smaller payload.
 */
export async function compressAttestation(a: SignedOffchainAttestation, signerAddress: string): Promise<string> {
  const payload = [
    a.domain.version,
    a.domain.chainId,
    a.domain.verifyingContract,
    a.signature.r,
    a.signature.s,
    a.signature.v,
    signerAddress,
    a.uid,
    a.message.schema,
    a.message.recipient === '0x0000000000000000000000000000000000000000' ? '0' : a.message.recipient,
    Number(a.message.time),
    Number(a.message.expirationTime),
    a.message.refUID === '0x0000000000000000000000000000000000000000000000000000000000000000' ? '0' : a.message.refUID,
    a.message.revocable,
    a.message.data,
    0,
    a.message.version,
    a.message.salt,
  ]
  const compressed = await deflateString(
    JSON.stringify(payload, stringifyBigints)
  )
  return compressed.substring(compressed.indexOf(',') + 1)
}

/**
 * Use DEFLATE compression plus Base64 encoding to shrink a text string to a smaller payload.
 *
 * The DEFLATE compression method makes a zip-like compressed binary stream that is significantly smaller than the
 * original string, but hard to pass as a payload in a URL parameter. So this function uses Base64 encoding to
 * present the final string, which is longer than expressing it as a binary string, but easier to transmit.
 */
async function deflateString(str: string): Promise<string> {
  // Convert the string to a byte stream.
  const stream = new Blob([str]).stream()

  // Create a compressed stream.
  const compressedStream: ReadableStream<Uint8Array> = stream.pipeThrough(new CompressionStream('deflate'))

  // Read all the bytes from this stream.
  const chunks: Array<Uint8Array> = []
  const reader = compressedStream.getReader()
  while (true) {
    const { done, value } = await reader.read()
    if (done) {
      break
    } else {
      chunks.push(value)
    }
  }

  // Convert the binary data stored as Uint8Array values into a Base64-encoded string
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.onloadend = () => {
      resolve(reader.result as string)
      const startLength = str.length
      const endLength = (reader.result as string).length
      const compressionRatio = ((1 - endLength / startLength) * 100).toFixed(2)
      console.debug(
        `Compressed input string from ${startLength} to ${endLength} characters long (${compressionRatio}% reduction)`
      )
    }

    reader.readAsDataURL(new Blob(chunks))
  })
}

export const attestHelpText = (
  <>
    <p>
      Your wallet will prompt you for a signature to confirm this data really comes from you.
      <br />
      This signature is not a transaction (doesn&rsquo;t modify anything onchain) and is therefore no cost to you.
      <br />
      This attestation has <strong>no expiration date</strong>, so will be permanent (unless you take an on-chain action
      to revoke it)
    </p>
    <p>
      This feature uses the <a href="https://attest.org/">Ethereum Attestation Service (EAS)</a> to structure and parse
      this data.
      <br />
      For more details about what an Attestation is, see{' '}
      <a href="https://docs.attest.org/docs/core--concepts/how-eas-works">their documentation</a>.
    </p>
  </>
)

export const revokeHelpText = (
  <>
    <p>
      This signature is for a transaction on the blockchain (so requires a small gas fee on that chain to finalize).
    </p>
    <p>
      This feature uses the <a href="https://attest.org/">Ethereum Attestation Service (EAS)</a> to structure and parse
      this data.
      <br />
      For more details about what an Attestation is, see{' '}
      <a href="https://docs.attest.org/docs/core--concepts/how-eas-works">their documentation</a>.
    </p>
  </>
)

export function useEas(chainId: number) {
  const signer = useSigner()

  const eas = new EAS(REGISTRY_ADDRESSES[chainId].eas)
  const schemaRegistry = new SchemaRegistry(REGISTRY_ADDRESSES[chainId].schemas)
  if (signer) {
    eas.connect(signer as any)
    schemaRegistry.connect(signer as any)
  }

  /**
   * Compose an Attestation and trigger a prompt for the user to sign it
   */
  async function offchainSign({
    schemaId,
    recipient,
    expiration = 0n,
    revocable = true,
    reference = '0x0000000000000000000000000000000000000000000000000000000000000000',
    data,
  }: AttestationDraft): Promise<BasicAttestation | null> {
    if (!signer) return null
    const offchain = await eas.getOffchain()
    const schemaRecord = await schemaRegistry.getSchema({ uid: schemaId })
    console.debug('Fetched schema record', schemaRecord.uid, schemaRecord.schema)

    const schemaEncoder = new SchemaEncoder(schemaRecord.schema)
    const encodedData = schemaEncoder.encodeData(data)

    return Promise.all([
      offchain.signOffchainAttestation(
        {
          recipient: recipient,
          expirationTime: expiration,
          revocable: revocable,
          time: BigInt(Math.floor(new Date().getTime() / 1000)),
          schema: schemaId,
          refUID: reference,
          data: encodedData,
        },
        signer as any
      ),
      signer.getAddress(),
    ]).then(([attestation, signerAddress]) => {
      const attPayload = JSON.stringify({ sig: attestation, signer: signerAddress }, stringifyBigints)
      localStorage.setItem('lastAttestation', attPayload)
      return fetch(`${API2_SERVER_ROOT}/attestations`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: attPayload,
      }).then((rs) => {
        return {
          sig: attestation,
          signer: signerAddress,
        }
      })
    })
  }

  async function revokeAttestations(uid: string | string[]) {
    const targetUids = Array.isArray(uid) ? uid : [uid]

    // Check if any of the Attestations were revoked out-of-band
    const precheck: boolean[] = await Promise.all(
      targetUids.map((uid) =>
        fetch(`${API2_SERVER_ROOT}/check-attestation?id=${uid}`, { method: 'POST' }).then((rs) => {
          if (!rs.ok) return true // Failed to fetch status of this attestation. Assume it still needs revoking
          return rs.json().then((data) => !(data.isRevoked > 0)) // If an attestation has a timestamp for `isRevoked`, no need to revoke it again
        })
      )
    )
    const needsRevoking = targetUids.filter((uid, index) => precheck[index])

    // Trigger a transaction to revoke the target Attestation
    let rs: Transaction<bigint[]>
    try {
      rs = await eas.multiRevokeOffchain(needsRevoking) // Await user signing transaction
    } catch (err: any) {
      throw new Error('Failed to submit the revoking transaction for the attestation')
    }
    await rs.wait() // Await transaction finalized in blockchain

    // Inform the API server of the changes
    await Promise.all(
      needsRevoking.map((uid) => fetch(`${API2_SERVER_ROOT}/check-attestation?id=${uid}`, { method: 'POST' }))
    )
  }

  return {
    eas,
    schemaRegistry,
    signer,
    offchainSign,
    revokeAttestations,
  }
}
