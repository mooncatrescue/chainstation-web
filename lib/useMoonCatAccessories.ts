'use client'
import { Address, parseAbi } from 'viem'
import { multicall, readContract } from 'wagmi/actions'
import { ACCESSORIES_ADDRESS, parseAccessoryMetabyte } from './util'
import { useQuery } from '@tanstack/react-query'
import { config } from './wagmi-config'

const ACCESSORIES = {
  address: ACCESSORIES_ADDRESS as Address,
  abi: parseAbi([
    'function totalAccessories() external view returns (uint256)',
    'function balanceOf(uint256 rescueOrder) external view returns (uint256)',
    'function ownedAccessoryByIndex(uint256 rescueOrder, uint256 ownedAccessoryIndex) external view returns ((uint232 accessoryId, uint8 paletteIndex, uint16 zIndex))',
    'function accessoryInfo(uint256 accessoryId) external view returns ((uint16 totalSupply, uint16 availableSupply, bytes28 name, address manager, uint8 metabyte, uint8 availablePalettes, bytes2[4] positions, bool availableForPurchase, uint256 price))',
  ]),
  chainId: 1,
} as const

export interface OwnedAccessory {
  accessoryId: bigint
  name: string
  paletteIndex: number
  availablePalettes: number
  isBackground: boolean
  zIndex: number
  ownedIndex: number
}

/**
 * Hook for fetching all owned Accessories for a specific MoonCat
 * Includes details about which ones are worn and palette options for each Accessory
 */
export default function useMoonCatAccessories(rescueOrder: number) {
  const { data: ownedAccessories, status } = useQuery({
    queryKey: ['mooncat-owned-accessories', rescueOrder],
    queryFn: async () => {
      // Start with fetching how many Accessories this MoonCat owns
      const count = await readContract(config, {
        ...ACCESSORIES,
        functionName: 'balanceOf',
        args: [BigInt(rescueOrder)],
      })

      // Iterate up to that accessory count number to fetch which Accessory ID it is, and if it's currently worn by this MoonCat
      const multicalls = []
      for (let i = 0n; i < count; i++) {
        multicalls.push({
          ...ACCESSORIES,
          functionName: 'ownedAccessoryByIndex',
          args: [BigInt(rescueOrder), i],
        } as const)
      }
      const ownedAccessories = await multicall(config, {
        contracts: multicalls,
        chainId: 1,
        allowFailure: false,
      })

      // Fetch additional detail about each Accessory that is owned
      const infoCalls = ownedAccessories.map(
        (ownedAcc) =>
          ({
            ...ACCESSORIES,
            functionName: 'accessoryInfo',
            args: [ownedAcc.accessoryId],
          } as const)
      )
      const accInfo = await multicall(config, {
        contracts: infoCalls,
        chainId: 1,
        allowFailure: false,
      })

      return ownedAccessories
        .map((ownedAcc, i) => {
          // Combined the owned-accessory info with the details about the accessory
          const info = accInfo[i]
          const meta = parseAccessoryMetabyte(info.metabyte)

          // Parse the Accessory Name into UTF8
          let nameBuffer = Buffer.from(info.name.substring(2), 'hex')
          const firstNull = nameBuffer.indexOf(0x00)
          if (firstNull >= 0) {
            nameBuffer = nameBuffer.subarray(0, firstNull)
          }

          return {
            ...ownedAcc,
            name: nameBuffer.toString('utf8'),
            availablePalettes: info.availablePalettes,
            isBackground: meta.background,
            ownedIndex: i,
          }
        })
        .sort((a, b) => {
          if (a.zIndex != b.zIndex) {
            return b.zIndex - a.zIndex
          }
          return a.name.localeCompare(b.name)
        })
    },
  })

  return {
    status,
    ownedAccessories: ownedAccessories ?? [],
  }
}
