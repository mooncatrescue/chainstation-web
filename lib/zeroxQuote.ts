import { Address } from "viem";

export type TokenName = 'mooncat' | 'mcat17'
export type AdoptType = 'random' | 'specific'

const POOL_CONFIG: Record<TokenName, { address: Address; vaultId: number }> = {
  mooncat: {
    address: '0x98968f0747E0A261532cAcC0BE296375F5c08398',
    vaultId: 25,
  },
  mcat17: {
    address: '0xa8b42c82a628dc43c2c2285205313e5106ea2853',
    vaultId: 451,
  },
}

export default async function zeroxQuote(apiKey: string, tokenName: TokenName, adoptType: AdoptType = 'specific') {
  const { address: tokenAddress, vaultId } = POOL_CONFIG[tokenName]
  let tokenAmount: string | null = null
  if (adoptType == 'specific') {
    tokenAmount = '1030000000000000000'
  } else if (adoptType == 'random') {
    tokenAmount = '1020000000000000000'
  }
  if (typeof tokenAddress == 'undefined' || tokenAmount === null) {
    // Invalid token request
    return Response.json({ ok: false }, { status: 400 })
  }
  const rs = await fetch(
    `https://api.0x.org/swap/v1/quote?${new URLSearchParams({
      buyToken: tokenAddress,
      sellToken: 'WETH',
      buyAmount: tokenAmount,
      slippagePercentage: '0.01',
    })}`,
    {
      headers: { '0x-api-key': apiKey },
      next: { revalidate: 900 },
    }
  )
  if (!rs.ok) {
    throw new Error('Failed to fetch from 0x: ' + rs.statusText)
  }
  return await rs.json()
}
