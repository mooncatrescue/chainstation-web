'use client'
import { useRouter, usePathname } from 'next/navigation'
import { useEffect, useState } from 'react'
import useIsMounted from './useIsMounted'

function getInitialTab(defaultTab: string) {
  if (typeof window == 'undefined') return defaultTab
  const hash = window.location.hash
  return hash == '' || hash == '#' ? defaultTab : hash.substring(1).toLowerCase()
}

/**
 * Hook for encapsulating logic used by pages that use hash-navigation to flip between different "tabs" within the page content
 */
export default function useTabHandler(initialTab: string) {
  const router = useRouter()
  const pathName = usePathname()
  const [activeTab, setActiveTab] = useState<string>(getInitialTab(initialTab))
  const isMounted = useIsMounted()

  /**
   * Changing the URL hash should change the active tab
   * Note, `window.history.pushState` does NOT cause the `hashchange` event to fire (https://developer.mozilla.org/en-US/docs/Web/API/History/pushState#description),
   * which means the NextJS `router.replace` and `router.push` actions also do not cause that event to fire.
   * So this handler will only fire if the user manually changes the hash in the address bar.
   */
  useEffect(() => {
    function handleHashChange() {
      console.debug('Hash change detected', window.location.hash)
      const tabId = getInitialTab(initialTab)
      setActiveTab((currentTab) => (tabId !== currentTab ? tabId : currentTab))
    }
    window.addEventListener('hashchange', handleHashChange)
    return () => {
      window.removeEventListener('hashchange', handleHashChange)
    }
  }, [initialTab])

  /**
   * Change the active tab by a user interaction
   */
  const toggleTab = (tabId: string) => {
    if (activeTab == tabId) return
    console.debug('Switching to tab', tabId)
    router.push(`${pathName}#${tabId}`, { scroll: false })
    setActiveTab((currentTab) => (tabId !== currentTab ? tabId : currentTab))
  }

  return { activeTab: isMounted ? activeTab : initialTab, toggleTab }
}
