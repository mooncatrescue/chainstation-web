import MoonCatLink from "components/MoonCatLink"

const linkMatch = /https?:\/\/[^\s\)]+[^\s\)\.\?\!]/i
const namedLinkMatch = /\[(.+?)\]\((.+?)\)/i
const moonCatMatch = /MoonCat #(\d{1,5})/i
const boldMatch = /(?<=\s|^)\*\*(.+?)\*\*(?=[\s\.\?\!]|$)/i
const italicMatch = /(?<=\s|^)\*(.+?)\*(?=[\s\.\?\!]|$)/i
const codeMatch = /(?<=\s|^)\`(.+?)\`(?=[\s\.\?\!]|$)/i

export default function parseDescription(raw: string): React.ReactNode {
  /**
   * Parse an individual output component, and determine if it can be broken down further.
   *
   * This is a mini-Markdown-style parsing engine to convert known-safe formatting indicators into JSX elements
   */
  function parseFragment(t: React.ReactNode): [boolean, React.ReactNode] {
    if (typeof t !== 'string') return [false, t]
    const randomKey = Math.floor(Math.random() * 9999999)

    // Find named links
    let rs = namedLinkMatch.exec(t)
    if (rs !== null) {
      const preamble = t.substring(0, rs.index)
      const label = rs[1]
      const url = rs[2]
      const suffix = t.substring(rs.index + rs[0].length)
      return [
        true,
        [
          preamble,
          <a key={randomKey} href={url} rel="external nofollow">{label}</a>,
          suffix,
        ]
      ]
    }

    // Find bare URLs to link
    rs = linkMatch.exec(t)
    if (rs !== null) {
      const preamble = t.substring(0, rs.index)
      const url = rs[0]
      const suffix = t.substring(rs.index + rs[0].length)
      return [
        true,
        [
          preamble,
          <a key={randomKey} href={url} rel="external nofollow">
            {url.length > 35 ? url.substring(0, 30) + '...' : url}
          </a>,
          suffix,
        ],
      ]
    }

    // Convert mentions of MoonCats into links
    rs = moonCatMatch.exec(t)
    if (rs !== null) {
      const preamble = t.substring(0, rs.index)
      const rescueOrder = rs[1]
      const suffix = t.substring(rs.index + rs[0].length)
      return [true, [preamble, <MoonCatLink key={randomKey} rescueOrder={Number(rescueOrder)} />, suffix]]
    }

    // Bold
    rs = boldMatch.exec(t)
    if (rs !== null) {
      const preamble = t.substring(0, rs.index)
      const bolded = rs[1]
      const suffix = t.substring(rs.index + rs[0].length)
      return [true, [preamble, <strong key={randomKey}>{bolded}</strong>, suffix]]
    }

    // Italic
    rs = italicMatch.exec(t)
    if (rs !== null) {
      const preamble = t.substring(0, rs.index)
      const italic = rs[1]
      const suffix = t.substring(rs.index + rs[0].length)
      return [true, [preamble, <em key={randomKey}>{italic}</em>, suffix]]
    }

    // Code
    rs = codeMatch.exec(t)
    if (rs !== null) {
      const preamble = t.substring(0, rs.index)
      const code = rs[1]
      const suffix = t.substring(rs.index + rs[0].length)
      return [true, [preamble, <code key={randomKey}>{code}</code>, suffix]]
    }

    return [false, t]
  }

  // Start by splitting the initial string into separate paragraphs
  const paragraphs = raw
    .replaceAll(/\n{3,}/g, '\n\n') // Replace all runs of three or more newlines with just two newlines
    .split('\n\n') // Split on double newlines to be paragraph chunks
    .map((t) => {
      let pieces: React.ReactNode[] = [t]
      let foundChanges: boolean = false
      let iterations = 0

      do {
        const parsed = pieces.reduce(
          (agg, p) => {
            const [rs, out] = parseFragment(p)
            if (rs) agg.foundChanges = true
            agg.out = agg.out.concat(out)
            return agg
          },
          { foundChanges: false, out: [] } as { foundChanges: boolean; out: React.ReactNode[] }
        )
        pieces = parsed.out.flat()
        foundChanges = parsed.foundChanges
        iterations++
        if (iterations > 500) {
          console.warn('parseDescription reached maximum iterations')
          break
        }
      } while (foundChanges == true)

      return pieces
    })
  return paragraphs.map((p, i) => <p key={i}>{p}</p>)
}
