import { LocalUserSession } from 'additional'
import { isSessionValid } from './firebase'
import { IronSessionData } from 'iron-session'
import { SiweMessage } from 'viem/siwe'

export default async function userVerifiedAddresses(
  keyring: IronSessionData['keyring'],
  chains: number[]
): Promise<[LocalUserSession[], LocalUserSession[]]> {
  if (!keyring || !Array.isArray(keyring) || keyring.length == 0) {
    // Keyring is missing/empty
    console.log('userVerifiedAddresses: User keyring is empty; nothing to validate')
    return [[], []]
  }

  const toCheck = keyring.filter((k) => k.siwe.chainId && chains.includes(k.siwe.chainId))
  if (toCheck.length == 0) {
    // No Keys for the target chains
    return [[], []]
  }

  let validKeys = []
  let invalidKeys = []
  for (let k of toCheck) {
    const siwe: SiweMessage = {
      ...k.siwe,
      issuedAt: k.siwe.issuedAt ? new Date(k.siwe.issuedAt) : undefined,
      expirationTime: k.siwe.expirationTime ? new Date(k.siwe.expirationTime) : undefined,
    }
    console.log('checking', siwe)

    const isValid = await isSessionValid(siwe, k.signature)
    if (isValid) {
      validKeys.push(k)
    } else {
      invalidKeys.push(k)
    }
  }
  return [validKeys, invalidKeys]
}
