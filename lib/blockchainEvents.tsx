import { Address, zeroAddress } from 'viem'
import {
  ACCESSORIES_ADDRESS,
  ACCLIMATOR_ADDRESS,
  ADDRESS_DETAILS,
  API_SERVER_ROOT,
  IPFS_GATEWAY,
  JUMPPORT_ADDRESS,
  MOMENTS_ADDRESS,
  RESCUE_ADDRESS,
  ZWS,
  formatAsDate,
  getAllMoments,
} from './util'
import EthereumAddress from 'components/EthereumAddress'
import Link from 'next/link'
import { Moment } from './types'
import AccessoryLink from 'components/AccessoryLink'
import MoonCatLink from 'components/MoonCatLink'

import momentsData from 'lib/moments_meta.json'
const moments = momentsData as Moment[]
const allMoments = getAllMoments()

export interface BlockchainEvent {
  args: Record<string, any>
  eventContract: Address
  logIndex: number
  tx: {
    data: `0x${string}`
    from: Address
    to: Address
    value: string
    hash: `0x${string}`
  }
  blockNumber: number
  eventName: string
  timestamp: number
}

interface MoonCatSubject {
  type: 'mooncat'
  id: string
  rescueOrder: number
}
interface AddressSubject {
  type: 'address'
  id: Address
}
interface NoSubject {
  type: null
  id: null
}
export type EventSubject = MoonCatSubject | AddressSubject | NoSubject

interface BlockchainEventDetails {
  title: string
  description: React.ReactNode
  image?: string
  dotClass: string
}

/**
 * For a raw Blockhain Event, parse out a more human-friendly description of what it represents
 */
export function getMessageForEvent(event: BlockchainEvent, subject: EventSubject): BlockchainEventDetails {
  // Transfer event from Acclimator contract
  if (event.eventName == 'Transfer' && event.eventContract == ACCLIMATOR_ADDRESS) {
    if (ADDRESS_DETAILS[event.args.to] && ADDRESS_DETAILS[event.args.to].type == 'pool') {
      // The address the transfer is going to is a known NFT pool address
      if (subject.type == 'mooncat') {
        return {
          title: 'Put up for adoption',
          description: (
            <p>
              Moved to <EthereumAddress address={event.args.to} /> to seek a new owner. Thanks for the time,{' '}
              <EthereumAddress address={event.args.from} />!
            </p>
          ),
          dotClass: 'dot',
        }
      } else if (subject.type == 'address') {
        if (subject.id == event.args.to) {
          // The subject of this timeline is the pool itself
          return {
            title: 'Received a MoonCat for adoption',
            description: (
              <p>
                Received <MoonCatLink rescueOrder={event.args.tokenId} /> from{' '}
                <EthereumAddress address={event.args.from} /> to help them find a new owner.
              </p>
            ),
            dotClass: 'dot',
          }
        } else {
          // This timeline view is from the perspective of the depositor
          return {
            title: 'Put a MoonCat up for adoption',
            description: (
              <p>
                Put <MoonCatLink rescueOrder={event.args.tokenId} /> up for adoption at{' '}
                <EthereumAddress address={event.args.to} /> to seek a new owner.
              </p>
            ),
            dotClass: 'dot',
          }
        }
      }
    }
    if (ADDRESS_DETAILS[event.args.from] && ADDRESS_DETAILS[event.args.from].type == 'pool') {
      // The address the transfer came from is a known NFT pool address
      if (subject.type == 'mooncat') {
        return {
          title: 'Adopted by new owner',
          description: (
            <p>
              Got adopted by <EthereumAddress address={event.args.to} /> from{' '}
              <EthereumAddress address={event.args.from} />!
            </p>
          ),
          dotClass: 'dot',
        }
      } else if (subject.type == 'address') {
        if (subject.id == event.args.from) {
          // The subject of this timeline is the pool itself
          return {
            title: 'Adopted out a MoonCat',
            description: (
              <p>
                Found a new home for <MoonCatLink rescueOrder={event.args.tokenId} />; they got adopted by{' '}
                <EthereumAddress address={event.args.to} />!
              </p>
            ),
            dotClass: 'dot',
          }
        } else {
          // This timeline view is from the perspective of the withdrawer
          return {
            title: 'Adopted a MoonCat',
            description: (
              <p>
                Adopted <MoonCatLink rescueOrder={event.args.tokenId} /> from{' '}
                <EthereumAddress address={event.args.from} />!
              </p>
            ),
            dotClass: 'dot',
          }
        }
      }
    }
    if (ADDRESS_DETAILS[event.args.to] && ADDRESS_DETAILS[event.args.to].type == 'bridge') {
      // The address the transfer is going to is a known NFT bridge address
      if (subject.type == 'mooncat') {
        return {
          title: 'Went on an adventure',
          description: (
            <p>
              Took a trip across <EthereumAddress address={event.args.to} /> to have some adventures elsewhere.
            </p>
          ),
          dotClass: 'dot',
        }
      } else if (subject.type == 'address') {
        return {
          title: 'Sent a MoonCat on an adventure',
          description: (
            <p>
              Sent
              <MoonCatLink rescueOrder={event.args.tokenId} /> on a trip across{' '}
              <EthereumAddress address={event.args.to} /> to have some adventures elsewhere.
            </p>
          ),
          dotClass: 'dot',
        }
      }
    }
    if (ADDRESS_DETAILS[event.args.from] && ADDRESS_DETAILS[event.args.from].type == 'bridge') {
      // The address the transfer came from is a known NFT bridge address
      if (subject.type == 'mooncat') {
        return {
          title: 'Returned from an adventure',
          description: (
            <p>
              Came back across <EthereumAddress address={event.args.from} /> to{' '}
              <EthereumAddress address={event.args.to} />, for further adventures here on ChainStation Alpha.
            </p>
          ),
          dotClass: 'dot',
        }
      } else if (subject.type == 'address') {
        return {
          title: 'Received a MoonCat back from an adventure',
          description: (
            <p>
              Received <MoonCatLink rescueOrder={event.args.tokenId} /> back from across{' '}
              <EthereumAddress address={event.args.from} />, for further adventures here on ChainStation Alpha.
            </p>
          ),
          dotClass: 'dot',
        }
      }
    }

    // No additional details are known for this transfer; treat it as an adoption
    if (subject.type == 'mooncat') {
      return {
        title: 'Adopted by new owner',
        description: (
          <p>
            Got adopted by <EthereumAddress address={event.args.to} />. Thanks for the time,{' '}
            <EthereumAddress address={event.args.from} />!
          </p>
        ),
        dotClass: 'dot',
      }
    } else if (subject.type == 'address') {
      if (subject.id == event.args.to) {
        // This address received the MoonCat
        return {
          title: 'Adopted a MoonCat',
          description: (
            <p>
              Adopted <MoonCatLink rescueOrder={event.args.tokenId} /> from{' '}
              <EthereumAddress address={event.args.from} />.
            </p>
          ),
          dotClass: 'dot',
        }
      } else {
        // This address sent the MoonCat
        return {
          title: 'Said farewell to a MoonCat',
          description: (
            <p>
              <MoonCatLink rescueOrder={event.args.tokenId} /> got adopted by a new owner;{' '}
              <EthereumAddress address={event.args.to} />.
            </p>
          ),
          dotClass: 'dot',
        }
      }
    }
  }

  // Child received to Acclimator contract
  if (event.eventName == 'ReceivedChild' && event.eventContract == ACCLIMATOR_ADDRESS) {
    if (subject.type == 'mooncat') {
      // First, is this MoonCat the one receiving, or the one being moved?
      if (event.args['_toTokenId'] == subject.rescueOrder) {
        // This MoonCat received a token. Check if this is a known child type
        let description: React.ReactNode
        if (event.args['_childContract'] == MOMENTS_ADDRESS) {
          const moment = allMoments[event.args['_childTokenId']]
          description = (
            <p>
              This MoonCat got{' '}
              <Link href={'/moments/' + moment.moment}>{`MoonCat${ZWS}Moment #${event.args['_childTokenId']}`}</Link> to
              hold onto.
            </p>
          )
        } else if (event.args['_childContract'] == ACCLIMATOR_ADDRESS) {
          description = (
            <p>
              <Link href={'/mooncats/' + event.args['_childTokenId']}>{`MoonCat #${event.args['_childTokenId']}`}</Link>{' '}
              came to visit this MoonCat.
            </p>
          )
        } else {
          description = (
            <p>
              This MoonCat got token {event.args['_childTokenId']} from collection{' '}
              <EthereumAddress address={event.args['_childContract']} /> to hold onto.
            </p>
          )
        }

        return {
          title: 'Received a token',
          description,
          dotClass: 'dot',
        }
      }

      // This MoonCat was the token that was moved
      return {
        title: 'Went for a visit',
        description: (
          <p>
            Went to go visit{' '}
            <Link href={'/mooncats/' + event.args['_toTokenId']}>{`MoonCat #${event.args['_toTokenId']}`}</Link>
          </p>
        ),
        dotClass: 'dot',
      }
    } else if (subject.type == 'address') {
      // Gave a gift to a MoonCat. Check if this is a known child type
      let description: React.ReactNode
      if (event.args['_childContract'] == MOMENTS_ADDRESS) {
        const moment = allMoments[event.args['_childTokenId']]
        description = (
          <p>
            Gave <Link href={'/moments/' + moment.moment}>{`MoonCat${ZWS}Moment #${event.args['_childTokenId']}`}</Link>{' '}
            to MoonCat {event.args['_toTokenId']} to hold onto.
          </p>
        )
      } else if (event.args['_childContract'] == ACCLIMATOR_ADDRESS) {
        description = (
          <p>
            Gave <MoonCatLink rescueOrder={event.args['_childTokenId']} /> to{' '}
            <MoonCatLink rescueOrder={event.args['_toTokenId']} />.
          </p>
        )
      } else {
        description = (
          <p>
            Gave token {event.args['_childTokenId']} from collection{' '}
            <EthereumAddress address={event.args['_childContract']} /> to{' '}
            <MoonCatLink rescueOrder={event.args['_toTokenId']} /> to hold onto.
          </p>
        )
      }

      return {
        title: 'Gave a gift to a MoonCat',
        description,
        dotClass: 'dot',
      }
    }
  }

  // Child sent from to Acclimator contract
  if (event.eventName == 'TransferChild' && event.eventContract == ACCLIMATOR_ADDRESS) {
    if (subject.type == 'mooncat') {
      // First, is this MoonCat the one sending, or the one being moved?
      if (event.args['_fromTokenId'] == subject.rescueOrder) {
        // This MoonCat sent a token. Check if this is a known child type
        let description: React.ReactNode
        if (event.args['_childContract'] == MOMENTS_ADDRESS) {
          const moment = allMoments[event.args['_childTokenId']]
          description = (
            <p>
              This MoonCat sent{' '}
              <Link href={'/moments/' + moment.moment}>{`MoonCat${ZWS}Moment #${event.args['_childTokenId']}`}</Link> on
              its way.
            </p>
          )
        } else if (event.args['_childContract'] == ACCLIMATOR_ADDRESS) {
          description = (
            <p>
              <Link href={'/mooncats/' + event.args['_childTokenId']}>{`MoonCat #${event.args['_childTokenId']}`}</Link>{' '}
              left to have their own adventures.
            </p>
          )
        } else {
          description = (
            <p>
              This MoonCat sent token {event.args['_childTokenId']} of collection{' '}
              <EthereumAddress address={event.args['_childContract']} /> on its way.
            </p>
          )
        }

        return {
          title: 'Sent a token',
          description,
          dotClass: 'dot',
        }
      }

      // This MoonCat was the token that was moved
      return {
        title: 'Returned from a visit',
        description: (
          <p>
            Came back from visiting{' '}
            <Link href={'/mooncats/' + event.args['_toTokenId']}>{`MoonCat #${event.args['_toTokenId']}`}</Link>
          </p>
        ),
        dotClass: 'dot',
      }
    } else if (subject.type == 'address') {
      // Withdrew token from a MoonCat. Check if this is a known child type
      let description: React.ReactNode
      if (event.args['_childContract'] == MOMENTS_ADDRESS) {
        const moment = allMoments[event.args['_childTokenId']]
        description = (
          <p>
            Withdrew{' '}
            <Link href={'/moments/' + moment.moment}>{`MoonCat${ZWS}Moment #${event.args['_childTokenId']}`}</Link> from{' '}
            <MoonCatLink rescueOrder={event.args['_fromTokenId']} />.
          </p>
        )
      } else if (event.args['_childContract'] == ACCLIMATOR_ADDRESS) {
        description = (
          <p>
            Withdrew <MoonCatLink rescueOrder={event.args['_childTokenId']} /> from{' '}
            <MoonCatLink rescueOrder={event.args['_fromTokenId']} />, to have their own adventures.
          </p>
        )
      } else {
        description = (
          <p>
            Withdrew {event.args['_childTokenId']} of collection{' '}
            <EthereumAddress address={event.args['_childContract']} /> from{' '}
            <MoonCatLink rescueOrder={event.args['_fromTokenId']} />.
          </p>
        )
      }

      return {
        title: 'Sent a token',
        description,
        dotClass: 'dot',
      }
    }
  }

  // Accessory purchased
  if (event.eventName == 'AccessoryPurchased' && event.eventContract == ACCESSORIES_ADDRESS) {
    if (subject.type == 'mooncat') {
      return {
        title: 'Bought an Accessory',
        description: (
          <p>
            This MoonCat added <AccessoryLink id={event.args.accessoryId} /> to their wardrobe
          </p>
        ),
        image: `${API_SERVER_ROOT}/image/${event.args.rescueOrder}?acc=${event.args.accessoryId}&scale=4&padding=10`,
        dotClass: 'dot',
      }
    } else {
      return {
        title: 'Bought an Accessory',
        description: (
          <p>
            Bought <AccessoryLink id={event.args.accessoryId} /> for{' '}
            <MoonCatLink rescueOrder={event.args.rescueOrder} /> to add to their wardrobe
          </p>
        ),
        image: `${API_SERVER_ROOT}/image/${event.args.rescueOrder}?acc=${event.args.accessoryId}&scale=4&padding=10`,
        dotClass: 'dot',
      }
    }
  }

  // Named event from Rescue contract
  if (event.eventName == 'CatNamed' && event.eventContract == RESCUE_ADDRESS) {
    if (subject.type == 'mooncat') {
      return {
        title: 'Named',
        description: (
          <p>
            This MoonCat was bestowed a name by <EthereumAddress address={event.tx.from} />.
          </p>
        ),
        dotClass: 'dot-large',
      }
    } else if (subject.type == 'address') {
      return {
        title: 'Named',
        description: (
          <p>
            Bestowed a name on <MoonCatLink catId={event.args['catId']} />.
          </p>
        ),
        dotClass: 'dot-large',
      }
    }
  }

  // Adopted event from Rescue contract
  if (event.eventName == 'CatAdopted' && event.eventContract == RESCUE_ADDRESS) {
    if (event.args.to == ACCLIMATOR_ADDRESS) {
      // The address this MoonCat is going to is the Acclimator contract; this was an Acclimation, not adoption
      if (subject.type == 'mooncat') {
        return {
          title: 'Acclimated',
          description: <p>This MoonCat visited the Acclimator and got accustomed to life on the modern blockchain!</p>,
          dotClass: 'dot',
        }
      } else if (subject.type == 'address') {
        return {
          title: 'Acclimated',
          description: (
            <p>
              Sent <MoonCatLink catId={event.args['catId']} /> to the Acclimator, for them to get accustomed to life on
              the modern blockchain!
            </p>
          ),
          dotClass: 'dot',
        }
      }
    }
    if (event.args.from == ACCLIMATOR_ADDRESS) {
      // The address this MoonCat is coming from is the Acclimator contract; this was Deacclimating, not adoption
      if (subject.type == 'mooncat') {
        return {
          title: 'Deacclimated',
          description: (
            <p>
              This MoonCat took some time away from the hustle and bustle of modern blockchain life to spend some time
              returning to their roots.
            </p>
          ),
          dotClass: 'dot',
        }
      } else if (subject.type == 'address') {
        return {
          title: 'Deacclimated',
          description: (
            <p>
              Sent <MoonCatLink catId={event.args['catId']} /> for some time away from the hustle and bustle of modern
              blockchain life to spend some time returning to their roots.
            </p>
          ),
          dotClass: 'dot',
        }
      }
    }
    if (ADDRESS_DETAILS[event.args.to] && ADDRESS_DETAILS[event.args.to].type == 'wrapper') {
      // The MoonCat is getting wrapped into some wrapper other than the Acclimator
      if (subject.type == 'mooncat') {
        return {
          title: 'Mysterious Voyage',
          description: (
            <p>
              This MoonCat went with <EthereumAddress address={event.args.from} /> to spend some time at{' '}
              <EthereumAddress address={event.args.to} />.
            </p>
          ),
          dotClass: 'dot',
        }
      } else if (subject.type == 'address') {
        return {
          title: 'Mysterious Voyage',
          description: (
            <p>
              Sent <MoonCatLink catId={event.args['catId']} /> to spend some time at{' '}
              <EthereumAddress address={event.args.to} />.
            </p>
          ),
          dotClass: 'dot',
        }
      }
    }
    if (ADDRESS_DETAILS[event.args.from] && ADDRESS_DETAILS[event.args.from].type == 'wrapper') {
      // The MoonCat is returning from being wrapped into some wrapper other than the Acclimator
      if (subject.type == 'mooncat') {
        return {
          title: 'Returned from Voyage',
          description: (
            <p>
              This MoonCat returned to <EthereumAddress address={event.args.to} /> after spending some time at{' '}
              <EthereumAddress address={event.args.from} />.
            </p>
          ),
          dotClass: 'dot',
        }
      } else if (subject.type == 'address') {
        return {
          title: 'Returned from Voyage',
          description: (
            <p>
              Received <MoonCatLink catId={event.args['catId']} /> back, after spending some time at{' '}
              <EthereumAddress address={event.args.from} />.
            </p>
          ),
          dotClass: 'dot',
        }
      }
    }
    if (event.args.from == zeroAddress) {
      // The address this MoonCat is coming from is the Zero Address; this only happens with Genesis MoonCats when adopted for the first time
      if (subject.type == 'mooncat') {
        return {
          title: 'Adopted by new owner',
          description: (
            <p>
              Got adopted by <EthereumAddress address={event.args.to} />.
            </p>
          ),
          dotClass: 'dot',
        }
      } else if (subject.type == 'address') {
        return {
          title: 'Adopted a MoonCat',
          description: (
            <p>
              Adopted <MoonCatLink catId={event.args['catId']} />!
            </p>
          ),
          dotClass: 'dot',
        }
      }
    }

    // Generic adoption event
    if (subject.type == 'mooncat') {
      return {
        title: 'Adopted by new owner',
        description: (
          <p>
            Got adopted by <EthereumAddress address={event.args.to} />. Thanks for the time,{' '}
            <EthereumAddress address={event.args.from} />!
          </p>
        ),
        dotClass: 'dot',
      }
    } else if (subject.type == 'address') {
      return {
        title: 'Adopted a MoonCat',
        description: (
          <p>
            Adopted <MoonCatLink catId={event.args['catId']} /> from <EthereumAddress address={event.args.from} />!
          </p>
        ),
        dotClass: 'dot',
      }
    }
  }

  // Rescued event from Rescue contract
  if (event.eventName == 'CatRescued' && event.eventContract == RESCUE_ADDRESS) {
    if (subject.type == 'mooncat') {
      return {
        title: 'Rescued',
        description: (
          <p>
            As part of the <a href="https://mooncat.community/blog/mcr-update4">Insanely Cute Operation</a>, this
            MoonCat was rescued from the moon by <EthereumAddress address={event.args.to} />.
          </p>
        ),
        dotClass: 'dot-large',
      }
    } else if (subject.type == 'address') {
      return {
        title: 'Rescued a MoonCat',
        description: (
          <p>
            Rescued <MoonCatLink catId={event.args['catId']} /> as part of the Insanely Cute Operation!
          </p>
        ),
        dotClass: 'dot-large',
      }
    }
  }

  // Genesis release batch
  if (event.eventName == 'GenesisCatsAdded' && event.eventContract == RESCUE_ADDRESS) {
    if (subject.type == 'mooncat') {
      return {
        title: 'Joined the Insanely Cute Operation',
        description: (
          <p>
            This <em>Genesis</em> MoonCat opted to join the{' '}
            <a href="https://mooncat.community/blog/mcr-update4">Insanely Cute Operation</a>, and help lead their
            brethren to ChainStation Alpha. Genesis MoonCats joined in batches of 16, and this was the moment the batch
            including this Genesis MoonCat joined in!
          </p>
        ),
        dotClass: 'dot-large',
      }
    }
  }

  // MoonCatMoment participation
  if (event.eventName == 'MomentMint' && event.eventContract == MOMENTS_ADDRESS) {
    const moment = moments[event.args.momentId]
    let imageSrc = moment.meta.image.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/')
    const dateDetail = formatAsDate(moment.eventDate)

    if (subject.type == 'mooncat') {
      return {
        title: 'Had a Moment',
        description: (
          <p>
            This MoonCat participated in{' '}
            <Link href={'/moments/' + moment.momentId}>{`MoonCat${ZWS}Moment #${moment.momentId}`}</Link>,{' '}
            <strong>{moment.meta.name}</strong> (event date {dateDetail})
          </p>
        ),
        image: imageSrc,
        dotClass: 'dot',
      }
    }
  }

  // Deposit event from JumpPort contract
  if (event.eventName == 'Deposit' && event.eventContract == JUMPPORT_ADDRESS) {
    if (subject.type == 'mooncat') {
      return {
        title: 'Visited JumpPort',
        description: (
          <p>
            This MoonCat visited the ChainStation JumpPort, to prepare for further explorations across the blockchain
            multiverse.
          </p>
        ),
        dotClass: 'dot',
      }
    } else if (subject.type == 'address') {
      return {
        title: 'Visited JumpPort',
        description: (
          <p>
            Sent <MoonCatLink rescueOrder={event.args['tokenId']} /> to visit the ChainStation JumpPort, to prepare for
            further explorations across the blockchain multiverse.
          </p>
        ),
        dotClass: 'dot',
      }
    }
  }

  // Withdraw event from JumpPort contract
  if (event.eventName == 'Withdraw' && event.eventContract == JUMPPORT_ADDRESS) {
    if (subject.type == 'mooncat') {
      return {
        title: 'Returned from JumpPort',
        description: <p>This MoonCat returned from their voyages through the ChainStation JumpPort.</p>,
        dotClass: 'dot',
      }
    } else if (subject.type == 'address') {
      return {
        title: 'Returned from JumpPort',
        description: (
          <p>
            Received <MoonCatLink rescueOrder={event.args['tokenId']} /> back from their voyages through the
            ChainStation JumpPort.
          </p>
        ),
        dotClass: 'dot',
      }
    }
  }

  // An unknown event type?
  if (subject.type == 'mooncat') {
    return {
      title: 'A Ripple in the Multiverse...',
      description: <p>This MoonCat had an un-quantifiable adventure of some sort on the blockchain...</p>,
      dotClass: 'dot',
    }
  } else if (subject.type == 'address') {
    return {
      title: 'A Ripple in the Multiverse...',
      description: <p>This address triggered something happening on the blockchain...</p>,
      dotClass: 'dot',
    }
  }

  return {
    title: 'A Ripple in the Multiverse...',
    description: <p>Something happened on the blockchain...</p>,
    dotClass: 'dot',
  }
}

/**
 * Given a data blob from an Ethereum transaction, determine a name for the function that was called.
 *
 * The first four bytes of a transaction's data blob is the signature (hash) of the function that is triggered by the transaction.
 * This function could call something like https://www.4byte.directory/ to get a name for the function, but for this application's
 * purposes, having a static list of known signatures is sufficient, since the functions are from a known subset for the most part.
 */
export function getFunctionName(txData: `0x${string}`): string {
  const fourBytes = txData.substring(0, 10)
  switch (fourBytes) {
    // Rescue
    case '0x4946e206':
      return 'rescueCat(bytes32 seed)'
    case '0xf884e54a':
      return 'giveCat(bytes5 catId, address to)'
    case '0x74fe6dea':
      return 'nameCat(bytes5 catId, bytes32 catName)'
    case '0x1be70510':
      return 'acceptAdoptionOffer(bytes5 catId)'
    case '0xa40c8ad0':
      return 'addGenesisCatGroup()'
    case '0xd4a03f60':
      return 'acceptAdoptionRequest(bytes5 catId)'

    // Wrappers/ERC721
    case '0x79b177ec':
      return 'wrap(bytes5 catId)'
    case '0xea598cb0':
      return 'wrap(uint256 rescueOrder)'
    case '0xde0e9a3e':
      return 'unwrap(uint256 tokenID)'
    case '0x440230d4':
      return 'batchWrap(uint256[] rescueOrders)'
    case '0x697b91e0':
      return 'batchReWrap(uint256[] rescueOrders, uint256[] oldTokenIds)'
    case '0xb88d4fde':
      return 'safeTransferFrom(address from, address to, uint256 tokenId, bytes data)'
    case '0x42842e0e':
      return 'safeTransferFrom(address from, address to, uint256 tokenId)'
    case '0x23b872dd':
      return 'transferFrom(address from, address to, uint256 tokenId)'

    case '0x1d98f3c5':
      return 'safeTransferChild(uint256 fromTokenId, address to, address childContract, uint256 childTokenId)'
    case '0x8d81f51e':
      return 'safeTransferChild(uint256 fromTokenId, address to, address childContract, uint256 childTokenId, bytes data)'
    case '0xbef44f18':
      return 'transferChild(uint256 fromTokenId, address to, address childContract, uint256 childTokenId)'

    // Accessories
    case '0xfc24100b':
      return 'buyAccessories'
    case '0x851caa67':
      return 'alterAccessories'

    // Moments
    case '0xac8ad13e':
      return 'mintClaimable(string uri, uint16[] rescueOrders)'
    case '0x3125288f':
      return 'batchClaim(uint256 momentId, uint256[] rescueOrders)'
    case '0x0c3fe6a6':
      return 'batchClaim(uint256[] momentIds, uint256 rescueOrders)'
    case '0x18078359':
      return 'batchClaim(uint256[] momentIds, uint256[] rescueOrders)'
    case '0x55aea767':
      return 'batchClaim(uint256 rescueOrder)'
    case '0xbc292782':
      return 'batchClaim(uint256[] rescueOrders)'
    case '0xc3490263':
      return 'claim(uint256 momentId, uint256 rescueOrder)'

    // JumpPort
    case '0xa71604e8':
      return 'deposit(address tokenAddress, uint256[] tokenIds)'
    case '0x5058c460':
      return 'safeWithdraw(address tokenAddress, uint256 tokenId)'

    // Marketplaces
    case '0xab834bab':
      return 'atomicMatch_'
    case '0xa8174404':
      return 'matchOrders'
    case '0xb3a34c4c':
      return 'fulfillOrder'
    case '0xfb0f3ee1':
      return 'fulfillBasicOrder'
    case '0xe7acab24':
      return 'fulfillAdvancedOrder'
    case '0x87201b41':
      return 'fulfillAvailableAdvancedOrders'
    case '0xf2d12b12':
      return 'matchAdvancedOrders'
    case '0x8585ae03':
      return 'executeTakerBid'
    case '0xe72853e1':
      return 'executeTakerAsk'
    case '0x7034d120':
      return 'takeBid'
    case '0xda815cb5':
      return 'takeBidSingle'
    case '0x3925c3c3':
      return 'takeAsk'
    case '0x70bce2d6':
      return 'takeAskSingle'
    case '0x336d8206':
      return 'takeAskSinglePool'
    case '0xb4e4b296':
      return 'matchAskWithTakerBidUsingETHAndWETH'
    case '0x3b6d032e':
      return 'matchBidWithTakerAsk'
    case '0xb3be57f8':
      return 'bulkExecute'
    case '0x9a1fc3a7':
      return 'execute'
    case '0x9a2b8115':
      return 'batchBuyWithETH'
    case '0x09ba153d':
      return 'batchBuyWithERC20s'
    case '0xd65ef8ed':
      return 'swap721(uint256 in, uint256 out)'
    case '0x32389b71':
      return 'bulkTransfer(tuple[] items, bytes32 conduitKey)'
    case '0x38e29209':
      return 'matchAskWithTakerBid(tuple takerBid, tuple makerAsk)'

    // Multi-Sig
    case '0x3f801f91':
      return 'proxyAssert(address dest, uint8 howToCall, bytes calldata)'

    // Pools
    case '0x7e067a60':
      return 'deposit(uint256[] amounts, uint256[] minAmounts, address referral)'
    case '0xf8f0e295':
      return 'execute(address wallet, address[] to, uint256[] value, bytes[] data)'
    case '0x0e47f8d3':
      return 'batchTransferERC721(address[] to, address[] registry, uint256[] id)'

    default:
      return fourBytes
  }
}

/**
 * The timeline view is mostly Events, but has some Labels between some events, to provide logical groupings between them
 */
interface TimelineEventNode {
  type: 'event'
  data: BlockchainEvent
}
interface TimelineLabelNode {
  type: 'label'
  data: string
}
export function generateTimelineNodes(events: BlockchainEvent[]) {
  // Insert labels into the timeline
  let timelineNodes: Array<TimelineEventNode | TimelineLabelNode> = []
  if (events.length > 0) {
    timelineNodes.push({
      type: 'event',
      data: events[0],
    })
    for (let i = 1; i < events.length; i++) {
      const previousEvent = events[i - 1]
      const curEvent = events[i]

      const previousDate = new Date(previousEvent.timestamp * 1000)
      const curDate = new Date(curEvent.timestamp * 1000)
      const previousYear = previousDate.getUTCFullYear()
      const curYear = curDate.getUTCFullYear()

      if (previousYear != curYear) {
        // These events span a year gap
        if (Number(previousYear) - Number(curYear) == 1) {
          timelineNodes.push({
            type: 'label',
            data: `Year ${curYear} became ${previousYear}...`,
          })
        } else {
          // More than a one-year gap
          const yearsDelta = (previousEvent.timestamp - curEvent.timestamp) / 60 / 60 / 24 / 365
          const yearsLabel = Math.floor(yearsDelta * 4) / 4
          timelineNodes.push({
            type: 'label',
            data: `${yearsLabel} years passed...`,
          })
        }
      }

      timelineNodes.push({
        type: 'event',
        data: curEvent,
      })
    }
  }
  return timelineNodes
}
