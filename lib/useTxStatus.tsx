'use client'
import LoadingIndicator from 'components/LoadingIndicator'
import WarningIndicator from 'components/WarningIndicator'
import { useState } from 'react'
import { Abi, BaseError, ContractFunctionArgs, ContractFunctionName } from 'viem'
import { waitForTransactionReceipt, writeContract, WriteContractParameters } from 'wagmi/actions'
import { config } from './wagmi-config'
import { switchToChain } from './util'

type TxStatus = 'start' | 'building' | 'authorizing' | 'confirming' | 'done' | 'error'
export default function useTxStatus() {
  const [status, setStatus] = useState<TxStatus>('start')
  const [message, setMessage] = useState<React.ReactNode>(null)
  let viewMessage: React.ReactNode
  switch (status) {
    case 'building':
      viewMessage = <LoadingIndicator message="Building transaction..." />
      break
    case 'authorizing':
      viewMessage = <LoadingIndicator message="Authorizing transaction..." />
      break
    case 'confirming':
      viewMessage = <LoadingIndicator message="Awaiting blockchain confirmation..." />
      break
    case 'error':
      viewMessage = <WarningIndicator message={message as string} />
      break
    case 'done':
      viewMessage = message
  }

  function setStatusAndMessage(status: TxStatus, msg?: React.ReactNode) {
    if (typeof msg != 'undefined') {
      setMessage(msg)
    }
    setStatus(status)
  }
  async function processTransaction<
    const abi extends Abi = Abi,
    functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
      abi,
      'nonpayable' | 'payable'
    >
  >(
    txConfig: WriteContractParameters<
      abi,
      functionName,
      ContractFunctionArgs<abi, 'nonpayable' | 'payable', functionName>,
      typeof config,
      (typeof config)['chains'][number]['id']
    >
  ) {
    if (txConfig.chainId) {
      setStatus('building')
      if (!(await switchToChain(txConfig.chainId))) {
        setStatusAndMessage('error', 'Wrong network chain')
        return
      }
    }
    setStatus('authorizing')
    let rs = writeContract(config, txConfig)
    try {
      // Send transaction to be signed
      let hash = await rs
      setStatus('confirming')
      // Await confirmation in the blockchain
      const receipt = await waitForTransactionReceipt(config, { hash })
      setStatus('done')
      return receipt
    } catch (err) {
      if (err instanceof BaseError) {
        console.error('Transaction failure', {
          name: err.name,
          shortMessage: err.shortMessage,
          cause: err.cause,
        })
        setStatusAndMessage('error', err.shortMessage)
      } else {
        console.error('Transaction unknown error', err)
        setStatusAndMessage('error', 'Transaction error')
      }
      return false
    }
  }
  return {
    status,
    viewMessage,
    setStatus: setStatusAndMessage,
    isProcessing: status == 'building' || status == 'authorizing' || status == 'confirming',
    processTransaction,
  }
}
