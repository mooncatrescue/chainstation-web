import { useQuery } from "@tanstack/react-query";
import { Listing } from "./reservoirData";
import { FIVE_MINUTES } from "./util";

/**
 * Hook for fetching all MoonCats that have an open Ask/Listing
 */
export default function useMoonCatListings() {
  return useQuery({
    queryKey: ['marketplace-asks'],
    queryFn: async (): Promise<Listing[]> => {
      const rs = await fetch(`/api/mooncat-listings`)
      if (!rs.ok) throw new Error('Failed to fetch listing data from back-end')
      return await rs.json()
    },
    staleTime: FIVE_MINUTES,
  })
}