import { MoonCatData } from './types'
import _rawTraits from 'lib/mooncat_traits.json'
const moonCatData = _rawTraits as MoonCatData[]

/**
 * Given a set of rescue orders or MoonCat hex IDs, get metadata about those MoonCats
 */
export default function getMoonCatData(moonCatIdentifiers: (number | bigint | string)[]): MoonCatData[] {
  return moonCatIdentifiers
    .map((id) => moonCatData.find((d) => d.rescueOrder == Number(id) || d.catId == id))
    .filter((d) => typeof d !== 'undefined')
}
