'use client'
import { Action } from './onchainActions'
import useLocalStorage from './useLocalStorage'
import { createContext, Dispatch } from 'react'
import { Optional } from './types'
import { Abi, ContractFunctionName } from 'viem'

// Local storage key for the user's actions queue
const ACTIONS_QUEUE_KEY = 'user-actions-queue'

// Action types for the actions queue reducer
interface UpdateAction<
  abi extends Abi = Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
> {
  type: 'UPDATE'
  payload: Optional<Action<abi, functionName>, 'id'>
}

interface DeleteAction {
  type: 'DELETE'
  payload: string
}

type ActionQueueAction<
  abi extends Abi = Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
> = UpdateAction<abi, functionName> | DeleteAction

/**
 * Context for the user's actions queue.
 *
 * This context provides the actions queue state and a dispatch function for updating the actions queue.
 * Individual components should use useContext(ActionQueueContext) to access the actions in the queue.
 */
export const ActionQueueContext = createContext<{
  actions: Action[]
  dispatch: Dispatch<ActionQueueAction>
}>({
  actions: [],
  dispatch: () => {
    console.error('Action Queue context not initialized properly!')
  },
})

/**
 * Reducer for the actions queue.
 *
 * The state for this reducer is a list of Actions that may belong to different addresses
 */
const actionQueueReducer = <
  const abi extends Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
>(
  state: Action[],
  action: ActionQueueAction<abi, functionName>
): Action[] => {
  switch (action.type) {
    case 'UPDATE': {
      const item = action.payload
      const nextVal = [...state]
      if (typeof item.id == 'undefined') {
        // New item to add
        item.id = crypto.getRandomValues(new Uint32Array(3)).join('')
        nextVal.unshift(item as Action)
      } else {
        // Update existing item
        const existingIndex = nextVal.findIndex((a) => a.id == item.id)
        if (existingIndex >= 0) {
          nextVal[existingIndex] = item as Action
        } else {
          // Cannot find this item; it's actually a new item to add
          nextVal.unshift(item as Action)
        }
      }
      return nextVal
    }
    case 'DELETE': {
      const existingIndex = state.findIndex((a) => a.id == action.payload)
      if (existingIndex < 0) {
        // No such item to delete
        return state
      }
      return state.filter((a) => a.id != action.payload)
    }
    default: {
      console.error('Unknown action', action)
      return state
    }
  }
}

/**
 * Provider for the actions queue.
 *
 * This provider manages the actions queue for all addresses that have connected to the app.
 * It uses local storage to persist the actions queue across sessions. It should be used at the root of the app only;
 * all other pages should use the ActionQueueContext to access the actions queue.
 */
const ActionQueueProvider = ({ children }: { children: React.ReactNode }) => {
  const [actions, setActions] = useLocalStorage<Action[]>(ACTIONS_QUEUE_KEY, [])

  // Custom dispatch function that ensures we only update actions for the currently-connected address
  function dispatch(action: ActionQueueAction) {
    setActions((curVal) => actionQueueReducer(curVal, action))
  }

  return <ActionQueueContext.Provider value={{ actions, dispatch }}>{children}</ActionQueueContext.Provider>
}

export default ActionQueueProvider
