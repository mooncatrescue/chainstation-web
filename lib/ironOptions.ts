import { SessionOptions } from 'iron-session'

const passwords: SessionOptions['password'] = {}
;['IRON_PASSWORD_ONE', 'IRON_PASSWORD_TWO', 'IRON_PASSWORD_THREE'].forEach((prop, index) => {
  if (typeof process.env[prop] != 'undefined' && process.env[prop] != '') {
    console.log(`Iron forging ${index}:${process.env[prop]![0]}`)
    passwords[index + 1] = process.env[prop]!
  }
})

const opts: SessionOptions = {
  cookieName: '__session',
  password: passwords,
  // secure: true should be used in production (HTTPS) but can't be used in development (HTTP)
  cookieOptions: {
    secure: process.env.NODE_ENV === 'production',
  },
}
export default opts
