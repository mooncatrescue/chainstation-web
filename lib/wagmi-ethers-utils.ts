'use client'
import { BrowserProvider, FallbackProvider, JsonRpcProvider, JsonRpcSigner } from 'ethers'
import { PublicClient, WalletClient, type HttpTransport } from 'viem'
import { useEffect, useState } from 'react'
import { usePublicClient, useWalletClient } from 'wagmi'

// Derived from https://gist.github.com/slavik0329/2e5b6fc31cb745b65d3d37f7cf1d7b36
// For use with EAS SDK: https://docs.attest.sh/docs/developer-tools/sdk-wagmi

/**
 * Take a PublicClient (wagmi) and convert it to a Provider (ethers)
 */
export function publicClientToProvider(publicClient: PublicClient) {
  const { chain, transport } = publicClient
  const network = {
    chainId: chain?.id,
    name: chain?.name,
    ensAddress: chain?.contracts?.ensRegistry?.address,
  }
  if (transport.type === 'fallback')
    return new FallbackProvider(
      (transport.transports as ReturnType<HttpTransport>[]).map(({ value }) => new JsonRpcProvider(value?.url, network))
    )
  return new JsonRpcProvider(transport.url, network)
}

/**
 * Take a WalletClient (wagmi) and convert it to a Signer (ethers)
 */
export function walletClientToSigner(walletClient: WalletClient) {
  const { account, chain, transport } = walletClient
  const network = {
    chainId: chain?.id,
    name: chain?.name,
    ensAddress: chain?.contracts?.ensRegistry?.address,
  }
  const provider = new BrowserProvider(transport, network)
  const signer = provider.getSigner(account?.address)

  return signer
}

/**
 * React hook to capture the connected wagmi client, and convert it for Ethers use (Signer)
 */
export function useSigner() {
  const { data: walletClient } = useWalletClient()
  const [signer, setSigner] = useState<JsonRpcSigner | undefined>(undefined)

  useEffect(() => {
    async function getSigner() {
      if (!walletClient) return
      setSigner((await walletClientToSigner(walletClient)) as any)
    }
    getSigner()
  }, [walletClient])
  return signer
}

/**
 * React hook to capture the application-level wagmi RPC connection, and convert it for Ethers use (Provider)
 */
export function useProvider(chainId: number = 1) {
  const publicClient = usePublicClient({ chainId })
  const [provider, setProvider] = useState<JsonRpcProvider | undefined>(undefined)

  useEffect(() => {
    async function getProvider() {
      if (!publicClient) return
      setProvider(publicClientToProvider(publicClient) as any)
    }
    getProvider()
  }, [publicClient])
  return provider
}
