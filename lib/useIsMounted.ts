'use client'
import { useState, useEffect } from 'react'

/**
 * Tracking whether the component has gone through its first render or not.
 * This implementation of this sort of logic is using a state variable for internal tracking,
 * which means it forces a second render to happen in components where it's used. This is needed because
 * wagmi caches some values and so in a browser session data may be pre-filled, while server-side rendering wouldn't,
 * so NextJS throws a hydration error. This is a workaround for that; should only be needed in wagmi-using components.
 * https://github.com/wagmi-dev/wagmi/issues/542#issuecomment-1144178142
 */
function useIsMounted() {
  const [mounted, setMounted] = useState(false) // Start as false
  useEffect(() => setMounted(true), []) // On first run, set to true (and therefore trigger a re-render)
  return mounted
}
export default useIsMounted
