import { CSSProperties, ChangeEventHandler } from 'react'
import { Address } from 'viem'

////////// API Data Generics //////////
export interface BlockchainMoment {
  txHash?: `0x${string}`
  blockHeight: number
  timestamp?: number
}
export interface BlockchainValue<T = any> {
  value: T
  modified: BlockchainMoment | null
  checked: BlockchainMoment | null
}

export interface DocListIndex<T = any> {
  count: number
  list: T[]
  modified?: number
}

////////// MoonCats //////////

type MoonCatClassification = 'genesis' | 'rescue'
type HueName =
  | 'white'
  | 'black'
  | 'red'
  | 'orange'
  | 'yellow'
  | 'chartreuse'
  | 'green'
  | 'teal'
  | 'cyan'
  | 'skyblue'
  | 'blue'
  | 'purple'
  | 'magenta'
  | 'fuchsia'
type PaleFilter = 'no' | 'yes'
type Expression = 'smiling' | 'grumpy' | 'pouting' | 'shy'
type Facing = 'right' | 'left'
type Pattern = 'pure' | 'tabby' | 'spotted' | 'tortie'
type Pose = 'standing' | 'sleeping' | 'pouncing' | 'stalking'
type RescueYear = 2017 | 2018 | 2019 | 2020 | 2021
type NameFilter = 'no' | 'yes' | 'valid' | 'invalid'

export type MoonCatViewPreference = 'accessorized' | 'mooncat' | 'face' | 'event'
export const isValidViewPreference = (view: string): view is MoonCatViewPreference =>
  ['accessorized', 'mooncat', 'face', 'event'].includes(view.toLowerCase())

// Structure of data used as a local cache
export interface MoonCatData {
  rescueOrder: number
  rescueYear: RescueYear
  catId: `0x${string}`
  genesis?: true
  hueInt: number
  hueName: HueName
  pale: boolean
  facing: Facing
  expression: Expression
  pattern: Pattern
  pose: Pose
  nameRaw?: string
  name?: string | true
  namedOrder?: number
  namedYear?: number
}

interface ContractDetails {
  tokenId: number
  description: string
  address: Address
  capabilities: string[]
}

// Structure of data returned by API server for /owner-profile `ownedMoonCats` results
export interface OwnedMoonCat {
  rescueOrder: number
  catId: string
  collection: {
    name: string
    address: string
  }
}

// Structure of data returned by API server
export interface MoonCatDetails {
  rescueIndex: number
  catId: string // 0x-prefixed Hex
  name: string | null
  isNamed: 'Yes' | 'No' | 'Invalid UTF8' | 'Redacted'
  expression: Expression
  pose: Pose
  pattern: Pattern
  pale: boolean
  isPale: boolean
  facing: Facing

  rescuedBy?: Address
  rescueYear: number

  isAcclimated: boolean
  owner?: Address
  lootprint: 'Claimed' | 'Lost'
  contract: ContractDetails
  accessories: AccessoryDetails[]

  classification: MoonCatClassification
  genesis: boolean
  genesisGroup?: number

  hue: string
  hueValue: number
  kInt: number
  kBin: string // Binary value
  glow: number[]

  litterId: string // Hexadecimal value
  litter: number[]
  litterSize: number
  onlyChild: boolean

  twinId: string // Hexadecimal value
  twinSet: number[]
  twinSetSize: number
  hasTwins: false

  mirrorId: string // Hexadecimal value
  mirrorSet: number[]
  hasMirrors: boolean
  mirrorSetSize: number

  cloneId: string // Hexadecimal value
  cloneSet: number[]
  cloneSetSize: number
  hasClones: boolean
}

////////// Others ////////

// https://stackoverflow.com/a/61108377/144756
export type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>

// Information from API server about Token Lists created by individual owners
export interface TokenListMeta {
  title: string
  tokenCount: number
  modified: number
  likesCount: number
  description?: string
}

// Structure of data returned by API server
export interface OwnerProfile {
  ownedMoonCats: OwnedMoonCat[]
  ownedMoments: { moonCat?: number; momentId: number }[]
  tokenLists: TokenListMeta[]
}

export interface MoonCatFilterSettings {
  rescueYear?: RescueYear
  classification?: MoonCatClassification
  hue?: HueName
  pale?: PaleFilter
  facing?: Facing
  expression?: Expression
  pattern?: Pattern
  pose?: Pose
  named?: NameFilter
  nameKeyword?: string
}

export interface MoonCatSummary {
  rescueOrder: number
  catId: `0x${string}`
  owner: Address
  name:
    | {
        isNamed: false
      }
    | { isNamed: true; value: string; rawValue: string }
  likesCount: number
  ownedAccessories: number
}

////////// Accessories //////////

export interface AccessoryData {
  id: number
}

// Structure of data returned by API server for accessory searches
export interface AccessorySummary {
  manager: Address
  isVerified: boolean
  availableSupply: number
  totalSales: number
  positionEnabled: Pose[]
  totalIncome: number
  metaByte: number
  price: number
  name: string
  description?: string
  width: number
  height: number
  id: string
  isEligibleListActive: boolean
  likesCount?: number
}

// Structure of data returned by API server for /accessory/traits
export interface AccessoryTraits {
  creator: Address
  created: BlockchainMoment
  positionsEnabled: Pose[]
  name: string
  nameRaw: `0x${string}`
  description?: string
  width: number
  height: number
  idat: `0x${string}`
  ownedBy: DocListIndex<{ catId: `0x${string}`; rescueOrder: number }>
  totalIncome: number
  totalSales: number
  isVerified: boolean
  metaByte: BlockchainValue<number>
  manager: BlockchainValue<Address>
  availableSupply: BlockchainValue<number>
  price: BlockchainValue<number>
  palettes: BlockchainValue<`0x${string}`[]>
  availablePalettes: number
  likesCount?: number
}

export interface AccessoryContractData {
  totalSupply: number
  availableSupply: number
  name: string
  manager: Address
  metabyte: number
  availablePalettes: number
  positions: readonly `0x${string}`[]
  availableForPurchase: boolean
  price: bigint
  eligibleList: `0x${string}`[]
}

// Structure of owned Accessory returned by API server
export interface AccessoryDetails {
  accessoryId: string // Note: integer expressed as string
  name: string
  displayName: string
  visible: boolean
}

export interface AccessoryFilterSettings {
  price_min?: string
  price_max?: string
  verified?: 'yes' | 'no' | 'any'
  for_sale?: 'yes' | 'no'
  available?: 'yes' | 'no'
  manager?: Address
  pose?: Pose
  eligible_list?: 'yes' | 'no'
  sort?: 'oldest' | 'newest' | 'name' | 'expensive' | 'cheap'
}

////////// Moments //////////
export interface MomentMeta {
  description: string
  external_url: string
  image: string
  name: string
  attributes: { trait_type: string; value: string }[]
}

export interface Moment {
  momentId: number
  startingTokenId: number
  issuance: number
  tokenURI: string
  eventDate: number
  meta: MomentMeta
  tx: {
    hash: `0x${string}`
    blockNumber: number
    timestamp: number
    from: Address
    to: Address
  }
  moonCats: number[]
}

export interface MomentFilterSettings {
  momentId?: number
}

////////// Web App //////////

/**
 * Define the structure of form fields to assemble into a form
 */

interface BaseFieldMeta {
  name: string
  type: string
  label: string
}

export interface SelectFieldMeta extends BaseFieldMeta {
  type: 'select'
  defaultLabel?: string
  options: Record<string, string>
}
export interface TextFieldMeta extends BaseFieldMeta {
  type: 'text'
}

export function isSelectFieldMeta(m: SelectFieldMeta | TextFieldMeta): m is SelectFieldMeta {
  return m.type == 'select' && typeof (m as SelectFieldMeta).options != 'undefined'
}
export interface FieldProps<T extends BaseFieldMeta, E extends HTMLElement> {
  meta: T
  style?: CSSProperties
  currentValue: any
  onChange: ChangeEventHandler<E>
}
