'use client'
import { useState } from 'react'

/**
 * Tracking the status of a remote call
 */
export type FetchStatus = 'start' | 'pending' | 'done' | 'error'
function useFetchStatus() {
  return useState<FetchStatus>('start')
}
export default useFetchStatus
