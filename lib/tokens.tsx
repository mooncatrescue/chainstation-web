import { Address, hexToNumber, pad, sha256, trim } from 'viem'
import {
  ACCLIMATOR_ADDRESS,
  API_SERVER_ROOT,
  IPFS_GATEWAY,
  MOMENTS_ADDRESS,
  MOONCAT_TRAITS_ARB,
  ZWS,
  bytes32ToString,
  getAllMoments,
} from './util'
import { AttestationDoc } from './eas'
import React from 'react'
import getMoonCatData from './getMoonCatData'

const allMoments = getAllMoments()

/**
 * A representation of an ERC721-ish token.
 * A simple state for representing a token that may or may not be part of the MoonCat ecosystem.
 */
export interface TokenMeta {
  collection: {
    chainId?: number
    address: Address
    label?: string
  }
  name?: string
  id: `0x${string}`
  imageSrc?: string
  link?: string
  extra?: Record<string, any>
}

/**
 * Given a token contract address and token ID, decide what human-friendly label, image, and link to use for that token
 */
export function getTokenMeta<T extends TokenMeta>(originalToken: T): T {
  const t = Object.assign({}, originalToken) // Don't mutate original parameter
  // Use collection address to determine if we know what sort of token that child token is
  switch (t.collection.address) {
    case ACCLIMATOR_ADDRESS:
      // Token is a MoonCat
      const rescueOrder = hexToNumber(t.id)
      t.collection.label = 'MoonCat'
      t.imageSrc = `${API_SERVER_ROOT}/image/${rescueOrder}?scale=3&padding=5&costumes=true`
      t.link = `/mooncats/${rescueOrder}`
      return t
    case MOONCAT_TRAITS_ARB:
      const hexId = pad(trim(t.id), { size: 5 })
      t.collection.label = 'MoonCat'
      t.imageSrc = `${API_SERVER_ROOT}/image/${hexId}?scale=3&padding=5&costumes=true`
      return t
    case MOMENTS_ADDRESS:
      // Token is a MoonCatMoment
      const tokenId = hexToNumber(t.id)
      let m = allMoments[Number(tokenId)]
      t.collection.label = `MoonCat${ZWS}Moment`
      t.imageSrc = m.meta.image.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/')
      t.link = `/moments/${m.moment}`
      return t
    default:
      // Token is an unknown token
      // Use a random anonymous image for this token
      const hash = sha256((t.collection.address + pad(t.id, { size: 32 })) as `0x${string}`, 'bytes')
      // Image is deterministic, based on collection and token IDs
      const num = (hash[0] % 3) + 1
      t.imageSrc = `/img/p${num}.png`
      return t
  }
}

/**
 * Given a list of token metadatas, search through it for MoonCats identified by their Hex ID and replace it with Rescue order
 */
export function parseTokenListMoonCats(tokens: UserListTokenMeta[]): UserListTokenMeta[] {
  const moonCatHexIds: Record<`0x${string}`, true> = {}
  // Find any MoonCats referenced in the list that are using Hex IDs instead of Rescue Orders
  for (const t of tokens) {
    if (t.collection.address == MOONCAT_TRAITS_ARB) {
      moonCatHexIds[pad(trim(t.id), { size: 5 })] = true
    }
  }
  const neededIds = Object.keys(moonCatHexIds)
  if (neededIds.length == 0) return tokens

  // Fetch metadata info about all the MoonCat Hex IDs found, to map to rescue orders
  const mcTraits: Record<string, bigint> = {}
  getMoonCatData(neededIds).forEach((mc) => {
    mcTraits[mc.catId] = BigInt(mc.rescueOrder)
  })

  return tokens.map((originalToken) => {
    const t = Object.assign({}, originalToken) // Don't mutate original parameter
    switch (t.collection.address) {
      case MOONCAT_TRAITS_ARB: {
        // Convert to standard MoonCat meta
        const hexId = pad(trim(t.id), { size: 5 })
        const rescueOrder = mcTraits[hexId]
        if (typeof rescueOrder == 'undefined') return t
        //t.id = numberToHex(rescueOrder)
        t.imageSrc = `${API_SERVER_ROOT}/image/${rescueOrder}?scale=3&padding=5&costumes=true`
        t.link = `/mooncats/${rescueOrder}`
        t.name = `#${rescueOrder}`
        return t
      }
      default:
        return t
    }
  })
}

/**
 * Given a token's metadata, determine how it should be displayed.
 * If the token has a collection label or token name specified, use those. Otherwise, fall back to using a hex representation of the address and id values
 */
export function tokenDisplayLabel<T extends TokenMeta>(t: T) {
  const collectionLabel =
    typeof t.collection.label == 'undefined' ? (
      <code title={t.collection.address}>{t.collection.address.substring(0, 10)}&hellip;</code>
    ) : (
      t.collection.label
    )
  let tokenName: React.ReactNode
  if (typeof t.name == 'undefined') {
    const tokenNum = BigInt(t.id)
    if (tokenNum < 50_000) {
      tokenName = '#' + tokenNum
    } else {
      const trimmedId = trim(t.id)
      if (trimmedId.length > 10) {
        tokenName = <code title={trimmedId}>{trimmedId.substring(0, 10)}&hellip;</code>
      } else {
        tokenName = <code>{trimmedId}</code>
      }
    }
  } else {
    tokenName = t.name
  }
  return [collectionLabel, tokenName]
}

/**
 * An ERC721-ish token that is a member of a User List Attestation
 */
export type UserListTokenMeta = TokenMeta & {
  attUID: `0x${string}`
  ownerAddress: string
  listTitle: string
}

/**
 * Parse a set of Attestations defining tokens for a User List
 */
export function parseTokenListAttestations(rawAtts: AttestationDoc[]): UserListTokenMeta[] {
  const parsedTokens: UserListTokenMeta[] = []

  rawAtts.forEach((att: AttestationDoc) => {
    att.data.tokenIds.value.forEach((tokenId: `0x${string}`) => {
      parsedTokens.push({
        ...getTokenMeta({
          collection: { chainId: Number(att.sig.domain.chainId), address: att.sig.message.recipient as Address },
          id: tokenId,
        } as TokenMeta),
        attUID: att.sig.uid as `0x${string}`,
        ownerAddress: att.signer,
        listTitle: bytes32ToString(att.data.name.value),
      })
    })
  })

  return parsedTokens
}
