import { Moment, MoonCatData, OwnerProfile } from 'lib/types'
import { readContract } from 'wagmi/actions'
import { API2_SERVER_ROOT, RESCUE_ADDRESS, interleave } from './util'
import Link from 'next/link'
import { Address, formatEther, parseAbi } from 'viem'
import { SCHEMAS } from './eas'

import momentsData from 'lib/moments_meta.json'
const moments = momentsData as Moment[]

import rawVoters from 'lib/hero_mooncats_eligible_voters.json'
import { config } from './wagmi-config'
const voters = rawVoters as Address[]

/**
 * Hue colors that are exactly in the middle of the range defined for that color name
 *
 * Each color range has two integers because hue values are truncated, so the degree
 * just below the exact value is also within one degree of that value.
 */
const trueHues: number[] = [
  359,
  0, // Red
  29,
  30, // Orange
  59,
  60, // Yellow
  89,
  90, // Chartreuse
  119,
  120, // Green
  149,
  150, // Teal
  179,
  180, // Cyan
  209,
  210, // SkyBlue
  239,
  240, // Blue
  269,
  270, // Purple
  299,
  300, // Magenta
  239,
  330, // Fuchsia
]

/**
 * Mapping for hue values that are on the edge of another color.
 *
 * The value of this mapping is the other color that it's close to.
 */
const edgeHues: Record<number, string> = {
  15: 'Orange', // is Red
  16: 'Red', // is Orange
  45: 'Yellow', // is Orange
  46: 'Orange', // is Yellow
  75: 'Chartreuse', // is Yellow
  76: 'Yellow', // is Chartreuse
  105: 'Green', // is Chartreuse
  106: 'Chartreuse', // is Green
  135: 'Teal', // is Green
  136: 'Green', // is Teal
  165: 'Cyan', // is Teal
  166: 'Teal', // is Cyan
  195: 'SkyBlue', // is Cyan
  196: 'Cyan', // is SkyBlue
  225: 'Blue', // is SkyBlue
  226: 'SkyBlue', // is Blue
  255: 'Purple', // is Blue
  256: 'Blue', // is Purple
  285: 'Magenta', // is Purple
  286: 'Purple', // is Magenta
  315: 'Fuchsia', // is Magenta
  316: 'Magenta', // is Fuchsia
  345: 'Red', // is Fuchsia
  346: 'Fuchsia', // is Red
}

/**
 * MoonCats who have a MoonCatPop flavor with them as the SpokesCat
 */
const moonCatPopSpokesCats: Record<number, { id: number; flavor: string }> = {
  0: { id: 0, flavor: 'Station Crasher' },
  2723: { id: 1, flavor: 'ZomBerry' },
  10484: { id: 2, flavor: 'Dapper' },
  3294: { id: 3, flavor: 'Pure 2017 Kitty Juice' },
  1756: {
    id: 4,
    flavor: 'Gm              Milk of Purring             Gn',
  },
  12693: { id: 5, flavor: '3 Star Garf' },
  1337: { id: 6, flavor: 'Moon-Mate' },
  2165: { id: 7, flavor: "Cosmonaut's Cold Ones" },
  1039: { id: 8, flavor: 'Garfield sips' },
  476: { id: 9, flavor: "Day1 Alien's Favorite" },
  217: { id: 10, flavor: 'Black as Midnight on a Moonless Night' },
  1117: { id: 11, flavor: 'White Genesis Pop' },
  15401: { id: 12, flavor: 'Kuipurr Belt Kool' },
  5766: { id: 13, flavor: 'Glamour Puss' },
  20451: { id: 14, flavor: 'Parallax Alien' },
  24673: { id: 15, flavor: 'Six-Nipped Golden Kitten Attack Ale  ' },
  2726: { id: 16, flavor: 'Stellar Starberry' },
  24704: { id: 17, flavor: 'Milky Whey' },
  527: { id: 18, flavor: 'The Invisible Drink' },
  39: { id: 19, flavor: "Whiskers' Astro Ginger Magic Ice" },
  18074: { id: 20, flavor: 'Funky Fruit Fizz' },
  1289: { id: 21, flavor: 'Midnight Blue Raspberry Stardust' },
  1069: { id: 22, flavor: 'Meowtain Purrfect Pawnch' },
  6939: { id: 23, flavor: 'Catty Refresher' },
  14949: { id: 24, flavor: "Alien's Tripel Speciale" },
  6: { id: 25, flavor: "mister moo's vegas vacation" },
  9: {
    id: 26,
    flavor: '.                  #9 Dream                   Gn',
  },
  19507: { id: 27, flavor: "Mather's Sour Pucker" },
  41: { id: 28, flavor: 'Coke Royal Mint Nr.41 by CRL' },
  1514: { id: 29, flavor: 'Hydra-tion Station' },
  1218: { id: 30, flavor: 'Tropical Titan + Saturn Splash' },
  13358: { id: 31, flavor: "80's Elixir" },
  23677: { id: 32, flavor: 'Catsymalist Fizzy Sparkle' },
  2665: { id: 33, flavor: 'WAGMI' },
  75: { id: 34, flavor: "Blueberry's Royal Elixir" },
  3138: { id: 35, flavor: 'Midnight Meowjito' },
  24234: { id: 36, flavor: 'TokenAde' },
  1070: { id: 37, flavor: 'Ammonia Blast' },
  10: { id: 38, flavor: "Collector's Edition" },
  18189: { id: 39, flavor: 'Pixel Star' },
  13349: { id: 40, flavor: 'Panther Milk' },
  1264: { id: 41, flavor: 'buff cat' },
  302: { id: 42, flavor: 'Day 1' },
  19604: { id: 43, flavor: 'Triple Pixel Punch' },
  17970: { id: 44, flavor: "Paws' Bubbly Refresher" },
  453: { id: 45, flavor: 'Blueberry Bantha Milk' },
  3632: { id: 46, flavor: 'Paw-Brewed Pixel' },
  5670: { id: 47, flavor: 'StellaCat Soda' },
  9665: { id: 48, flavor: "Garfield's Lasagna - Nutrient Shake" },
  283: { id: 49, flavor: 'Lion Malt' },
  1382: { id: 50, flavor: "Frida's Feline Fresca" },
  6755: { id: 51, flavor: 'Who Loves Orange Soda?!' },
  13872: { id: 52, flavor: 'CopyCat Cola' },
  19373: { id: 53, flavor: 'Gutter Juice Cola' },
  20800: { id: 54, flavor: 'Tuna Moon Juice' },
  403: { id: 55, flavor: 'Day One Daiquiri' },
  3204: { id: 56, flavor: '2017 Fizzy Bubbly' },
  86: { id: 57, flavor: 'GENESIS KAIJU PURE BLACK' },
  1684: { id: 58, flavor: 'Void Slurp' },
  12: { id: 59, flavor: 'Thirst Drink Panther' },
  22313: { id: 60, flavor: 'Cherry Mecha' },
  22: { id: 61, flavor: "Doc Wander's Patent Ponderade" },
  3137: { id: 62, flavor: 'Meowberry Purrfection' },
  8125: { id: 63, flavor: 'probably nothing serum' },
  899: { id: 64, flavor: 'formula 899' },
  7500: { id: 65, flavor: 'gm' },
  13675: { id: 66, flavor: 'Milky Way Juice' },
  14737: { id: 67, flavor: 'WizardX style' },
  419: { id: 68, flavor: 'MeowTwo #419' },
  24335: { id: 69, flavor: 'Diet Scratching Post Punch' },
  1829: { id: 70, flavor: 'Magic Bus Brew' },
  13097: { id: 71, flavor: 'Sedating Refresher' },
  2710: { id: 72, flavor: 'Red Moon Dew' },
  7265: { id: 73, flavor: 'Popsi Purrfect' },
  526: { id: 74, flavor: 'Pure Genesis MoonCat #526' },
  19533: { id: 75, flavor: 'Machine Cat' },
  635: { id: 76, flavor: 'inversebrah' },
  6612: { id: 77, flavor: 'Moon Rum' },
  3028: { id: 78, flavor: 'Fuchsia Meow-garita' },
  2807: { id: 79, flavor: "Orangebeard's Delight" },
  4723: { id: 80, flavor: '- Drink Me -' },
  15508: { id: 81, flavor: "Salem's Soiree Seltzer" },
  11120: { id: 82, flavor: "Kepler's Supurrnova" },
  11676: { id: 83, flavor: 'Paw-Brewed Triple-Nipped Cougar Pop' },
}

/**
 * MoonCats whose owner bought a MoonCat plushie in the first merch offering
 */
const firstPlushieMoonCats = [
  1289, 444, 4444, 17970, 1378, 39, 1765, 2648, 2653, 6100, 6130, 6910, 7142, 7289, 7774, 8529, 8554, 8582, 8671, 8720,
  9337, 9442, 9863, 9864, 9918, 9983, 10244, 10959, 11028, 11170, 11544, 11683, 11716, 12202, 13237, 13320, 13859,
  14185, 15309, 15526, 16231, 16631, 17423, 17543, 17727, 18000, 18125, 18330, 18776, 18864, 19866, 20399, 20490, 20926,
  21234, 21340, 21776, 22521, 22531, 22733, 22843, 23948, 24440, 24616, 24621, 25005, 11519, 22841, 23020, 25184, 1586,
  6586, 8677, 10168, 13940, 15153, 24129, 24779, 2882, 13280, 23448, 24604, 6567, 13274, 15150, 3617, 6759,
]

/**
 * If a MoonCat was rescued early in the project's timeline, give a label to how early they were
 */
function earlyRescue(mc: MoonCatData) {
  if (mc.rescueOrder <= 491) {
    return 'First Day'
  } else if (mc.rescueOrder <= 1568) {
    return 'First Week'
  }
  return false
}

/**
 * If a MoonCat was the first MoonCat rescued in a given calendar year, return that year
 */
function yearStart(mc: MoonCatData) {
  switch (mc.rescueOrder) {
    case 0:
      return '2017'
    case 3365:
      return '2018'
    case 5684:
      return '2019'
    case 5755:
      return '2020'
    case 5758:
      return '2021'
  }
  return false
}

/**
 * If a MoonCat was the last MoonCat rescued in a given calendar year, return that year
 */
function yearEnd(mc: MoonCatData) {
  switch (mc.rescueOrder) {
    case 3364:
      return '2017'
    case 5683:
      return '2018'
    case 5754:
      return '2019'
    case 5757:
      return '2020'
    case 25439:
      return '2021'
  }
  return false
}

/**
 * Parse a MoonCat's metadata and show minor, interesting things about it
 */
export function getMoonCatTidbits(moonCat: MoonCatData): Record<string, boolean | React.ReactNode> {
  let bytes = []
  for (let i = 2; i < moonCat.catId.length; i += 2) {
    bytes.push(parseInt(moonCat.catId.slice(i, i + 2), 16))
  }

  const hexRepeats = moonCat.catId.slice(2).match(/([a-f0-9])\1{2,}/gi)
  const glowSum = bytes[2] + bytes[3] + bytes[4]

  let inMoments = []
  for (let moment of moments) {
    if (moment.moonCats.includes(moonCat.rescueOrder)) {
      inMoments.push(moment.momentId)
    }
  }
  let momentDetail: React.ReactNode = false
  if (inMoments.length == 1) {
    momentDetail = <Link href={'/moments/' + inMoments[0]}>{'Moment ' + inMoments[0]}</Link>
  } else if (inMoments.length > 1) {
    momentDetail = (
      <>
        Moments{' '}
        {interleave(
          inMoments.map((id) => (
            <Link key={id} href={'/moments/' + id}>
              {id}
            </Link>
          )),
          ', '
        )}
      </>
    )
  }

  const pop = moonCatPopSpokesCats[moonCat.rescueOrder]
  const spokesCat =
    typeof pop != 'undefined' ? (
      <a href={'https://pop.mooncat.community/vm/' + pop.id} rel="noreferrer" target="_blank">
        {pop.flavor}
      </a>
    ) : (
      false
    )

  return {
    numericId: /^[0-9]+$/.test(moonCat.catId.slice(2)),
    alphaId: /^[a-f]+$/i.test(moonCat.catId.slice(4)),
    repeatId: hexRepeats == null ? false : hexRepeats.join(', '),
    earlyRescue: earlyRescue(moonCat),
    yearStart: yearStart(moonCat),
    yearEnd: yearEnd(moonCat),
    earlyNaming: typeof moonCat.namedOrder != 'undefined' && moonCat.namedOrder <= 368,
    trueColor: trueHues.includes(moonCat.hueInt),
    edgeColor: typeof edgeHues[moonCat.hueInt] == 'undefined' ? false : edgeHues[moonCat.hueInt],
    darkGlow: glowSum < 180 ? `${glowSum}/765 brightness` : false,
    brightGlow: glowSum > 585 ? `${glowSum}/765 brightness` : false,
    inMoments: momentDetail,
    spokesCat: spokesCat,
    firstPlushieMoonCat: firstPlushieMoonCats.includes(moonCat.rescueOrder),
  }
}

/**
 * Ethereum addresses who represent people who made a purchase in the first MoonCat merch plushie offering
 */
const firstPlushieParticipants = [
  '0x5dEA60eEF2bFCEF3a808a219B562145D1b80bd7D', // midnight.eth
  '0x8B0cBF0Aa84B9Dc05B9385b5D249c487aC38EA03', // pawsrpg.eth
  '0x9fF612c15b52D437924c0c26a52Ed3dD23C962f1',
  '0xE581B4CbE18172d4Ed99e68F9e126a91e09AB304', // cold.0xdamn.eth
  '0x9C6485538993306a2Ba3Be95a52237D9DdF1B6a9',
  '0xC8F3A121d068d85d91c5CEE5e714ce5892BCb7BC', // tarzanturk.eth
  '0x84c74863C2Ff8c529d1651924cB41D13ae3921C3', // zerogvault.eth
  '0xaCf4C2950107eF9b1C37faA1F9a866C8F0da88b9', // JamesCarnley.eth
  '0x0F73622476E604d34dDF24ef4D709968a264A259', // loltapes.eth
  '0x793274608864CaF6F48E3c13201F35C45Fed96a5', // 3ank.eth
  '0x42c836641A7A4B620aF65D5FBeb3d35C612947C0', // robdoteth.eth
  '0x8665f7CeaDfbFF09E2cC572F7a6E43198D87fF88', // tweetious.eth
]

/**
 * Show minor, interesting things about an individual Ethereum address
 */
export async function getOwnerTidbits(
  address: Address,
  ownerMeta: OwnerProfile
): Promise<Record<string, React.ReactNode>> {
  const ownedMoonCatCount = ownerMeta.ownedMoonCats.length
  let ownedMoonCats
  if (ownedMoonCatCount > 0) {
    const label = ownedMoonCatCount == 1 ? 'MoonCat' : 'MoonCats'
    ownedMoonCats = ownedMoonCatCount + ' ' + label
  }

  let eligibleVoter = voters.indexOf(address.toLowerCase() as Address)
  let heroVoter
  if (eligibleVoter >= 0) {
    // Owner was eligible to vote; check and see if they did
    const rs = await readContract(config, {
      address: '0x1916F482BB9F3523a489791Ae3d6e052b362C777',
      chainId: 1,
      abi: parseAbi(['function hasVoted(address owner) external view returns (bool)']),
      functionName: 'hasVoted',
      args: [address],
    })
    heroVoter = rs ? 'voted' : 'abstained'
  } else {
    heroVoter = false
  }

  const MAX_GMS = 100
  const gmAtts = await fetch(
    `${API2_SERVER_ROOT}/attestations?${new URLSearchParams({
      schema: `arb:${SCHEMAS.GM}`,
      sender: `arb:${address}`,
      limit: String(MAX_GMS),
    })}`
  )
  let gmCount: string | false = false
  if (gmAtts.ok) {
    const rawAtts = await gmAtts.json()
    if (rawAtts.length == 1) {
      gmCount = '1 GM'
    } else if (rawAtts.length >= MAX_GMS) {
      gmCount = MAX_GMS + '+ GMs'
    } else if (rawAtts.length > 0) {
      gmCount = rawAtts.length + ' GMs'
    }
  }

  const pendingWithdrawals = await readContract(config, {
    address: RESCUE_ADDRESS,
    chainId: 1,
    abi: parseAbi(['function pendingWithdrawals(address owner) external view returns (uint256)']),
    functionName: 'pendingWithdrawals',
    args: [address],
  })
  const adoptionCenterWithdrawal = pendingWithdrawals > 0 ? formatEther(pendingWithdrawals) : false

  return {
    moonCats: ownedMoonCats,
    firstPlushieParticipants: firstPlushieParticipants.indexOf(address) >= 0,
    heroVoter,
    adoptionCenterWithdrawal,
    gmCount,
  }
}
