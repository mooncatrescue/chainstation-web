import { Address } from "viem"

export interface ReservoirAsk {
  id: `0x${string}`
  kind: string
  maker: Address
  price: {
    currency: {
      contract: Address,
      name: string
      symbol: string
      decimals: number
    },
    amount: {
      raw: string
      decimal: number
    }
  }
  validFrom: number
  validUntil: number
  criteria: {
    kind: 'token',
    data: {
      token: {
        tokenId: string
      }
    }
  },
  source: {
    domain: string
    name: string
    icon: string
    url: string
  }
}

export interface Listing {
  moonCat: number
  price: ReservoirAsk['price']
  source: ReservoirAsk['source']
}

/**
 * Fetch Marketplace data from Reservoir
 * 
 * https://docs.reservoir.tools/reference/getordersasksv5
 * Created as a separate async function so NextJS handles the caching of this query. Set to re-cache the data every 15 minutes.
 */
export const getReservoirAsks = async function (): Promise<{ orders: Listing[], continuation?: string }> {
  const API_KEY = process.env['RESERVOIR_API_KEY']
  if (typeof API_KEY == 'undefined' || API_KEY == '') {
    throw new Error('No Reservoir API key set!')
  }
  const rs = await fetch(
    `https://api.reservoir.tools/orders/asks/v5?${new URLSearchParams({
      contracts: '0xc3f733ca98e0dad0386979eb96fb1722a1a05e69',
      limit: '1000'
    })}`,
    {
      method: 'GET',
      headers: { accept: '*/*', 'x-api-key': API_KEY },
      next: { revalidate: 60 }
    }
  )
  if (!rs.ok) throw new Error('Failed to fetch from Reservoir: ' + rs.statusText)
  const data = await rs.json()

  data.orders = data.orders.map((o: ReservoirAsk): Listing => {
    if (o.source.name == 'NFTX') {
      // Fix Reservoir "source" for NFTX (missing URL)
      switch (o.maker) {
        case '0x98968f0747e0a261532cacc0be296375f5c08398':
        case '0xa8b42c82a628dc43c2c2285205313e5106ea2853':
          // NFTX v2 vault
          o.source.url = `https://v2.nftx.io/vault/0x98968f0747e0a261532cacc0be296375f5c08398/${o.criteria.data.token.tokenId}/`
          break;
        default:
          // NFTX v3 vault
          o.source.url = 'https://v3.nftx.io/eth/collections/acclimatedmooncats/buy'
      }
    }
    return {
      moonCat: Number(o.criteria.data.token.tokenId),
      price: o.price,
      source: o.source
    }
  })
  return data
}
