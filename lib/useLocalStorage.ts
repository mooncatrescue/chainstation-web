'use client'
import { useState, useEffect } from 'react'
import { deserializeBigints, serializeBigints } from './util'

/**
 * Custom hook for binding a React state value to localStorage
 *
 * The logic for this hook has the React state as primary. There's a
 * useEffect listener that fires whenever the React state value changes,
 * and decides if then the localStorage value needs to be updated.
 *
 * This hook listens for window "storage" events, which allows this hook to
 * keep the React state in-sync across multiple tabs.
 */
function useLocalStorage<T>(key: string, initialValue: T) {
  const [storedValue, setStoredValue] = useState<T>(() => {
    if (typeof window === 'undefined') return initialValue
    try {
      const item = window.localStorage.getItem(key)
      return item == null ? initialValue : JSON.parse(item, deserializeBigints)
    } catch (error) {
      console.error(error)
      return initialValue
    }
  })

  // Whenever the React state value changes, update the localStorage version
  useEffect(() => {
    if (typeof window === 'undefined') return
    const toSave = JSON.stringify(storedValue, serializeBigints)
    const existingSaved = window.localStorage.getItem(key)
    if (existingSaved == toSave) return
    window.localStorage.setItem(key, toSave)
  }, [storedValue, key])

  // Listen to LocalStorage events from other tabs
  useEffect(() => {
    if (typeof window === 'undefined') return
    let ignore = false
    const handleStorageChange = (e: StorageEvent) => {
      if (ignore || e.key !== key) return
      console.debug('Heard storage event for key', key, e.newValue)
      setStoredValue((curVal) => {
        if (e.newValue == null) {
          // Update value to be initial value
          return curVal == initialValue ? curVal : initialValue
        } else {
          // Update value to be the new value
          try {
            const newValue: T = JSON.parse(e.newValue, deserializeBigints)
            return curVal == newValue ? curVal : newValue
          } catch (error) {
            console.error(error)
            return curVal
          }
        }
      })
    }
    window.addEventListener('storage', handleStorageChange)
    return () => {
      ignore = true
      window.removeEventListener('storage', handleStorageChange)
    }
  }, [key, initialValue])

  return [storedValue, setStoredValue] as const
}

export default useLocalStorage
