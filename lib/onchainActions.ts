import { Abi, Address, ContractFunctionArgs, ContractFunctionName } from 'viem'
import { readContract, WriteContractParameters } from 'wagmi/actions'
import { AttestationDraft } from './eas'
import { config } from './wagmi-config'

export interface ContractActionStepBase<
  abi extends Abi = Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
> {
  type: 'CONTRACT'
  label: string
  config: WriteContractParameters<
    abi,
    functionName,
    ContractFunctionArgs<abi, 'nonpayable' | 'payable', functionName>,
    typeof config,
    (typeof config)['chains'][number]['id']
  >
  /**
   * Optional precheck function. If provided, it will be called, passing in an instance of the readContract function.
   * If the precheck function returns true, the step will be considered complete.
   * 
   * @example
   * precheck: async (rc) => {
   *   const allowance = await rc({
   *     address: TOKEN_ADDRESS,
   *     abi: erc20Abi,
   *     functionName: 'allowance',
   *     args: [userAddress, spenderAddress]
   *   })
   *   erturn allowance >= amountNeeded
   * }
   */
  precheck?: (rc: typeof readContract) => Promise<boolean>
  userAddress: Address
}

/**
 * A ContractStep that is not finalized.
 * If it has a `txPending` property, it has been submitted to the mempool, but is not yet on-chain.
 */
export interface ContractStepPending<
  abi extends Abi = Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
> extends ContractActionStepBase<abi, functionName> {
  txPending?: {
    hash: `0x${string}`
    timestamp: number
  }
}

/**
 * A ContractStep that was skipped because the precheck returned true.
 */
export interface ContractStepSkipped<
  abi extends Abi = Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
> extends ContractActionStepBase<abi, functionName> {
  skipped: true
}

/**
 * A ContractStep that was executed and is now on-chain.
 */
export interface ContractStepComplete<
  abi extends Abi = Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
> extends ContractActionStepBase<abi, functionName> {
  tx: {
    hash: `0x${string}`
    timestamp: number
  }
}

/**
 * Helper function for Contract Steps
 *
 * Typescript only infers generic types from arguments to functions, so in order to not be so verbose with types
 * when saving an object separately from using it in a function, this function can be used.
 *
 * See ADR0014 for further details.
 */
export function constructContractStep<
  const abi extends Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'>
>(step: ContractStepPending<abi, functionName>): ContractStepPending<abi, functionName> {
  return step
}

export interface AttestationStepPending {
  type: 'ATTEST'
  label: string
  msg: AttestationDraft
  chainId: number
  userAddress: Address
}
export interface AttestationStepComplete extends AttestationStepPending {
  uid: `0x${string}`
  timestamp: number
}

export interface RevokeAttestationsStepPending {
  type: 'REVOKE'
  label: string
  uids: `0x${string}`[]
  chainId: number
  userAddress: Address
}
export interface RevokeAttestationsStepComplete extends RevokeAttestationsStepPending {
  timestamp: number
}

export type ActionStep<
  abi extends Abi = Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
> =
  | ContractStepPending<abi, functionName>
  | ContractStepSkipped<abi, functionName>
  | ContractStepComplete<abi, functionName>
  | AttestationStepPending
  | AttestationStepComplete
  | RevokeAttestationsStepPending
  | RevokeAttestationsStepComplete

interface BaseAction<
  abi extends Abi = Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
> {
  id: string
  label: string
  fromAddress: Address
  steps: ActionStep<abi, functionName>[]
}
export interface ModifyUserListAction extends BaseAction {
  listTitle: string
}
export interface AcclimateAction<
  abi extends Abi = Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
> extends BaseAction<abi, functionName> {
  steps: (
    | ContractStepPending<abi, functionName>
    | ContractStepSkipped<abi, functionName>
    | ContractStepComplete<abi, functionName>
  )[]
}

export type Action<
  abi extends Abi = Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
> = BaseAction | ModifyUserListAction | AcclimateAction<abi, functionName>

export function actionIsComplete<
  abi extends Abi = Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
>(action: BaseAction<abi, functionName>) {
  return !action.steps.some((s) => !stepIsComplete<abi, functionName>(s))
}

export function stepIsComplete<
  const abi extends Abi = Abi,
  functionName extends ContractFunctionName<abi, 'nonpayable' | 'payable'> = ContractFunctionName<
    abi,
    'nonpayable' | 'payable'
  >
>(step: ActionStep<abi, functionName>) {
  switch (step.type) {
    case 'CONTRACT': {
      return 'tx' in step || 'skipped' in step
    }
    case 'ATTEST': {
      return 'uid' in step
    }
    case 'REVOKE': {
      return 'timestamp' in step
    }
    default: {
      throw new Error(`Unknown step type '${(step as any).type}'`)
    }
  }
}
