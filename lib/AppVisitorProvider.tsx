'use client'
import { createContext, Dispatch, Reducer, useEffect, useReducer } from 'react'
import { isValidViewPreference, MoonCatViewPreference } from './types'
import {
  AppGlobalState,
  AppGlobalAction,
  AppGlobalActionType,
  doUserCheck,
  LOCALSTORAGE_MOONCATVIEW_KEY,
  LOCALSTORAGE_VIEWCHANGE_KEY,
} from './util'
import { wagmiAdapter, walletConnectProject } from './wagmi-config'
import ShoppingCartProvider from './ShoppingCartProvider'
import ActionQueueProvider from './ActionsQueueProvider'
import { usePathname, useSearchParams } from 'next/navigation'
import { pageview } from './analytics'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { WagmiProvider } from 'wagmi'
import { createAppKit } from '@reown/appkit/react'
import { arbitrum, mainnet } from '@reown/appkit/networks'
import Navigation from 'components/Navigation'
import { AppEventType, emit } from './useGlobalEvents'
import Footer from 'components/Footer'
import MoonCatCharacters from 'components/MoonCatCharacters'
import BackgroundStars from 'components/BackgroundStars'
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'

/**
 * Global context for the visitor within the application
 */
export const AppVisitorContext = createContext<{
  state: AppGlobalState
  dispatch: Dispatch<AppGlobalAction>
}>({
  state: {
    verifiedAddresses: {
      status: 'start',
      value: [],
    },
    viewPreference: 'accessorized',
    awokenMoonCats: new Set<number>(),
  },
  dispatch: function () {
    console.error('Dispatcher called before initialized!')
  },
})

function getViewPreference(): MoonCatViewPreference {
  if (typeof window == 'undefined') return 'accessorized'
  const params = new URLSearchParams(window.location.search)

  // Set MoonCat view style preference
  const queryPreference = params.get('view')
  if (queryPreference !== null && isValidViewPreference(queryPreference)) {
    // Query parameter takes priority
    return queryPreference as MoonCatViewPreference
  }

  // Check local storage
  let savedPreference = localStorage.getItem(LOCALSTORAGE_MOONCATVIEW_KEY) as MoonCatViewPreference | null
  if (savedPreference != null) {
    console.debug('Restoring saved view preference:', savedPreference)
    return savedPreference
  }

  console.debug('Setting default view preference')
  return 'accessorized'
}

/**
 * Application global state management reducer function
 */
const appReducer: Reducer<AppGlobalState, AppGlobalAction> = function (state, action) {
  const { type, payload } = action
  switch (type) {
    case AppGlobalActionType.SET_VIEW_PREFERENCE: {
      // User updated their preference for what view of MoonCats to show
      if (payload != null) {
        // Save to local storage, to make this value persistent
        localStorage.setItem(LOCALSTORAGE_MOONCATVIEW_KEY, payload)
        localStorage.setItem(LOCALSTORAGE_VIEWCHANGE_KEY, new Date().getTime().toString())
      }
      return {
        ...state,
        viewPreference: payload,
      }
    }
    case AppGlobalActionType.UPDATE_VERIFIED_ADDRESSES: {
      // List of addresses the visitor has proven ownership of has changed
      return {
        ...state,
        verifiedAddresses: payload,
      }
    }
    case AppGlobalActionType.AWAKEN: {
      // A MoonCat has come to life!
      const newSet = new Set(state.awokenMoonCats)
      newSet.add(payload)
      return {
        ...state,
        awokenMoonCats: newSet,
      }
    }
    default: {
      console.warn('Unknown reducer action', action)
      return state
    }
  }
}

const projectMeta = {
  name: 'MoonCatRescue ChainStation',
  description: 'Playground of the colorful felines rescued from the moon',
  url: 'https://chainstation.mooncatrescue.com',
  icons: ['https://chainstation.mooncatrescue.com/img/avatar.jpg'],
}

// Create the modal
const modal = createAppKit({
  adapters: [wagmiAdapter],
  projectId: walletConnectProject,
  networks: [mainnet, arbitrum],
  defaultNetwork: mainnet,
  metadata: projectMeta,
  features: {
    analytics: true, // Optional - defaults to your Cloud configuration
  },
})

// Set up queryClient
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      // With SSR, we usually want to set some default staleTime
      // above 0 to avoid refetching immediately on the client
      staleTime: 60 * 1000,
    },
  },
})

const AppVisitorProvider = ({ children }: { children: React.ReactNode }) => {
  const pathname = usePathname()
  const searchParams = useSearchParams()

  const [state, dispatch] = useReducer(
    appReducer,
    null,
    (): AppGlobalState => ({
      verifiedAddresses: {
        status: 'start',
        value: [],
      },
      viewPreference: getViewPreference(),
      awokenMoonCats: new Set<number>(),
    })
  )
  // On first mount, check and see if user is logged-in
  useEffect(() => {
    if (state.verifiedAddresses.status !== 'start') return
    doUserCheck(dispatch)
  }, [state.verifiedAddresses.status])

  // When the visitor navigates to a new screen, log it as a pageview
  useEffect(() => {
    pageview()
  }, [pathname, searchParams])

  return (
    <WagmiProvider config={wagmiAdapter.wagmiConfig}>
      <QueryClientProvider client={queryClient}>
        <AppVisitorContext.Provider value={{ state, dispatch }}>
          <ShoppingCartProvider>
            <ActionQueueProvider>
              <Navigation />
              <div
                id="content"
                onClick={(e) => {
                  emit({ type: AppEventType.CLOSE_DIALOGS, payload: null })
                }}
              >
                {children}
                <Footer />
              </div>
              <MoonCatCharacters />
              <BackgroundStars />
            </ActionQueueProvider>
          </ShoppingCartProvider>
        </AppVisitorContext.Provider>
        <ReactQueryDevtools initialIsOpen={false} />
      </QueryClientProvider>
    </WagmiProvider>
  )
}

export default AppVisitorProvider
