'use client'
import { useRef, useState, useEffect } from 'react'

/**
 * Custom hook for handling HTML Dialog element from within React state context
 */
function useDialog(isOpenInitially: boolean = false) {
  const ref = useRef<HTMLDialogElement>(null)
  const [isOpen, setIsOpen] = useState<boolean>(isOpenInitially)

  useEffect(() => {
    if (isOpen) {
      ref.current?.showModal()
    } else {
      ref.current?.close()
    }
  }, [ref, isOpen])

  return {
    ref,
    setIsOpen,
  }
}
export default useDialog
