'use client'
import { useEffect } from "react"

export enum AppEventType {
  CLOSE_DIALOGS = 'Everybody scram!',
  AWAKEN = 'Here kitty, kitty, kitty!'
}

interface AppEventBase<T = unknown> {
  type: AppEventType,
  payload: T
}

export interface CloseDialogsEvent extends AppEventBase<null> {
  type: AppEventType.CLOSE_DIALOGS
}
export interface AwakenEvent extends AppEventBase<{
  rescueOrder: number,
  x?: number,
  y?: number
}> {
  type: AppEventType.AWAKEN,
}

type AppEvent = CloseDialogsEvent | AwakenEvent
export type AppEventHandler = (payload: AppEvent) => void

const eventListeners = new Set<AppEventHandler>()

export function emit<T extends AppEvent>(payload: T) {
  eventListeners.forEach(l => l(payload))
}

export function useGlobalEvents(handler: AppEventHandler) {
  useEffect(() => {
    eventListeners.add(handler)
    return () => {
      eventListeners.delete(handler)
    }
  }, [handler])
}
