'use client'
import { CartItem } from './util'
import { Address } from 'viem'
import useLocalStorage from './useLocalStorage'
import { createContext, Dispatch, Reducer } from 'react'
import { useAccount } from 'wagmi'
import { Optional } from './types'

// Local storage key for shopping cart data
const PREFERENCE_CART_KEY = 'cart'

// Action types for the cart reducer
interface UpdateAction<T extends CartItem> {
  type: 'UPDATE'
  payload: Optional<T, 'id'>
}
interface DeleteAction {
  type: 'DELETE'
  payload: string
}
interface ClearAction {
  type: 'CLEAR'
}
type Action<T extends CartItem = CartItem> = UpdateAction<T> | DeleteAction | ClearAction

/**
 * Context for the user's pending shopping cart.
 *
 * This context provides the cart state and a dispatch function for updating the cart.
 * It is used to manage the cart state for the currently-connected address. Individual components should use useContext(CartContext)
 * to access the items in the cart.
 */
export const CartContext = createContext<{
  cart: CartItem[]
  dispatch: Dispatch<Action>
}>({
  cart: [],
  dispatch: () => {
    console.error('Shopping Cart context not initialized properly!')
  },
})

/**
 * Reducer for a single address' cart state.
 *
 * This function operates on one address' cart state, while the ShoppingCartProvider
 * is responsible for operating on the cart state for all addresses.
 */
const cartReducer: Reducer<CartItem[], Action> = (state, action) => {
  switch (action.type) {
    case 'UPDATE': {
      // Add or edit a cart item.
      // If the item's ID is undefined, then it's a new item to add. Otherwise it's an item to update
      const item = action.payload
      if (typeof item.id == 'undefined') {
        // New item to add
        item.id = crypto.getRandomValues(new Uint32Array(3)).join('')
        return [...state, item as CartItem]
      } else {
        // Update existing item
        const existingIndex = state.findIndex((i) => i.id == item.id)
        if (existingIndex >= 0) {
          const newCart = [...state]
          newCart[existingIndex] = item as CartItem
          return newCart
        } else {
          // Cannot find this item; it's actually a new item to add
          return [...state, item as CartItem]
        }
      }
    }
    case 'DELETE': {
      // Remove the cart item who's ID is equal to the action payload
      const existingIndex = state.findIndex((i) => i.id == action.payload)
      if (existingIndex < 0) {
        // No such item in the cart
        return state
      }
      return state.filter((i) => i.id != action.payload)
    }
    case 'CLEAR': {
      // Completely empty this user's cart
      return []
    }
    default: {
      console.error('Unknown action', action)
      return state
    }
  }
}

/**
 * Provider for the shopping cart.
 *
 * This provider manages the cart state for all addresses that have connected to the app.
 * It uses local storage to persist the cart state across sessions. It should be used at the root of the app only;
 * all other pages should use the CartContext to access the cart state.
 */
const ShoppingCartProvider = ({ children }: { children: React.ReactNode }) => {
  const { address } = useAccount()

  // In local storage, we store shopping cart data for all addresses that have connected to this app
  const [rawCart, setRawCart] = useLocalStorage<Record<Address, CartItem[]>>(PREFERENCE_CART_KEY, {})

  // Custom dispatch function that ensures we only update the cart for the currently-connected address
  function dispatch(action: Action) {
    setRawCart((curVal) => {
      if (typeof address == 'undefined') {
        console.error('No address connected; cannot update cart')
        return curVal
      }
      return {
        ...curVal,
        [address]: cartReducer(curVal[address] ?? [], action),
      }
    })
  }

  const cart = typeof address == 'undefined' || typeof rawCart[address] == 'undefined' ? [] : rawCart[address]
  return <CartContext.Provider value={{ cart, dispatch }}>{children}</CartContext.Provider>
}
export default ShoppingCartProvider
