import { useContext } from 'react'
import { AppGlobalState, SIWE_LOGIN_STATEMENT, doUserCheck } from './util'
import { createSiweMessage } from 'viem/siwe'
import { useAccount, useSignMessage } from 'wagmi'
import useFetchStatus, { FetchStatus } from './useFetchStatus'
import { AppVisitorContext } from './AppVisitorProvider'
import { Address } from 'viem'

interface Response {
  verifiedAddresses: AppGlobalState['verifiedAddresses']['value']
  status: FetchStatus // Doing the process of signing in
  verificationStatus: FetchStatus // Doing the process of fetching
  doSignIn: () => Promise<boolean>
  doLogOut: (address?: Address) => Promise<boolean>
}

interface DisconnectedResponse extends Response {
  connectedAddress: undefined
  connectedChain: undefined
  isConnected: false
  isSignedIn: false
}
interface ConnectedResponse extends Response {
  connectedAddress: Address
  connectedChain: number
  isConnected: true
  isSignedIn: false
}

interface VerifiedResponse extends Response {
  connectedAddress: Address
  connectedChain: number
  isConnected: true
  isSignedIn: true
}

export default function useSignedIn(): DisconnectedResponse | ConnectedResponse | VerifiedResponse {
  const { address, chain } = useAccount()
  const { signMessageAsync } = useSignMessage()
  const [status, setStatus] = useFetchStatus()
  const {
    state: {
      verifiedAddresses: { status: verificationStatus, value: verifiedAddresses },
    },
    dispatch,
  } = useContext(AppVisitorContext)

  async function doSignIn(): Promise<boolean> {
    setStatus('pending')
    if (!address) {
      console.error('Not connected')
      setStatus('error')
      return false
    }

    const chainId = chain?.id
    if (!chainId) {
      console.error('No network detected')
      setStatus('error')
      return false
    }

    const nonceRes = await fetch('/api/nonce')
    const nonce = await nonceRes.text()

    const expires = new Date()
    expires.setTime(expires.getTime() + 24 * 60 * 60 * 1000) // Shift forward 24 hours

    const message = createSiweMessage({
      domain: window.location.host,
      address,
      statement: SIWE_LOGIN_STATEMENT,
      uri: window.location.origin,
      version: '1',
      chainId,
      nonce: nonce,
      expirationTime: expires,
    })
    let signature
    try {
      signature = await signMessageAsync({ message })
    } catch (err: any) {
      setStatus('error')
      if (err.name == 'UserRejectedRequestError') {
        console.warn('User rejected signing')
        return false
      }
      console.error('Signing error', err)
      return false
    }

    // Verify signature
    const verifyRes = await fetch('/api/siwe-verify', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ message, signature }),
    })
    if (!verifyRes.ok) {
      setStatus('error')
      console.error('Fetch error', verifyRes)
      return false
    }
    let rs = await verifyRes.json()
    if (!rs.ok) {
      setStatus('error')
      console.error('Signature failed validation')
      return false
    }

    // Verification succeeded.
    doUserCheck(dispatch) // Refresh which addresses are verified now
    setStatus('done')
    return true
  }

  async function doLogOut(address?: Address) {
    setStatus('pending')
    const url = address ? '/api/logout?address=' + address : '/api/logout'
    const rs = await fetch(url)
    if (!rs.ok) {
      setStatus('error')
      console.error('Fetch error', rs)
      return false
    }
    if (dispatch) doUserCheck(dispatch) // Refresh which addresses are verified now
    setStatus('done')
    return true
  }

  const isConnected = typeof address != 'undefined'

  /**
   * For a given address, if the user has submitted a verification for that address on ANY chain, consider them verified.
   * For EOA accounts, the wallet might currently be pointing to a different chain, but we can still trust the visitor is the true owner of that address.
   */
  const isVerified = isConnected && verifiedAddresses.filter((a) => a.address == address).length > 0

  if (!isConnected) {
    // Disconnected
    return {
      connectedAddress: undefined,
      connectedChain: undefined,
      verifiedAddresses: [],
      isConnected: false,
      isSignedIn: false,
      status,
      verificationStatus,
      doSignIn,
      doLogOut,
    }
  } else {
    // Connected or Verified
    return {
      connectedAddress: address,
      connectedChain: chain!.id,
      verifiedAddresses: verifiedAddresses,
      isConnected: true,
      isSignedIn: isVerified,
      status,
      verificationStatus,
      doSignIn,
      doLogOut,
    }
  }
}
