import { Address, getAddress, isAddress } from "viem"
import { mainnetClient } from "./publicClient"
import { normalize } from "viem/ens"

/**
 * Given a user input string, determine if it's an ENS representation of an Ethereum Address.
 * Intended for back-end use, when an ENS name is used as a URL parameter, to resolve it to be a full Ethereum Address.
 */
export async function parseRawAddress(rawAddress: string): Promise<Address | null> {
  // If the input is an address already, return a normalized version of it
  if (isAddress(rawAddress)) {
    return getAddress(rawAddress)
  }

  // Parse the input value as an ENS name
  return await mainnetClient.getEnsAddress({
    name: normalize(rawAddress),
  })
}

