'use client'
import { useState, useEffect } from 'react'

/**
 * Debounce a string value for a specific length of time
 *
 * If the value changes, this hook will re-render. The rerender will cancel the timeout, and only the new value will be queued to be set after a delay.
 * https://dev.to/arnonate/debouncing-react-query-with-hooks-2ek6
 * @param value The string value being modified
 * @param delay The amount of delay (in milliseconds to debounce)
 */
function useDebounce<T>(value: T, delay: number = 500): T {
  const [debouncedValue, setDebouncedValue] = useState(value)

  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value)
    }, delay)

    // Cancel the timeout if value changes (also on delay change or unmount)
    return () => {
      clearTimeout(handler)
    }
  }, [value, delay])

  return debouncedValue
}
export default useDebounce
