'use client'
import { useQuery } from "@tanstack/react-query"
import { AttestationDoc, SCHEMAS } from "./eas"
import { API2_SERVER_ROOT } from "./util"

export function useGMs(cachedAtts?: AttestationDoc[]) {
  return useQuery({
    queryKey: ['gm-attestations'],
    queryFn: async (): Promise<AttestationDoc[]> => {
      const rs = await fetch(`${API2_SERVER_ROOT}/attestations?limit=200&schema=arb:${SCHEMAS.GM}`)
      if (!rs.ok) throw new Error('Failed to fetch GM listings')
      return (await rs.json()).map((att: any): AttestationDoc => {
        // JSON format does not have BigInts, so those values are stored as strings. Convert them back to BigInts to adhere to EAS-SDK type
        att.sig.message.time = BigInt(att.sig.message.time)
        att.sig.message.expirationTime = BigInt(att.sig.message.expirationTime)
        return att
      })
    },
    initialData: cachedAtts
  })
}