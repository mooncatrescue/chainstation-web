'use client'
import { ACCLIMATOR_ADDRESS, MOMENTS_ADDRESS } from './util'
import { Address, parseAbi } from 'viem'
import { Moment } from './types'
import { multicall } from 'wagmi/actions'
import { useQuery } from '@tanstack/react-query'
import { config } from './wagmi-config'

const MOMENTS = {
  address: MOMENTS_ADDRESS as Address,
  abi: parseAbi(['function ownerOf(uint256) external view returns (address)']),
}

const ACCLIMATOR = {
  address: ACCLIMATOR_ADDRESS as Address,
  abi: parseAbi([
    'function ownerOfChild(address childContract, uint256 childTokenId) external view returns (bytes32 parentTokenOwner, uint256 parentTokenId)',
    'function rootOwnerOfChild(address childContract, uint256 childTokenId) external view returns (bytes32 rootOwner)',
  ]),
}

/**
 * Fetch ownership information for all Moments in a given release set
 *
 * Since each batch of Moments has a known (and relatively small) quantity, this function assembles a multicall query to fetch ownership
 * of all of them. Of the ones that are minted, if any of them are owned by the Acclimator contract, it then checks to see if those are
 * in that contract because they're in the Purrse of a specific MoonCat. If so, it returns the owner of that MoonCat.
 */
export default function useMomentOwners(moment: Moment) {
  return useQuery({
    queryKey: ['moment-set-owners', moment.momentId],
    queryFn: async (): Promise<{ tokenId: bigint; owner: Address; moonCat?: number }[]> => {
      const startingToken = BigInt(moment.startingTokenId)

      let multicalls = []
      for (let i = 0n; i < moment.issuance; i++) {
        multicalls.push({
          ...MOMENTS,
          functionName: 'ownerOf',
          args: [i + startingToken],
        })
      }
      const owners = await multicall(config, {
        contracts: multicalls,
        allowFailure: true,
        chainId: 1,
      })

      // Remove ones that failed (not claimed yet), and parse tokens owned by MoonCats
      let parsed: { tokenId: bigint; owner: Address; moonCat?: number }[] = []
      multicalls = []
      for (let i = 0; i < owners.length; i++) {
        if (owners[i].error) continue
        const tokenId = BigInt(i) + startingToken
        if (owners[i].result == ACCLIMATOR_ADDRESS) {
          // This token is owned by a MoonCat
          multicalls.push({
            ...ACCLIMATOR,
            functionName: 'ownerOfChild',
            args: [MOMENTS_ADDRESS, tokenId],
          } as const)
        } else {
          // This token is held directly
          parsed.push({
            tokenId: tokenId,
            owner: owners[i].result as Address,
          })
        }
      }
      if (multicalls.length > 0) {
        const moonCatOwners = await multicall(config, {
          contracts: multicalls,
          allowFailure: false,
          chainId: 1,
        })
        for (let i = 0; i < moonCatOwners.length; i++) {
          const call = multicalls[i]
          const o = moonCatOwners[i]
          if (o[0].substring(0, 10) != '0xcd740db5') {
            console.error('Bad ERC998 return value', {
              expectedPrefix: '0xcd740db5',
              received: o,
              args: call.args,
            })
            continue
          }

          parsed.push({
            tokenId: call.args[1],
            owner: ('0x' + o[0].substring(o[0].length - 40)) as Address,
            moonCat: Number(o[1]),
          })
        }
        parsed.sort((a, b) => Number(a.tokenId - b.tokenId))
      }
      return parsed
    },
  })
}
