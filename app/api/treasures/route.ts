import { cookies } from 'next/headers'
import ironOptions from 'lib/ironOptions'
import { getIronSession, IronSessionData } from 'iron-session'
import { MoonCatData, OwnedMoonCat } from 'lib/types'
import { SearchTreasure, MappedTreasure, getTreasures } from 'lib/firebase'
import { API2_SERVER_ROOT, filterMoonCatList, getAllMoments } from 'lib/util'
import userVerifiedAddresses from 'lib/userVerifiedAddresses'
import _rawTraits from 'lib/mooncat_traits.json'

const allMoments = getAllMoments()

// Versions of a Treasure object that don't have the private data
type SearchTreasureView = Omit<SearchTreasure, 'ipfs'>
type MappedTreasureView = Omit<MappedTreasure, 'mapping'>

/**
 * Given a list of MoonCat identifiers, fetch which Ethereum address owns them
 */
async function getOwnersOfMoonCats(moonCats: string[]) {
  let moonCatOwnershipMap: { [catId: string]: string } = {}

  await Promise.all(
    moonCats.map(async (catId) => {
      const rs = await fetch(`${API2_SERVER_ROOT}/mooncat/traits/${catId}`, { next: { revalidate: 3600 } })
      const traits = await rs.json()
      if (typeof traits.owner == 'undefined') {
        console.error('No owner information for MoonCat', catId)
      } else if (
        traits.owner['0x60cd862c9C687A9dE49aecdC3A99b74A4fc54aB6'].value != '0xc3f733ca98E0daD0386979Eb96fb1722A1A05E69'
      ) {
        moonCatOwnershipMap[traits.rescueOrder] = traits.owner['0x60cd862c9C687A9dE49aecdC3A99b74A4fc54aB6'].value
      } else if (
        traits.owner['0xc3f733ca98E0daD0386979Eb96fb1722A1A05E69'].value != '0xF4d150F3D03Fa1912aad168050694f0fA0e44532'
      ) {
        moonCatOwnershipMap[traits.rescueOrder] = traits.owner['0xc3f733ca98E0daD0386979Eb96fb1722A1A05E69'].value
      } else {
        moonCatOwnershipMap[traits.rescueOrder] = traits.owner['0xF4d150F3D03Fa1912aad168050694f0fA0e44532'].value
      }
    })
  )
  return moonCatOwnershipMap
}

/**
 * Given a list of Ethereum Addresses, fetch which assets they own from the data API server
 */
async function getAssetsOfAddresses(addresses: string[]) {
  let assetOwnership: { moonCats: { [catId: string]: string }; moments: { [momentId: number]: string } } = {
    moonCats: {},
    moments: {},
  }

  await Promise.all(
    addresses.map(async (addr) => {
      const rs = await fetch(`${API2_SERVER_ROOT}/owner-profile/${addr}`, { next: { revalidate: 3600 } })
      const ownerData = await rs.json()
      for (let mc of ownerData.ownedMoonCats as OwnedMoonCat[]) {
        assetOwnership.moonCats[mc.rescueOrder] = addr
      }
      for (let moment of ownerData.ownedMoments) {
        assetOwnership.moments[moment.momentId] = addr
      }
    })
  )

  return assetOwnership
}

export async function GET(request: Request) {
  const session = await getIronSession<IronSessionData>(cookies(), ironOptions)
  const query = new URL(request.url).searchParams

  const targetChain = 1 // Currently all treasures activity happens on the main chain
  const [validKeys] = await userVerifiedAddresses(session.keyring, [targetChain])
  const verifiedAddresses: string[] = validKeys.reduce((collector: string[], k) => {
    if (k.siwe.address) collector.push(k.siwe.address)
    return collector
  }, [])
  const treasures = await getTreasures()
  let assetOwnership: { moonCats: { [catId: string]: string }; moments: { [momentId: number]: string } } = {
    moonCats: {},
    moments: {},
  }

  // What MoonCat(s) are we caring about?
  const moonCatRaw = query.get('mooncat')
  if (moonCatRaw != null && moonCatRaw != '') {
    let rs = await getOwnersOfMoonCats(moonCatRaw.split(','))
    assetOwnership.moonCats = rs
  } else {
    // No MoonCat specified; look up by Ethereum Address

    // What Ethereum Addresses are we caring about?
    let targetAddresses: string[] = []
    const addressRaw = query.get('address')
    if (addressRaw != null && addressRaw != '') {
      targetAddresses = addressRaw.split(',')
    } else {
      targetAddresses = verifiedAddresses
    }

    assetOwnership = await getAssetsOfAddresses(targetAddresses)
  }

  // Any of the assets that have enumerated owners in the assetOwnership mapping are assets this request is searching for.
  // Grab expanded metadata for each of them
  const targetMoonCats: MoonCatData[] = (_rawTraits as MoonCatData[]).filter(
    (moonCat) => typeof assetOwnership.moonCats[moonCat.rescueOrder] != 'undefined'
  )
  const targetMoments = allMoments.filter((m) => typeof assetOwnership.moments[m.id] !== 'undefined')

  // For every Treasure, look up which of the owned assets qualify for it.
  const parsedTreasures: {
    treasure: SearchTreasure | SearchTreasureView | MappedTreasure | MappedTreasureView | null
    moonCats?: number[]
    moments?: number[]
  }[] = treasures
    .map((t) => {
      switch (t.type) {
        case 'search': {
          ////////// Search Treasure //////////
          if ('moonCat' in t.criteria) {
            ////////// Search Treasure targeting MoonCats //////////
            let qualifiedMoonCats: number[] = []
            const moonCatCriteria = t.criteria.moonCat
            if (moonCatCriteria.length == 0) {
              // All MoonCats qualify
              qualifiedMoonCats = targetMoonCats.map((moonCat) => moonCat.rescueOrder)
            } else {
              let matchedMoonCats: { [rescueOrder: number]: any } = {}
              for (let c of moonCatCriteria) {
                filterMoonCatList(targetMoonCats, c).forEach((moonCat) => {
                  matchedMoonCats[moonCat.rescueOrder] = moonCat.rescueOrder
                })
              }
              qualifiedMoonCats = Object.values(matchedMoonCats)
            }

            // To determine whether we show the secret for the treasure,
            // check and see if at least one of the qualifying MoonCats is owned by
            // a verified address for this session
            const isVerified = qualifiedMoonCats.some((catId) => {
              const owner = assetOwnership.moonCats[catId]
              return verifiedAddresses.includes(owner)
            })
            if (isVerified) {
              return {
                treasure: t,
                moonCats: qualifiedMoonCats,
              }
            } else {
              const view: SearchTreasureView = {
                type: t.type,
                label: t.label,
                icon: t.icon,
                details: t.details,
                criteria: t.criteria,
              }
              return {
                treasure: view,
                moonCats: qualifiedMoonCats,
              }
            }
          } else {
            ////////// Search Treasure targeting Moments //////////
            let qualifiedMoments: number[] = []
            const momentCriteria = t.criteria.moment
            if (momentCriteria.length == 0) {
              // All Moments qualify
              qualifiedMoments = targetMoments.map((m) => m.id)
            } else {
              let matchedMoments: { [momentId: number]: any } = {}
              for (let c of momentCriteria) {
                targetMoments
                  .filter((m) => m.moment == c.momentId)
                  .forEach((m) => {
                    matchedMoments[m.id] = m.id
                  })
              }
              qualifiedMoments = Object.values(matchedMoments)
            }

            // To determine whether we show the secret for the treasure,
            // check and see if at least one of the qualifying MoonCats is owned by
            // a verified address for this session
            const isVerified = qualifiedMoments.some((momentId) => {
              const owner = assetOwnership.moments[momentId]
              return verifiedAddresses.includes(owner)
            })
            if (isVerified) {
              return {
                treasure: t,
                moonCats: qualifiedMoments,
              }
            } else {
              const view: SearchTreasureView = {
                type: t.type,
                label: t.label,
                icon: t.icon,
                details: t.details,
                criteria: t.criteria,
              }
              return {
                treasure: view,
                moments: qualifiedMoments,
              }
            }
          }
        }
        case 'mapped': {
          ////////// Mapped Treasure //////////
          if ('moonCat' in t.mapping) {
            ////////// Mapped Treasure targeting MoonCats //////////
            const moonCatMapping = t.mapping.moonCat
            const qualifiedMoonCats = targetMoonCats.filter((m) => moonCatMapping[m.rescueOrder])

            // To determine whether we show the secret for the treasure,
            // check and see if at least one of the qualifying MoonCats is owned by
            // a verified address for this session
            const isVerified = qualifiedMoonCats.some((m) => {
              const owner = assetOwnership.moonCats[m.catId]
              return verifiedAddresses.includes(owner)
            })
            if (isVerified) {
              // Return the private links as part of the response. But, the mapping needs to be narrowed to just the qualified MoonCats,
              // to only deliver those secret links the visitor should be able to see
              let shownLinks: { [rescueOrder: number]: string } = {}
              qualifiedMoonCats.forEach((m) => {
                shownLinks[m.rescueOrder] = moonCatMapping[m.rescueOrder]
              })
              t.mapping.moonCat = shownLinks
              return {
                treasure: t,
                moonCats: qualifiedMoonCats.map((m) => m.rescueOrder),
              }
            } else {
              const view: MappedTreasureView = {
                type: t.type,
                label: t.label,
                icon: t.icon,
                details: t.details,
              }
              return {
                treasure: view,
                moonCats: qualifiedMoonCats.map((m) => m.rescueOrder),
              }
            }
          } else {
            ////////// Mapped Treasure targeting Moments //////////
            const momentMapping = t.mapping.moment
            const qualifiedMoments = targetMoments.filter((m) => momentMapping[m.moment])

            // To determine whether we show the secret for the treasure,
            // check and see if at least one of the qualifying Moments is owned by
            // a verified address for this session
            const isVerified = qualifiedMoments.some((m) => {
              const owner = assetOwnership.moments[m.id]
              return verifiedAddresses.includes(owner)
            })
            if (isVerified) {
              // Return the private links as part of the response. But, the mapping needs to be narrowed to just the qualified Moments,
              // to only deliver those secret links the visitor should be able to see
              let shownLinks: { [momentId: number]: string } = {}
              qualifiedMoments.forEach((m) => {
                shownLinks[m.moment] = momentMapping[m.moment]
              })
              t.mapping.moment = shownLinks
              return {
                treasure: t,
                moments: qualifiedMoments.map((m) => m.id),
              }
            } else {
              const view: MappedTreasureView = {
                type: t.type,
                label: t.label,
                icon: t.icon,
                details: t.details,
              }
              return {
                treasure: view,
                moonCats: qualifiedMoments.map((m) => m.id),
              }
            }
          }
        }
        default:
          // Unknown type
          console.warn('Treasure found with unknown type', (t as any).label)
          return {
            treasure: null,
          }
      }
    })
    .filter((t) => (t.moonCats && t.moonCats.length > 0) || (t.moments && t.moments.length > 0)) // Only keep Treasures that have at least one qualifying asset

  return Response.json(parsedTreasures)
}
