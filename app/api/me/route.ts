import { cookies } from 'next/headers'
import ironOptions from 'lib/ironOptions'
import { getIronSession, IronSessionData } from 'iron-session'
import userVerifiedAddresses from 'lib/userVerifiedAddresses'
import { NextRequest } from 'next/server'
import { Address } from 'viem'

interface VerifiedAddressMeta {
  address: Address
  chainId: number
  issued?: string
  expires: string
}

export async function GET(request: NextRequest) {
  function finalize(rs: VerifiedAddressMeta[]) {
    return Response.json({ addresses: rs })
  }
  const session = await getIronSession<IronSessionData>(cookies(), ironOptions)

  // Filter current keyring down to only chains we care about, and sort into invalid or valid
  const [validKeys, invalidKeys] = await userVerifiedAddresses(session.keyring, [1, 42161])

  if (invalidKeys.length > 0) {
    console.error('Stale session attempted for', invalidKeys.map((k) => k.siwe.address).join(', '))
    console.error('forwarded-for', request.headers.get('x-forwarded-for'))
    console.error('remote address', request.ip)
  }

  // Replace user's local state with only valid keys
  if (validKeys.length == 0) {
    session.destroy()
  } else {
    session.keyring = validKeys
    await session.save()
  }
  console.log('valid keys', validKeys)
  return finalize(
    validKeys.map((k) => ({
      address: k.siwe.address as Address,
      chainId: k.siwe.chainId,
      issued: k.siwe.issuedAt,
      expires: k.siwe.expirationTime!,
    }))
  )
}
