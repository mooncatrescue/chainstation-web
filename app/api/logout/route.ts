import { cookies } from 'next/headers'
import ironOptions from 'lib/ironOptions'
import { getIronSession, IronSessionData } from 'iron-session'
import { getAppFirestore, isSessionValid } from 'lib/firebase'
import { USER_SESSION_COLLECTION } from 'lib/util'
import { SiweMessage } from 'viem/siwe'

export async function GET(request: Request) {
  function finalize() {
    return Response.json({ ok: true })
  }
  const session = await getIronSession<IronSessionData>(cookies(), ironOptions)
  // Check query parameter for target address to log out
  // If no parameter is set, log out all addresses
  const url = new URL(request.url)
  const targetAddress = url.searchParams.get('address') ?? 'all'

  const keyring = session.keyring
  if (!keyring || !Array.isArray(keyring) || keyring.length == 0) {
    // Keyring is missing/empty; nothing to do
    return finalize()
  }

  let newKeyring = []
  for (let k of keyring) {
    if (targetAddress != 'all' && k.siwe.address != targetAddress) {
      // Keep keys for other addresses with no further checks
      newKeyring.push(k)
      continue
    }

    // This key is for the target chain; verify it
    if (!k.signature || !k.siwe) {
      // Incomplete session data (older version of app?)
      // Consider it already invalid; don't add to new keyring
      continue
    }

    // Convert strings to Dates
    const siweMessage: SiweMessage = {
      ...k.siwe,
      expirationTime: k.siwe.expirationTime ? new Date(k.siwe.expirationTime) : undefined,
      issuedAt: k.siwe.issuedAt ? new Date(k.siwe.issuedAt) : undefined,
    }

    const isValid = await isSessionValid(siweMessage, k.signature)
    if (!isValid) continue

    // User is currently holding a valid session for a specific address
    // Therefore they are authorized to log this address out on the database-side,
    // which will log out all other sessions
    const db = getAppFirestore()
    const sessionRef = db.collection(USER_SESSION_COLLECTION).doc(`${k.siwe.address}-${k.siwe.chainId}`)
    await sessionRef.delete()

    // This session was valid, but is now logged-out; don't add it to the keyring to keep
  }

  if (newKeyring.length == 0) {
    session.destroy()
  } else {
    session.keyring = newKeyring
    await session.save()
  }
  return finalize()
}
