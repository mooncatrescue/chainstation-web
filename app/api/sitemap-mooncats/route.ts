import { NextRequest } from 'next/server'
const { SitemapStream } = require('sitemap')

/**
 * Generate an XML sitemap listing out all MoonCats by ID
 */
export async function GET(request: NextRequest) {
  try {
    const smStream = new SitemapStream({ hostname: 'https://chainstation.mooncatrescue.com/' })

    for (let rescueOrder = 0; rescueOrder < 25440; rescueOrder++) {
      smStream.write({ url: `/mooncats/${rescueOrder}` })
    }
    smStream.end()
    return new Response(smStream, { headers: { 'Content-Type': 'application/xml' } })
  } catch (e) {
    console.error(e)
    return new Response('Streaming error', { status: 500 })
  }
}
