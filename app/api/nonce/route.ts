import { cookies } from 'next/headers'
import ironOptions from 'lib/ironOptions'
import { getIronSession, IronSessionData } from 'iron-session'
import { generateSiweNonce } from 'viem/siwe'

export async function GET(request: Request) {
  const session = await getIronSession<IronSessionData>(cookies(), ironOptions)

  // On every request, update the expected nonce the user should sign with
  session.nonce = generateSiweNonce()

  await session.save() // Encrypt the session data and sent it to the user as a cookie to return on future requests
  return new Response(session.nonce, { headers: { 'Content-Type': 'text/plain' } })
}
