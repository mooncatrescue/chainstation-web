import { Moment } from "lib/types";
import { API_SERVER_ROOT, getQueryInt, IPFS_GATEWAY } from "lib/util";
import { NextRequest } from "next/server";
import momentsData from 'lib/moments_meta.json'

interface OembedResponse {
  type: string
  version: '1.0'
  title?: string
  author_name?: string
  author_url?: string
  provider_name?: string
  provider_url?: string
  cache_age?: number
  thumbnail_url?: string
  thumbnail_width?: number
  thumbnail_height?: number
  [x: string]: unknown
}

interface OembedPhotoResponse extends OembedResponse {
  type: 'photo'
  url: string
  width: number
  height: number
}

interface OembedVideoResponse extends OembedResponse {
  type: 'video'
  html: string
  width: number
  height: number
}

interface OembedRichResponse extends OembedResponse {
  type: 'rich'
  html: string
  width: number
  height: number
}

async function getPNGDimensions(url: string) {
  const rs = await fetch(url, {
    headers: { Range: 'bytes=0-32' } // Request only the first 32 bytes
  });
  if (!rs.ok) {
    throw new Error(`HTTP Error: ${rs.status}`);
  }

  const arrayBuffer = await rs.arrayBuffer();
  const buffer = Buffer.from(arrayBuffer);

  // Check PNG signature
  if (buffer.toString('hex', 0, 8) !== '89504e470d0a1a0a') {
    throw new Error('Not a valid PNG file');
  }

  // IHDR chunk starts at byte 8
  const width = buffer.readUInt32BE(16);
  const height = buffer.readUInt32BE(20);

  return { width, height };
}

async function getJPGDimensions(url: string) {
  const rs = await fetch(url, {
    headers: { Range: 'bytes=0-250' } //  Request first 250 bytes, usually enough for SOF
  });
  if (!rs.ok) {
    throw new Error(`HTTP Error: ${rs.status}`);
  }

  const arrayBuffer = await rs.arrayBuffer();
  const buffer = Buffer.from(arrayBuffer);

  // Check JPEG signature
  if (buffer[0] !== 0xFF || buffer[1] !== 0xD8) {
    throw new Error('Not a valid JPEG file');
  }

  let offset = 2;
  while (offset < buffer.length) {
    if (buffer[offset] !== 0xFF) {
      throw new Error('Invalid JPEG format');
    }

    const marker = buffer[offset + 1];
    if (marker === 0xC0 || marker === 0xC1 || marker === 0xC2) {
      // SOF0, SOF1, or SOF2 marker
      const height = buffer.readUInt16BE(offset + 5);
      const width = buffer.readUInt16BE(offset + 7);
      return { width, height };
    }

    offset += 2 + buffer.readUInt16BE(offset + 2);
  }

  throw new Error('Could not find SOF marker');
}

export async function GET(request: NextRequest) {
  const query = new URL(request.url).searchParams

  const targetUrlRaw = query.get('url')
  if (targetUrlRaw == null || targetUrlRaw == '') {
    return new Response('No URL specified', { status: 400 })
  }
  let targetURL: URL
  try {
    targetURL = new URL(targetUrlRaw)
  } catch (err) {
    return new Response('Bad URL specified', { status: 400 })
  }

  const requestedFormat = query.get('format')
  if (requestedFormat !== null && requestedFormat !== 'json') {
    return new Response('Only JSON format supported', { status: 501 })
  }

  // Parse the requested URL, and determine what type of response to give
  if (targetURL.host !== 'chainstation.mooncatrescue.com') {
    return new Response('Not found', { status: 404 })
  }
  const pathPieces = targetURL.pathname.split('/').slice(1)

  const requestedWidth = query.get('maxwidth')
  const requestedHeight = query.get('maxheight')
  switch (pathPieces[0]) {
    case 'mooncats': {
      if (pathPieces.length == 1) {
        return new Response('Not found', { status: 404 })
      }
      const targetMoonCat = pathPieces[1]
      const targetWidth = getQueryInt(requestedWidth, 500)
      const targetHeight = getQueryInt(requestedHeight, 500)
      return Response.json({
        ok: true,
        type: 'rich',
        version: '1.0',
        width: targetWidth,
        height: targetHeight,
        html: `<iframe frameborder="0" scrolling="no" style="width: 100%; overflow: hidden; max-width: ${targetWidth}px; max-height: ${targetHeight}px;" src="${API_SERVER_ROOT}/dynamic/${targetMoonCat}"></iframe>`
      } as OembedRichResponse)
    }
    case 'accessories': {
      if (pathPieces.length == 1) {
        return new Response('Not found', { status: 404 })
      }
      const targetAccessory = pathPieces[1]
      const photoURL = `${API_SERVER_ROOT}/accessory-image/${targetAccessory}`
      try {
        const { width, height } = await getPNGDimensions(photoURL)
        return Response.json({
          ok: true,
          type: 'photo',
          version: '1.0',
          url: photoURL,
          width: width,
          height: height
        } as OembedPhotoResponse)
      } catch (err) {
        console.error('Error fetching Accessory PNG', (err as Error).message)
        return new Response('Upstream error', { status: 502 })
      }
    }
    case 'moments': {
      if (pathPieces.length == 1) {
        return new Response('Not found', { status: 404 })
      }
      const targetMoment = pathPieces[1]
      const moment = (momentsData as Moment[]).find((m) => m.momentId == Number(targetMoment))
      if (typeof moment == 'undefined') {
        return new Response('Not found', { status: 404 })
      }
      const photoURL = moment.meta.image.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/')
      try {
        const { width, height } = await getJPGDimensions(photoURL)
        return Response.json({
          ok: true,
          type: 'photo',
          version: '1.0',
          url: photoURL,
          width: width,
          height: height
        } as OembedPhotoResponse)
      } catch (err) {
        console.error('Error fetching Moment JPG', (err as Error).message)
        return new Response('Upstream error', { status: 502 })
      }
    }
    default: {
      return new Response('Not found', { status: 404 })
    }
  }

}