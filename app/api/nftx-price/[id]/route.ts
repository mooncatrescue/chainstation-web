import zeroxQuote, { AdoptType, TokenName } from 'lib/zeroxQuote'
import { NextRequest } from 'next/server'

const VALID_TOKEN_NAMES = ['mooncat', 'mcat17'] as const
const isTokenName = (t: string): t is TokenName => {
  return VALID_TOKEN_NAMES.includes(t as TokenName)
}

const VALID_ADOPTION_TYPES = ['random', 'specific'] as const
const isAdoptType = (t: string): t is AdoptType => {
  return VALID_ADOPTION_TYPES.includes(t as AdoptType)
}

export async function GET(request: NextRequest, { params }: { params: { id: string } }) {
  const tokenId = params.id
  if (typeof tokenId == 'undefined' || Array.isArray(tokenId)) {
    return Response.json({ ok: false }, { status: 400 })
  }
  const [tokenName, adoptType] =
    tokenId.indexOf('-') > 0 ? tokenId.toLowerCase().split('-') : [tokenId.toLowerCase(), 'specific']
  if (!isTokenName(tokenName) || !isAdoptType(adoptType)) {
    return Response.json({ ok: false }, { status: 400 })
  }

  const API_KEY = process.env['ZEROX_API_KEY']
  if (typeof API_KEY == 'undefined' || API_KEY == '') {
    console.error('No 0x API key set!')
    return Response.json({ ok: false }, { status: 500 })
  }
  try {
    const jsonData = await zeroxQuote(API_KEY, tokenName, adoptType)
    return Response.json({
      'WETH': {
        calldata: jsonData.data,
        sellAmount: jsonData.sellAmount,
      },
    })
  } catch (err) {
    console.error('Error fetching 0x quote:', err)
    return Response.json({ ok: false }, { status: 502 })
  }
}
