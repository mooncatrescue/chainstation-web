import { API2_SERVER_ROOT } from 'lib/util'
import { NextRequest } from 'next/server'
const { SitemapStream } = require('sitemap')

/**
 * Generate an XML sitemap listing out all Accessories by ID
 */
export async function GET(request: NextRequest) {
  let maxId = 1000
  try {
    let rs = await fetch(`${API2_SERVER_ROOT}/accessories?sort=newest`, { next: { revalidate: 3600 } })
    const jsonData = await rs.json()
    if (!jsonData) {
      console.error('Failed to parse Accessory count value', rs)
    } else {
      maxId = jsonData[0].id
    }
  } catch (err) {
    console.error('Failed to get Accessory count', err)
  }

  try {
    const smStream = new SitemapStream({ hostname: 'https://chainstation.mooncatrescue.com/' })
    for (let accessoryId = 0; accessoryId <= maxId; accessoryId++) {
      smStream.write({ url: `/accessories/${accessoryId}` })
    }
    smStream.end()
    return new Response(smStream, { headers: { 'Content-Type': 'application/xml' } })
  } catch (e) {
    console.error(e)
    return new Response('Streaming error', { status: 500 })
  }
}
