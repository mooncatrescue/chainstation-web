import { getReservoirAsks } from 'lib/reservoirData'
import { NextRequest } from 'next/server'

export async function GET(request: NextRequest) {
  try {
    const reservoirData = await getReservoirAsks()
    return Response.json(reservoirData.orders)
  } catch (error) {
    console.error('Failed to fetch reservoir listing data', error)
    return Response.json(
      {
        ok: false,
        error: 'Failed to retrieve listing data',
      },
      { status: 500 }
    )
  }
}
