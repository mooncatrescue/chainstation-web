/* eslint @next/next/no-sync-scripts: 0 */
import getMoonCatData from 'lib/getMoonCatData'
import { notFound } from 'next/navigation'

const IPFS_PATH = 'https://ipfs.io/ipfs/bafybeifxqtzf635xy3q3lrp2cygrz2vatregvtnfe2dsl4jskjyuneexzu'

async function getVox(rescueOrder: number) {
  const rs = await fetch(`${IPFS_PATH}/${rescueOrder}.vox`, { next: { revalidate: 3600 } })
  if (!rs.ok) {
    console.error('Failed to fetch VOX file', rs)
    return null
  }
  return await rs.arrayBuffer()
}

type Props = {
  params: { id: string }
}

export default async function Page({ params }: Props) {
  const rescueOrder = parseInt(params.id)
  if (Number.isNaN(rescueOrder) || rescueOrder < 0) notFound()
  const vox = await getVox(rescueOrder)
  if (vox == null) notFound()
  const voxSerialized = Buffer.from(vox).toString('base64')

  const parsed = getMoonCatData([rescueOrder])
  if (parsed.length == 0) notFound()
  const moonCat = parsed[0]

  return (
    <>
      <script
        type="importmap"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify({
            imports: {
              'three': 'https://cdn.jsdelivr.net/npm/three@0.169.0/build/three.module.js',
              'VOXLoader': '/VOXLoader.js',
              'three/addons/': 'https://cdn.jsdelivr.net/npm/three@0.169.0/examples/jsm/',
            },
          }),
        }}
      />
      <div id="mooncat-data" data-pose={moonCat.pose} data-facing={moonCat.facing} data-vox={voxSerialized} />
      <script type="module" src="/vox.js" />
    </>
  )
}
