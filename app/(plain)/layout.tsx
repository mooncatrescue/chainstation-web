import { Metadata } from 'next'
import { Roboto_Mono, Press_Start_2P } from 'next/font/google'
import localFont from 'next/font/local'
import 'styles/globals.scss'

const roboto = Roboto_Mono({
  weight: ['700'],
  style: ['italic', 'normal'],
  subsets: ['latin'],
  variable: '--font-roboto-mono',
  display: 'swap',
})
const player2 = Press_Start_2P({
  weight: ['400'],
  subsets: ['latin'],
  variable: '--font-p2',
  display: 'swap',
})
const setback = localFont({
  src: '../../public/fonts/setbackt.ttf',
  variable: '--font-setback',
  display: 'swap',
})

type Props = {
  params: { id: string }
}

export const metadata: Metadata = {
  title: 'MoonCat',
}

export default function PlainLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en" className={`${roboto.variable} ${player2.variable} ${setback.variable}`}>
      <body>{children}</body>
    </html>
  )
}
