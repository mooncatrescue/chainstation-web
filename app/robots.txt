User-agent: *
Allow: /

Sitemap: https://chainstation.mooncatrescue.com/sitemap.xml
Sitemap: https://chainstation.mooncatrescue.com/api/sitemap-mooncats
Sitemap: https://chainstation.mooncatrescue.com/api/sitemap-accessories
