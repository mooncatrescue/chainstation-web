import { ZWS } from 'lib/util'
import type { Metadata } from 'next'
import Link from 'next/link'

const pageTitle = 'ChainStation KnowledgeBase - Signing in'
export const metadata: Metadata = {
  title: pageTitle,
  description: 'Documentation on signing-in/authenticating to the ChainStation Web3 application',
  openGraph: {
    title: pageTitle,
    description: 'Documentation on signing-in/authenticating to the ChainStation Web3 application',  
  }
}
export default function Page() {
  return (
    <div id="content-container">
      <div className="text-container">
        <h1 className="hero">ChainStation Signing In</h1>
        <section className="card-help">
          <p>
            The ChainStation web application is a <em>Web3</em>-style application, which means it uses a lot of
            information from a blockchain source (Ethereum), and users use a separate <em>Wallet</em> software to
            authenticate to prove they are who they say they are. Therefore, the process of proving you are who you say
            you are is a little different than other web applications you may be more familiar with.
          </p>

          <h3>Why should I Connect?</h3>
          <p>
            Many of the tools in the MoonCat{ZWS}Rescue ecosystem can be used by anyone. You don&rsquo;t need to own a
            MoonCat yourself to view each MoonCat, or to <Link href="/mcns/">Announce an ENS subdomain for them</Link>,
            but you will need some connection to the blockchain to do so. Once you are connected, you can take actions
            that anyone on the blockchain can do. This includes taking anonymous actions for MoonCats you own.
          </p>

          <h3>Why should I Sign In?</h3>
          <p>
            Signing in allows this application to know for certain you as a visiting user really do have ownership of a
            specific address. This site will not use second-person grammar (i.e. indicating &ldquo;This is <em>your</em>{' '}
            wallet&rdquo; or &ldquo;these are <em>your</em> assets&rdquo;) unless you have signed in with the ERC4361
            method. This helps make it harder for bad actors to trick you into thinking they have access to your
            logged-in account by only spoofing <em>connecting</em> as you.
          </p>
          <p>
            Taking actions that can only be done by the owner of an asset (e.g. buying Accessories for a MoonCat or
            Acclimating them) will be more streamlined when logged in. Some things in the MoonCat{ZWS}Rescue ecosystem have
            off-chain secrets, and require signing in to get access to them (e.g. additional artwork linked to each
            MoonCatMoment).
          </p>

          <h3>What&rsquo;s the difference between Connected and Signed In?</h3>
          <p>
            <em>Connecting</em> your wallet software gives a hint to the application who you <em>want</em> to be
            identified as, but does not provide proof that you actually own that address. Connecting also grants the web
            application access to use your wallet&rsquo;s connection to the blockchain to get updated data. If
            you&rsquo;re wanting to sign in, you&rsquo;ll need to connect as a first step, and then take the additional
            step of signing in.
          </p>
          <p>
            An address&rsquo; <em>signature</em> on a particular message is <em>proof</em> the owner of the private key
            associated with that address did approve the message. Like a signature on a check or a signature on a legal
            contract, it makes the document active/actionable. What the signature activates depends on what the message
            it&rsquo;s attached to is.
          </p>
          <p>
            This web application uses specifically-formatted text messages (
            <a href="https://eips.ethereum.org/EIPS/eip-4361">ERC4361: Sign-In with Ethereum</a>) as proof that a
            visitor is indeed the owner of a specific public address, and won&rsquo;t consider the user{' '}
            <em>signed in</em> until that proof is presented.
          </p>

          <h3>Vocabulary</h3>
          <p>
            As this sort of web application is a new frontier for many users, here&rsquo;s some vocabulary and further
            information to know more clearly what access to your data is happening while interacting with this
            application:
          </p>
          <ul>
            <li>
              <strong>Address</strong>: Similar to a <em>username</em> in traditional web applications. Your address is
              a public identifier that is safe to share with others; others who know your public address can see what
              assets you hold there, and what blockchain activity you&rsquo;ve done there, but just knowing a public
              address does not give someone the ability to take actions for assets in that account (only the person with
              the <em>private key</em> for that specific address can take blockchain actions for it).
            </li>
            <li>
              <strong>Wallet Software</strong>: In addition to the web browser you&rsquo;re currently using to browse
              this site, to interact with the Ethereum blockchain, you&rsquo;ll need to have an application that manages
              your cryptocurrency wallet for you. The main purpose of wallet software is to keep your{' '}
              <em>private key</em> (usually represented by a 12- to 24-word <em>mnemonic phrase</em>) safe, share your{' '}
              <em>public address</em> with applications that request it, and let you know when applications have
              submitted a blockchain transaction for you to sign. Never enter your private key or mnemonic phrase into a
              web application; that should only be entered into wallet software you trust
            </li>
            <li>
              <strong>Connected</strong>: The action of having a Web3-powered app ask the wallet software what the
              public address of the user is. Most wallet software only presents public addresses to be connected if they
              have the proper private key to match the public address, but it&rsquo;s possible for wallet software to
              give any public address.
            </li>
            <li>
              <strong>Signature</strong>: A mathematic <em>proof</em> that anyone can verify is associated with a
              specific public address, but only someone with the private key related to that public address can
              generate. Signatures can be attached to any sort of message (plain text) including specific structured
              messages (e.g. blockchain transaction data). If your wallet software says a message has been presented
              that it wants to know if it should sign for you, make sure you know what type of message it is (whether it
              will cause an on-chain transaction to happen, or is for some off-chain purpose).
            </li>
          </ul>
        </section>
      </div>
    </div>
  )
}
