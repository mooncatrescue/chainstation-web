import { ZWS } from 'lib/util'
import { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'lootprints (for MoonCats)',
  description: 'Deep Space exploration vehicles, custom-tailored for MoonCats',
  openGraph: {
    title: 'lootprints (for MoonCats)',
    description: 'Deep Space exploration vehicles, custom-tailored for MoonCats',
  },
}
export default function Page() {
  return (
    <div id="content-container">
      <div className="text-container">
        <h1 className="hero">lootprints</h1>
        <section className="card">
          <p>
            <strong>Hard Hats Required!</strong> This area of ChainStation Alpha is undergoing some intense
            construction. So, please pardon the dust, and check back a bit later to see what pops up here!
          </p>
        </section>
        <section className="card-notice">
          <p>
            While this site is under construction, more information about lootprints can be accessed{' '}
            <a href="https://mooncat.community/blog/lootprints">on the MoonCat{ZWS}Community website</a>.
          </p>
        </section>
      </div>
    </div>
  )
}
