import ContractDocsHub from 'components/ContractDocsHub'
import { ZWS } from 'lib/util'
import { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'Smart Contract Documentation',
  description: 'Human-friendly notes for MoonCatRescue Ecosystem smart contracts',
  openGraph: {
    title: 'Smart Contract Documentation',
    description: 'Human-friendly notes for MoonCatRescue Ecosystem smart contracts',
  },
}
export default function Page() {
  return (
    <div id="content-container">
      <div className="text-container">
        <h1 className="hero">Smart Contract Documentation</h1>
        <section className="card-help">
          <p>
            The MoonCat{ZWS}Rescue project is built on the <a href="https://ethereum.org">Ethereum</a> blockchain, and
            as such uses several <a href="https://ethereum.org/en/smart-contracts/">smart contracts</a>
            to make the various parts work. In lore, we use different terms to make the concepts more user-friendly, but
            in this area of the site, you can find low-level information about the individual contracts that make the
            magic work. This can be useful if you&rsquo;re wanting to look up MoonCat-related data on other sites (e.g.
            block explorers) or if you&rsquo;re a developer investigating integration with the MoonCat{ZWS}Rescue
            ecosystem.
          </p>
        </section>
        <ContractDocsHub />
      </div>
    </div>
  )
}
