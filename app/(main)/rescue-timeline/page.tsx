import { Metadata } from 'next'
import RescueTimelineChart from 'components/RescueTimelineChart'

export const metadata: Metadata = {
  title: 'Rescue Timeline',
  description: 'Exploring the process of rescuing a slew of colorful felines from the moon',
  openGraph: {
    title: 'Rescue Timeline',
    description: 'Exploring the process of rescuing a slew of colorful felines from the moon',
  },
}
export default function Page() {
  return (
    <div id="content-container">
      <div className="text-container">
        <h1 className="hero">The Rescue</h1>
        <section style={{ textAlign: 'center'}}>
          <RescueTimelineChart />
        </section>
      </div>
    </div>
  )
}
