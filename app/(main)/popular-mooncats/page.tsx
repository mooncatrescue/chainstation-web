import MoonCatLeaderboard from 'components/MoonCatLeaderboard'
import { MoonCatSummary } from 'lib/types'
import { API2_SERVER_ROOT } from 'lib/util'
import { Metadata } from 'next'

/**
 * Get current leaderboard of popular MoonCats
 *
 * Server-side function that NextJS will automatically turn into a server-side render.
 */
async function getData(): Promise<MoonCatSummary[]> {
  const rs = await fetch(`${API2_SERVER_ROOT}/mooncats?sort=liked&limit=50`, {
    next: { tags: ['stars', 'mooncat-stars'], revalidate: 3600 },
  })
  if (!rs.ok) {
    throw new Error('Failed to fetch popular MoonCat data from back-end')
  }

  return await rs.json()
}

export const metadata: Metadata = {
  title: 'Popular MoonCats',
  description: 'Leaderboard of which MoonCats have earned the most "stars" from their fellow Etherians',
  openGraph: {
    title: 'Popular MoonCats',
    description: 'Leaderboard of which MoonCats have earned the most "stars" from their fellow Etherians',
  },
}
export default async function Page() {
  const moonCats = await getData()
  return <MoonCatLeaderboard moonCats={moonCats} />
}
