import { parseRawAddress } from 'lib/ens'
import { ACCLIMATOR_ADDRESS, API2_SERVER_ROOT, ZWS } from 'lib/util'
import { OwnedMoonCat } from 'lib/types'
import { Address, getAddress, isAddress } from 'viem'
import { Metadata } from 'next'
import { notFound } from 'next/navigation'
import AcclimatedCatTree from 'components/AcclimatedCatTree'
import EthereumAddress from 'components/EthereumAddress'

type Props = {
  params: { id: string }
}

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const ownerAddress = isAddress(params.id) ? getAddress(params.id) : null
  const pageTitle = ownerAddress == null ? `MoonCatAcclimator Lounge` : `MoonCatAcclimator Lounge - ${ownerAddress}`
  return {
    title: pageTitle,
    openGraph: {
      title: pageTitle,
    },
  }
}

async function getOwnerProfile(
  rawAddress: string
): Promise<{ ownerAddress: Address | null; moonCats: OwnedMoonCat[] }> {
  const ownerAddress = await parseRawAddress(rawAddress)
  if (ownerAddress == null) {
    return { ownerAddress: null, moonCats: [] }
  }

  let rs = await fetch(`${API2_SERVER_ROOT}/owner-profile/${ownerAddress}`, { next: { revalidate: 300 } })
  if (!rs.ok) {
    console.error('Ownership info fetch error', rs)
    return { ownerAddress: null, moonCats: [] }
  }
  let owned: OwnedMoonCat[] = (await rs.json()).ownedMoonCats
  return {
    ownerAddress,
    moonCats: owned,
  }
}

export default async function Page({ params }: Props) {
  const { ownerAddress, moonCats } = await getOwnerProfile(params.id)
  if (ownerAddress == null) {
    notFound()
  }
  const acclimatedMoonCats = moonCats
    .filter((m) => m.collection.address == ACCLIMATOR_ADDRESS)
    .map((mc) => mc.rescueOrder)

  if (acclimatedMoonCats.length == 0) {
    return (
      <div id="content-container">
        <div className="text-container">
          <h1 className="hero">MoonCat{ZWS}Acclimator Lounge</h1>
          <section className="card">
            <p>
              The address <EthereumAddress address={ownerAddress} /> does not own any Acclimated MoonCats at the moment.
            </p>
          </section>
        </div>
      </div>
    )
  }

  return (
    <div id="content-container">
      <div className="text-container">
        <h1 className="hero">MoonCat{ZWS}Acclimator Lounge</h1>
        <section className="card">
          <p>
            Look at those well-acclimated MoonCats! <EthereumAddress address={ownerAddress} /> has{' '}
            {acclimatedMoonCats.length.toLocaleString()} spacetuna-loving friends, who are just so ready for their next
            adventure.
          </p>
        </section>
        <AcclimatedCatTree moonCats={acclimatedMoonCats} />
      </div>
    </div>
  )
}
