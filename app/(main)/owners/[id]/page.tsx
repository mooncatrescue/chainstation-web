import OwnerDetailsPage from 'components/OwnerDetailsPage'
import { parseRawAddress } from 'lib/ens'
import { OwnerProfile } from 'lib/types'
import { API2_SERVER_ROOT } from 'lib/util'
import { Metadata } from 'next'
import { notFound } from 'next/navigation'
import { Address } from 'viem'

type Props = {
  params: { id: string }
}

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const ownerAddress = await parseRawAddress(params.id)
  if (ownerAddress == null) return { title: 'Owner Profile' }
  const pageTitle = `Owner Profile - ${ownerAddress}`
  return {
    title: pageTitle,
    openGraph: {
      title: pageTitle,
    },
    icons: {
      other: [
        {
          rel: 'canonical',
          url: `https://chainstation.mooncatrescue.com/owners/${ownerAddress}`,
        },
      ],
    },
  }
}

async function getOwnerProfile(
  rawAddress: string
): Promise<{ ownerAddress: Address; details: OwnerProfile } | { ownerAddress: null; details: null }> {
  const ownerAddress = await parseRawAddress(rawAddress)
  if (ownerAddress == null) {
    return { ownerAddress: null, details: null }
  }

  // Get detailed metadata about that address
  const rs = await fetch(`${API2_SERVER_ROOT}/owner-profile/${ownerAddress}`, { next: { revalidate: 300 } })
  if (!rs.ok) {
    console.error(`Ownership info fetch error for ${ownerAddress}`, {
      status: rs.status,
      statusText: rs.statusText,
    })
    return { ownerAddress: null, details: null }
  }
  const ownerProfile: OwnerProfile = await rs.json()
  return {
    ownerAddress,
    details: ownerProfile,
  }
}

export default async function Page({ params }: Props) {
  const { ownerAddress, details } = await getOwnerProfile(params.id)
  if (ownerAddress == null) {
    notFound()
  }

  return <OwnerDetailsPage ownerAddress={ownerAddress} details={details} />
}
