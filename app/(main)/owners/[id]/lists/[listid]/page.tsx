import { API2_SERVER_ROOT, nameToUriSlug } from 'lib/util'
import { Address, stringToHex } from 'viem'
import { OwnerProfile, TokenListMeta } from 'lib/types'
import { UserListTokenMeta, parseTokenListAttestations } from 'lib/tokens'
import { SCHEMAS } from 'lib/eas'
import { notFound } from 'next/navigation'
import ListDetails from 'components/ListDetails'
import { Metadata } from 'next'
import { parseRawAddress } from 'lib/ens'

// https://arbitrum.easscan.org/schema/view/0x58de78ba175d13cb1699fdbc6c25a80ba2b21a943b5aac252ec367c878808d09
const SCHEMA_TOKEN_LIST = SCHEMAS.TOKEN_LIST

type Props = {
  params: { id: string; listid: string }
}

async function getListDetails(
  rawAddress: string,
  listSlug: string
): Promise<{ ownerAddress: Address; listMeta: TokenListMeta; tokens: UserListTokenMeta[] } | null> {
  const ownerAddress = await parseRawAddress(rawAddress)
  if (ownerAddress == null) {
    return null
  }

  // Get detailed metadata about that address
  let rs = await fetch(`${API2_SERVER_ROOT}/owner-profile/${ownerAddress}`, { next: { revalidate: 300 } })
  if (!rs.ok) {
    console.error('Ownership info fetch error', rs)
    return null
  }
  const ownerProfile: OwnerProfile = await rs.json()

  // Find the User List being asked for
  const matchingSlugs = ownerProfile.tokenLists.filter((l) => listSlug == nameToUriSlug(l.title))
  if (matchingSlugs.length == 0) {
    console.error(`List with slug ${listSlug} not found among ${ownerProfile.tokenLists.map((l) => l.title).join(',')}`)
    return null
  } else if (matchingSlugs.length > 1) {
    console.warn(
      `Multiple lists with slug ${listSlug} found for ${rawAddress}: ${matchingSlugs.map((l) => l.title).join(', ')}`
    )
  }
  const listMeta = matchingSlugs[0]

  // Get the tokens for that User List
  const rawListTitle = stringToHex(listMeta.title, { size: 32 })
  rs = await fetch(
    `${API2_SERVER_ROOT}/attestations?${new URLSearchParams({
      schema: `arb:${SCHEMA_TOKEN_LIST}`,
      sender: `arb:${ownerAddress}`,
      data_key: 'name',
      data_value: rawListTitle,
      limit: '50',
    })}`,
    { next: { revalidate: 300 } }
  )

  if (!rs.ok) {
    console.error(`Token fetching for list ${listMeta.title} fetch error`, rs)
    return null
  }
  const rawAtts = (await rs.json()).reverse()
  const tokens = await parseTokenListAttestations(rawAtts)

  return {
    ownerAddress,
    listMeta,
    tokens,
  }
}

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const rs = await getListDetails(params.id, decodeURIComponent(params.listid))
  if (rs == null) {
    return { title: `User List - ${params.listid}` }
  }
  return {
    title: `User List - ${rs.listMeta.title}`,
    description: rs.listMeta.description?.slice(0, 300),
    openGraph: {
      title: `User List - ${rs.listMeta.title}`,
      description: rs.listMeta.description?.slice(0, 300),
    },
  }
}

export default async function Page({ params }: Props) {
  const rs = await getListDetails(params.id, decodeURIComponent(params.listid))
  if (rs == null) notFound()

  return <ListDetails ownerAddress={rs.ownerAddress} listMeta={rs.listMeta} tokens={rs.tokens} />
}
