import AddressLookup from 'components/AddressLookup'
import RandomMoonCatRow from 'components/RandomMoonCatRow'
import { ADDRESS_DETAILS, API2_SERVER_ROOT } from 'lib/util'
import type { Metadata } from 'next'
import Link from 'next/link'
import EthereumAddress from 'components/EthereumAddress'
import Icon from 'components/Icon'
import { Address } from 'viem'

interface OwnerSummary {
  address: Address
  ownedMoonCats: number
}

async function getData(): Promise<OwnerSummary[]> {
  let rs = await fetch(`${API2_SERVER_ROOT}/owner-profile?sort=mooncats&limit=30`, { next: { revalidate: 300 } })
  if (!rs.ok) {
    throw new Error('Failed to fetch owner listing from back-end')
  }
  let owners: OwnerSummary[] = await rs.json()
  return owners
}

export const metadata: Metadata = {
  title: 'Owner Profiles',
  description: 'Gallery view of individual Ethereum address holdings',
  openGraph: {
    title: 'Owner Profiles',
    description: 'Gallery view of individual Ethereum address holdings',
  },
}

export default async function Page() {
  const moonCatOwners = await getData()
  // Show the top twenty holders. But if there's a tie at 20th place, show all the holders with that amount.
  let largeHolders = moonCatOwners.filter((o) => typeof ADDRESS_DETAILS[o.address] == 'undefined')
  const targetNumber = largeHolders[20].ownedMoonCats
  largeHolders = largeHolders.filter((o) => o.ownedMoonCats >= targetNumber)

  const pageTitle = 'Owner Profiles'

  return (
    <div id="content-container" itemScope itemType="https://schema.org/CollectionPage">
      <meta itemProp="isFamilyFriendly" content="true" />
      <div className="text-container">
        <h1 className="hero">Owner Profiles</h1>
        <RandomMoonCatRow />
        <section className="card">
          <AddressLookup style={{ marginBottom: '1em' }} linkTemplate="/owners/" />
          <p>Enter an Ethereum address (or ENS name) to look up that address&rsquo; information.</p>
        </section>
        <section className="card">
          <h2>NFT Pools</h2>
          <div style={{ display: 'flex', flexWrap: 'wrap', gap: '1em' }}>
            <ul style={{ flex: '0 1 30%', boxSizing: 'border-box', margin: 0, minWidth: '15em' }}>
              {Object.entries(ADDRESS_DETAILS)
                .filter(([k, v]) => {
                  return v.type == 'pool'
                })
                .map(([k, v]) => {
                  return (
                    <li key={k}>
                      <Link href={`/owners/${k}`}>{v.label}</Link>
                    </li>
                  )
                })}
            </ul>
            <p style={{ flex: '1 1', minWidth: '20em' }}>
              Each of these addresses are NFT pools, so all the MoonCats in them are up for adoption <em>right now</em>.
            </p>
          </div>
          <h2>Lots of Friends!</h2>
          <p>Want to see a bunch of MoonCats all in one place? Check out the number of adoptees these owners have:</p>
          <div style={{ columnWidth: '20em' }}>
            <table cellSpacing="0" cellPadding="0" className="zebra" style={{ margin: '0 auto', fontSize: '0.8rem' }}>
              <tbody>
                {largeHolders.map((o) => {
                  return (
                    <tr key={o.address}>
                      <td>
                        <EthereumAddress address={o.address} />
                      </td>
                      <td style={{ padding: '0.2rem 1rem', textAlign: 'right' }}>{o.ownedMoonCats}</td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
          <nav className="breadcrumb" style={{ textAlign: 'right', marginBottom: '1.5em' }}>
            <Link href="/owners-leaderboard">
              <>
                Browse even more <Icon name="arrow-right" style={{ verticalAlign: '-0.2em' }} />
              </>
            </Link>
          </nav>

          <p>
            Total MoonCats owned includes original and Acclimated MoonCats. Owners of MoonCats in other wrappers can{' '}
            <a href="https://mooncat.community/acclimator">visit the Acclimator</a> to get them included in these
            counts.
          </p>
        </section>
      </div>
    </div>
  )
}
