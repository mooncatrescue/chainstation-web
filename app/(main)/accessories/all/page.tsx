import type { Metadata } from 'next'
import AccessoryGrid from 'components/AccessoryGrid'

export const metadata: Metadata = {
  title: 'All Accessories',
  description: 'Browse the entire collection of stylish accessories for MoonCats',
  openGraph: {
    title: 'All Accessories',
    description: 'Browse the entire collection of stylish accessories for MoonCats',
  },
}

export default function Page() {
  return (
    <div id="content-container" itemScope itemType="https://schema.org/ImageGallery">
      <meta itemProp="isFamilyFriendly" content="true" />
      <div className="text-container">
        <h1 className="hero">Browse all Accessories</h1>
      </div>
      <AccessoryGrid />
    </div>
  )
}
