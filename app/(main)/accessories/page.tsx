import type { Metadata } from 'next'
import Link from 'next/link'
import EthAmount from 'components/EthAmount'
import AccessoryKeywordSearch from 'components/AccessoryKeywordSearch'
import RecentAccessorySales from 'components/RecentAccessorySales'
import AccessoriesStrip from 'components/AccessoriesStrip'
import RecentlyLaunchedAccessories from 'components/RecentlyLaunchedAccessories'

export const metadata: Metadata = {
  title: 'Accessories',
  description: 'Browse the entire collection of stylish accessories for MoonCats',
  openGraph: {
    title: 'Accessories',
    description: 'Browse the entire collection of stylish accessories for MoonCats',
  },
}

export default function Page() {
  return (
    <div id="content-container" itemScope itemType="https://schema.org/ImageGallery">
      <meta itemProp="isFamilyFriendly" content="true" />
      <div className="text-container">
        <h1 className="hero">Accessories</h1>
        <section className="card">
          <p>
            As MoonCats are playful, fashionable, and always exploring, they&rsquo;ve found plenty of costumes, toys,
            and other accessories in their adventures through Deep Space. This evolved to a Boutique{' '}
            <a href="https://mooncat.community/blog/accessories-launch">was established</a> on ChainStation Alpha to
            allow creators of such fine wares to market them to all the MoonCats.
          </p>
        </section>
        <section className="card-help">
          <p>
            MoonCats are able to be customized by their owner by purchasing Accessories and choosing which are active on
            their MoonCat. Once a MoonCat has purchased an Accessory, they keep the right to wear that accessory forever
            (a MoonCat&rsquo;s <em>wardrobe</em> of available accessories goes with them, even if adopted by another
            owner). But they don&rsquo;t have to wear all their accessories all the time; their current owner can set
            which are visible at any time.
          </p>
          <ul>
            <li>
              <Link href="/accessories/all">Browse all Accessories</Link>
            </li>
            <li>
              <Link href="/popular-accessories">Show popular Accessories</Link>
            </li>
          </ul>
        </section>
      </div>
      <AccessoryKeywordSearch />
      <RecentAccessorySales />
      <RecentlyLaunchedAccessories />
      <section className="card">
        <h2>Savvy Shopping</h2>
        <p>
          Accessories costing <EthAmount amount={10000000000000000n} /> or less
        </p>
        <AccessoriesStrip
          filters={{
            price_max: '10000000000000000',
            price_min: '0',
            available: 'yes',
            verified: 'yes',
            eligible_list: 'no',
            sort: 'oldest',
          }}
        />
      </section>
      <section className="card">
        <h2>Off the Market</h2>
        <p>Accessories not available to be purchased right now, but could become available again in the future</p>
        <AccessoriesStrip
          filters={{
            for_sale: 'no',
            available: 'yes',
            verified: 'yes',
          }}
        />
      </section>
      <section className="card">
        <h2>Discontinued</h2>
        <p>Accessories that are sold out. Only the MoonCats who currently own them will ever be able to own these</p>
        <AccessoriesStrip
          filters={{
            available: 'no',
            verified: 'yes',
            sort: 'oldest',
          }}
        />
      </section>
    </div>
  )
}
