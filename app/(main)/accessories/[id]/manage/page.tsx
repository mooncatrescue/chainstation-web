import ManageAccessory from 'components/ManageAccessory'
import { AccessoryTraits } from 'lib/types'
import { API2_SERVER_ROOT, API_SERVER_ROOT } from 'lib/util'
import { Metadata } from 'next'
import { notFound } from 'next/navigation'

type Props = {
  params: { id: string }
}

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  return {
    title: `Accessory #${params.id}`,
    openGraph: {
      title: `Accessory #${params.id}`,
      images: [{ url: `${API_SERVER_ROOT}/accessory-image/${params.id}` }],
    },
  }
}

async function getAccessoryTraits(id: number): Promise<AccessoryTraits | null> {
  const rs = await fetch(`${API2_SERVER_ROOT}/accessory/traits/${id}`, { next: { revalidate: 300 } })
  if (!rs.ok) {
    console.error('Failed to fetch accessory traits', rs)
    return null
  }
  return await rs.json()
}

export default async function Page({ params }: Props) {
  const accessory = parseInt(params.id)
  if (Number.isNaN(accessory) || accessory < 0) notFound()

  const traits = await getAccessoryTraits(accessory)
  if (traits == null) notFound()

  return <ManageAccessory accessory={accessory} traits={traits} />
}
