import type { Metadata } from 'next'
import Link from 'next/link'
import Icon from 'components/Icon'
import { Moment } from 'lib/types'
import { IPFS_GATEWAY, ZWS } from 'lib/util'

import momentsData from 'lib/moments_meta.json'
import { notFound } from 'next/navigation'
import MomentClaim from 'components/MomentClaim'
const moments = momentsData as Moment[]

type Props = {
  params: { id: string }
}

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const moment = getMomentMeta(parseInt(params.id))
  const imageSrc = moment?.meta.image.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/')
  const pageTitle = moment == null ? `MoonCatMoment Claim` : `MoonCatMoment - ${moment.meta.name} Claim`
  return {
    title: pageTitle,
    description: 'Claim a MoonCatMoment NFT for MoonCats who qualify.',
    openGraph: {
      ...(imageSrc && { images: [{ url: imageSrc }] }),
      title: pageTitle,
      description: 'Claim a MoonCatMoment NFT for MoonCats who qualify.',
    },
  }
}

function getMomentMeta(id: number) {
  const moment = moments.find((m) => m.momentId === id)
  return typeof moment == 'undefined' ? null : moment
}

export default async function Page({ params }: Props) {
  const moment = getMomentMeta(parseInt(params.id))
  if (moment == null) notFound()

  return (
    <div id="content-container">
      <nav className="breadcrumb">
        <Link href={'/moments/' + moment.momentId}>
          <>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Back to Moment details
          </>
        </Link>
      </nav>
      <div className="text-container">
        <h1 className="hero">{moment.meta.name} Claim</h1>
        <section className="card">
          <p>
            <strong>Claim your Moment!</strong> When a MoonCat{ZWS}Moment token is claimed, it gets given directly to
            the MoonCat who participated in that Moment. Anyone can pay the gas cost to claim a Moment for a MoonCat,
            but only the MoonCat&rsquo;s owner will be able to transfer the token out of the MoonCat&rsquo;s purrse
            after it&rsquo;s claimed.
          </p>
        </section>
      </div>
      <MomentClaim moment={moment} />
    </div>
  )
}
