import Icon from 'components/Icon'
import MoonCatGrid from 'components/MoonCatGrid'
import { Moment } from 'lib/types'
import { IPFS_GATEWAY, ZWS, formatAsDate, getCurrentEvent } from 'lib/util'
import type { Metadata } from 'next'
import Link from 'next/link'
import { notFound } from 'next/navigation'
import MomentOwners from 'components/MomentOwners'

import momentsData from 'lib/moments_meta.json'
import JsonLd from 'components/JsonLd'
const moments = momentsData as Moment[]

type Props = {
  params: { id: string }
}

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const moment = getMomentMeta(parseInt(params.id))
  if (moment == null) {
    return {
      title: 'MoonCatMoment',
      openGraph: {
        title: 'MoonCatMoment',
      },
    }
  }
  let imageSrc = moment.meta.image.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/')
  return {
    title: `MoonCatMoment - ${moment.meta.name}`,
    description: moment?.meta.description,
    icons: {
      other: {
        rel: 'alternate',
        type: 'application/json+oembed',
        url: `https://chainstation.mooncatrescue.com/api/oembed?format=json&url=${encodeURIComponent(
          `https://chainstation.mooncatrescue.com/moments/${moment.momentId}`
        )}`,
      },
    },
    openGraph: {
      title: `MoonCatMoment - ${moment.meta.name}`,
      description: moment?.meta.description,
      images: [{ url: imageSrc }],
    },
  }
}

function getMomentMeta(id: number) {
  const moment = moments.find((m) => m.momentId === id)
  return typeof moment == 'undefined' ? null : moment
}

export default async function Page({ params }: Props) {
  const moment = getMomentMeta(parseInt(params.id))
  if (moment == null) notFound()
  const currentEvent = await getCurrentEvent()

  let imageSrc = moment.meta.image.replace(/^ipfs:\/\//, IPFS_GATEWAY + '/ipfs/')
  const dateDetail = formatAsDate(moment.eventDate)

  return (
    <div id="content-container" itemScope itemType="https://schema.org/ItemPage">
      <meta itemProp="isFamilyFriendly" content="true" />
      <nav className="breadcrumb">
        <Link href="/moments">
          <>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Browse all Moments
          </>
        </Link>
      </nav>
      <JsonLd
        data={{
          '@context': 'https://schema.org',
          '@type': 'BreadcrumbList',
          'itemListElement': [
            {
              '@type': 'ListItem',
              'position': 1,
              'name': 'MoonCatMoments',
              'item': 'https://chainstation.mooncatrescue.com/moments',
            },
            {
              '@type': 'ListItem',
              'position': 2,
              'name': moment.meta.name,
            },
          ],
        }}
      />
      <h1 className="hero" itemProp="name">
        {moment.meta.name}
      </h1>
      <div>
        <picture>
          <img
            src={imageSrc}
            alt={`MoonCat${ZWS}Moment #${moment.momentId}`}
            style={{ display: 'block', margin: '0 auto', maxHeight: '70vh' }}
            itemProp="image"
          />
        </picture>
      </div>
      <div className="text-container">
        <section className="card">
          <p>
            <strong>Moment</strong> #{moment.momentId}, <strong>date</strong> {dateDetail}, <strong>issuance</strong>{' '}
            {moment.issuance}
          </p>

          <p>{moment.meta.description}</p>
          <p>
            <a href={`${moment.momentId}/claim`}>
              Claim eligible mints
              <Icon name="arrow-right" style={{ marginLeft: '0.5em', verticalAlign: '-0.2em' }} />
            </a>
          </p>
        </section>
      </div>
      <h2>Token Holders</h2>
      <MomentOwners moment={moment} />
      <h2>MoonCats Pictured</h2>
      <MoonCatGrid moonCats={moment.moonCats} isEventActive={currentEvent != null} />
    </div>
  )
}
