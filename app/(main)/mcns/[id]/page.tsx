import EthereumAddress from 'components/EthereumAddress'
import { API2_SERVER_ROOT } from 'lib/util'
import { OwnedMoonCat } from 'lib/types'
import { Address, getAddress, isAddress } from 'viem'
import { Metadata } from 'next'
import { notFound } from 'next/navigation'
import MCNSProfile from 'components/MCNSProfile'

type Props = {
  params: { id: string }
}

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const ownerAddress = isAddress(params.id) ? getAddress(params.id) : null
  const pageTitle = ownerAddress == null ? `MoonCatNameService` : `MoonCatNameService - ${ownerAddress}`
  return {
    title: pageTitle,
    openGraph: {
      title: pageTitle
    }
  }
}

async function getOwnerProfile(
  rawAddress: string
): Promise<{ ownerAddress: Address | null; moonCats: OwnedMoonCat[] }> {
  const ownerAddress = isAddress(rawAddress) ? getAddress(rawAddress) : null
  if (ownerAddress == null) {
    return { ownerAddress: null, moonCats: [] }
  }

  let rs = await fetch(`${API2_SERVER_ROOT}/owner-profile/${ownerAddress}`, { next: { revalidate: 300 } })
  if (!rs.ok) {
    console.error('Ownership info fetch error', rs)
    return { ownerAddress: null, moonCats: [] }
  }
  let owned: OwnedMoonCat[] = (await rs.json()).ownedMoonCats
  return {
    ownerAddress,
    moonCats: owned,
  }
}

export default async function Page({ params }: Props) {
  const { ownerAddress, moonCats } = await getOwnerProfile(params.id)
  if (ownerAddress == null) {
    notFound()
  }

  return (
    <div id="content-container">
      <div className="text-container">
        <h1 className="hero">MoonCatNameService</h1>
        <p style={{ textAlign: 'center', marginTop: '-1em', fontSize: '1.5rem' }} className="text-scrim">
          <EthereumAddress address={ownerAddress} mode="bar" linkProfile={false} />
        </p>
        <section className="card">
          <p>
            MoonCats form close bonds with their Etherian owners, but while both are traveling the digital and physical
            metaverse realms, it can cause some insecurity for the MoonCats to know where their owners are at. To
            rectify this issue, the MoonCats tapped into a galactic beacon network, so their owners would be only a ping
            away!
          </p>
        </section>
      </div>
      <MCNSProfile ownerAddress={ownerAddress} moonCats={moonCats} />
    </div>
  )
}
