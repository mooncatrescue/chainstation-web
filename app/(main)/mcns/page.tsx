import React from 'react'
import type { Metadata } from 'next'
import Link from 'next/link'
import AddressLookup from 'components/AddressLookup'
import RandomMoonCatRow from 'components/RandomMoonCatRow'
import { ZWS } from 'lib/util'

export const metadata: Metadata = {
  title: 'MoonCatNameService',
  description: 'Ethereum Name Service (ENS) domains for each MoonCat',
  openGraph: {
    title: 'MoonCatNameService',
    description: 'Ethereum Name Service (ENS) domains for each MoonCat',
  },
}

export default function Page() {
  return (
    <div id="content-container">
      <div className="text-container">
        <h1 className="hero">MoonCat{ZWS}NameService</h1>
        <section className="card">
          <p>
            MoonCats form close bonds with their Etherian owners, but while both are traveling the digital and physical
            metaverse realms, it can cause some insecurity for the MoonCats to know where their owners are at. To
            rectify this issue, the MoonCats tapped into a galactic beacon network, so their owners would be only a ping
            away!
          </p>
          <p>
            <Link href="/mcns/faq">More details...</Link>
          </p>
        </section>
        <RandomMoonCatRow />
        <section className="card">
          <AddressLookup style={{ marginBottom: '1em' }} linkTemplate="/mcns/" />
          <p>Enter an Ethereum address (or ENS name) to look up that address&rsquo; information.</p>
        </section>
      </div>
    </div>
  )
}
