import EthereumAddress from 'components/EthereumAddress'
import { ADDRESS_DETAILS, API2_SERVER_ROOT } from 'lib/util'
import { Metadata } from 'next'
import Link from 'next/link'
import { Address } from 'viem'

interface OwnerSummary {
  address: Address
  ownedMoonCats: number
}

/**
 * Get current leaderboard of large MoonCat collections
 *
 * Server-side function that NextJS will automatically turn into a server-side render.
 */
async function getData(): Promise<OwnerSummary[]> {
  const rs = await fetch(`${API2_SERVER_ROOT}/owner-profile?sort=mooncats&limit=130`, {
    next: { tags: ['mooncat-owners'], revalidate: 3600 },
  })
  if (!rs.ok) {
    throw new Error('Failed to fetch popular MoonCat data from back-end')
  }

  return await rs.json()
}

export const metadata: Metadata = {
  title: 'MoonCat Owners Leaderboard',
  description: 'Leaderboard of Etherians who have collected the most MoonCats in the universe',
  openGraph: {
    title: 'MoonCat Owners Leaderboard',
    description: 'Leaderboard of Etherians who have collected the most MoonCats in the universe',  
  }
}
export default async function Page() {
  const owners = await getData()

  // Show the top 100 holders. But if there's a tie at 100th place, show all the holders with that amount.
  let largeHolders = owners.filter((o) => typeof ADDRESS_DETAILS[o.address] == 'undefined')
  const targetNumber =
    largeHolders.length > 100 ? largeHolders[100].ownedMoonCats : largeHolders[largeHolders.length - 1].ownedMoonCats
  largeHolders = largeHolders.filter((o) => o.ownedMoonCats >= targetNumber)

  // Calculate the ranked place value for each holding address
  let places = [1]
  let curPlace = 1
  for (let i = 1; i < largeHolders.length; i++) {
    if (largeHolders[i].ownedMoonCats != largeHolders[i - 1].ownedMoonCats) curPlace = i + 1
    places[i] = curPlace
  }

  return (
    <div id="content-container" className="leaderboard">
      <div className="text-container leaderboard-wrap">
        <h1 className="hero">Cuddle Party!</h1>
        <section className="card" style={{ marginTop: 140 }}>
          <p>
            Look at which Etherians have collected the most <Link href="/mooncats">MoonCats</Link>! Total MoonCats owned
            includes original and Acclimated MoonCats. Owners of MoonCats in other wrappers can{' '}
            <a href="https://mooncat.community/acclimator">visit the Acclimator</a> to get them included in these
            counts.
          </p>
        </section>
        <section className="card">
          <ul className="zebra">
            {largeHolders.map((d, index) => (
              <li key={d.address}>
                <Link href={`/owners/${d.address}`}>
                  <div>
                    <strong>{places[index]}.</strong>
                  </div>
                  <div style={{ flex: '1 1 auto' }}>
                    <EthereumAddress address={d.address} mode="text" />
                  </div>
                  <div>
                    <em>{d.ownedMoonCats} MoonCats</em>
                  </div>
                </Link>
              </li>
            ))}
          </ul>
        </section>
      </div>
    </div>
  )
}
