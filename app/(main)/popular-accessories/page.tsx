import AccessoryLeaderboard from 'components/AccessoryLeaderboard'
import { AccessorySummary } from 'lib/types'
import { API2_SERVER_ROOT } from 'lib/util'
import { Metadata } from 'next'

/**
 * Get current leaderboard of popular Accessories
 *
 * Server-side function that NextJS will automatically turn into a server-side render.
 */
async function getData(): Promise<AccessorySummary[]> {
  const rs = await fetch(`${API2_SERVER_ROOT}/accessories?sort=liked&limit=50`, {
    next: { tags: ['stars', 'accessory-stars'], revalidate: 3600 },
  })
  if (!rs.ok) {
    throw new Error('Failed to fetch popular Accessory data from back-end')
  }

  return await rs.json()
}

export const metadata: Metadata = {
  title: 'Popular Accessories',
  description: 'Leaderboard of which Accessories have earned the most "stars" from their fellow Etherians',
  openGraph: {
    title: 'Popular Accessories',
    description: 'Leaderboard of which Accessories have earned the most "stars" from their fellow Etherians',
  },
}
export default async function Page() {
  const accessories = await getData()
  return <AccessoryLeaderboard accessories={accessories} />
}
