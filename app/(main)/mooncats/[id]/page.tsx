import type { Metadata } from 'next'
import { isValidViewPreference, MoonCatData, MoonCatDetails, MoonCatViewPreference } from 'lib/types'
import { API_SERVER_ROOT } from 'lib/util'
import { notFound } from 'next/navigation'
import MoonCatDetail from 'components/MoonCatDetail'
import { getAddress } from 'viem'
import { getReservoirAsks } from 'lib/reservoirData'

import _rawTraits from 'lib/mooncat_traits.json'
const moonCatTraits = _rawTraits as MoonCatData[]

type Props = {
  params: { id: string }
  searchParams: { [key: string]: string | string[] | undefined }
}

function ucFirst(str: string) {
  return str.charAt(0).toUpperCase() + str.slice(1)
}

export async function generateMetadata({ params, searchParams }: Props): Promise<Metadata> {
  const moonCat = getMoonCatData(params.id)
  if (moonCat == null) return { title: 'MoonCat' }

  const details = await getMoonCatDetails(moonCat.rescueOrder)

  let displayName = ` (${moonCat.catId})`
  if (details !== null && details.isNamed != 'No' && details.name != null) {
    // If the API server metadata has a name for this MoonCat, use that
    displayName = `: ${details.name}`
  } else if (moonCat.name === true) {
    // If the MoonCat's name is not a valid string, use unknown character
    displayName = ': \ufffd'
  } else if (moonCat.name) {
    // MoonCat is named and the name is a UTF8 string
    displayName = `: ${moonCat.name}`
  }

  let description
  let type = moonCat.genesis ? 'Genesis MoonCat' : 'MoonCat'
  if (details == null) {
    // Loading details failed; give summary data
    description = `An adorable ${ucFirst(moonCat.pattern)} ${type}, rescued in ${moonCat.rescueYear}`
  } else {
    let coatDetail = `${ucFirst(details.hue)} ${ucFirst(moonCat.pattern)}`
    if (!details.genesis && details.isPale) coatDetail = 'Pale ' + coatDetail
    description = `An adorable ${coatDetail} ${type}, rescued in ${moonCat.rescueYear}`
  }

  let metaImage: MoonCatViewPreference = 'accessorized'
  const queryPreference = searchParams.view
  if (queryPreference && !Array.isArray(queryPreference) && isValidViewPreference(queryPreference)) {
    // Query parameter takes priority
    metaImage = queryPreference.toLowerCase() as MoonCatViewPreference
  }
  let metaImageSrc = ''
  switch (metaImage) {
    case 'mooncat':
      metaImageSrc = `${API_SERVER_ROOT}/cat-image/${moonCat.catId}`
      break
    case 'face':
      metaImageSrc = `${API_SERVER_ROOT}/face-image/${moonCat.catId}`
      break
    case 'event':
      metaImageSrc = `${API_SERVER_ROOT}/event-image/${moonCat.catId}`
      break
    default:
      metaImageSrc = `${API_SERVER_ROOT}/image/${moonCat.catId}?costumes=true`
      break
  }

  return {
    title: `MoonCat #${moonCat.rescueOrder}${displayName}`,
    description: description,
    openGraph: {
      title: `MoonCat #${moonCat.rescueOrder}${displayName}`,
      description: description,
      images: [{ url: metaImageSrc }],
    },
    icons: {
      other: [
        {
          rel: 'canonical',
          url: `https://chainstation.mooncatrescue.com/mooncats/${moonCat.rescueOrder}`,
        },
        {
          rel: 'alternate',
          type: 'application/json+oembed',
          url: `https://chainstation.mooncatrescue.com/api/oembed?format=json&url=${encodeURIComponent(
            `https://chainstation.mooncatrescue.com/mooncats/${moonCat.rescueOrder}`
          )}`,
        },
      ],
    },
  }
}

function getMoonCatData(id: string): MoonCatData | null {
  if (id.substring(0, 2) == '0x') {
    // ID is a hex ID
    let targetId = id.toLowerCase()
    for (let i = 0; i < moonCatTraits.length; i++) {
      if (moonCatTraits[i].catId.toLowerCase() == targetId) return moonCatTraits[i]
    }
    return null
  } else if (!Number.isNaN(Number(id))) {
    // ID is the rescue order
    return moonCatTraits[Number(id)]
  }
  return null
}

/**
 * Fetch details about a MoonCat from the API server
 */
async function getMoonCatDetails(rescueOrder: number): Promise<MoonCatDetails | null> {
  const rs = await fetch(`${API_SERVER_ROOT}/traits/${rescueOrder}`, { next: { revalidate: 3600 } })
  if (!rs.ok) {
    console.error('API call returned', rs.status)
    return null
  }

  const jsonData = await rs.json()
  if (typeof jsonData == 'undefined') {
    console.error('Failed to parse JSON body')
    return null
  }

  const details = jsonData.details as MoonCatDetails
  if (details.owner) {
    // Old-wrapped MoonCats are not enumerated
    details.owner = getAddress(details.owner) // Ensure address is in checksummed format
  }
  if (details.rescuedBy) {
    details.rescuedBy = getAddress(details.rescuedBy) // Ensure address is in checksummed format
  }
  return details
}

export default async function Page({ params }: Props) {
  const moonCat = getMoonCatData(params.id)
  if (moonCat == null) notFound()

  const details = await getMoonCatDetails(moonCat.rescueOrder)

  const reservoirData = await getReservoirAsks()
  const listings = reservoirData.orders.filter((l) => l.moonCat == moonCat?.rescueOrder)

  return <MoonCatDetail moonCat={moonCat} details={details} listings={listings} />
}
