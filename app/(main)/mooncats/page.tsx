import type { Metadata } from 'next'
import Link from 'next/link'
import { ZWS } from 'lib/util'
import MoonCatLookup from 'components/MoonCatLookup'
import MoonCatsHome from 'components/MoonCatsHome'

export const dynamic = 'force-dynamic'
export const metadata: Metadata = {
  title: 'MoonCats',
  description: 'Browse the entire collection of colorful astral felines',
  openGraph: {
    title: 'MoonCats',
    description: 'Browse the entire collection of colorful astral felines',
  },
}

export default async function Page() {
  return (
    <div id="content-container" itemScope itemType="https://schema.org/ImageGallery">
      <meta itemProp="isFamilyFriendly" content="true" />
      <div className="text-container">
        <h1 className="hero">MoonCats</h1>
        <section className="card">
          <p>
            Discovered living on the moon back in 2017, they were <strong>rescued</strong> by many adventurous
            Etherians, and brought onto the blockchain. They now coexist happily with their human Etherian friends,
            serving as <em>companions</em> and <em>guides</em> into the uncharted <strong>Deep Space</strong>.
          </p>
        </section>
        <section className="card-help">
          <p>
            MoonCats are the primary NFT collection of the MoonCat{ZWS}Rescue project. The{' '}
            <a href="https://etherscan.io/address/0x60cd862c9c687a9de49aecdc3a99b74a4fc54ab6">original contract</a> was
            deployed in 2017, before &quot;NFT&quot; was an established term. They were <em>rescued</em>{' '}
            (&quot;minted&quot;) and brought to <em>ChainStation Alpha</em> (the Ethereum mainnet blockchain), and are
            the key characters guiding their human owners through <em>Deep Space</em> (the forward-looking possibilities
            of blockchain/web3 technologies).
          </p>
          <section
            style={{
              display: 'flex',
              flexWrap: 'wrap',
              justifyContent: 'space-evenly',
              gap: '2rem',
            }}
          >
            <ul style={{ flex: '1 1 40%' }}>
              <li>
                <Link href="/named-mooncats">Browse named MoonCats</Link>
              </li>
              <li>
                <Link href="/popular-mooncats">Show popular MoonCats</Link>
              </li>
            </ul>
            <MoonCatLookup style={{ flex: '1 1 40%' }} />
          </section>
        </section>
      </div>
      <MoonCatsHome />
    </div>
  )
}
