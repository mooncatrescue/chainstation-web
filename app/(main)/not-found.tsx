import RandomMoonCatRow from "components/RandomMoonCatRow";

export default function NotFound() {
  return (
    <div id="content-container">
      <div className="text-container">
        <h1 className="hero">-- 404 -- </h1>
        <RandomMoonCatRow />
        <section className="card">
          <p>
            <strong>Lost in Deep Space!</strong> Navigation computer is showing you&rsquo;re now in the vacinity of the{' '}
            <em>Foroh Fore</em> nebula. You seem to have taken a wrong turn somewhere...
          </p>
        </section>
      </div>
    </div>
  )
}