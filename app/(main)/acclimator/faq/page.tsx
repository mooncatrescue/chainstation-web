import type { Metadata } from 'next'
import Link from 'next/link'
import Icon from 'components/Icon'
import { ZWS } from 'lib/util'
import JsonLd from 'components/JsonLd'
import EthereumAddress from 'components/EthereumAddress'

const pageTitle = 'MoonCatAcclimator - FAQ'
export const metadata: Metadata = {
  title: pageTitle,
  description: 'Learn about wrapping your MoonCats with the official MoonCatAcclimator contract',
  openGraph: {
    title: pageTitle,
    description: 'Learn about wrapping your MoonCats with the official MoonCatAcclimator contract',
  },
}

export default function Page() {
  return (
    <div id="content-container">
      <nav className="breadcrumb">
        <Link href="/acclimator">
          <>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Acclimator Home
          </>
        </Link>
      </nav>
      <JsonLd
        data={{
          '@context': 'https://schema.org',
          '@type': 'FAQPage',
          'isFamilyFriendly': true,
          'name': pageTitle,
          'breadcrumb': {
            '@type': 'BreadcrumbList',
            'itemListElement': [
              {
                '@type': 'ListItem',
                'position': 1,
                'name': 'Acclimator',
                'item': 'https://chainstation.mooncatrescue.com/acclimator',
              },
              {
                '@type': 'ListItem',
                'position': 2,
                'name': pageTitle,
              },
            ],
          },
        }}
      />
      <div className="text-container">
        <h1 className="hero">MoonCat{ZWS}Acclimator FAQ</h1>
        <section className="card-help">
          <h2>Highlights</h2>
          <ul>
            <li>
              The <strong>Acclimator</strong> smart contract is a &ldquo;wrapper&rdquo;-style contract that allows
              MoonCats to interact with and own other NFTs
            </li>
            <li>
              It adheres to the <a href="https://erc721.org/">ERC721 standard</a> (so many wallets will automatically
              show which MoonCats you own directly in their interfaces), and keeps intact each MoonCat&rsquo;s full{' '}
              <strong>trait-metadata</strong> (rescue order, name, color, fur pattern, etc).
            </li>
            <li>
              Acclimated MoonCats are <strong>fully supported</strong> by the ecosystem, including{' '}
              <Link href="/accessories">accessories</Link> and future projects
            </li>
            <li>
              Other wrappers do not have these benefits. They are not officially supported and don&rsquo;t support the
              maintaining team nor the wider MoonCat{ZWS}Rescue ecosystem.
            </li>
          </ul>

          <h2>MoonCat Acclimation Basics</h2>

          <h3>Why should I wrap my MoonCats?</h3>
          <ol>
            <li>
              <strong>So that they can interact with the world!</strong> The official wrapping contract at{' '}
              <EthereumAddress address="0xc3f733ca98E0daD0386979Eb96fb1722A1A05E69" /> allows your MoonCats to interact
              with wallets and marketplaces that use the ERC-721 Non-Fungible Token Standard. ERC-721 is the standard
              that many marketplaces (e.g. OpenSea) use for NFTs. MoonCat{ZWS}Rescue was created before the original
              draft this standard was even submitted(!), and that&rsquo;s why the MoonCats need to be brought up to
              date.
            </li>
            <li>
              <strong>So that they can own things!</strong> The wrapping contract also implements a new, proposed
              standard: the ERC-998 Composable Non-Fungible Token Standard. ERC-998 is a standard that allows ERC-721
              tokens to own other tokens directly (rather than just being owned by the same address), and therefore
              allow transferring ownership of whole collections of ERC-721s together take just a single transaction.
            </li>
            <li>
              <strong>So you can support continued MoonCat development!</strong> If you use the official wrapping
              contract&rsquo;s collection on NFT marketplaces and pool platforms, some of them give the option to send a
              fraction of each sale to the MoonCat{ZWS}Rescue development team. So, if you appreciate what work that
              team has done, and wish to support further development of the MoonCat ecosystem of applications, doing it
              via the official collection helps contribute to that cause.
            </li>
          </ol>

          <h3>Is wrapping permanent?</h3>
          <p>
            Nope! It&rsquo;s not a permanent process; if you wish, you can &ldquo;unwrap&rdquo; your MoonCat and return
            it to its original state at any time.
          </p>

          <h3>But wait, isn&rsquo;t there already a wrapper?</h3>
          <p>
            Yes, an unofficial ERC-721 wrapper (located at{' '}
            <EthereumAddress address="0x7c40c393dc0f283f318791d746d894ddd3693572" />) was created in early 2021, shortly
            after the MoonCat rediscovery. This wrapper was expeditiously put together so that the new community was
            able to use platforms like OpenSea and NFT20 as soon as possible. Unfortunately, that wrapper doesn&rsquo;t
            contain <code>tokenURI</code> metadata and doesn&rsquo;t preserve the &ldquo;rescue order&rdquo; (mint
            order) of MoonCats, which has caused some confusion about what different IDs mean. So, while the old wrapper
            does provide some functionality, it has several missing features that make it not recommended.
          </p>

          <h3>Do I HAVE to wrap or re-wrap my MoonCats?</h3>
          <p>
            No, your MoonCats from the original MoonCat{ZWS}Rescue contract and those wrapped with the{' '}
            <code>0x7c40c3...</code> do not have any known security flaws, so there is no rush to Acclimate them. The{' '}
            <code>0x7c40c3...</code> wrapping contract has an <code>unwrap</code> function that is not time-dependent,
            so you can do that at any time in the future. Future tooling and applications created by the MoonCat{ZWS}
            Rescue team will focus on supporting MoonCats wrapped with the new wrapper, so if you choose to not re-wrap
            your MoonCats, they may not be able to play fully with their other brethren.
          </p>

          <hr />

          <h2>Deeper Down the MoonCat Crater</h2>
          <p>
            <em>
              (Everything else you ever wanted to know about MoonCat Acclimation, and for those who wish to engage the
              contract directly)
            </em>
          </p>

          <h3>Why is Metamask setting the gas limits so high?</h3>
          <p>
            When using the Acclimator web UI, your browser popup for Metamask will auto-compute how much the gas limit
            is needing to be. But if you compare that &ldquo;gas limit&rdquo; value to completed transactions on the
            blockchain from who have acclimated before you, you may notice that the actual gas charged for those
            transactions is a lot less. And so the overall final price paid for the transaction&rsquo;s gas looks like
            it will be much higher, but ends up being lower. This is especially evident when doing batched actions. Why
            is that?
          </p>

          <p>
            It has to do with how the Ethereum EVM refunds gas when state is freed/deleted from the blockchain. When
            doing a &ldquo;re-wrap&rdquo; action on a formerly-wrapped MoonCat, the old token&rsquo;s state is deleted
            in the process, and that earns a refund, but that refund only gets applied at the very end of the
            transaction&rsquo;s execution. So what you see in completed transactions is really the net gas cost (the
            gross total gas needed to run all the smart contract actions, minus the gas refunded by state-freeing
            actions). If you set the &ldquo;gas limit&rdquo; of your transaction to be just at that &ldquo;net gas
            cost&rdquo;, the transaction will fail, because it needs to first do the actions up to the &ldquo;gross gas
            cost&rdquo; first, before getting the refund.
          </p>

          <p>Final gas units that get used for the various actions are:</p>
          <ul>
            <li>Preparing a never-been-wrapped-before MoonCat: about 58,000 gas per MoonCat</li>
            <li>
              Batch Wrapping never-been-wrapped-before MoonCats: about 170,000 gas per MoonCat for small batches (down
              to about 152,000 per MoonCat for a batch of 8)
            </li>
            <li>Preparing all previously-wrapped MoonCats: about 46,000 gas</li>
            <li>
              Batch Re-wrapping previously-wrapped MoonCats: about 240,000 gas per MoonCat for small batches (down to
              about 202,000 per MoonCat for a batch of 8)
            </li>
          </ul>

          <p>
            Using those figures you can estimate what the final costs will be, even though the transaction must be
            submitted with higher gas limits to be successful.
          </p>

          <h3>How do I wrap my MoonCats with the new wrapping contract myself?</h3>
          <p>
            The MoonCat Acclimator web application structures the interaction transactions for you, but if you want to
            craft the transactions yourself (or just want to peek under the hood and see what it&rsquo;s doing), the
            next few questions detail which functions on the smart contracts are available for different situations.
          </p>

          <h3>How do I wrap my MoonCats who have never been wrapped before?</h3>
          <p>
            You must first make an adoption offer to the wrapping contract to adopt your MoonCat for zero ETH (
            <code>makeAdoptionOfferToAddress</code> function on the MoonCat{ZWS}Rescue contract, targeting address{' '}
            <EthereumAddress address="0xc3f733ca98E0daD0386979Eb96fb1722A1A05E69" />, which is the official wrapping
            contract. You&rsquo;ll need the MoonCat&rsquo;s <strong>five-byte hexadecimal ID</strong> for this call).
            Once that has succeeded, call the <code>wrap</code> function on the official wrapping contract, passing in
            the MoonCat&rsquo;s <strong>rescue order</strong> as the function&rsquo;s parameter. If you have many
            MoonCats you&rsquo;d like to wrap this way, you&rsquo;ll still need to call the{' '}
            <code>makeAdoptionOfferToAddress</code> function once for each MoonCat, but then you can use the{' '}
            <code>batchWrap</code> function to submit multiple rescue order numbers at once, and wrap them all, using
            just one transaction. (<code>m + 1</code> transactions needed, for <code>m</code> MoonCats)
          </p>

          <h3>
            How do I re-wrap my MoonCats who have been wrapped with the <code>0x7c40c3...</code> wrapping contract?
          </h3>
          <p>
            To transition these MoonCats, they will need to be unwrapped from the old contract and re-wrapped into the
            new one. To use these methods, you&rsquo;ll need to know the <strong>token ID</strong> of the wrapped
            MoonCat in the <code>0x7c40c3...</code> wrapping contract, as well as the <strong>rescue order</strong> for
            each of your MoonCats. There&rsquo;s a few options built into the official wrapping contract to support
            different needs for different users:
          </p>

          <ul>
            <li>
              <em>Re-wrap several MoonCats now, and maintain current owner as owner of them all:</em> On the{' '}
              <code>0x7c40c3...</code> wrapping contract, call <code>setApprovalForAll</code>, targeting contract{' '}
              <EthereumAddress address="0xc3f733ca98E0daD0386979Eb96fb1722A1A05E69" /> (<strong>the Acclimator</strong>{' '}
              official wrapping contract). Then you can call <code>batchReWrap</code> on the official wrapping contract,
              passing in a list of <strong>rescue orders</strong> and other-wrapping-contract <strong>token IDs</strong>
              . (2 transactions needed, for any number of MoonCats)
            </li>
            <li>
              <em>Re-wrap one MoonCat, maintaining its current owner:</em> On the <code>0x7c40c3...</code> wrapping
              contract, call the
              <code>safeTransferFrom</code> function (using the variant of that function that has an additional{' '}
              <code>data</code> parameter). The <code>data</code> parameter needs to be the{' '}
              <strong>rescue order</strong> of the MoonCat being transferred. Convert that value to hexadecimal and then
              add zeroes on the left end of it to pad it out to 32 bytes long. Then use the{' '}
              <code>safeTransferFrom</code> function to send the token to the official wrapper (the four parameters are{' '}
              <code>_from</code> (your address; the owner of the MoonCat currently), <code>_to</code> (
              <code>0xc3f733ca98E0daD0386979Eb96fb1722A1A05E69</code>, the official wrapping contract),
              <code>_tokenId</code> (the <strong>token ID</strong> of the MoonCat in the other wrapping contract), and{' '}
              <code>data</code> (<strong>rescue order</strong> of that MoonCat, as a hex value, padded out to 32 bytes
              long)). (one transaction per MoonCat)
            </li>
            <li>
              <em>Re-wrap on first transfer:</em> There is a separate &ldquo;Lookup&rdquo; contract at
              <EthereumAddress address="0x2736AEf2efc7D144EdfC4F8dB97c625983e38911" />, which can be used to record the
              mappings between <strong>token IDs</strong>
              for the <code>0x7c40c3...</code> wrapping contract and <strong>rescue orders</strong>. Use the{' '}
              <code>submitRescueOrder</code> function on the Lookup contract to save the link between existing{' '}
              <strong>token IDs</strong> and <strong>rescue order</strong> (it&rsquo;s a batch function so you can
              submit all your MoonCats at once). Then, on the <code>0x7c40c3...</code> wrapping contract, call{' '}
              <code>setApprovalForAll</code>, targeting contract <code>0xc3f733ca98E0daD0386979Eb96fb1722A1A05E69</code>{' '}
              (the official wrapping contract). At this point, the official wrapping contract will be able to enumerate
              all your MoonCats on the <code>0x7c40c3...</code>
              wrapping contract as well as your MoonCats in the official wrapper (now calling <code>balanceOf</code> on
              the official Wrapping contract will return a total sum of your <code>0xc3f733...</code> MoonCats and{' '}
              <code>0x7c40c3...</code> MoonCats). The first time they&rsquo;re transferred (using the{' '}
              <code>safeTransferFrom</code> function on the official wrapping contract, not on the{' '}
              <code>0x7c40c3...</code> wrapping contract), they will automatically be re-wrapped. This would allow you
              to do things like list them for sale on an ERC721 marketplace (as an officially-wrapped MoonCat), and the
              first buyer will pay the transaction fee for wrapping as they buy/transfer the MoonCat. (one setup
              transaction for any number of MoonCats, and then one transaction per MoonCat to wrap and transfer)
            </li>
          </ul>
        </section>
      </div>
    </div>
  )
}
