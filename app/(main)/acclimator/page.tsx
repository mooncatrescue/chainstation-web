import type { Metadata } from 'next'
import Link from 'next/link'
import Acclimator from 'components/Acclimator'
import AcclimationScene from 'components/AcclimationScene'

export const metadata: Metadata = {
  title: 'Acclimator',
  description: 'Adapting MoonCats to be able to interact with the latest and greatest of Etherian technologies',
  openGraph: {
    title: 'Acclimator',
    description: 'Adapting MoonCats to be able to interact with the latest and greatest of Etherian technologies',
  },
}
export default function Page() {
  return (
    <div id="content-container">
      <h1 className="hero">The Acclimator</h1>
      <div className="text-container">
        <section className="card">
          <p>
            Employing the latest in experimental nanopurr technology, this advanced apparatus will painlessly prepare
            your MoonCats to <strong>explore their new blockchain world</strong>. As you know, they aren&rsquo;t big
            fans of change, and there&rsquo;s a lot that&rsquo;s different on this side of the rocket: the atmosphere,
            the gravity, and the sense of everlasting optimism.
          </p>
          <p>
            There&rsquo;s just two easy steps and your MoonCats will be frolicking care-free through the Ether. First
            each cat must be <strong>prepared</strong> for the transition, a gentle coaxing into their own personal
            acclimation pod should do fine. Once they are settled in, simply <strong>activate</strong> the device and
            watch your little buddies <em>glow</em>.
          </p>
          <p>
            After your MoonCats have been safely acclimated, visit them in{' '}
            <Link href="/acclimator/lounge/">the lounge</Link> as they recouperate in their pods. Those craving more
            technical details may slake their thirst for knowledge in <Link href="/acclimator/faq">the FAQ</Link>
          </p>
        </section>
      </div>
      <AcclimationScene />
      <div className="text-container">
        <Acclimator />
      </div>
    </div>
  )
}
