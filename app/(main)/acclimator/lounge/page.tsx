import React from 'react'
import type { Metadata } from 'next'
import AddressLookup from 'components/AddressLookup'
import RandomMoonCatRow from 'components/RandomMoonCatRow'
import { ZWS } from 'lib/util'
import RandomMoonCat from 'components/RandomMoonCat'
import AcclimatedCatTree from 'components/AcclimatedCatTree'

export const metadata: Metadata = {
  title: 'MoonCatAcclimator Lounge',
  description: 'Ethereum Name Service (ENS) domains for each MoonCat',
  openGraph: {
    title: 'MoonCatAcclimator Lounge',
    description: 'Ethereum Name Service (ENS) domains for each MoonCat',
  },
}

export default function Page() {
  return (
    <div id="content-container">
      <div className="text-container">
        <h1 className="hero">MoonCat{ZWS}Acclimator Lounge</h1>
        <div style={{ display: 'flex', justifyContent: 'space-evenly', alignItems: 'flex-end', gap: '1em' }}>
          <div className="only-wide">
            <RandomMoonCat />
          </div>
          <div>
            <picture>
              {/* Can you figure out why these three are the figureheads of the MoonCat Acclimator Lounge? */}
              <img
                style={{ maxWidth: '80vw' }}
                src={`/api/img/mooncat-tree?mooncats=1326,5739,21247`}
                alt="MoonCats #1326, #5739, and #21247, lounging on a cat tree"
              />
            </picture>
          </div>
          <div className="only-wide">
            <RandomMoonCat />
          </div>
        </div>
        <section className="card">
          <AddressLookup style={{ marginBottom: '1em' }} linkTemplate="/owners/{addr}/acclimator-lounge" />
          <p>
            Enter an Ethereum address (or ENS name) to take a peek at that owner&rsquo;s Acclimated MoonCats relaxing in
            the lounge.
          </p>
        </section>
      </div>
    </div>
  )
}
