import GMLeaderboard from 'components/GMLeaderboard'
import { AttestationDoc, SCHEMAS } from 'lib/eas'
import { API2_SERVER_ROOT } from 'lib/util'
import { Metadata } from 'next'

/**
 * Get current leaderboard of recent GMs
 *
 * Server-side function that NextJS will automatically turn into a server-side render.
 */
async function getData(): Promise<AttestationDoc[]> {
  const rs = await fetch(`${API2_SERVER_ROOT}/attestations?limit=200&schema=arb:${SCHEMAS.GM}`, {
    next: { tags: ['gm-attestations'], revalidate: 3600 },
  })
  if (!rs.ok) {
    throw new Error('Failed to fetch GM Attestation data from back-end')
  }

  const rawAtts: AttestationDoc[] = (await rs.json()).map((att: any) => {
    // JSON format does not have BigInts, so those values are stored as strings. Convert them back to BigInts to adhere to EAS-SDK type
    att.sig.message.time = BigInt(att.sig.message.time)
    att.sig.message.expirationTime = BigInt(att.sig.message.expirationTime)
    return att
  })
  return rawAtts
}

export const metadata: Metadata = {
  title: 'GM Leaderboard',
  description: 'Leaderboard of which Etherians have sent the most GMs to the world',
  openGraph: {
    title: 'GM Leaderboard',
    description: 'Leaderboard of which Etherians have sent the most GMs to the world',
  },
}
export default async function Page() {
  const atts = await getData()

  return <GMLeaderboard atts={atts} />
}
