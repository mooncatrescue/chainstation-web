import Icon from 'components/Icon'
import ShoppingCart from 'components/ShoppingCart'
import SignInFirst from 'components/SignInFirst'
import { Metadata } from 'next'
import Link from 'next/link'

export const metadata: Metadata = {
  title: 'Shopping Cart',
  description: 'Your cosmic cart of artifacts from across Deep Space, from awaiting adoption',
  openGraph: {
    title: 'Shopping Cart',
    description: 'Your cosmic cart of artifacts from across Deep Space, from awaiting adoption',
  },
}

export default function Page() {
  return (
    <div id="content-container">
      <nav className="breadcrumb">
        <Link href="/profile">
          <>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Browse all profile actions
          </>
        </Link>
      </nav>
      <div className="text-container">
        <h1 className="hero">Shopping Cart</h1>
        <SignInFirst className="card-help">
          <ShoppingCart />
        </SignInFirst>
      </div>
    </div>
  )
}
