import Icon from 'components/Icon'
import MomentsToClaim from 'components/MomentsToClaim'
import SignInFirst from 'components/SignInFirst'
import { ZWS } from 'lib/util'
import { Metadata } from 'next'
import Link from 'next/link'

export const metadata: Metadata = {
  title: 'MoonCatMoments',
  description: 'Check all your owned MoonCats for special moments they have participated in during their lunar adventures',
  openGraph: {
    title: 'MoonCatMoments',
    description: 'Check all your owned MoonCats for special moments they have participated in during their lunar adventures',
  },
}

export default function Page() {
  return (
    <div id="content-container">
      <nav className="breadcrumb">
        <Link href="/profile">
          <>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Browse all profile actions
          </>
        </Link>
      </nav>
      <div className="text-container">
        <h1 className="hero">MoonCat{ZWS}Moments</h1>
        <SignInFirst className="card-help">
          <MomentsToClaim />
        </SignInFirst>
      </div>
    </div>
  )
}
