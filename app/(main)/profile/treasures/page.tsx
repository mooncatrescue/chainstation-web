import Icon from 'components/Icon'
import SignInFirst from 'components/SignInFirst'
import TreasuresList from 'components/TreasuresList'
import { Metadata } from 'next'
import Link from 'next/link'

export const metadata: Metadata = {
  title: 'My Treasures',
  description: 'Claim unlockable bonus content that holding different assets grants you',
  openGraph: {
    title: 'My Treasures',
    description: 'Claim unlockable bonus content that holding different assets grants you',
  },
}

export default function Page() {
  const preamble = (
    <p>
      There are many projects and products that find a home in ChainStation Alpha. And it being an open and
      collaborative space, many merchants, traders, and other creators like giving gifts to other denizens of the space.
      To see the treasures you can claim, you need to verify you are the owner of this wallet address first.
    </p>
  )
  return (
    <div id="content-container">
      <nav className="breadcrumb">
        <Link href="/profile">
          <>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Browse all profile actions
          </>
        </Link>
      </nav>
      <div className="text-container">
        <h1 className="hero">Treasures</h1>
        <SignInFirst className="card-help" preamble={preamble}>
          <section className="card-help">
            <p>
              There are many projects and products that find a home in ChainStation Alpha. And it being an open and
              collaborative space, many merchants, traders, and other creators like giving gifts to other denizens of
              the space. Here&rsquo;s the treasures you as an Etherean have access to:
            </p>
          </section>
          <TreasuresList />
        </SignInFirst>
      </div>
    </div>
  )
}
