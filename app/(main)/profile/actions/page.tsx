import ActionList from 'components/ActionList'
import Icon from 'components/Icon'
import SignInFirst from 'components/SignInFirst'
import { Metadata } from 'next'
import Link from 'next/link'

export const metadata: Metadata = {
  title: 'Actions Queue',
  description: 'Mission control for your pending on-chain operations',
  openGraph: {
    title: 'Actions Queue',
    description: 'Mission control for your pending on-chain operations',
  },
}

export default function Page() {
  return (
    <div id="content-container">
      <nav className="breadcrumb">
        <Link href="/profile">
          <>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Browse all profile actions
          </>
        </Link>
      </nav>
      <div className="text-container">
        <h1 className="hero">Pending Actions</h1>
        <SignInFirst className="card-help">
          <ActionList />
        </SignInFirst>
      </div>
    </div>
  )
}
