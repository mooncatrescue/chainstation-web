import Icon from 'components/Icon'
import MyStars from 'components/MyStars'
import SignInFirst from 'components/SignInFirst'
import { Metadata } from 'next'
import Link from 'next/link'

export const metadata: Metadata = {
  title: 'My Stars',
  description: 'Explore your constellation of starred items in the MoonCatRescue ecosystem',
  openGraph: {
    title: 'My Stars',
    description: 'Explore your constellation of starred items in the MoonCatRescue ecosystem',
  },
}

export default function Page() {
  const preamble = (
    <p>
      As you journey around ChainStation Alpha, there&rsquo;s several opportunities for you to signal your appreciation of
      items by <strong>starring</strong> them. Forgotten all the things you&rsquo;ve given a star to? Log in and
      we&rsquo;ll list them all out:
    </p>
  )
  return (
    <div id="content-container">
      <nav className="breadcrumb">
        <Link href="/profile">
          <>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Browse all profile actions
          </>
        </Link>
      </nav>
      <div className="text-container">
        <h1 className="hero">My Stars</h1>
        <SignInFirst className="card-help" preamble={preamble}>
          <MyStars />
        </SignInFirst>
      </div>
    </div>
  )
}
