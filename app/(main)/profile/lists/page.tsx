import Icon from 'components/Icon'
import MyLists from 'components/MyLists'
import SignInFirst from 'components/SignInFirst'
import { Metadata } from 'next'
import Link from 'next/link'

export const metadata: Metadata = {
  title: 'User Lists',
  description: 'Manage lists of blockchain tokens you own',
  openGraph: {
    title: 'User Lists',
    description: 'Manage lists of blockchain tokens you own',
  },
}

export default function Page() {
  return (
    <div id="content-container">
      <nav className="breadcrumb">
        <Link href="/profile">
          <>
            <Icon name="arrow-left" style={{ marginRight: '0.5em', verticalAlign: '-0.2em' }} />
            Browse all profile actions
          </>
        </Link>
      </nav>
      <div className="text-container">
        <h1 className="hero">User Lists</h1>
        <SignInFirst className="card-help">
          <MyLists />
        </SignInFirst>
      </div>
    </div>
  )
}
