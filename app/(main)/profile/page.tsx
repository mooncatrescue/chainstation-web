import ProfilePage from 'components/ProfilePage'
import { Metadata } from 'next'

export const dynamic = 'force-dynamic'
export const metadata: Metadata = {
  title: 'Profile',
  description: 'Your own user profile as a user and owner of MoonCatRescue ecosystem assets',
  openGraph: {
    title: 'Profile',
    description: 'Your own user profile as a user and owner of MoonCatRescue ecosystem assets',
  },
}

export default function Page() {
  return (
    <div id="content-container">
      <ProfilePage />
    </div>
  )
}
