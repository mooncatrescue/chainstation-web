import type { Metadata } from 'next'
import RandomMoonCatRow from 'components/RandomMoonCatRow'
import GMPage from 'components/GMPage'

export const metadata: Metadata = {
  title: 'GM!',
  description: 'Send a gm to the rest of the community',
  openGraph: {
    title: 'GM!',
    description: 'Send a gm to the rest of the community',
  },
}

export default function Page() {
  return (
    <div id="content-container">
      <div className="text-container">
        <h1 className="hero">Send a GM</h1>
        <RandomMoonCatRow />
        <GMPage />
      </div>
    </div>
  )
}
