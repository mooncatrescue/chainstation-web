import type { Metadata } from 'next'
import { ZWS } from 'lib/util'
import ContributePage from 'components/ContributePage'

export const dynamic = 'force-dynamic'
export const metadata: Metadata = {
  title: 'Contribute',
  description: 'Details on various community initiatives and ways to support the project as it grows',
  openGraph: {
    title: 'Contribute',
    description: 'Details on various community initiatives and ways to support the project as it grows',
  },
}
export default function Page() {
  return (
    <div id="content-container">
      <div className="text-container">
        <h1 className="hero">Contribute</h1>
        <section className="card-help">
          <p>
            The MoonCat{ZWS}Rescue project has grown a bunch since it launched in 2017 (more information about the
            project overall <a href="https://mooncat.community/about">over here</a>), and the current team of
            maintainers continues to raise awareness about the project, educate newcomers about NFTs in general, and
            coordinate added utility to the MoonCat ecosystem projects.
          </p>

          <p>
            If you&rsquo;d like to see this project continue to grow and evolve, there&rsquo;s many ways you can chip in
            funds, skills, and/or social awareness that would be greatly appreciated!
          </p>
        </section>
        <ContributePage />
      </div>
    </div>
  )
}
