import type { Metadata } from 'next'
import RandomMoonCatRow from 'components/RandomMoonCatRow'

const pageTitle = 'Thank You!'
export const metadata: Metadata = {
  title: pageTitle,
  openGraph: {
    title: pageTitle,
  },
}

export default function Page() {
  return (
    <div id="content-container">
      <div className="text-container">
        <h1 className="hero">Thank You!</h1>
        <RandomMoonCatRow />
        <section className="card">
          <p>Thanks for believing in us!</p>
          <p>
            As a two-person team, we appreciate all the passion and support - financial and otherwise - our community
            has shown over the years. Big or small, this matters in ensuring MoonCats last through thick and thin.
          </p>
        </section>
      </div>
    </div>
  )
}
