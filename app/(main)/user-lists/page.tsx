import EthereumAddress from 'components/EthereumAddress'
import Icon from 'components/Icon'
import { API2_SERVER_ROOT, nameToUriSlug } from 'lib/util'
import { Metadata } from 'next'
import Link from 'next/link'
import { Address } from 'viem'

interface ListSummary {
  likesCount: number
  tokenCount: number
  modified: number
  title: string
  id: `0x${string}`
  owner: Address
}

/**
 * Get current leaderboard of popular User-created Lists
 *
 * Server-side function that NextJS will automatically turn into a server-side render.
 */
async function getData(): Promise<ListSummary[]> {
  const rs = await fetch(`${API2_SERVER_ROOT}/user-lists?sort=likes&limit=50`, {
    next: { tags: ['stars', 'user-lists'], revalidate: 3600 },
  })
  if (!rs.ok) {
    throw new Error('Failed to fetch User-created lists data from back-end')
  }

  return await rs.json()
}

export const metadata: Metadata = {
  title: 'User-created Lists',
  description: 'Collections of tokens, curated by MoonCat owners for different projects',
  openGraph: {
    title: 'User-created Lists',
    description: 'Collections of tokens, curated by MoonCat owners for different projects',
  },
}

export default async function Page() {
  const lists = await getData()
  return (
    <div id="content-container" className="leaderboard">
      <div className="text-container leaderboard-wrap">
        <h1 className="hero">User-created Lists</h1>
        <section className="card" style={{ marginTop: 140 }}>
          <p>
            As Etherians find their way across the vast cosmos, they have a high liklihood of finding strange and
            interesting things along the way. The ChainStation archives provide a means for any Etherian to keep track
            of discoveries for later, adding items to different lists. Here&rsquo;s some of the most notable lists
            created by MoonCat owners:
          </p>
        </section>
        <section className="card">
          <ul className="user-list-list">
            {lists.map((l) => (
              <li key={l.id}>
                <Link href={`/owners/${l.owner}/lists/${nameToUriSlug(l.title)}`}>
                  <div className="name">{l.title}</div>
                  <div className="details">
                    by <EthereumAddress address={l.owner} mode="text" />, with {l.tokenCount}{' '}
                    {l.tokenCount > 1 ? 'MoonCats' : 'MoonCat'}
                  </div>
                  {l.likesCount > 0 && (
                    <div className="icon-pill" aria-label={`${l.likesCount} likes`}>
                      {l.likesCount}
                      <Icon style={{ marginLeft: '0.25rem', verticalAlign: '-1px' }} name="star-empty" />
                    </div>
                  )}
                </Link>
              </li>
            ))}
          </ul>
        </section>
      </div>
    </div>
  )
}
