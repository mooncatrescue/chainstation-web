import React from 'react'
import type { Metadata } from 'next'
import { ZWS } from 'lib/util'
import MoonCatsNamedHome from 'components/MoonCatsNamedHome'

export const metadata: Metadata = {
  title: 'Named MoonCats',
  description: 'Browse the collection of colorful astral felines who have been given a permanent name',
  openGraph: {
    title: 'Named MoonCats',
    description: 'Browse the collection of colorful astral felines who have been given a permanent name',
  },
}

export default async function Page() {
  return (
    <div id="content-container" itemScope itemType="https://schema.org/ImageGallery">
      <meta itemProp="isFamilyFriendly" content="true" />
      <div className="text-container">
        <h1 className="hero">Named MoonCats</h1>
        <section className="card">
          <p>
            Discovered living on the moon back in 2017, they were <strong>rescued</strong> by many adventurous
            Etherians, and brought onto the blockchain. Their human Etherian friends watch over them, and can grant each
            of them a true <strong>name</strong>.
          </p>
        </section>
        <section className="card-help">
          <p>
            MoonCats are the primary NFT collection of the MoonCat{ZWS}Rescue project. The{' '}
            <a href="https://etherscan.io/address/0x60cd862c9c687a9de49aecdc3a99b74a4fc54ab6">original contract</a> was
            deployed in 2017, before &quot;NFT&quot; was an established term. They were <em>rescued</em>{' '}
            (&quot;minted&quot;) and brought to <em>ChainStation Alpha</em> (the Ethereum mainnet blockchain), and are
            the key characters guiding their human owners through <em>Deep Space</em> (the forward-looking possibilities
            of blockchain/web3 technologies).
          </p>
          <p>
            This sub-listing shows MoonCats who have been named on the blockchain (each MoonCat can be named by its
            current owner, but once named cannot ever be renamed), and are shown in the order they were named in.
          </p>
        </section>
      </div>
      <MoonCatsNamedHome />
    </div>
  )
}
